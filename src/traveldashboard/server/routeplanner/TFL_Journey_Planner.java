package traveldashboard.server.routeplanner;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import traveldashboard.data.TubeStation;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.MongoDbManager;


public class TFL_Journey_Planner {
	
	private static final String API_URL = "http://journeyplanner.tfl.gov.uk/user/";
	private static final String FIRST_STOP_QUERY_URL = "XML_TRIP_REQUEST2?language=en&sessionID=0&place_origin=London&type_origin=stop&name_origin=";
	private static final String SECOND_STOP_QUERY_URL = "20Road&place_destination=London&type_destination=stop&name_destination=";
	private static final String DATE_QUERY_URL = "&itdDate="; //Format must be 20110801
	private static final String TIME_QUERY_URL = "&itdTime="; //Format must be 0800
	
	private static final HashMap<Integer, MotType> MOT_TYPES;
	
	static {
		MOT_TYPES = new HashMap<Integer, MotType>();
		MOT_TYPES.put(0, MotType.TRAIN);
		MOT_TYPES.put(1, MotType.COMMUTER_RAILWAY);
		MOT_TYPES.put(2, MotType.UNDERGROUND);
		MOT_TYPES.put(3, MotType.CITY_RAIL);
		MOT_TYPES.put(4, MotType.TRAM);
		MOT_TYPES.put(5, MotType.CITY_BUS);
		MOT_TYPES.put(6, MotType.REGIONAL_BUS);
		MOT_TYPES.put(7, MotType.COACH);
		MOT_TYPES.put(8, MotType.CABLE_CAR);
		MOT_TYPES.put(9, MotType.BOAT);
		MOT_TYPES.put(10, MotType.TRANSIT);
		MOT_TYPES.put(11, MotType.OTHER);		
	}
	

	public static void test() {
//		String depStopName = "Convent Garden";
			String depStopName = "Convent Garden Underground Station";
			String arrStopName = "Wembley Park Underground Station";
//			String depStopName = "Acton Town Underground Station";
//			String arrStopName = "Wembley Park Underground Station";
			depStopName = "Alperton Underground Station";// Got bus
			arrStopName = "Wembley Park Underground Station";
//			depStopName = "Ealing Broadway Underground Station"; //Got walking
//			arrStopName = "Wembley Park Underground Station";
//			depStopName = "Ealing Common Underground Station"; 
//			arrStopName = "Wembley Park Underground Station";
//			depStopName = "Greenford Underground Station"; 
//			arrStopName = "Wembley Park Underground Station";
//			depStopName = "Greenford Underground Station"; 
//			arrStopName = "Wembley Park Underground Station";
//			depStopName = "Hanger Lane Underground Station";
//			arrStopName = "Wembley Park Underground Station";
//			depStopName = "North Ealing Underground Station";
//			arrStopName = "Wembley Park Underground Station";
			String date = "20131121";
			String time = "0800";
			//int[] motTypeIncluded = {MotType.CITY_BUS.getId()};
			int[] motTypeIncluded = {};
			
			getItdItinerary(depStopName, arrStopName, date, time, motTypeIncluded);
    }
	
	private static String generateQueryUrl(
			String firstStopName,
			String secondStopName,
			String date,
			String time,
			int[] motTypeIncluded
			) {
		
		String encodedFirstStopName = firstStopName;
		
		try {
			encodedFirstStopName = URLEncoder.encode(firstStopName, "UTF-8");
		} catch (Exception exception) {exception.printStackTrace();}
		

		String encodedSecondStopName = secondStopName;
		
		try {
			encodedSecondStopName = URLEncoder.encode(secondStopName, "UTF-8");
		} catch (Exception exception) {exception.printStackTrace();}
		
		String newURL = API_URL 
				+ FIRST_STOP_QUERY_URL + encodedFirstStopName
				+ SECOND_STOP_QUERY_URL + encodedSecondStopName 
				+ DATE_QUERY_URL + date
				+ TIME_QUERY_URL + time;
		
		//Append inclueded mot type
		if (motTypeIncluded.length > 0) {
			newURL = newURL + "&includedMeans=1";
		}
		
		for (int j = 0; j < motTypeIncluded.length; j++) {
			newURL = newURL + "&inclMOT_" + String.valueOf(motTypeIncluded[j]) + "=-";
		}
		
		if (DataConstants.DEBUG) System.out.println("new url = " + newURL);
		
		return newURL;
	}
	
	public static ItdItinerary getItdItinerary(String depStopName, String arrStopName,
			String depDate, String time, int[] motTypeIncluded) {

		ItdItinerary itdItinerary = null;
		
		try {

			HttpClient httpClient = new DefaultHttpClient();
			
			String newUrl = generateQueryUrl(depStopName, arrStopName, depDate, time, motTypeIncluded);
			
			HttpGet httpGet = new HttpGet(newUrl);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);
			
			/*
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("journey_planner1_XML_TRIP_REQUEST2.xml")));
			String line;
			StringBuilder sb = new StringBuilder();
	
			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
	
			br.close();
			
			String json = sb.toString();
			*/
			//if (DataConstants.DEBUG) System.out.println("Xml string = " + json);
			
			
		    // Convert XML to JSON Object
	        JSONObject xmlJSONObj = XML.toJSONObject(json);
	        
	        //if (DataConstants.DEBUG) System.out.println(xmlJSONObj.toString(4));
	        
	        JSONObject itdRequestObj = xmlJSONObj.getJSONObject("itdRequest");
	        
	        //if (DataConstants.DEBUG) System.out.println(itdRequestObj.toString(4));
	        
	        JSONObject itdTripRequestObj = itdRequestObj.getJSONObject("itdTripRequest");
	        
	        //if (DataConstants.DEBUG) System.out.println(itdTripRequestObj.toString(4));
	        
	        JSONObject itdItineraryObj = itdTripRequestObj.getJSONObject("itdItinerary");
	        
	        //if (DataConstants.DEBUG) System.out.println(itdItineraryObj.toString(4));
	        
	        JSONObject itdRouteListObj = itdItineraryObj.optJSONObject("itdRouteList");
	        
	        //Error getting route list
	        if (itdRouteListObj == null) return itdItinerary;
	        
	        //if (DataConstants.DEBUG) System.out.println(itdRouteListObj.toString(4));
	        
	        JSONArray itdRouteArray = itdRouteListObj.getJSONArray("itdRoute");
	        
	        //if (DataConstants.DEBUG) System.out.println(itdRouteArray.toString(4));
	        
	        if (DataConstants.DEBUG) System.out.println("Route size = " + itdRouteArray.length());
	        
	        List<ItdRoute> itdRouteList = new ArrayList<ItdRoute>();
	        
	        for (int z = 0; z < itdRouteArray.length(); z++) {
		        	ItdRoute itdRoute = null;
		        	
		        	int routeTripIndex;
		        	int vehicleTime;
		        	String publicDuration;
		        	
		            JSONObject itdRoute1Obj = itdRouteArray.getJSONObject(z);
			        
		            routeTripIndex = itdRoute1Obj.getInt("routeTripIndex");
			        vehicleTime = itdRoute1Obj.getInt("vehicleTime");
			        publicDuration = itdRoute1Obj.getString("publicDuration");
			        
			        if (DataConstants.DEBUG) System.out.println("routeTripIndex = " + routeTripIndex + ", vehicleTime = " + vehicleTime + ", publicDuration = " + publicDuration);
			        
			        JSONObject itdPartialRouteListObj = itdRoute1Obj.getJSONObject("itdPartialRouteList");
			        
			        //if (DataConstants.DEBUG) System.out.println(itdPartialRouteListObj.toString(4));
			        
			        JSONArray itdPartialRouteArray = itdPartialRouteListObj.optJSONArray("itdPartialRoute");
			        
			        List<ItdPartialRoute> itdPartialRouteList = new ArrayList<ItdPartialRoute>();
			        
			        if (itdPartialRouteArray != null) {
				        //if (DataConstants.DEBUG) System.out.println(itdPartialRouteArray.toString(4));
				        
				        //if (DataConstants.DEBUG) System.out.println("Partial route size = " + itdPartialRouteArray.length());
				        
				        //Create itdPartialRouteList

				        
				        for (int i = 0; i < itdPartialRouteArray.length(); i++) {
				        	
				        	ItdPartialRoute itdPartialRoute;
				        	
				        	JSONObject itdPartialRouteJsonObj = itdPartialRouteArray.getJSONObject(i);
				        	
				        	//if (DataConstants.DEBUG) System.out.println(itdPartialRouteJsonObj.toString(4));
				        	
				        	int timeMinute = itdPartialRouteJsonObj.getInt("timeMinute");
				        	
				        	JSONArray itdPointJsonArray = itdPartialRouteJsonObj.getJSONArray("itdPoint");
				        	
				        	//Depart point
				        	JSONObject depItdPointJsonObj  = itdPointJsonArray.getJSONObject(0);

				        	String depStationName = depItdPointJsonObj.getString("name");
				        	String depPlatformName = depItdPointJsonObj.getString("platformName");
				        	
				        	JSONObject depItdDateTimeJsonObj = depItdPointJsonObj.getJSONObject("itdDateTime");
				        	JSONObject depItdDate = depItdDateTimeJsonObj.getJSONObject("itdDate");
				        	JSONObject depItdTime = depItdDateTimeJsonObj.getJSONObject("itdTime");
				        	int depDay = depItdDate.getInt("day");
				        	int depMonth = depItdDate.getInt("month");
				        	int depWeekday = depItdDate.getInt("weekday");
				        	int depYear = depItdDate.getInt("year");
				        	int depHour = depItdTime.getInt("hour");
				        	int depMinute = depItdTime.getInt("minute");
				        	ItdDateTime depItdDateTime = new ItdDateTime(depYear, depMonth, depDay, depWeekday, depHour, depMinute);
				        	
				        	//Create departure point
				        	//FIXME: Add crowd and noise
				        	String depStationNameOfDb = depStationName.replace(" Underground Station", "");
				        	TubeStation depStation = MongoDbManager.getLondonMetroStationByStopName(depStationNameOfDb);
				        	
				        	int depCrowdLevel = 0;
				        	int depNoiseLevel = 0;
				        	
				        	if (depStation != null) {
				        		String depHourString = String.valueOf(depHour);
				        		if (depHour < 10) depHourString = "0" + depHourString;
				        		
				        		String depMinuteString = String.valueOf(depMinute);
				        		if (depMinute < 10) depMinuteString = "0" + depMinuteString;
				        		
				        		String depQueryTime = depHourString + depMinuteString;
				        		
				        		depCrowdLevel = MongoDbManager.getMetroClassifiedCrowdByQueryTime(DataConstants.LONDON, depStation.getId(), depStation.getTubes()[0].getId(), depQueryTime).getCrowd().getCrowdLevel().getLevel();
				        		depNoiseLevel = MongoDbManager.getMetroClassifiedNoiseByQueryTime(DataConstants.LONDON, depStation.getId(), depStation.getTubes()[0].getId(), depQueryTime).getNoiseLevel();
				        	}
				        	
				        	ItdPoint depPoint = new ItdPoint(depStationName, depPlatformName, ItdPointUsage.DEPARTUTE, depItdDateTime, depCrowdLevel, depNoiseLevel);

				        	//Arrival point
				        	JSONObject arrItdPointJsonObj  = itdPointJsonArray.getJSONObject(1);

				        	String arrStationName = arrItdPointJsonObj.getString("name");
				        	String arrPlatformName = arrItdPointJsonObj.getString("platformName");
				        	
				        	JSONObject arrItdDateTimeJsonObj = arrItdPointJsonObj.getJSONObject("itdDateTime");
				        	JSONObject arrItdDate = arrItdDateTimeJsonObj.getJSONObject("itdDate");
				        	JSONObject arrItdTime = arrItdDateTimeJsonObj.getJSONObject("itdTime");
				        	int arrDay = arrItdDate.getInt("day");
				        	int arrMonth = arrItdDate.getInt("month");
				        	int arrWeekday = arrItdDate.getInt("weekday");
				        	int arrYear = arrItdDate.getInt("year");
				        	int arrHour = arrItdTime.getInt("hour");
				        	int arrMinute = arrItdTime.getInt("minute");
				        	ItdDateTime arrItdDateTime = new ItdDateTime(arrYear, arrMonth, arrDay, arrWeekday, arrHour, arrMinute);            	

				        	//Create arrival point
				        	//FIXME: Add crowd and noise level
				        	String arrStationNameOfDb = arrStationName.replace(" Underground Station", "");
				        	TubeStation arrStation = MongoDbManager.getLondonMetroStationByStopName(arrStationNameOfDb);
				        	
				        	int arrCrowdLevel = 0;
				        	int arrNoiseLevel = 0;
				        	
				        	if (arrStation != null) {
				        		String arrHourString = String.valueOf(arrHour);
				        		if (arrHour < 10) arrHourString = "0" + arrHourString;
				        		
				        		String arrMinuteString = String.valueOf(arrMinute);
				        		if (arrMinute < 10) arrMinuteString = "0" + arrMinuteString;
				        		
				        		String arrQueryTime = arrHourString + arrMinuteString;
				        		
				        		arrCrowdLevel = MongoDbManager.getMetroClassifiedCrowdByQueryTime(DataConstants.LONDON, arrStation.getId(), arrStation.getTubes()[0].getId(), arrQueryTime).getCrowd().getCrowdLevel().getLevel();
				        		
				        		arrNoiseLevel = MongoDbManager.getMetroClassifiedNoiseByQueryTime(DataConstants.LONDON, arrStation.getId(), arrStation.getTubes()[0].getId(), arrQueryTime).getNoiseLevel();
				        	}
				        	
				        	
				        	ItdPoint arrPoint = new ItdPoint(arrStationName, arrPlatformName, ItdPointUsage.ARRIVAL, arrItdDateTime, arrCrowdLevel, arrNoiseLevel);
				        	
				        	//Get means of transport list
				        	List<ItdMeansOfTransport> itdMeansOfTransportList = new ArrayList<ItdMeansOfTransport>();
				        		        	
				        	JSONObject itdFrequencyInfoJsonObj = itdPartialRouteJsonObj.optJSONObject("itdFrequencyInfo");
				        	
				        	if (itdFrequencyInfoJsonObj != null) {
					        	JSONObject itdMeansOfTransportListJsonObj = itdFrequencyInfoJsonObj.getJSONObject("itdMeansOfTransportList");
					        	
					        	JSONArray itdMeansOfTransportJsonArray = itdMeansOfTransportListJsonObj.optJSONArray("itdMeansOfTransport");
					        	
					        	if (itdMeansOfTransportJsonArray != null) {
					        		
						        	for (int g = 0; g < itdMeansOfTransportJsonArray.length(); g++) {
						        		JSONObject itdMeansOfTransportJsonObj = itdMeansOfTransportJsonArray.getJSONObject(g);
						        		
						        		String lineName = itdMeansOfTransportJsonObj.getString("shortname");
						        		String destination = itdMeansOfTransportJsonObj.getString("destination");
						        		int motType = itdMeansOfTransportJsonObj.getInt("motType");
						  
						        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport(lineName, destination, MOT_TYPES.get(motType));
						        		itdMeansOfTransportList.add(itdMeansOfTransport);
						        	}
						        	
					        	} else {
					        		JSONObject itdMeansOfTransportJsonObj = itdMeansOfTransportListJsonObj.getJSONObject("itdMeansOfTransport");
					        		String lineName = itdMeansOfTransportJsonObj.getString("shortname");
					        		String destination = itdMeansOfTransportJsonObj.getString("destination");
					        		int motType = itdMeansOfTransportJsonObj.getInt("motType");
					  
					        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport(lineName, destination, MOT_TYPES.get(motType));
					        		itdMeansOfTransportList.add(itdMeansOfTransport);
					        		
					        	} //End itdMeansOfTransportJsonArray != null
				        	} else {
				        		
				        		JSONObject itdMeansOfTransportJsonObj = itdPartialRouteJsonObj.getJSONObject("itdMeansOfTransport");
				        		
				        		//if (DataConstants.DEBUG) System.out.println(itdMeansOfTransportJsonObj.toString(4));
				        		
				        		//Train
				        		JSONObject itdOperatorJsonObj = itdMeansOfTransportJsonObj.optJSONObject("itdOperator");
				        		
				        		if (itdOperatorJsonObj != null) {
					        		String lineName = itdOperatorJsonObj.getString("name");
					        		String destination = itdMeansOfTransportJsonObj.getString("destination");
					        		int motType = itdMeansOfTransportJsonObj.getInt("motType");
					        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport(lineName, destination, MOT_TYPES.get(motType));
					        		itdMeansOfTransportList.add(itdMeansOfTransport);
				        		} else {
				        			//Walking
					        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport("Walking", "", MOT_TYPES.get(11));
					        		itdMeansOfTransportList.add(itdMeansOfTransport);				        			
				        		}

				        	} //End itdFrequencyInfoJsonObj != null
				        	
	

				        	//Create itdPartialRoute
				        	itdPartialRoute = new ItdPartialRoute(timeMinute, depPoint, arrPoint, itdMeansOfTransportList);
				        	
				        	//Add itdPartialRoute in itdPartiaRouteList
				        	itdPartialRouteList.add(itdPartialRoute);
				        	
				        }
			        } else {
			        	ItdPartialRoute itdPartialRoute;
			        	
			        	JSONObject itdPartialRouteJsonObj = itdPartialRouteListObj.getJSONObject("itdPartialRoute");
			        	
			        	int timeMinute = itdPartialRouteJsonObj.getInt("timeMinute");
			        	
			        	JSONArray itdPointJsonArray = itdPartialRouteJsonObj.getJSONArray("itdPoint");
			        	
			        	//Depart point
			        	JSONObject depItdPointJsonObj  = itdPointJsonArray.getJSONObject(0);

			        	String depStationName = depItdPointJsonObj.getString("name");
			        	String depPlatformName = depItdPointJsonObj.getString("platformName");
			        	JSONObject depItdDateTimeJsonObj = depItdPointJsonObj.getJSONObject("itdDateTime");
			        	JSONObject depItdDate = depItdDateTimeJsonObj.getJSONObject("itdDate");
			        	JSONObject depItdTime = depItdDateTimeJsonObj.getJSONObject("itdTime");
			        	int depDay = depItdDate.getInt("day");
			        	int depMonth = depItdDate.getInt("month");
			        	int depWeekday = depItdDate.getInt("weekday");
			        	int depYear = depItdDate.getInt("year");
			        	int depHour = depItdTime.getInt("hour");
			        	int depMinute = depItdTime.getInt("minute");
			        	ItdDateTime depItdDateTime = new ItdDateTime(depYear, depMonth, depDay, depWeekday, depHour, depMinute);
			        	
			        	//Create departure point
			        	//FIXME: Add crowd and noise level
			        	String depStationNameOfDb = depStationName.replace(" Underground Station", "");
			        	TubeStation depStation = MongoDbManager.getLondonMetroStationByStopName(depStationNameOfDb);
			        	
			        	int depCrowdLevel = 0;
			        	int depNoiseLevel = 0;
			        	
			        	if (depStation != null) {
			        		String depHourString = String.valueOf(depHour);
			        		if (depHour < 10) depHourString = "0" + depHourString;
			        		
			        		String depMinuteString = String.valueOf(depMinute);
			        		if (depMinute < 10) depMinuteString = "0" + depMinuteString;
			        		
			        		String depQueryTime = depHourString + depMinuteString;
			        		
			        		depCrowdLevel = MongoDbManager.getMetroClassifiedCrowdByQueryTime(DataConstants.LONDON, depStation.getId(), depStation.getTubes()[0].getId(), depQueryTime).getCrowd().getCrowdLevel().getLevel();
			        		
			        		depNoiseLevel = MongoDbManager.getMetroClassifiedNoiseByQueryTime(DataConstants.LONDON, depStation.getId(), depStation.getTubes()[0].getId(), depQueryTime).getNoiseLevel();
			        	}
			        	
			        	ItdPoint depPoint = new ItdPoint(depStationName, depPlatformName, ItdPointUsage.DEPARTUTE, depItdDateTime, depCrowdLevel, depNoiseLevel);

			        	//Arrival point
			        	JSONObject arrItdPointJsonObj  = itdPointJsonArray.getJSONObject(1);

			        	String arrStationName = arrItdPointJsonObj.getString("name");
			        	String arrPlatformName = arrItdPointJsonObj.getString("platformName");
			        	
			        	JSONObject arrItdDateTimeJsonObj = arrItdPointJsonObj.getJSONObject("itdDateTime");
			        	JSONObject arrItdDate = arrItdDateTimeJsonObj.getJSONObject("itdDate");
			        	JSONObject arrItdTime = arrItdDateTimeJsonObj.getJSONObject("itdTime");
			        	int arrDay = arrItdDate.getInt("day");
			        	int arrMonth = arrItdDate.getInt("month");
			        	int arrWeekday = arrItdDate.getInt("weekday");
			        	int arrYear = arrItdDate.getInt("year");
			        	int arrHour = arrItdTime.getInt("hour");
			        	int arrMinute = arrItdTime.getInt("minute");
			        	ItdDateTime arrItdDateTime = new ItdDateTime(arrYear, arrMonth, arrDay, arrWeekday, arrHour, arrMinute);            	

			        	//Create arrival point
			        	//FIXME: Add crowd and noise level
			        	String arrStationNameOfDb = arrStationName.replace(" Underground Station", "");
			        	TubeStation arrStation = MongoDbManager.getLondonMetroStationByStopName(arrStationNameOfDb);
			        	
			        	int arrCrowdLevel = 0;
			        	int arrNoiseLevel = 0;
			        	
			        	if (arrStation != null) {
			        		String arrHourString = String.valueOf(arrHour);
			        		if (arrHour < 10) arrHourString = "0" + arrHourString;
			        		
			        		String arrMinuteString = String.valueOf(arrMinute);
			        		if (arrMinute < 10) arrMinuteString = "0" + arrMinuteString;
			        		
			        		String arrQueryTime = arrHourString + arrMinuteString;
			        		
			        		arrCrowdLevel = MongoDbManager.getMetroClassifiedCrowdByQueryTime(DataConstants.LONDON, arrStation.getId(), arrStation.getTubes()[0].getId(), arrQueryTime).getCrowd().getCrowdLevel().getLevel();
			        		
			        		arrNoiseLevel = MongoDbManager.getMetroClassifiedNoiseByQueryTime(DataConstants.LONDON, arrStation.getId(), arrStation.getTubes()[0].getId(), arrQueryTime).getNoiseLevel();
			        	}
			        	    	
			        	ItdPoint arrPoint = new ItdPoint(arrStationName, arrPlatformName, ItdPointUsage.ARRIVAL, arrItdDateTime, arrCrowdLevel, arrNoiseLevel);

			        	//Get means of transport list
			        	List<ItdMeansOfTransport> itdMeansOfTransportList = new ArrayList<ItdMeansOfTransport>();
			        		        	
			        	JSONObject itdFrequencyInfoJsonObj = itdPartialRouteJsonObj.optJSONObject("itdFrequencyInfo");
			        	
			        	if (itdFrequencyInfoJsonObj != null) {
				        	JSONObject itdMeansOfTransportListJsonObj = itdFrequencyInfoJsonObj.getJSONObject("itdMeansOfTransportList");
				        	
				        	JSONArray itdMeansOfTransportJsonArray = itdMeansOfTransportListJsonObj.optJSONArray("itdMeansOfTransport");
				        	
				        	if (itdMeansOfTransportJsonArray != null) {
				        		
					        	for (int g = 0; g < itdMeansOfTransportJsonArray.length(); g++) {
					        		JSONObject itdMeansOfTransportJsonObj = itdMeansOfTransportJsonArray.getJSONObject(g);
					        		
					        		String lineName = itdMeansOfTransportJsonObj.getString("shortname");
					        		String destination = itdMeansOfTransportJsonObj.getString("destination");
					        		int motType = itdMeansOfTransportJsonObj.getInt("motType");
					  
					        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport(lineName, destination, MOT_TYPES.get(motType));
					        		itdMeansOfTransportList.add(itdMeansOfTransport);
					        	}
					        	
				        	} else {
				        		JSONObject itdMeansOfTransportJsonObj = itdMeansOfTransportListJsonObj.getJSONObject("itdMeansOfTransport");
				        		String lineName = itdMeansOfTransportJsonObj.getString("shortname");
				        		String destination = itdMeansOfTransportJsonObj.getString("destination");
				        		int motType = itdMeansOfTransportJsonObj.getInt("motType");
				  
				        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport(lineName, destination, MOT_TYPES.get(motType));
				        		itdMeansOfTransportList.add(itdMeansOfTransport);
				        		
				        	} //End itdMeansOfTransportJsonArray != null
			        	} else {
			        		
			        		JSONObject itdMeansOfTransportJsonObj = itdPartialRouteJsonObj.getJSONObject("itdMeansOfTransport");
			        		
			        		//if (DataConstants.DEBUG) System.out.println(itdMeansOfTransportJsonObj.toString(4));
			        		
			        		//Train
			        		JSONObject itdOperatorJsonObj = itdMeansOfTransportJsonObj.optJSONObject("itdOperator");
			        		
			        		if (itdOperatorJsonObj != null) {
				        		String lineName = itdOperatorJsonObj.getString("name");
				        		String destination = itdMeansOfTransportJsonObj.getString("destination");
				        		int motType = itdMeansOfTransportJsonObj.getInt("motType");
				        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport(lineName, destination, MOT_TYPES.get(motType));
				        		itdMeansOfTransportList.add(itdMeansOfTransport);
			        		} else {
			        			//Walking
				        		ItdMeansOfTransport itdMeansOfTransport = new ItdMeansOfTransport("Walking", "", MOT_TYPES.get(11));
				        		itdMeansOfTransportList.add(itdMeansOfTransport);				        			
			        		}

			        	} //End itdFrequencyInfoJsonObj != null

			        	//Create itdPartialRoute
			        	itdPartialRoute = new ItdPartialRoute(timeMinute, depPoint, arrPoint, itdMeansOfTransportList);
			        	
			        	//Add itdPartialRoute in itdPartiaRouteList
			        	itdPartialRouteList.add(itdPartialRoute);
			        	
			        } //End if itdPartialRouteArray != null


			        //Create itdRoute
			        itdRoute =  new ItdRoute(routeTripIndex, vehicleTime, publicDuration, itdPartialRouteList);
			        //if (DataConstants.DEBUG) System.out.println(itdRoute.toString());
			        
			        itdRouteList.add(itdRoute);
			        
			        //End of z iteration
		        }
		        
		        itdItinerary = new ItdItinerary(itdRouteList);
		        //if (DataConstants.DEBUG) System.out.println(itdItinerary.toString());
	        
		} catch(Exception e) {
			if (DataConstants.DEBUG) e.printStackTrace();
		}
		
		return itdItinerary;
	}

}
