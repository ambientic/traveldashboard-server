package traveldashboard.server.init;

import java.io.File;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.ibicoop.init.IbicoopInit;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;

import traveldashboard.gtfs.process.GtfsProcessor;
import traveldashboard.gtfs.process.GtfsProcessor.GtfsParseCallback;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.MongoDbManager;
import traveldashboard.server.data.TDMessageGeneratorFromDatabase;
import traveldashboard.server.data.TdServerLogger;
import traveldashboard.server.data.TestDb;
import traveldashboard.server.data.common.PullHandler;
import traveldashboard.server.data.common.PullHandlersApplication;
import traveldashboard.server.data.common.PushHandlersApplication;
import traveldashboard.server.handler.AllStopsHandler;
import traveldashboard.server.handler.BikeHandler;
import traveldashboard.server.handler.BikeUpdateHandler;
import traveldashboard.server.handler.BusCountdownHandler;
import traveldashboard.server.handler.BusStopHandler;
import traveldashboard.server.handler.ClassifiedNoiseHandler;
import traveldashboard.server.handler.CommentEvent;
import traveldashboard.server.handler.CrowdEvent;
import traveldashboard.server.handler.FacebookHandler;
import traveldashboard.server.handler.IncentiveHandler;
import traveldashboard.server.handler.NoiseEvent;
import traveldashboard.server.handler.DataEventListener;
import traveldashboard.server.handler.NoiseHandler;
import traveldashboard.server.handler.RatingEvent;
import traveldashboard.server.handler.RoutePlannerHandler;
import traveldashboard.server.handler.TubeRatingHandler;
import traveldashboard.server.handler.TubeStatusHandler;
import traveldashboard.server.handler.UserRatingHandler;
import traveldashboard.server.handler.GeneralCrowdednessHandler;
import traveldashboard.server.handler.ClassifiedCrowdednessHandler;
import traveldashboard.server.handler.TramCountdownHandler;
import traveldashboard.server.handler.TramStationHandler;
import traveldashboard.server.handler.TubeCommentHandler;
import traveldashboard.server.handler.TubeCountdownHandler;
import traveldashboard.server.handler.AllTubeRatingHandler;
import traveldashboard.server.handler.TubeStationHandler;
import traveldashboard.server.handler.AllTubeStatusHandler;
import traveldashboard.server.handler.TubeStatusUpdateHandler;
import traveldashboard.server.handler.TwitterHandler;
import traveldashboard.server.handler.UserCommentHandler;
import traveldashboard.server.handler.AllUserRatingHandler;
import traveldashboard.server.handler.WeatherHandler;
import traveldashboard.server.handler.happiness.HappinessPullHandler;
import traveldashboard.server.handler.happiness.HappinessPushHandler;


@WebListener
public class AppContextListener implements ServletContextListener {

	private static boolean DEBUG = true;
	private static String TAG = "AppContextListener";
	
	/** The ibicoop conf prop file. */
	static public String IBICOOP_CONF_PROP_FILE = "/WEB-INF/ibicoop.properties";

	/** The ibicoop ca cert file. */
	static public String IBICOOP_CA_CERT_FILE = "/WEB-INF/ibicoopcacert.crt";
	
    /** THE RATP GTFS files directory */
    //static public String GTFS_DIRECTORY = "/usr/share/tomcat7/webapps/ratp"; // USE THIS FOR DEPLOYMENT
	//static public String GTFS_DIRECTORY = "/home/kim-hwa/ratp"; // USE THIS FOR DEPLOYMENT
	static public String GTFS_DIRECTORY = "D:\\apache-tomcat-7.0.23\\webapps\\RATP_GTFS_FULL"; 
	
	private static final int MINUTES = 30;
	
	//City
	private static final String PARIS = "paris";
	private static final String LONDON = "london";
	
	//RESET
	private static final boolean RESET = false;
	
	
	//
	//
	//Periodic thread to update bike and tube status
	private BikeUpdateHandler bikeUpdateHandler;
	private TubeStatusUpdateHandler tubeStatusUpdateHandler;
	
	
	
	
	
	//
	//
	//IbiReceiver
	
	
	//Route planner handler
	private RoutePlannerHandler routePlannerHandler;
	
	
	//
	//
	//Stops handler
	private AllStopsHandler allStopsHandler;
	private BikeHandler bikeHandler;
	private BusStopHandler busStopHandler;
	private TramStationHandler tramStationHandler;
	private TubeStationHandler tubeStationHandler;	
	
	
	
	//
	//
	//Countdown handler
	private BusCountdownHandler busCountdownHandler;
	private TramCountdownHandler tramCountdownHandler;
	private TubeCountdownHandler tubeCountdownHandler;
	
	
	
	//
	//
	//Incentive
	private IncentiveHandler incentiveHandler;
	
	
	//
	//
	//Noise
	private NoiseHandler noiseHandler;
	private ClassifiedNoiseHandler classifiedNoiseHandler;
	
	
	//
	//
	//Crowd
	private GeneralCrowdednessHandler generalCrowdednessHandler;
	private ClassifiedCrowdednessHandler classifiedCrowdednessHandler;
	
	
	
	
	
	//
	//
	//Ratings
	private AllTubeRatingHandler allTubeRatingHandler;
	private TubeRatingHandler tubeRatingHandler;
	private AllUserRatingHandler allUserRatingHandler;
	private UserRatingHandler userRatingHandler;
	
	
	
	
	//
	//
	//Comments
	private FacebookHandler facebookHandler;
	private TwitterHandler twitterHandler;
	private TubeCommentHandler tubeCommentHandler;
	private UserCommentHandler userCommentHandler;
	


	//
	//
	//Status
	private TubeStatusHandler tubeStatusHandler;
	private AllTubeStatusHandler allTubeStatusHandler;


	
	//
	//
	//Weather
	private WeatherHandler weatherHandler;

	
	

	//
	//
	//Data event listener for data reporting by publish subscribe
	private DataEventListener eventListener;

	
	private PullHandlersApplication pullHandlersApp;
	private PushHandlersApplication pushHandlersApp;
	
	private String userId;
	    
    //
    //
    //
    /*
     * (non-Javadoc)
     * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
     */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		if (DEBUG) TdServerLogger.print(TAG, "contextInitialized", "TravelDashboard server starting...");
		final ServletContext context = event.getServletContext();

		

		try { // initialize iBICOOP
			IbicoopInit.getInstance().setPropConfURL(
					context.getResource(IBICOOP_CONF_PROP_FILE));
			IbicoopInit.getInstance().setCACertURL(
					context.getResource(IBICOOP_CA_CERT_FILE));
			
			String[] ibicoopInput = new String[3];
			userId = DataConstants.USER_HEAD + String.valueOf(System.currentTimeMillis()) + DataConstants.USER_END;
			ibicoopInput[0] = userId;
			ibicoopInput[1] = DataConstants.APPLICATION;
			ibicoopInput[2] = DataConstants.TERMINAL;
			IbicoopInit.getInstance().start(ibicoopInput);
		} catch (MalformedURLException e1) {
			TdServerLogger.printError(TAG, "contextInitialized", e1.getMessage());
		}

		
		//
		//
		//Set up log file writer
		String format = "yyyy-MM-dd'_'HHmmss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String date = sdf.format(new Date());
		TdServerLogger.setUpFileManager(date);
		
		//
		//
		//Data reporting
		eventListener = new DataEventListener();
		
		
		//
		//
		//Init database configuration
		InputStream is_for_db_config = context.getResourceAsStream("/WEB-INF/td_db_config.xml");		
		MongoDbManager.initDbConfiguration(is_for_db_config);

		
		if (RESET) {
			if (DEBUG) TdServerLogger.print(TAG, "contextInitialized", "reset = " + MongoDbManager.resetAllTables(PARIS));
		}
		
		try {
			//Close input stream
			is_for_db_config.close();	
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, "contextInitialized", exception.getMessage());
		}

//		//Reset db
//		MongoDbManager.resetAllTables(DataConstants.PARIS);
//		MongoDbManager.resetAllTables(DataConstants.LONDON);
//		MongoDbManager.resetOthersDb(DataConstants.PARIS);
//		MongoDbManager.resetOthersDb(DataConstants.LONDON);
		
		//MongoDbManager.resetClassifiedCrowdDb(DataConstants.PARIS);
		
		if (MongoDbManager.getMetroStopsSize(PARIS) == 0) {
			
			TdServerLogger.print(TAG, "insertParisMetroData", "Need to insert paris metro data");
			
			final InputStream is_for_paris_metro = context.getResourceAsStream("/WEB-INF/parisMetroStop.xml");
			
			new Thread(new Runnable() {
				@Override
				public void run() {
					
					MongoDbManager.insertParisMetroData(is_for_paris_metro);
					
					try {
						is_for_paris_metro.close();
					} catch (Exception exception) {
						TdServerLogger.printError(TAG, "insertParisMetroData", exception.getMessage());
					}
					
					if (MongoDbManager.needInitParisCrowdData()) {
						MongoDbManager.initParisClassifiedCrowdData();
					}
					
					if (MongoDbManager.needInitParisNoiseData()) {
						MongoDbManager.initParisClassifiedNoiseData();
					}
					
				}
			}).start();
			
		} else {
			
			if (MongoDbManager.needInitParisCrowdData()) {
				MongoDbManager.initParisClassifiedCrowdData();
			}
			
			if (MongoDbManager.needInitParisNoiseData()) {
				MongoDbManager.initParisClassifiedNoiseData();
			}
		}
	
		//
		//
		//Create new tube stations database for london
		//MongoDbManager.resetClassifiedCrowdDb("london");
		//MongoDbManager.resetGeneralCrowdDb("london");
		
		if (MongoDbManager.needToInsertLondonMetroStopsData()) {

			//Read stations*.csv file
			InputStream[] stationsIs = new InputStream[8];
			
			for (int index = 1; index <= 8; index++) {
				String fileName = "/WEB-INF/stations" + index + ".csv";
				stationsIs[index - 1] = context.getResourceAsStream(fileName);
			}
			
			MongoDbManager.insertLondonMetroStopsData(stationsIs);
			
			//Close all input streams for CSV file reading
			for (int j = 0; j < stationsIs.length; j++) {
				try {
					stationsIs[j].close();
				} catch (Exception exception) {
					System.err.println(exception.getMessage());
				}
			}
			
			if (MongoDbManager.needInitLondonCrowdData()) {
				InputStream crowd_is = context.getResourceAsStream("/WEB-INF/crowdedness.json");
				MongoDbManager.insertLondonClassifiedCrowdData(crowd_is);
				//Close input stream for crowdedness
				try {
					crowd_is.close();
				} catch (Exception exception) {
					System.err.println(exception.getMessage());
				}
			}
			
			if (MongoDbManager.needInitLondonClassifiedNoiseData()) {
				InputStream noise_is = context.getResourceAsStream("/WEB-INF/crowdedness.json");
				MongoDbManager.insertLondonClassifiedNoiseData(noise_is);
				try {
					noise_is.close();
				} catch (Exception exception) {
					System.err.println(exception.getMessage());
				}
			}			
			
		} else {
			
			if (MongoDbManager.needInitLondonCrowdData()) {
				InputStream crowd_is = context.getResourceAsStream("/WEB-INF/crowdedness.json");
				MongoDbManager.insertLondonClassifiedCrowdData(crowd_is);
				//Close input stream for crowdedness
				try {
					crowd_is.close();
				} catch (Exception exception) {
					System.err.println(exception.getMessage());
				}
			}
			
			if (MongoDbManager.needInitLondonClassifiedNoiseData()) {
				InputStream noise_is = context.getResourceAsStream("/WEB-INF/crowdedness.json");
				MongoDbManager.insertLondonClassifiedNoiseData(noise_is);
				try {
					noise_is.close();
				} catch (Exception exception) {
					System.err.println(exception.getMessage());
				}
			}	
		}
		
		
		//
		//
		// Create london bus station db
		// Long execution, need a thread
		//MongoDbManager.resetBusDb(DataConstants.LONDON);
		if (MongoDbManager.needToInsertLondonBusStopsData()) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					InputStream bus_is = context.getResourceAsStream("/WEB-INF/londonBusStopCsv.csv");
					MongoDbManager.insertLondonBusStopsData(bus_is);
					
					try {
						bus_is.close();
					} catch (Exception exception) {
						System.err.println(exception.getMessage());						
					}
				}
			}).start();
		}
		
		
		
		//
		//
		// Create london classified crowd db
//		MongoDbManager.resetClassifiedCrowdDb("paris");
//		MongoDbManager.resetClassifiedCrowdDb("london");
//		MongoDbManager.resetGeneralCrowdDb("paris");
//		MongoDbManager.resetGeneralCrowdDb("london");
		
//		if (MongoDbManager.needInitLondonCrowdData()) {
//			InputStream crowd_is = context.getResourceAsStream("/WEB-INF/crowdedness.json");
//			MongoDbManager.insertLondonClassifiedCrowdData(crowd_is);
//			//Close input stream for crowdedness
//			try {
//				crowd_is.close();
//			} catch (Exception exception) {
//				System.err.println(exception.getMessage());
//			}
//		}


		
		
		
		
		
		//
		//
		// Initialize ratings
//		if (MongoDbManager.needToInitGeneralRatings(PARIS)) {
//			MongoDbManager.initGeneralRatings(PARIS);
//		}
//
//		if (MongoDbManager.needToInitGeneralRatings(LONDON)) {
//			MongoDbManager.initGeneralRatings(LONDON);
//		}		
//		
		
		
		
		
		
		
		//
		//
		// Initialize tube status
		if (MongoDbManager.needToInitMetroStatus(PARIS)) {
			MongoDbManager.initMetroStatus(PARIS);
		}

		if (MongoDbManager.needToInitMetroStatus(LONDON)) {
			MongoDbManager.initMetroStatus(LONDON);
		}
		
		//
		//
		//Initialize london tube station fact
		if (MongoDbManager.needToInsertLondonTubeStationFactsData()) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					InputStream londonTubeStationFactIs = context.getResourceAsStream("/WEB-INF/londonTubeStationWikiCsv.csv");
					MongoDbManager.insertLondonTubeStationFactsData(londonTubeStationFactIs);
					
					try {
						londonTubeStationFactIs.close();
					} catch (Exception exception) {
						System.err.println(exception.getMessage());						
					}
				}
			}).start();			
		}
		
		//
		//
		//Initialize london tube station joke
		if (MongoDbManager.needToInsertLondonTubeStationJokesData()) {
			new Thread(new Runnable() {
				@Override
				public void run() {
					InputStream londonTubeStationJokeIs = context.getResourceAsStream("/WEB-INF/londonTubeStationJokeCsv.csv");
					MongoDbManager.insertLondonTubeStationJokesData(londonTubeStationJokeIs);
					try {
						londonTubeStationJokeIs.close();
					} catch (Exception exception) {
						System.err.println(exception.getMessage());						
					}
				}
			}).start();			
		}		
		
		
//		//
//		//
//		//Stops handler
//		//Metro stops
//		tubeStationHandler = new TubeStationHandler();
//		tubeStationHandler.startReceiver();
//				
//		//Bus stops
//		busStopHandler = new BusStopHandler();
//		busStopHandler.startReceiver();
		
		//Tram stops
		//tramStationHandler = new TramStationHandler();
		//tramStationHandler.startReceiver();
		
//		//Bike stops
//		bikeHandler = new BikeHandler();
//		bikeHandler.startReceiver();
		

		
		//
		//
		//Route planner handler
		routePlannerHandler = new RoutePlannerHandler();
		routePlannerHandler.startReceiver();
		
		
		
		//
		//
		//Countdown handler
		//Metro countdown
		tubeCountdownHandler = new TubeCountdownHandler();
		tubeCountdownHandler.startReceiver();
		
		//Bus countdown
		busCountdownHandler = new BusCountdownHandler();
		busCountdownHandler.startReceiver();		
		
		//Tram countdown
		//tramCountdownHandler = new TramCountdownHandler();
		//tramCountdownHandler.startReceiver();		
			
		
		
		//
		//
		//Incentive
		incentiveHandler = new IncentiveHandler();
		incentiveHandler.startReceiver();
		
		
		
		//
		//
		//Noise
		noiseHandler = new NoiseHandler();
		noiseHandler.startReceiver();
		
		classifiedNoiseHandler = new ClassifiedNoiseHandler();
		classifiedNoiseHandler.startReceiver();

		
		
		//Insert artificial noise data
//		//Embankement
//		
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.5072418, -0.122360216, 200);
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.5072418, -0.122360216, 400);
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.5072418, -0.122360216, 600);
//		//Charing cross
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.50859321, -0.124754687, 200);
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.50859321, -0.124754687, 400);
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.50859321, -0.124754687, 600);	
//		//Convent garden
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.51290959, -0.124159261, 200);
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.51290959, -0.124159261, 400);
//		MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.51290959, -0.124159261, 600);			
//	
		
		//
		//
		//Crowd
		generalCrowdednessHandler = new GeneralCrowdednessHandler();
		generalCrowdednessHandler.startReceiver();
		
		classifiedCrowdednessHandler = new ClassifiedCrowdednessHandler();
		classifiedCrowdednessHandler.startReceiver();
		
		
		
		
		//
		//
		//Ratings
//		allTubeRatingHandler = new AllTubeRatingHandler();
//		allTubeRatingHandler.startReceiver();
//		
//		tubeRatingHandler = new TubeRatingHandler();
//		tubeRatingHandler.startReceiver();
		
		//allUserRatingHandler = new AllUserRatingHandler();
		//allUserRatingHandler.startReceiver();
		
		//userRatingHandler = new UserRatingHandler();
		//userRatingHandler.startReceiver();
		
		
		
		
		//
		//
		//Comments
		facebookHandler = new FacebookHandler();
		facebookHandler.startReceiver();
		
		twitterHandler = new TwitterHandler();
		twitterHandler.startReceiver();
		
		tubeCommentHandler = new TubeCommentHandler();
		tubeCommentHandler.startReceiver();
		
		//userCommentHandler = new UserCommentHandler();
		//userCommentHandler.startReceiver();


		
		//
		//
		//Status
//		tubeStatusHandler = new TubeStatusHandler();
//		tubeStatusHandler.startReceiver();
	
		allTubeStatusHandler = new AllTubeStatusHandler();
		allTubeStatusHandler.startReceiver();
		
		//
		//
		//Weather
//		weatherHandler = new WeatherHandler();
//		weatherHandler.startReceiver();
//		
		
		

		

		


		
//		//Set up noise event
		NoiseEvent.enableNoiseTopic(eventListener);
		
		//Set up crowd event
		CrowdEvent.enableCrowdTopic(eventListener);
//		
//		//Set up rating event
//		RatingEvent.enableRatingTopic(eventListener);
//		
//		//Set up comment event
		CommentEvent.enableCommentTopic(eventListener);
		
		
		
		
//		//
//		//
//		//Initialize bike database
//		//Bike data update	
//		bikeUpdateHandler = new BikeUpdateHandler(1000*60*MINUTES, context); //30 minutes
//		//Tube status update
		tubeStatusUpdateHandler = new TubeStatusUpdateHandler(1000*60*MINUTES); //30 minutes
		
//		//TestDb.testDb();
//		
//		
//		


		try {
		System.out.println("come here");
		// XXX:
		pullHandlersApp = new PullHandlersApplication(new Class[]{HappinessPullHandler.class});
		pullHandlersApp.startReceivers();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			pushHandlersApp = new PushHandlersApplication(new Class[]{HappinessPushHandler.class});
			pushHandlersApp.startNotification();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		//
		//
		//
		TdServerLogger.print(TAG, "contextInitialized", "TravelDashboard server context with handler started");
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		if (bikeUpdateHandler != null) bikeUpdateHandler.stop();
		if (tubeStatusUpdateHandler != null) tubeStatusUpdateHandler.stop();
		
		
		//
		//Route planner handler
		if (routePlannerHandler != null) routePlannerHandler.stopReceiver();
		
		
		//
		//Stops handler
		if (allStopsHandler != null) allStopsHandler.stopReceiver();
		if (bikeHandler != null) bikeHandler.stopReceiver();
		if (busStopHandler != null) busStopHandler.stopReceiver();
		if (tramStationHandler != null) tramStationHandler.stopReceiver();
		if (tubeStationHandler != null) tubeStationHandler.stopReceiver();
		
		
		
		//
		//
		//Countdown handler
		if (busCountdownHandler != null) busCountdownHandler.stopReceiver();
		if (tramCountdownHandler != null) tramCountdownHandler.stopReceiver();
		if (tubeCountdownHandler != null) tubeCountdownHandler.stopReceiver();
		
		
		//
		//
		//Incentive handler
		if (incentiveHandler != null) incentiveHandler.stopReceiver();
		
		
		//
		//
		//Noise handler
		if (noiseHandler != null) noiseHandler.stopReceiver();
		if (classifiedNoiseHandler != null) classifiedNoiseHandler.stopReceiver();
		
		
		//
		//
		//Crowd
		if (generalCrowdednessHandler != null) generalCrowdednessHandler.stopReceiver();
		if (classifiedCrowdednessHandler != null) classifiedCrowdednessHandler.stopReceiver();
		
		
		
		
		
		//
		//
		//Ratings
		if (allTubeRatingHandler != null) allTubeRatingHandler.stopReceiver();
		if (tubeRatingHandler != null) tubeRatingHandler.stopReceiver();
		if (allUserRatingHandler != null) allUserRatingHandler.stopReceiver();
		if (userRatingHandler != null) userRatingHandler.stopReceiver();
		
		
		
		
		//
		//
		//Comments
		if (facebookHandler != null) facebookHandler.stopReceiver();
		if (twitterHandler != null) twitterHandler.stopReceiver();
		if (tubeCommentHandler != null) tubeCommentHandler.stopReceiver();
		if (userCommentHandler != null) userCommentHandler.stopReceiver();
		


		//
		//
		//Status
		if (tubeStatusHandler != null) tubeStatusHandler.stopReceiver();
		if (allTubeStatusHandler != null) allTubeStatusHandler.stopReceiver();


		
		//
		//
		//Weather
		if (weatherHandler != null) weatherHandler.stopReceiver();
		
		
		// XXX:
		if (pullHandlersApp != null) {
			pullHandlersApp.stopReceivers();
		}

		if (pushHandlersApp != null) {
			pushHandlersApp.stopNotification();
		}
		
		IbicoopInit.getInstance().terminate();
		TdServerLogger.print(TAG, "contextDestroyed", "TravelDashboard server context destroyed");
	}
}
