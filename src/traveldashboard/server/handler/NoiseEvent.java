package traveldashboard.server.handler;

import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopLoader;

import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.TdServerLogger;

/**
 * Handle noise reporting event
 * @author khoo
 *
 */
public class NoiseEvent {
	
	private static final String TAG = "NoiseEvent";
	
	/**
	 * Create noise ibitopic if not created, else get the topic, and subscribe to the topic 
	 * @return
	 */
	public static synchronized IbiTopic enableNoiseTopic(DataEventListener dataEventListener) {
		
		String methodTag = "enableNoiseTopic";
		
        TdServerLogger.print(TAG, methodTag, "Enable noise topic...");
		
		IbiTopic ibiTopic = getNoiseTopic();
		
		if (ibiTopic == null) {
			ibiTopic = createNoiseTopic();
		}
		
		//Subscribe to the noise ibitopic
		ibiTopic.subscribe();

		//Start event notification
		IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME)
				.startNotification(dataEventListener);
		
		return ibiTopic;
	}
	
	/**
	 * Get noise topic
	 * @return Noise topic
	 */
	public static IbiTopic getNoiseTopic() {
		
		String methodTag = "getNoiseTopic";
		
        TdServerLogger.print(TAG, methodTag, "Getting noise topic...");
        
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		String topicName = DataConstants.TOPIC_NAME_NOISE;
		String topicType = DataConstants.TOPIC_TYPE_NOISE;
	    TdServerLogger.print(TAG, methodTag, "Get noise topic name : " + topicName + ", topic type : " + topicType);
		topicFilter.setName(topicName);
		topicFilter.setType(topicType);
		IbiTopic[] topics = brokerManager.getTopics(topicFilter);
		
		if (topics == null || topics.length == 0) {
	        TdServerLogger.printError(TAG, methodTag, "Topic is null!");
			return null;
		} else {
	        TdServerLogger.print(TAG, methodTag, "Found topic");
			return topics[0];
		}
	}
	
	/**
	 * Create noise topic
	 * @return Noise topic
	 */
	public static IbiTopic createNoiseTopic() {
		
		String methodTag = "createNoiseTopic";
		
        TdServerLogger.print(TAG, methodTag, "Creating noise topic...");		
		
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		String topicName = DataConstants.TOPIC_NAME_NOISE;
		String topicType = DataConstants.TOPIC_TYPE_NOISE;
		
        TdServerLogger.print(TAG, methodTag, "Noise topic name : " + topicName + ", topic type : " + topicType);
		
        IbiTopic noiseTopic = brokerManager.createIbiTopic(topicName, topicName);
		noiseTopic.setType(topicType);
		noiseTopic.registerTopic();
		return noiseTopic;
	}

}
