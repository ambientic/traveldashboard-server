package traveldashboard.server.handler;

import java.util.ArrayList;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import traveldashboard.data.status.TubeStatus;
import traveldashboard.data.status.TubeStatusCollection;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.MongoDbManager;
import traveldashboard.server.data.TdServerLogger;
import traveldashboard.server.data.TubeStatusResponse;
import traveldashboard.server.data.TwitterResponse;


public class TubeStatusUpdateHandler {
	
	private static final long START_AFTER_MS = 5000;
	private static final boolean DEBUG = DataConstants.DEBUG;

	private static final String TAG = "TubeStatusUpdateHandler";
	private static final int CORE_POOL_SIZE = 10;
	private ScheduledThreadPoolExecutor threadExecutor;
	
	/**
	 * 
	 * @param period Period in millisecond
	 * @param context ServletContext
	 */
	public TubeStatusUpdateHandler(long period) {
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		threadExecutor.scheduleAtFixedRate(new UpdateTubeStatusThread(),  START_AFTER_MS, period, TimeUnit.MILLISECONDS);
	}
	
	public void stop() {
		threadExecutor.shutdownNow();
	}
	
	protected class UpdateTubeStatusThread implements Runnable {
		String methodTag = "UpdateTubeStatusThread";

		@Override
		public void run() {
			if (DEBUG) TdServerLogger.print(TAG, methodTag, "Update tube status info");
			
	        ArrayList<String> ROUTE_ID = MongoDbManager.getMetroRouteIdList(DataConstants.PARIS);
	        
			//Paris
			for (String routeId : ROUTE_ID) {
				try {


					TubeStatus tubeStatus = TwitterResponse.getParisMetroStatus(routeId);
					
					if (DEBUG) {
						TdServerLogger.print(TAG, methodTag, "Route id = " + routeId);
						 TdServerLogger.print(TAG, methodTag, "Paris : " + tubeStatus.toString());
					}

					MongoDbManager.updateMetroStatus(DataConstants.PARIS, routeId, tubeStatus.getStatus(), tubeStatus.getDescription());
				} catch (Exception e) {
					TdServerLogger.printError(TAG, methodTag, e.getMessage());
				}
			}
	
			//London
			TubeStatusCollection londonMetroStatusCollection = TubeStatusResponse.getTubeStatusCollection();
			TubeStatus[] londonMetroStatus = londonMetroStatusCollection.getTubeStatusCollection();
			for (TubeStatus status : londonMetroStatus) {
				//TdServerLogger.print(TAG, methodTag, "London : " + status.toString());
				MongoDbManager.updateMetroStatus(DataConstants.LONDON, status.getTube().getId(), status.getStatus(), status.getDescription());
			}
		}
	}
}
