package traveldashboard.server.handler;


import org.ibicoop.adaptation.AdaptationAction;
import org.ibicoop.adaptation.communication.RestartReceiverAction;
import org.ibicoop.adaptation.communication.StartReceiverAction;
import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiReceiver;
import org.ibicoop.communication.common.ReceiverListener;
import org.ibicoop.exchange.ExchangeManager;
import org.ibicoop.exchange.ExchangeSender;
import org.ibicoop.exchange.ExchangeSenderListener;
import org.ibicoop.exchange.ExchangeSession;
import org.ibicoop.filemanager.FileManager;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.DataBuffer;

import traveldashboard.server.data.BusStopGetterUtils;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.ResponseUtils;
import traveldashboard.server.data.TdServerLogger;

/**
 * Get bus stops
 * @author khoo
 *
 */
public class BusStopHandler {

	private static final String TAG = "BusStopHandler";
	
	//Ibicoop
	private IbiReceiver ibiReceiver = null;
	private AdaptationAction action = null;
	
	//Ibicoop file exchange sender
	private ExchangeSender exchangeSender;
	
	private boolean isFinish;
	
	//Ibicoop file exchange sender listener
	private ExchangeSenderListener exchangeSenderListener = new ExchangeSenderListener() {
		
		@Override
		public void exchangeSndResumed(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeSndResumed","exchangeSndResumed");
		}
		
		@Override
		public void exchangeSndPaused(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeSndPaused", "exchangeSndPaused");
		}
		
		@Override
		public void exchangeSndInterrupted(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeSndInterrupted", "exchangeSndInterrupted");
			isFinish = true;
		}
		
		@Override
		public void exchangeSndCompleted(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeSndCompleted", "exchangeSndCompleted");	
			isFinish = true;
		}
		
		@Override
		public void exchangeSndAborted(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeSndAborted", "exchangeSndAborted");				
			isFinish = true;
		}
		
		@Override
		public void exchangeRefused(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeRefused", "exchangeRefused");
			isFinish = true;
		}
		
		@Override
		public void exchangeBlockSent(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeBlockSent", "exchangeBlockSent");
		}
		
		@Override
		public void exchangeAccepted(ExchangeSession exchangeSession) {
			TdServerLogger.print(TAG, "exchangeAccepted", "exchangeAccepted");		
		}
	};
	
	ReceiverListener busStopReceiverListener = new ReceiverListener() {

		@Override
		public byte[] receivedMessageRequest(IbiReceiver arg0, String arg1,
				int arg2, int arg3, byte[] data) {
			String methodTag = "receivedMessageRequest";
			
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Received an iBICOOP request: " + new String(data));
			
			isFinish = false;
			
			//Get the request network message
			NetworkMessage message = NetworkMessageXml.readMessage(data);
			
			//return BusStopGetterUtils.getBusStopMessage(message).encode();
			
			//Prepare the reply network message
			NetworkMessage resp = ResponseUtils.prepareMsgReturn(message);

			//List<NetworkMessage> responseMessageList = BusStopGetterUtils.getBusStopMessageArray(message);
			String reply = "OK";
			
			try {
				//Get bus stop json string
				String json = BusStopGetterUtils.getBusStopJsonString(message);
				
				//Create ibicoop file manager
				FileManager fileManager = IbicoopInit.getInstance().getFileManager();
				
				//Write json string into file
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Bus stop handler : writting json string into " + DataConstants.EXCHANGE_COMMON_FILE_NAME + "!");				
				fileManager.deleteUserContent(DataConstants.EXCHANGE_COMMON_FILE_NAME);
				fileManager.createFileUserContent(DataConstants.EXCHANGE_COMMON_FILE_NAME);
				fileManager.writeUserContent(DataConstants.EXCHANGE_COMMON_FILE_NAME, json.getBytes());
				
				String ibiurl = message.getPayload(DataConstants.FILE_EXCHANGE_IBIURL);
				
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Exchange receiver ibiurl = " + ibiurl);
				
				//Create exchange receiver ibiurl from the request network message
				IBIURL exchangeReceiverIbiurl = new IBIURL(ibiurl.trim());
				
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "After convert to IBIURL = " + exchangeReceiverIbiurl);
				
				//Create exchange sender: ? in a thread or directly here ? synchrone or asynchrone
				//Use communication mode: socket, not proxy
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Bus stop handler : Create file exchange manager!");
				
				ExchangeManager emg = IbicoopInit.getInstance().getExchangeManager();
				//emg.init("http://www.ambientalk.com/IbicoopWebAppP2T/CommunicationServlet",
				emg.init(ibiurl.trim(), 
				new CommunicationMode(CommunicationConstants.MODE_SOCKET), 
						DataConstants.TERMINAL, 
						DataConstants.USER);
				
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Bus stop handler : Create file exchange sender!");
				
				exchangeSender = 	emg.createExchangeSender(
									exchangeReceiverIbiurl, 
									new CommunicationMode(CommunicationConstants.MODE_SOCKET),
									exchangeSenderListener);
				
				//Send the file
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Bus stop handler : sending " + DataConstants.EXCHANGE_COMMON_FILE_NAME + "!");
				
			
				exchangeSender.sendFile(DataConstants.EXCHANGE_COMMON_FILE_NAME);
				
				int timeout = DataConstants.TIMEOUT_SEND_FILE_S; //waiting for 30 seconds
				
				while (!isFinish && (timeout > 0)) {
					Thread.sleep(1000);
					timeout--;
				}
				
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Bus stop handler : end of while loop");	
				
			} catch(Exception exception) {
				TdServerLogger.printError(TAG, methodTag,"Send bus stop data failed");
				reply = "FAILED";
			}
			
			resp.addPayload(DataConstants.BUS_STOP_MESSAGE, reply);
			
			return resp.encode();
		}

		@Override
		public void receivedMessageData(IbiReceiver arg0, String arg1,
				int arg2, DataBuffer arg3) {		
			TdServerLogger.print(TAG, "receivedMesssageData", "Received an iBICOOP message: " + new String(arg3.internalData, 0, arg3.dataLength));	
		}

        @Override
        public void connectionStatus(IbiReceiver receiver, int statusCode) {
        	
        	String methodTag = "connectionStatus";
        	
        	String message = "Receive connection status: " +
        			"receiver = " + receiver.getSource().toString() + ", " +
        			"statusCode = " + statusCode;

        	TdServerLogger.print(TAG, methodTag, message);
        	
            if (statusCode != CommunicationConstants.OK) {
                // We restart the receiver
            	TdServerLogger.print(TAG, methodTag,"Restart bus stop handler receiver");
                try {
                    RestartReceiverAction action = new RestartReceiverAction(receiver);
                    IbicoopInit.getInstance().getAdaptationManager().addAction(action);
                } catch (Exception e) {
                	TdServerLogger.printError(TAG, methodTag, e.getMessage());
                } 
            }  else if (statusCode == CommunicationConstants.OK) {
            	//Take the receiver if is ok
            	if (ibiReceiver == null) {
            		TdServerLogger.print(TAG, methodTag,"Take the callback bus stop handler receiver");
                    ibiReceiver = receiver;               	
            	}
            }
        }

		@Override
		public boolean acceptSenderConnection(IbiReceiver arg0, String arg1) {
			return true;
		}
	};
	

	public void startReceiver(){
		
		String methodTag = "startReceiver";
		
		try {
			TdServerLogger.print(TAG, methodTag, "Creating bus stop handler...");
			//Create receiver with the method of start receiver action
			IBIURL uri = new IBIURL("ibiurl", DataConstants.USER,
					DataConstants.TERMINAL, DataConstants.APPLICATION, DataConstants.BUS_STOP_SERVICE, DataConstants.PATH_GET_BUS_STOP);
			
			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));
			
			String proxyAddress = IbicoopLoader.load().getProxyURL();
			action = new StartReceiverAction(uri, options, proxyAddress, busStopReceiverListener, false);
			
			IbicoopLoader.load().getAdaptationManager().addAction(action);			
	
			TdServerLogger.print(TAG, methodTag, "Creating bus stop file exchange directory");
			
			//Create ibicoop file manager
			FileManager fileManager = IbicoopInit.getInstance().getFileManager();
			
			//Create file exchange directory and file
			fileManager.createDirectoryUserContent(DataConstants.EXCHANGE_LOCATION);
			fileManager.createDirectoryUserContent(DataConstants.EXCHANGE_COMMON_LOCATION);
			fileManager.createFileUserContent(DataConstants.EXCHANGE_COMMON_FILE_NAME);
						
		} catch (Exception e) {
        	TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
	}
	
	public void stopReceiver() {
		if (ibiReceiver != null) ibiReceiver.stop();
	}
}
