package traveldashboard.server.handler;

import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopLoader;


import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.TdServerLogger;

/**
 * Posting comment event
 * @author khoo
 *
 */
public class CommentEvent {
	
	private static final String TAG = "CommentEvent";
	
	/**
	 * Create comment ibitopic if not created, else get the topic, and subscribe to the topic 
	 * @return
	 */
	public static synchronized IbiTopic enableCommentTopic(DataEventListener dataEventListener) {
		
		String methodTag = "enableCommentTopic";
		
        TdServerLogger.print(TAG, methodTag, "Enable comment topic...");
		
		IbiTopic ibiTopic = getCommentTopic();
		
		if (ibiTopic == null) {
			ibiTopic = createCommentTopic();
		}
		
		//Subscribe to the noise ibitopic
		ibiTopic.subscribe();

		//Start event notification
		IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME)
				.startNotification(dataEventListener);
		
		TdServerLogger.outLog(TAG, methodTag, "Enable comment topic OK");
		
		return ibiTopic;
	}
	
	/**
	 * Get comment topic
	 * @return Comment topic
	 */
	public static IbiTopic getCommentTopic() {
		
		String methodTag = "getCommentTopic";
		
        TdServerLogger.print(TAG, methodTag, "Getting comment topic...");
        
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		String topicName = DataConstants.TOPIC_NAME_COMMENT;
		String topicType = DataConstants.TOPIC_TYPE_COMMENT;
	    TdServerLogger.print(TAG, methodTag, "Get comment topic name : " + topicName + ", topic type : " + topicType);
		topicFilter.setName(topicName);
		topicFilter.setType(topicType);
		IbiTopic[] topics = brokerManager.getTopics(topicFilter);
		
		if (topics == null || topics.length == 0) {
	        TdServerLogger.printError(TAG, methodTag, "Topic is null!");
			return null;
		} else {
	        TdServerLogger.print(TAG, methodTag, "Found topic");
			return topics[0];
		}
	}
	
	/**
	 * Create noise topic
	 * @return Noise topic
	 */
	public static IbiTopic createCommentTopic() {
		
		String methodTag = "createCommentTopic";
		
        TdServerLogger.print(TAG, methodTag, "Creating comment topic...");		
		
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		String topicName = DataConstants.TOPIC_NAME_COMMENT;
		String topicType = DataConstants.TOPIC_TYPE_COMMENT;
		
        TdServerLogger.print(TAG, methodTag, "Comment topic name : " + topicName + ", topic type : " + topicType);
		
        IbiTopic commentTopic = brokerManager.createIbiTopic(topicName, topicName);
		commentTopic.setType(topicType);
		commentTopic.registerTopic();
		return commentTopic;
	}
}
