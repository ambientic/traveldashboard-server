package traveldashboard.server.handler.happiness;

import traveldashboard.data.Station;
import traveldashboard.data.happiness.Happiness;
import traveldashboard.data.happiness.HappinessLevel;
import traveldashboard.server.data.common.PushHandler;

/**
 * This class is used to handle happiness at a station voted by users.
 * To be simple, this class only appends happiness into a list in memory
 * so that when users ask for happiness at the station, we only find information
 * in the memory list.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public class HappinessPushHandler extends PushHandler<Happiness, Station, HappinessLevel> {

	public HappinessPushHandler() {
		super(Happiness.class);
	}

	@Override
	public void receiveData(Happiness data) {
		HappinessPullHandler.update(data);
	}

}
