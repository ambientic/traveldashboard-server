package traveldashboard.server.handler.happiness;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import traveldashboard.data.Station;
import traveldashboard.data.happiness.Happiness;
import traveldashboard.data.happiness.HappinessLevel;
import traveldashboard.data.happiness.HappinessRequest;
import traveldashboard.data.happiness.HappinessVote;
import traveldashboard.server.data.common.PullHandler;

/**
 * This class is used to respond to mobile phones when they ask for happiness at a station.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public class HappinessPullHandler extends PullHandler<HappinessVote, Station, HappinessLevel, HappinessRequest> {
	
	private static final long INTERVAL = HappinessRequest.INTERVAL;
	

	/**Attribute which contains all the happiness voted by people*/
	private static List<Happiness> happinesses = new ArrayList<Happiness>();

	public HappinessPullHandler() {
		super(HappinessVote.class, HappinessRequest.class);
	}

	/**
	 * This method first finds all the happiness information about the station indicated by
	 * the request in an interval of time. Then, it finds a happiness level which was voted
	 * by major people. Finally, this happiness level will be sent to mobile phones with
	 * statistic data.
	 * 
	 * @more {@link PullHandler#response(HappinessRequest)}
	 */
	@Override
	public HappinessVote response(HappinessRequest request) {
		
		Station place = request.getStation();
		
		List<Happiness> totalList = getHappinessList(place, request.getTimestamp());
		
		List<Happiness> modeList = getModeHappinessList(totalList);
		
		HappinessVote hv = new HappinessVote();
		
		hv.setTimeStamp(request.getTimestamp());
		hv.setHotSpot(place);
		hv.setTotalVotes(totalList.size());
		hv.setNumberOfVotes(modeList.size());
		if (modeList.size() > 0) {
			hv.setCrowdSourcedValue(modeList.get(0).getCrowdSourcedValue());
		}
		
		modeList.clear();
		totalList.clear();
		
		return hv;
	}

	/**
	 * Find a list of happiness that number of people who voted for that value is maximal.
	 * @param totalList
	 * @return
	 */
	private List<Happiness> getModeHappinessList(List<Happiness> totalList) {
		Map <Integer, Integer> maps = new HashMap<Integer, Integer>();
		for (Happiness tmp : totalList) {
			int level = tmp.getCrowdSourcedValue().getLevel();
			if (maps.containsKey(level)) {
				int val = maps.get(level);
				maps.put(level, val + 1);
			} else maps.put(level, 1);
		}
		int maxOccurences = 0;
		int modeLevel = -1;
		for (Integer key : maps.keySet()) {
			int val = maps.get(key);
			if (val > maxOccurences) {
				modeLevel = key;
				maxOccurences = val;
			}
			
		}
		List<Happiness> rets = new ArrayList<Happiness>();
		for (Happiness tmp : totalList) {
			if (tmp.getCrowdSourcedValue().getLevel() == modeLevel) rets.add(tmp);
		}
		return rets;
	}

	/**
	 * Gets all happiness information at a station in an interval of time.
	 * @param place
	 * @param timestamp
	 * @return
	 */
	private List<Happiness> getHappinessList(Station place, long timestamp) {
		List<Happiness> rets = new ArrayList<Happiness>();
		for (Happiness tmp : happinesses) {
			long tmpTime = tmp.getTimeStamp();
			if (place.equals(tmp.getHotSpot()) && timestamp <= tmpTime && tmpTime <= timestamp + INTERVAL) {
				rets.add(tmp);
			}
		}
		return rets;
	}

	public static void update(Happiness happiness) {
		happinesses.add(happiness);
	}

}
