package traveldashboard.server.handler;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletContext;

import traveldashboard.data.BikeStationsCollection;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.MongoDbManager;
import traveldashboard.server.data.TdServerLogger;
import traveldashboard.server.data.TflBikeInfoProcess;
import traveldashboard.server.data.VelibInfoProcess;

public class BikeUpdateHandler {
	
	private static final String TAG = "BikeUpdateHandler";
	private static final boolean DEBUG = DataConstants.DEBUG;
	protected static final int CORE_POOL_SIZE = 10;
	protected ScheduledThreadPoolExecutor threadExecutor;
	protected ServletContext context;
	/**
	 * 
	 * @param period Period in millisecond
	 * @param context ServletContext
	 */
	public BikeUpdateHandler(long period, ServletContext context) {
		this.context = context;
		threadExecutor = new ScheduledThreadPoolExecutor(CORE_POOL_SIZE);
		threadExecutor.scheduleAtFixedRate(new UpdateBikeThread(), 0, period, TimeUnit.MILLISECONDS);
	}
	
	public void stop() {
		threadExecutor.shutdownNow();
	}
	
	protected class UpdateBikeThread implements Runnable {
		@Override
		public void run() {
            if (DEBUG) TdServerLogger.print(TAG, "UpdateBikeThread", 
            		"update bike db");

			//Update London bike info
			MongoDbManager.updateBikeDb("london", TflBikeInfoProcess.getBikeStationsCollection());

			//Update Paris bike info
			BikeStationsCollection velibCollection = VelibInfoProcess.getBikeStationsCollection(context);
			if (velibCollection != null) {
				//Update only if the access is not blocked
				MongoDbManager.updateBikeDb("paris", velibCollection);
			}

		}
	}
}
