package traveldashboard.server.handler;

import org.ibicoop.adaptation.AdaptationAction;
import org.ibicoop.adaptation.communication.RestartReceiverAction;
import org.ibicoop.adaptation.communication.StartReceiverAction;
import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiReceiver;
import org.ibicoop.communication.common.ReceiverListener;
import org.ibicoop.exceptions.MalformedIbiurlException;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.DataBuffer;

import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.AllUserRatingGetterUtils;
import traveldashboard.server.data.TdServerLogger;

public class AllUserRatingHandler {
	
	private static final String TAG = "AllUserRatingHandler";
	
	//Ibicoop
	private IbiReceiver ibiReceiver = null;
	private AdaptationAction action = null;
	
	ReceiverListener allUserRatingReceiverListener = new ReceiverListener() {

		@Override
		public byte[] receivedMessageRequest(IbiReceiver arg0, String arg1,
				int arg2, int arg3, byte[] data) {
			TdServerLogger.print(TAG, "receivedMessageRequest", "Received an iBICOOP request: " + new String(data));
			NetworkMessage message = NetworkMessageXml.readMessage(data);
			return AllUserRatingGetterUtils.getAllUserRatingMessage(message).encode();
		}

		@Override
		public void receivedMessageData(IbiReceiver arg0, String arg1,
				int arg2, DataBuffer arg3) {		
			TdServerLogger.print(TAG, "receivedMessageData","Received an iBICOOP message: " + new String(arg3.internalData, 0, arg3.dataLength));
		}

        @Override
        public void connectionStatus(IbiReceiver receiver, int statusCode) {
        	
        	String methodTag = "connectionStatus";
        	
        	String message = "Receive connection status: " +
        			"receiver = " + receiver.getSource().toString() + ", " +
        			"statusCode = " + statusCode;

        	TdServerLogger.print(TAG, methodTag, message);
        	
            if (statusCode != CommunicationConstants.OK) {
                // We restart the receiver
            	TdServerLogger.print(TAG, methodTag, "Restart all user rating handler receiver");
            	
                try {
                    RestartReceiverAction action = new RestartReceiverAction(receiver);
                    IbicoopInit.getInstance().getAdaptationManager().addAction(action);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }   else if (statusCode == CommunicationConstants.OK) {
            	//Take the receiver if is ok
            	if (ibiReceiver == null) {
            		TdServerLogger.print(TAG, methodTag,"Take the callback all user rating handler receiver");
                    ibiReceiver = receiver;               	
            	}
            }
        }

		@Override
		public boolean acceptSenderConnection(IbiReceiver arg0, String arg1) {
			return true;
		}
	};
	

	public void startReceiver(){
		String methodTag = "startReceiver";
		
		try {
			TdServerLogger.print(TAG, methodTag,"Creating all user rating handler...");
			
			IBIURL uri = new IBIURL("ibiurl", DataConstants.USER,
					DataConstants.TERMINAL, DataConstants.APPLICATION, DataConstants.ALL_USER_RATING_SERVICE, DataConstants.PATH_GET_ALL_USER_RATING);
			
			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			String proxyAddress = IbicoopLoader.load().getProxyURL();
			action = new StartReceiverAction(uri, options, proxyAddress, allUserRatingReceiverListener, false);
			
			IbicoopLoader.load().getAdaptationManager().addAction(action);			
		} catch (MalformedIbiurlException e) {
        	TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
	}
	
	public void stopReceiver() {
		if (ibiReceiver != null) ibiReceiver.stop();
	}
}
