package traveldashboard.server.handler;

import java.util.TimeZone;

import org.ibicoop.broker.common.EventNotificationListener;
import org.ibicoop.broker.common.IbiResource;
import org.ibicoop.broker.common.IbiResourceEvent;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import traveldashboard.data.TransportArea;
import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.MongoDbManager;
import traveldashboard.server.data.StationNotifSender;
import traveldashboard.server.data.TdServerLogger;
public class DataEventListener implements EventNotificationListener {

	private static final String TAG = "DataEventListener";
	
	private int brokerStatus = -1;
	
	public DataEventListener() {
		//Start event notification
		IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME)
				.startNotification(this);
		waitForBrokerStatus(3);
	}
	
	private void waitForBrokerStatus(int code) {
		int retry = 20; //20s
		while (retry > 0 && brokerStatus != code) {
			try {
				Thread.sleep(1000);
				TdServerLogger.print(TAG, "waitForBrokerStatus", "retry = " + retry);
			} catch (InterruptedException e) {
				TdServerLogger.printError(TAG, "waitForBrokerStatus", "retry = " + retry + ", error = " + e.getMessage());
			}
			retry--;
		}
	}
	
	
	/*
	 * (non-Javadoc)
	 * @see org.ibicoop.broker.common.EventNotificationListener#acceptNotificationFrom(java.lang.String)
	 */
	@Override
	public boolean acceptNotificationFrom(String eventPublisher) {
		String methodTag = "acceptNotificationFrom";
        TdServerLogger.print(TAG, methodTag, "Accept notification from : " + eventPublisher);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ibicoop.broker.common.EventNotificationListener#brokerStatus(java.lang.String, int)
	 */
	@Override
	public void brokerStatus(String brokerName, int statusCode) {
		String methodTag = "brokerStatus";
        TdServerLogger.print(TAG, methodTag, brokerName + " status : " + statusCode);
        brokerStatus = statusCode;
	}

	/*
	 * (non-Javadoc)
	 * @see org.ibicoop.broker.common.EventNotificationListener#receiveEvent(org.ibicoop.broker.common.IbiTopic, org.ibicoop.broker.common.IbiTopicEvent)
	 */
	@Override
	public void receiveEvent(IbiTopic forTopic, IbiTopicEvent event) {
		String methodTag = "receiveEvent";
		
		//Get event timestamp
		long timestamp = event.getPublicationDate().getTime();
				
		//Get event name
		String eventName = event.getName();
		
		//Get event type
		String eventType = event.getType();
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, ("Receive event : " 
				+ "timestamp = " + timestamp + ", "
				+ "eventName = " + eventName + ", "
				+ "eventType = " + eventType));
		
		if (eventName.equals(DataConstants.EVENT_NAME_NOISE)) {
			if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "This is really a noise event!");
			
			//Get noise event network message
			NetworkMessage message = NetworkMessageXml.readMessage(event.getData());
			
			//Process message if not null
			if (message != null) {
				//Get noise network message parameters
				//Get noise data latitude
				double latitude = Double.parseDouble(message.getPayload(DataConstants.PARAM_KEY_STOP_LAT));
				//Get noise data longitude
				double longitude = Double.parseDouble(message.getPayload(DataConstants.PARAM_KEY_STOP_LON));
				//Get noise level
				int noiseLevel = Integer.parseInt(message.getPayload(DataConstants.PARAM_NOISE_LEVEL));
				
				String city = TransportArea.getArea(latitude, longitude);
				
				String timeZone = "";
				
				if (city.equals(DataConstants.PARIS)) {
					timeZone = "Europe/Paris";
					
				} else if (city.equals(DataConstants.LONDON)) {
					timeZone = "Europe/London";
				}

				//Set to local timestamp
				timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
		    	TimeZone.getTimeZone(timeZone).getRawOffset();
				
				
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Latitude = " + latitude + ", longitude = " + longitude + ", noise level = " + noiseLevel + ", timestamp = " + timestamp);
				//Insert data into database
				MongoDbManager.insertNoiseData(timestamp, latitude, longitude, noiseLevel);				
				//? Notify users listening to the station channel in a new Thread
			} else {
				TdServerLogger.printError(TAG, methodTag, "Network message is null!");
			}
		} else if (eventName.equals(DataConstants.EVENT_NAME_CROWD)) {				
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "This is really a crowd event!");
			
				//Get crowd event network message
				NetworkMessage message = NetworkMessageXml.readMessage(event.getData());
				
				//Process message if not null
				if (message != null) {
					//Get crowd network message parameters
					//Get city
					String city = message.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
					//Get stopId
					String stopId = message.getPayload(DataConstants.PARAM_KEY_STOP_ID);
					//Get routeId
					String routeId = message.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);					
					//Get crowd level
					int level = Integer.parseInt(message.getPayload(DataConstants.PARAM_KEY_LEVEL));
					
					String timeZone = "";
					
					if (city.equals(DataConstants.PARIS)) {
						timeZone = "Europe/Paris";
						
					} else if (city.equals(DataConstants.LONDON)) {
						timeZone = "Europe/London";
					}

					//Set to local timestamp
					timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
			    	TimeZone.getTimeZone(timeZone).getRawOffset();
					
					
					if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, 
			        		"city = " + city 
			        		+ ", stopId = " + stopId
			        		+ ", routeId = " + routeId
			        		+ ", timestamp = " + timestamp
			        		+ ", level = " + level
			        		);
					
					if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, 
			        		"timequery = " + MongoDbManager.convertTimestampToTimeQueryString(timestamp) 
			        		);			        
			        
			        //In Paris we can post different crowd for diffrent lines
			        //FIXME: We can only post the same crowd for all lines in a tube station in London
			        if (city.equals(DataConstants.LONDON)) {
			        	TubeStation metroStation = MongoDbManager.getLondonMetroStation(stopId);
			        	Tube[] tubes = metroStation.getTubes();
			        	
			        	for (Tube tube: tubes) {
			        		String routeIdLondon = tube.getId();
			        		MongoDbManager.insertCrowdData(city, stopId, routeIdLondon, timestamp, level);
			        	}
			        } else if (city.equals(DataConstants.PARIS)) {
						//Insert crowd data into database
						MongoDbManager.insertCrowdData(city, stopId, routeId, timestamp, level);	
			        }
			        
					// notify users listening to the station channel in a new Thread
					StationNotifSender.send(stopId, ""+ level);			        
			        
				}	
				else {
					if (DataConstants.DEBUG) TdServerLogger.printError(TAG, methodTag, "Network message is null!");
						}
				
			} else if (eventName.equals(DataConstants.EVENT_NAME_RATING)) {
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "This is really a rating event!");
				
				//Get noise event network message
				NetworkMessage message = NetworkMessageXml.readMessage(event.getData());
				
				//Process message if not null
				if (message != null) {
					//Get rating network message parameters
					//Get city
					String city = message.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
					//Get routeName
					String routeId = message.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
					//Get userName
					String userName = message.getPayload(DataConstants.PARAM_KEY_USER_NAME);
					//Get rating
					int rating  = Integer.parseInt(message.getPayload(DataConstants.PARAM_KEY_RATING));
					
					if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, 
							"city = " + city 
							+ ", routeId = " + routeId 
							+ ", user name = " + userName
							+ ", rating = " + rating
							);
					
					//Insert data into database					
					MongoDbManager.updateUserRatings(city, userName, routeId, rating);
			} else {
				TdServerLogger.printError(TAG, methodTag, "Network message is null!");
			}
		} else if (eventName.equals(DataConstants.EVENT_NAME_COMMENT)) {
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "This is really a comment event!");
			
			//Get noise event network message
			NetworkMessage message = NetworkMessageXml.readMessage(event.getData());
			
			//Process message if not null
			if (message != null) {
				//Get comment network message parameters
				//Get city
				String city = message.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
				//Get stopId
				String stopId = message.getPayload(DataConstants.PARAM_KEY_STOP_ID);
				//Get routeId
				String routeId = message.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);				
				//Get userName
				String userName = message.getPayload(DataConstants.PARAM_KEY_USER_NAME);
				//Get comment
				String comment  = message.getPayload(DataConstants.PARAM_KEY_COMMENT);
				
				String timeZone = "";
				
				if (city.equals(DataConstants.PARIS)) {
					timeZone = "Europe/Paris";
					
				} else if (city.equals(DataConstants.LONDON)) {
					timeZone = "Europe/London";
				}

				//Set to local timestamp
				timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
		    	TimeZone.getTimeZone(timeZone).getRawOffset();
				
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, 
						"city = " + city
						+ ", stopId = " + stopId
						+ ", userName = " + userName
						+ ", comment = " + comment
						+ ", timestamp = " + timestamp
						);
				
				
		        //In Paris we can post different comment for diffrent lines
		        //FIXME: We can only post the same comment for all lines in a tube station in London
		        if (city.equals(DataConstants.LONDON)) {
		        	TubeStation metroStation = MongoDbManager.getLondonMetroStation(stopId);
		        	Tube[] tubes = metroStation.getTubes();
		        	
		        	for (Tube tube: tubes) {
		        		String routeIdLondon = tube.getId();
						//Insert comment into database
		        		
		        		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, 
								"city = " + city
								+ ", routeId = " + routeIdLondon
								);
						
						MongoDbManager.insertComment(city, timestamp, userName, stopId, routeIdLondon, comment);
		        	}
		        } else if (city.equals(DataConstants.PARIS)) {
					//Insert comment into database					
					MongoDbManager.insertComment(city, timestamp, userName, stopId, routeId, comment);	
		        }
					
			} else {
				TdServerLogger.printError(TAG, methodTag, "Network message is null!");
			}
		}  
        
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Process event finished");
	}

	@Override
	public void receiveEvent(IbiTopic forTopic,
			IbiResourceEvent resourceEvent) {
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, "receiveEvent", "forTopic = " + forTopic.getName() + ", publisher = " + resourceEvent.getPublisher());
	}

	@Override
	public void receiveEvent(IbiResource forResource,
			IbiResourceEvent resourceEvent) {	
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, "receiveEvent", "forResource = " + forResource.getResName() + ", publisher = " + resourceEvent.getPublisher());
	}
}
