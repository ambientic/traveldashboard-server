package traveldashboard.server.handler;

import org.ibicoop.adaptation.AdaptationAction;
import org.ibicoop.adaptation.communication.RestartReceiverAction;
import org.ibicoop.adaptation.communication.StartReceiverAction;
import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiReceiver;
import org.ibicoop.communication.common.ReceiverListener;
import org.ibicoop.exceptions.MalformedIbiurlException;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.DataBuffer;

import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.TdServerLogger;
import traveldashboard.server.data.TubeStatusGetterUtils;

public class TubeStatusHandler  {
	
	private static final String TAG = "TubeStatusHandler";
	
	//Ibicoop
	private IbiReceiver ibiReceiver = null;
	private AdaptationAction action = null;
	
	ReceiverListener tubeStatusReceiverListener = new ReceiverListener() {

		@Override
		public byte[] receivedMessageRequest(IbiReceiver arg0, String arg1,
				int arg2, int arg3, byte[] data) {
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, "receivedMessageRequest", "Received an iBICOOP request: " + new String(data));
			NetworkMessage message = NetworkMessageXml.readMessage(data);
			return TubeStatusGetterUtils.getTubeStatusMessage(message).encode();
		}

		@Override
		public void receivedMessageData(IbiReceiver arg0, String arg1,
				int arg2, DataBuffer arg3) {		
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, "receivedMessageData","Received an iBICOOP message: " + new String(arg3.internalData, 0, arg3.dataLength));
		}

        @Override
        public void connectionStatus(IbiReceiver receiver, int statusCode) {
        	
        	String methodTag = "connectionStatus";
        	
        	String message = "Receive connection status: " +
        			"receiver = " + receiver.getSource().toString() + ", " +
        			"statusCode = " + statusCode;

        	TdServerLogger.print(TAG, methodTag, message);
        	
            if (statusCode != CommunicationConstants.OK) {
                // We restart the receiver
            	TdServerLogger.print(TAG, methodTag,"Restart single tube status handler receiver");
            	
                try {
                    RestartReceiverAction action = new RestartReceiverAction(receiver);
                    IbicoopInit.getInstance().getAdaptationManager().addAction(action);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }  else if (statusCode == CommunicationConstants.OK) {
            	//Take the receiver if is ok
            	if (ibiReceiver == null) {
            		TdServerLogger.print(TAG, methodTag,"Take the callback tube status handler receiver");
                    ibiReceiver = receiver;               	
            	}
            }
        }

		@Override
		public boolean acceptSenderConnection(IbiReceiver arg0, String arg1) {
			return true;
		}
	};
	

	public void startReceiver(){
		
		String methodTag = "startReceiver";
		
		try {
			TdServerLogger.print(TAG, methodTag,"Creating tube status handler...");
			
			IBIURL uri = new IBIURL("ibiurl", DataConstants.USER,
					DataConstants.TERMINAL, DataConstants.APPLICATION, DataConstants.TUBE_STATUS_SERVICE, DataConstants.PATH_GET_TUBE_STATUS);
			
			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));
			
			String proxyAddress = IbicoopLoader.load().getProxyURL();
			action = new StartReceiverAction(uri, options, proxyAddress, tubeStatusReceiverListener, false);
			
			IbicoopLoader.load().getAdaptationManager().addAction(action);			
		} catch (MalformedIbiurlException e) {
        	TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
	}
	
	public void stopReceiver() {
		if (ibiReceiver != null) ibiReceiver.stop();
	}
}

