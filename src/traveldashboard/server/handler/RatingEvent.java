package traveldashboard.server.handler;

import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopLoader;

import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.TdServerLogger;

/**
 * Posting rating event
 * @author khoo
 *
 */
public class RatingEvent {
	
	private static final String TAG = "RatingEvent";
	
	/**
	 * Create rating ibitopic if not created, else get the topic, and subscribe to the topic 
	 * @return
	 */
	public static synchronized IbiTopic enableRatingTopic(DataEventListener dataEventListener) {
		
		String methodTag = "enableRatingTopic";
		
        TdServerLogger.print(TAG, methodTag, "Enable rating topic...");
		
		IbiTopic ibiTopic = getRatingTopic();
		
		if (ibiTopic == null) {
			ibiTopic = createRatingTopic();
		}
		
		//Subscribe to the rating ibitopic
		ibiTopic.subscribe();

		//Start event notification
		IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME)
				.startNotification(dataEventListener);
		
		return ibiTopic;
	}
	
	/**
	 * Get rating topic
	 * @return Rating topic
	 */
	public static IbiTopic getRatingTopic() {
		
		String methodTag = "getRatingTopic";
		
        TdServerLogger.print(TAG, methodTag, "Getting rating topic...");
        
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		String topicName = DataConstants.TOPIC_NAME_RATING;
		String topicType = DataConstants.TOPIC_TYPE_RATING;
	    TdServerLogger.print(TAG, methodTag, "Get rating topic name : " + topicName + ", topic type : " + topicType);
		topicFilter.setName(topicName);
		topicFilter.setType(topicType);
		IbiTopic[] topics = brokerManager.getTopics(topicFilter);
		
		if (topics == null || topics.length == 0) {
	        TdServerLogger.printError(TAG, methodTag, "Topic is null!");
			return null;
		} else {
	        TdServerLogger.print(TAG, methodTag, "Found topic");
			return topics[0];
		}
	}
	
	/**
	 * Create rating topic
	 * @return Rating topic
	 */
	public static IbiTopic createRatingTopic() {
		
		String methodTag = "createRatingTopic";
		
        TdServerLogger.print(TAG, methodTag, "Creating rating topic...");		
		
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		String topicName = DataConstants.TOPIC_NAME_RATING;
		String topicType = DataConstants.TOPIC_TYPE_RATING;
		
        TdServerLogger.print(TAG, methodTag, "Rating topic name : " + topicName + ", topic type : " + topicType);
		
        IbiTopic ratingTopic = brokerManager.createIbiTopic(topicName, topicName);
		ratingTopic.setType(topicType);
		ratingTopic.registerTopic();
		return ratingTopic;
	}
}
