package traveldashboard.server.handler;

import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopLoader;

import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.TdServerLogger;

/**
 * Posting crowd event
 * @author khoo
 *
 */
public class CrowdEvent {
	
	private static final String TAG = "CrowdEvent";
	
	/**
	 * Create crowd ibitopic if not created, else get the topic, and subscribe to the topic 
	 * @return
	 */
	public static synchronized IbiTopic enableCrowdTopic(DataEventListener dataEventListener) {
		String methodTag = "enableCrowdTopic";
		
        TdServerLogger.print(TAG, methodTag, "Enable crowd topic...");
		
		IbiTopic ibiTopic = getCrowdTopic();
		
		if (ibiTopic == null) {
			ibiTopic = createCrowdTopic();
		}
		
		//Subscribe to the noise ibitopic
		ibiTopic.subscribe();

		//Start event notification
		IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME)
				.startNotification(dataEventListener);
		
		TdServerLogger.outLog(TAG, methodTag, "Enable crowd topic OK");
		
		return ibiTopic;
	}
	
	/**
	 * Get crowd topic
	 * @return Crowd topic
	 */
	public static IbiTopic getCrowdTopic() {
		String methodTag = "getCrowdTopic";
		
        TdServerLogger.print(TAG, methodTag, "Getting crowd topic...");
		
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		
		String topicName = DataConstants.TOPIC_NAME_CROWD;
		String topicType = DataConstants.TOPIC_TYPE_CROWD;
		
        TdServerLogger.print(TAG, methodTag, "Crowd topic name : " + topicName + ", topic type : " + topicType);
		
		topicFilter.setName(topicName);
		topicFilter.setType(topicType);
		IbiTopic[] topics = brokerManager.getTopics(topicFilter);
		
		if (topics == null || topics.length == 0) {
	        TdServerLogger.printError(TAG, methodTag, "Topic is null!");
			return null;
		} else {
	        TdServerLogger.print(TAG, methodTag, "Found topic");
			return topics[0];
		}
	}
	
	/**
	 * Create crowd topic
	 * @return Crowd topic
	 */
	public static IbiTopic createCrowdTopic() {
		String methodTag = "createCrowdTopic";
		
        TdServerLogger.print(TAG, methodTag, "Creating crowd topic...");
		
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(DataConstants.BROKER_NAME);
		
		String topicName = DataConstants.TOPIC_NAME_CROWD;
		String topicType = DataConstants.TOPIC_TYPE_CROWD;

        TdServerLogger.print(TAG, methodTag, "Crowd topic name : " + topicName + ", topic type : " + topicType);
		
		IbiTopic crowdTopic = brokerManager.createIbiTopic(topicName, topicName);
		
		crowdTopic.setType(topicType);
		crowdTopic.registerTopic();
		
		return crowdTopic;
	}
}
