package traveldashboard.server.data;

import java.util.TimeZone;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class ClassifiedNoiseGetterUtils extends ResponseUtils {
	
	private static final String TAG = "ClassifiedNoiseGetterUtils";
	
	public static NetworkMessage getClassifiedNoiseMessage(NetworkMessage req) {
		String methodTag = "getClassifiedNoiseMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Getting classified noise message...");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		
		long timestamp = Long.parseLong(req.getPayload(DataConstants.PARAM_KEY_TIMESTAMP));
		
		String timeZone = "";
		
		if (city.equals(DataConstants.PARIS)) {
			timeZone = "Europe/Paris";
			
		} else if (city.equals(DataConstants.LONDON)) {
			timeZone = "Europe/London";
		}

		//Set to local timestamp
		timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
    	TimeZone.getTimeZone(timeZone).getRawOffset();		
		
		Gson gson = new Gson();

		String json = gson.toJson(MongoDbManager.getMetroClassifiedNoise(city, stopId, routeId, timestamp));
		
		resp.addPayload(DataConstants.CLASSIFIED_NOISE_MESSAGE, json);
		
		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "End get classified noise message : " + new String(resp.encode()));
        
		return resp;
	}
}
