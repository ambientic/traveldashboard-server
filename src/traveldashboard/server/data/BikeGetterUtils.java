package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class BikeGetterUtils extends ResponseUtils {
	
	private static final String TAG = "BikeGetterUtils";
	
	public static NetworkMessage getBikeMessage(NetworkMessage req) {
		
		String methodTag = "getBikeMessage";
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get bike message");
        
		NetworkMessage resp = prepareMsgReturn(req);
		
		String latitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LAT);	
		String longitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LON);
		String radius = req.getPayload(DataConstants.PARAM_KEY_RADIUS);

		if ((latitude == null) || (longitude == null) || (radius == null) || (latitude.length() == 0) || (longitude.length() == 0) || (radius.length() == 0)) {
			resp.addPayload(DataConstants.BIKE_MESSAGE, "null");
			return resp;
		}
		
		Gson gson = new Gson();
		String json = gson.toJson(MongoDbManager.getBikeStationsCollection(latitude, longitude, radius));
		resp.addPayload(DataConstants.BIKE_MESSAGE, json);

		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Finish get bike message : "  + new String(resp.encode()));
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "bike message size = "  + resp.encode().length);
        
		return resp;
	}
}
