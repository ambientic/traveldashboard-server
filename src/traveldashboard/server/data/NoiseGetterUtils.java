package traveldashboard.server.data;

import java.util.TimeZone;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class NoiseGetterUtils extends ResponseUtils {
	
	private static final String TAG = "NoiseGetterUtils";
	
	public static NetworkMessage getNoiseMessage(NetworkMessage req) {
		String methodTag = "getNoiseMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Getting noise message...");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		
		long timestamp = Long.parseLong(req.getPayload(DataConstants.PARAM_KEY_TIMESTAMP));
		long interval = Long.parseLong(req.getPayload(DataConstants.PARAM_KEY_INTERVAL));
		
		String timeZone = "";
		
		if (city.equals(DataConstants.PARIS)) {
			timeZone = "Europe/Paris";
			
		} else if (city.equals(DataConstants.LONDON)) {
			timeZone = "Europe/London";
		}

		//Set to local timestamp
		timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
    	TimeZone.getTimeZone(timeZone).getRawOffset();		
		
		Gson gson = new Gson();

		String json = gson.toJson(MongoDbManager.getMetroNoiseDatasCollection(city, stopId, routeId, timestamp, interval));
		
		resp.addPayload(DataConstants.NOISE_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get noise message : " + new String(resp.encode()));
        
		return resp;
	}
}
