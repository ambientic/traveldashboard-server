package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.BusStationsCollection;


public class BusStopResponse {

	//http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?circle=51.52352400539179,-0.13282299041748047,1500&StopPointState=0&ReturnList=StopCode1,StopPointName,StopPointIndicator,StopPointType,Latitude,Longitude
	//http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?circle=51.52352400539179,-0.13282299041748047,1500&StopPointState=0&ReturnList=StopCode1,StopPointName,LineName,DestinationText,Latitude,Longitude
	private static final String URL_HEAD = "http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?circle=";
	//private static final String URL_END = "&StopPointState=0&ReturnList=StopCode1,StopPointName,StopPointIndicator,StopPointType,Latitude,Longitude";
	private static final String URL_END_WITH_LINE = "&StopPointState=0&ReturnList=StopCode1,StopPointName,LineName,DestinationText,Latitude,Longitude";
	private static final String URL_HEAD_WITH_STOP_CODE = "http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?stopcode1=";
	private static final String URL_END_WITH_STOP_CODE = "&StopPointState=0&ReturnList=StopPointName,DestinationText,LineName,Latitude,Longitude";
	private static boolean debug = DataConstants.DEBUG;
	
	public static BusStationsCollection getBusStationsCollection(String latitude, String longitude, String radius) {
		String allParameters = latitude + "," + longitude + "," + radius;
		return getBusStationsCollection(allParameters);
	}
	
	public static BusStationsCollection getBusStationsCollectionByTextFile(String latitude, String longitude, String radius) {
		String allParameters = latitude + "," + longitude + "," + radius;
		return getBusStationsCollectionByTextFile(allParameters);
	}

	public static BusStationsCollection getBusStationsCollectionByTextFile(String allParameters) {
		List<String> jsonStringList = new ArrayList<String>(); //Used to filter out same buses
		String previousName = "";
		String previousCode = "";
		double previousLat = 0.0;
		double previousLon = 0.0;
		List<BusStation> busStations = new ArrayList<BusStation>();
		BusStationsCollection busStationsCollection = null;
		
		try {

			InputStream is = new FileInputStream("londonBus.txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();

			while((line=br.readLine())!= null){
			    sb.append(line.trim() + "\n");
			}

			br.close();
			
			String json = sb.toString();
			
			//System.out.println(json);
			
			String[] jsonStringArray = json.split("\n");
			
			if (DataConstants.DEBUG) System.out.println("jsonStringArray length = " + jsonStringArray.length);

			// We don't want the first element
			for (int i = 1; i < jsonStringArray.length; i++) {
				
				String infoRow = jsonStringArray[i].trim();
				
				if (!jsonStringList.contains(infoRow)) {
					jsonStringList.add(infoRow);
				}
			}
			
			List<Bus> buses = new ArrayList<Bus>();
			
			for (int k = 0; k < jsonStringList.size(); k++) {
				String jsonString = jsonStringList.get(k);
				//System.out.println(jsonString);
				
				String[] infoArray = jsonString.split(",");
				
				int j = 1;
				
				String stationName = infoArray[j].replaceAll("\"", "");
				
				if (infoArray[j + 1].equals("null")) continue;
				
				if (infoArray[j + 1].replaceAll("\"", "").equals("NONE")) {
					continue;
				}
				
				while(!containsDigit(infoArray[j + 1].replaceAll("\"", ""))) {
					//System.out.println("(j + 1) = " + (j + 1));
					stationName = stationName + "," + infoArray[j + 1].replaceAll("\"", "");
					//System.out.println("station name = " + stationName);
					j++;
				}
				
				String stationCode = infoArray[j + 1].replaceAll("\"", "");
					
				double stationLatitude = Double.parseDouble(infoArray[j + 2]);
				
				double stationLongitude = Double.parseDouble(infoArray[j + 3]);
				
				String busId = infoArray[j + 4].replaceAll("\"", "");
				
				String busName = busId;
									
				String destination = "";
				
				if ((j + 5) == (infoArray.length - 1)) {
					infoArray[j + 5] = infoArray[j + 5].replaceAll("\"", "");
					destination = infoArray[j + 5].replace("]", "");
				} else {
					
					destination = infoArray[j + 5].replaceAll("\"", "");;
					
					for (int a = j + 6; a < infoArray.length; a++) {						
						if (a == (infoArray.length - 1)) {
							infoArray[a] = infoArray[a].replaceAll("\"", "");
							destination = destination + "," + infoArray[a].replace("]", "");
						} else {
							destination = destination + "," + infoArray[a].replaceAll("\"", "");
						}
					}
					
					//System.out.println("destination = " + destination);
				}
				
				if (debug) {
					System.out.println("stationName = " + stationName
							+ " stationCode = " + stationCode
							+ " stationLatitude = " + String.valueOf(stationLatitude)
							+ " stationLongitude = " + String.valueOf(stationLongitude)
							+ " busId " + busId
							+ " busName " + busName
							+ " destination " + destination);
				}
				
				if (!previousCode.equals(stationCode)) {
						
					if (!previousCode.equals("")) {
						//Not in the first iteration, create new bus stations collection
						
						Bus[] busArray = new Bus[buses.size()];
						
						for (int i = 0; i < buses.size(); i++) {
							busArray[i] = buses.get(i);
						}
						
						//Create new bus station
						if (debug) System.out.println(">>>Create new bus station<<<");
						
						BusStation busStation = new BusStation(previousCode, previousName, previousLat, previousLon, busArray);
						
						if (debug) {
							System.out.println(busStation.toString());
							System.out.println(">>>done<<<");
						}
						
						busStations.add(busStation);
						
						//Reset buses list
						buses = new ArrayList<Bus>();
					} 
					
					Bus bus = new Bus(busId, busName, destination);
					buses.add(bus);
					
					previousName = stationName;
					previousCode = stationCode;
					previousLat = stationLatitude;
					previousLon = stationLongitude;
						
				} else {
					//Buses on the same station
					Bus bus = new Bus(busId, busName, destination);
					buses.add(bus);
					
					if (k == (jsonStringList.size() - 1)) {
						//If it is the last element
						Bus[] busArray = new Bus[buses.size()];
						
						for (int i = 0; i < buses.size(); i++) {
							busArray[i] = buses.get(i);
						}
						
						//Create new bus station
						if (debug) System.out.println(">>>Create new bus station<<<");
						
						BusStation busStation = new BusStation(previousCode, previousName, previousLat, previousLon, busArray);
						
						if (debug) {
							System.out.println(busStation.toString());
							System.out.println(">>>done<<<");
						}

						busStations.add(busStation);
						
						//Reset buses list
						buses = new ArrayList<Bus>();
					}
				}
			}

			if (debug) System.out.println("Bus station size = " + busStations.size());
			BusStation[] busStationsArray = new BusStation[busStations.size()];
			for (int i = 0; i < busStations.size(); i++) {
				busStationsArray[i] = busStations.get(i);
			}
			
			busStationsCollection = new BusStationsCollection(busStationsArray);
          	    
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}

		if (debug) System.out.println("#############");
		
		return busStationsCollection;
	}	
	
	
	public static BusStationsCollection getBusStationsCollection(String allParameters) {
		List<String> jsonStringList = new ArrayList<String>(); //Used to filter out same buses
		String previousName = "";
		String previousCode = "";
		double previousLat = 0.0;
		double previousLon = 0.0;
		List<BusStation> busStations = new ArrayList<BusStation>();
		BusStationsCollection busStationsCollection = null;
		
		try {
			
			String uri = URL_HEAD + allParameters + URL_END_WITH_LINE;
			
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);
			
			if (debug) System.out.println(json);
			
			String[] jsonStringArray = json.split("\n");
			
			// We don't want the first element
			for (int i = 1; i < jsonStringArray.length; i++) {
				
				String infoRow = jsonStringArray[i].trim();
				
				if (!jsonStringList.contains(infoRow)) {
					jsonStringList.add(infoRow);
				}
			}
			
			List<Bus> buses = new ArrayList<Bus>();
			
			for (int k = 0; k < jsonStringList.size(); k++) {
				String jsonString = jsonStringList.get(k);
				//System.out.println(jsonString);
				
				String[] infoArray = jsonString.split(",");
				
				int j = 1;
				
				String stationName = infoArray[j].replaceAll("\"", "");
				
				if (infoArray[j + 1].equals("null")) continue;
				
				if (infoArray[j + 1].replaceAll("\"", "").equals("NONE")) {
					continue;
				}
				
				while(!containsDigit(infoArray[j + 1].replaceAll("\"", ""))) {
					//System.out.println("(j + 1) = " + (j + 1));
					stationName = stationName + "," + infoArray[j + 1].replaceAll("\"", "");
					//System.out.println("station name = " + stationName);
					j++;
				}
				
				String stationCode = infoArray[j + 1].replaceAll("\"", "");
					
				double stationLatitude = Double.parseDouble(infoArray[j + 2]);
				
				double stationLongitude = Double.parseDouble(infoArray[j + 3]);
				
				String busId = infoArray[j + 4].replaceAll("\"", "");
				
				String busName = busId;
									
				String destination = "";
				
				if ((j + 5) == (infoArray.length - 1)) {
					infoArray[j + 5] = infoArray[j + 5].replaceAll("\"", "");
					destination = infoArray[j + 5].replace("]", "");
				} else {
					
					destination = infoArray[j + 5].replaceAll("\"", "");;
					
					for (int a = j + 6; a < infoArray.length; a++) {						
						if (a == (infoArray.length - 1)) {
							infoArray[a] = infoArray[a].replaceAll("\"", "");
							destination = destination + "," + infoArray[a].replace("]", "");
						} else {
							destination = destination + "," + infoArray[a].replaceAll("\"", "");
						}
					}
					
					//System.out.println("destination = " + destination);
				}
				
				if (debug) {
					System.out.println("stationName = " + stationName
							+ " stationCode = " + stationCode
							+ " stationLatitude = " + String.valueOf(stationLatitude)
							+ " stationLongitude = " + String.valueOf(stationLongitude)
							+ " busId " + busId
							+ " busName " + busName
							+ " destination " + destination);
				}
				
				if (!previousCode.equals(stationCode)) {
						
					if (!previousCode.equals("")) {
						//Not in the first iteration, create new bus stations collection
						
						Bus[] busArray = new Bus[buses.size()];
						
						for (int i = 0; i < buses.size(); i++) {
							busArray[i] = buses.get(i);
						}
						
						//Create new bus station
						if (debug) System.out.println(">>>Create new bus station<<<");
						
						BusStation busStation = new BusStation(previousCode, previousName, previousLat, previousLon, busArray);
						
						if (debug) {
							System.out.println(busStation.toString());
							System.out.println(">>>done<<<");
						}
						
						busStations.add(busStation);
						
						//Reset buses list
						buses = new ArrayList<Bus>();
					} 
					
					Bus bus = new Bus(busId, busName, destination);
					buses.add(bus);
					
					previousName = stationName;
					previousCode = stationCode;
					previousLat = stationLatitude;
					previousLon = stationLongitude;
						
				} else {
					//Buses on the same station
					Bus bus = new Bus(busId, busName, destination);
					buses.add(bus);
					
					if (k == (jsonStringList.size() - 1)) {
						//If it is the last element
						Bus[] busArray = new Bus[buses.size()];
						
						for (int i = 0; i < buses.size(); i++) {
							busArray[i] = buses.get(i);
						}
						
						//Create new bus station
						if (debug) System.out.println(">>>Create new bus station<<<");
						
						BusStation busStation = new BusStation(previousCode, previousName, previousLat, previousLon, busArray);
						
						if (debug) {
							System.out.println(busStation.toString());
							System.out.println(">>>done<<<");
						}

						busStations.add(busStation);
						
						//Reset buses list
						buses = new ArrayList<Bus>();
					}
				}
			}

			if (debug) System.out.println("Bus station size = " + busStations.size());
			BusStation[] busStationsArray = new BusStation[busStations.size()];
			for (int i = 0; i < busStations.size(); i++) {
				busStationsArray[i] = busStations.get(i);
			}
			
			busStationsCollection = new BusStationsCollection(busStationsArray);
          	    
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}

		if (debug) System.out.println("#############");
		
		return busStationsCollection;
	}	
	
	public static Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
	    public int compare(Map<String, String> m1, Map<String, String> m2) {
	        return m1.get("Time").compareTo(m2.get("Time"));
	    }
	};
	
	public static boolean containsDigit(String word) {
		
		return (word.contains("0") || word.contains("1") || word.contains("2")
				|| word.contains("3") || word.contains("4") || word.contains("5")
				|| word.contains("6") || word.contains("7") || word.contains("8")
				|| word.contains("9"));
	}
	
	//Create new bus station with known stop code
	public static BusStation getBusStation(String stopCode) {
		
		List<String> jsonStringList = new ArrayList<String>(); //Used to filter out same buses
		BusStation busStation = null;
		
		try {	
			String uri = URL_HEAD_WITH_STOP_CODE + stopCode + URL_END_WITH_STOP_CODE;
			
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, 5000);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);
			
			if (debug) System.out.println(json);
			
			String[] jsonStringArray = json.split("\n");
			
			// We don't want the first element
			for (int i = 1; i < jsonStringArray.length; i++) {
				
				String infoRow = jsonStringArray[i].trim();
				
				if (!jsonStringList.contains(infoRow)) {
					jsonStringList.add(infoRow);
				}
			}
			
			List<Bus> buses = new ArrayList<Bus>();
			String stationName = "";
			double stationLatitude = 0.0;
			double stationLongitude = 0.0;
			String destination = "";
			
			for (int k = 0; k < jsonStringList.size(); k++) {
				String jsonString = jsonStringList.get(k);
				//System.out.println(jsonString);
				
				String[] infoArray = jsonString.split(",");
				
				int j = 1;
				
				stationName = infoArray[j].replaceAll("\"", "");
				
				if (infoArray[j + 1].equals("null")) continue;
				
				if (infoArray[j + 1].replaceAll("\"", "").equals("NONE")) {
					continue;
				}
				
				while(!containsDigit(infoArray[j + 1].replaceAll("\"", ""))) {
					//System.out.println("(j + 1) = " + (j + 1));
					stationName = stationName + ", " + infoArray[j + 1];
					if (DataConstants.DEBUG) System.out.println("station name = " + stationName);
					j++;
				}
					
				stationLatitude = Double.parseDouble(infoArray[j + 1]);
				
				stationLongitude = Double.parseDouble(infoArray[j + 2]);
				
				String busId = infoArray[j + 3].replaceAll("\"", "");
				
				String busName = busId;
									
				if ((j + 4) == (infoArray.length - 1)) {
					destination = infoArray[j + 4].replace("]", "");
				} else {
					
					destination = infoArray[j + 4].replaceAll("\"", "");;
					
					for (int a = j + 5; a < infoArray.length; a++) {						
						if (a == (infoArray.length - 1)) {
							destination = destination + ", " + infoArray[a].replace("]", "");
						} else {
							destination = destination + ", " + infoArray[a].replaceAll("\"", "");
						}
					}
					
					if (DataConstants.DEBUG) System.out.println("destination = " + destination);
				}
				
				if (debug) {
					System.out.println("stationName = " + stationName
							+ " stationCode = " + stopCode
							+ " stationLatitude = " + String.valueOf(stationLatitude)
							+ " stationLongitude = " + String.valueOf(stationLongitude)
							+ " busId " + busId
							+ " busName " + busName
							+ " destination " + destination);
				}
				
					Bus bus = new Bus(busId, busName, destination);
					buses.add(bus);
			}
			
			Bus[] busArray = new Bus[buses.size()];
			
			for (int i = 0; i < buses.size(); i++) {
				busArray[i] = buses.get(i);
			}
			
			busStation = new BusStation(stopCode, stationName, stationLatitude, stationLongitude, busArray);
          	    
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return busStation;
	}	
}
