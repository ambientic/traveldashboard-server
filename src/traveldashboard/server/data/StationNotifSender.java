package traveldashboard.server.data;

import org.ibicoop.broker.common.BrokerConstants;
import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopLoader;

public class StationNotifSender extends Thread{
	private static final String TAG = "StationNotifSender";
	
	private String stationId;
	private String level;

	private StationNotifSender(String stationId, String level){
		this.stationId = stationId;
		this.level = level;
	}
	
	public static void send(String stationId, String level){
		new StationNotifSender(stationId, level).start();
	}
	
	@Override
	public void run(){
		String methodTag = "run";
		
		BrokerManager brokMng = IbicoopLoader.load().getBrokerManager(
				DataConstants.BROKER_NAME);
		IbiTopicEvent event = brokMng.createIbiEvent("vote_response_event", level, "null".getBytes());
		IbiTopic[] topics = getStopTopic();
		
		if(topics != null) {
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "topics length = " + topics.length);
			
			for (int i = 0; i < topics.length; i++) {
				int success = topics[i].publishEvent(event);
				if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Sent notification to topic " + topics[i].getName());
				//System.out.format("Sent notification to %s with level %s", topics[i].getName(), level);
				if (success == BrokerConstants.OK) {
					if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Sent notification to topic " + topics[i].getName() + " success");	
				} else {
					if (DataConstants.DEBUG) TdServerLogger.printError(TAG, methodTag, "Sent notification to topic " + topics[i].getName() + " failed");	
				}
			}
			
		} else {
			TdServerLogger.printError(TAG, methodTag, "topics = null");
		}
	}
	
	public IbiTopic[] getStopTopic() {
		String methodTag = "getStopTopic";
		
		BrokerManager brokMng = IbicoopLoader.load().getBrokerManager(
				DataConstants.BROKER_NAME);
		
		IbiTopicFilter topicFilter = brokMng.createIbiTopicFilter();
		
		topicFilter.setName("StationCh_" + this.stationId);
		
		IbiTopic[] topics = brokMng.getTopics(topicFilter);

		if (topics == null || topics.length == 0) {
			TdServerLogger.printError(TAG, methodTag, "topics = null or topics length = 0");				
			return null;
		} else {
			return topics;
		}
	}
}
