package traveldashboard.server.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.info.BusRealTimeInfo;
import traveldashboard.data.info.BusRealTimeInfosCollection;

public class BusCountdownResponse {
	
	//http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?stopcode1=71636&StopPointState=0&ReturnList=DestinationText,LineName,EstimatedTime
	private static final String URL_HEAD = "http://countdown.api.tfl.gov.uk/interfaces/ura/instant_V1?stopcode1=";
	private static final String URL_MIDDLE = "&LineName=";	
	private static final String URL_END = "&StopPointState=0&ReturnList=DestinationText,LineName,EstimatedTime";
	
	/**
	 * Get London bus countdown
	 * @param stopId
	 * @param maxValue
	 * @return
	 */
	public static BusRealTimeInfosCollection getBusRealTimeInfosCollection(String stopId, int maxValue) {
		
		BusRealTimeInfosCollection busRTInfosCollection = null;
		BusStation busStation = MongoDbManager.getLondonBusStation(stopId);
		if (DataConstants.DEBUG) System.out.println("Bus station : " + busStation.toString());
		List<BusRealTimeInfo> busRTInfos = new ArrayList<BusRealTimeInfo>();
		
		try {
			String uri = URL_HEAD + stopId + URL_END;
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);
			
			String[] jsonStringArray = json.split("\n");
			
			List<Map<String, String>> list = new ArrayList<Map<String, String>>();
			
			long currentMillisecond = System.currentTimeMillis();
			
			// We don't want the first element
			for (int i = 1; i < jsonStringArray.length; i++) {
				String infoRow = jsonStringArray[i].trim();
				String[] infoArray = infoRow.split(",");
			
				HashMap<String, String> temp = new HashMap<String, String>();
				temp.put("Route", infoArray[1].replaceAll("\"", ""));
				temp.put("Destination", infoArray[2].replaceAll("\"", ""));
				infoArray[3] = infoArray[3].replace("]", "");
				temp.put("Time", infoArray[3]);
				list.add(temp);
			}
			
			Collections.sort(list, mapComparator);
			
			for (Map<String, String> map : list) {
				//Convert to minutes
				long millis = Long.parseLong(map.get("Time"));
				long remainMiliSecond = millis - currentMillisecond;			
				if (remainMiliSecond < 0) remainMiliSecond = 0;
				String timeString = "";
				if (TimeUnit.MILLISECONDS.toMinutes(remainMiliSecond) == 0) timeString = "due";
				else timeString = TimeUnit.MILLISECONDS.toMinutes(remainMiliSecond) + " min";
				map.put("Time", timeString);
			}
			
			for (Map<String, String> map : list) {
				JSONObject tempJSONObject = new JSONObject();
				tempJSONObject.put("Route", map.get("Route"));
				tempJSONObject.put("Destination", map.get("Destination"));
				tempJSONObject.put("Time", map.get("Time"));
				Bus bus = new Bus(map.get("Route"), map.get("Route"), map.get("Destination"));
				BusRealTimeInfo busRTInfo = new BusRealTimeInfo(bus, busStation, map.get("Time"));
				busRTInfos.add(busRTInfo);
			}
			
			int iteration = maxValue;
			
			if (busRTInfos.size() < iteration) {
				iteration = busRTInfos.size();
			}
			
			BusRealTimeInfo[] busRTArray = new BusRealTimeInfo[iteration];

			for (int i = 0; i < busRTArray.length; i++) {
				busRTArray[i] = busRTInfos.get(i);
			}
			
			busRTInfosCollection = new BusRealTimeInfosCollection(busRTArray);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return busRTInfosCollection;
	}
	
	
	/**
	 * Get London bus countdown
	 * @param stopId
	 * @param routeId
	 * @param maxValue
	 * @return
	 */
	public static BusRealTimeInfosCollection getBusRealTimeInfosCollection(String stopId, String routeId, int maxValue) {
		
		BusRealTimeInfosCollection busRTInfosCollection = null;
		BusStation busStation = MongoDbManager.getLondonBusStation(stopId);
		
		String routeName = MongoDbManager.getLondonBusRouteName(routeId);
		if (DataConstants.DEBUG) System.out.println("Bus station : " + busStation.toString());
		List<BusRealTimeInfo> busRTInfos = new ArrayList<BusRealTimeInfo>();
		
		try {
			String uri = URL_HEAD + stopId + URL_MIDDLE + routeName + URL_END;
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);
			
			String[] jsonStringArray = json.split("\n");
			
			List<Map<String, String>> list = new ArrayList<Map<String, String>>();
			
			long currentMillisecond = System.currentTimeMillis();
			
			// We don't want the first element
			for (int i = 1; i < jsonStringArray.length; i++) {
				String infoRow = jsonStringArray[i].trim();
				String[] infoArray = infoRow.split(",");
			
				HashMap<String, String> temp = new HashMap<String, String>();
				temp.put("Route", infoArray[1].replaceAll("\"", ""));
				temp.put("Destination", infoArray[2].replaceAll("\"", ""));
				infoArray[3] = infoArray[3].replace("]", "");
				temp.put("Time", infoArray[3]);
				list.add(temp);
			}
			
			Collections.sort(list, mapComparator);
			
			for (Map<String, String> map : list) {
				//Convert to minutes
				long millis = Long.parseLong(map.get("Time"));
				long remainMiliSecond = millis - currentMillisecond;			
				if (remainMiliSecond < 0) remainMiliSecond = 0;
				String timeString = "";
				if (TimeUnit.MILLISECONDS.toMinutes(remainMiliSecond) == 0) timeString = "due";
				else timeString = TimeUnit.MILLISECONDS.toMinutes(remainMiliSecond) + " min";
				map.put("Time", timeString);
			}
			
			for (Map<String, String> map : list) {
				JSONObject tempJSONObject = new JSONObject();
				tempJSONObject.put("Route", map.get("Route"));
				tempJSONObject.put("Destination", map.get("Destination"));
				tempJSONObject.put("Time", map.get("Time"));
				Bus bus = new Bus(map.get("Route"), map.get("Route"), map.get("Destination"));
				BusRealTimeInfo busRTInfo = new BusRealTimeInfo(bus, busStation, map.get("Time"));
				busRTInfos.add(busRTInfo);
			}
			
			int iteration = maxValue;
			
			if (busRTInfos.size() < iteration) {
				iteration = busRTInfos.size();
			}
			
			BusRealTimeInfo[] busRTArray = new BusRealTimeInfo[iteration];

			for (int i = 0; i < busRTArray.length; i++) {
				busRTArray[i] = busRTInfos.get(i);
			}
			
			busRTInfosCollection = new BusRealTimeInfosCollection(busRTArray);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return busRTInfosCollection;
	}
	
	
	
	public static Comparator<Map<String, String>> mapComparator = new Comparator<Map<String, String>>() {
	    public int compare(Map<String, String> m1, Map<String, String> m2) {
	        return m1.get("Time").compareTo(m2.get("Time"));
	    }
	};
	
}
