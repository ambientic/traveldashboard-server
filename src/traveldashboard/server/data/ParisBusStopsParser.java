package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import au.com.bytecode.opencsv.CSVWriter;

public class ParisBusStopsParser {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("parisBusStop.xml")));
			
			StringBuilder sb = new StringBuilder();
			
			String line;
			
			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
			
			br.close();
			
			
			CSVWriter writer = new CSVWriter(
        			new FileWriter("parisBusStopCsv.csv"),
        			';',
        			CSVWriter.NO_QUOTE_CHARACTER);

		    // Convert XML to JSON Object
            JSONObject xmlJSONObj = XML.toJSONObject(sb.toString());
            
            //System.out.print(xmlJSONObj.toString(4));
            JSONObject networkMessageObj = xmlJSONObj.getJSONObject("NetworkMessage");
            
            JSONArray msgElements = networkMessageObj.getJSONArray("msgElement");
            
            //System.out.print(msgElements.toString(4));
            
            //Iterate each message element
            int msgElementsSize = msgElements.length();
            
            for (int i = 0; i < msgElementsSize; i = i + 7) {
            	//Each element contains 7 elements
            	//Id
            	JSONObject stopId_obj = msgElements.getJSONObject(i);
            	int stopId = stopId_obj.getInt("key");
            	
            	//Name
            	JSONObject stopName_obj = msgElements.getJSONObject(i + 1);
            	String stopName = stopName_obj.getString("content");
            	
            	//Lon
            	JSONObject stopLon_obj = msgElements.getJSONObject(i + 2);
            	double stopLon = stopLon_obj.getDouble("content");          	
            	
            	//Lat
            	JSONObject stopLat_obj = msgElements.getJSONObject(i + 3);
            	double stopLat = stopLat_obj.getDouble("content");            	
            	
            	//RouteId
            	JSONObject routeId_obj = msgElements.getJSONObject(i + 4);
            	int routeId = routeId_obj.getInt("content");
            	
            	//RouteName
            	JSONObject routeName_obj = msgElements.getJSONObject(i + 5);
            	String routeName = routeName_obj.getString("content");           	
            	
            	//Direction
            	JSONObject routeDirection_obj = msgElements.getJSONObject(i + 6);
            	int routeDirection = routeDirection_obj.getInt("content");
            	
            	System.out.println(""
            			+ "stopId = " + stopId + ", "
            			+ "stopName = " + stopName + ", "
            			+ "stopLon = " + stopLon + ", "
            			+ "stopLat = " + stopLat + ", "
            			+ "routeId = " + routeId + ", "
            			+ "routeName = " + routeName + ", "
            			+ "routeDirection = " + routeDirection
            			);
            	
		    	String[] newData = {
		    			"" + stopId,
		    			"" + stopName,
		    			"" + stopLon,
		    			"" + stopLat,
		    			"" + routeId,
		    			"" + routeName,
		    			"" + routeDirection
		    	};
		    	
		    	writer.writeNext(newData);
            }
            
            writer.close();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

}
