package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import traveldashboard.data.TubeStation;
import traveldashboard.data.incentive.Incentive;
import traveldashboard.data.incentive.IncentiveType;


import com.google.gson.Gson;

public class IncentiveGetterUtils extends ResponseUtils {
	
	private static final String TAG = "IncentiveGetterUtils";
	
	public static NetworkMessage getIncentiveMessage(NetworkMessage req) {

		String methodTag = "getIncentiveMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Getting incentive message...");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_VALUE_STOP_ID);
		String incentiveType =  req.getPayload(DataConstants.PARAM_INCENTIVE_TYPE);	
		
		Gson gson = new Gson();

		String json = "null";
		
		if (incentiveType.equals(IncentiveType.FACT.toString())) {
			
			//Fact
			if (city.equals(DataConstants.LONDON)) {
				//Get london tube station fact
				json = gson.toJson(MongoDbManager.getLondonTubeStationFact(stopId));
			} else {
				//Paris fact not available, just show thank you message
				Incentive parisFact = new Incentive(stopId,
						MongoDbManager.getParisMetroStationName(stopId),
						IncentiveType.FACT, 
						"Thank you for reporting!");
				
				json = gson.toJson(parisFact);
			}
			
		} else if (incentiveType.equals(IncentiveType.JOKE.toString())) {
			//Joke not available
			
			//Joke
			if (city.equals(DataConstants.LONDON)) {
				//Get real time joke
				TubeStation station = MongoDbManager.getLondonMetroStation(stopId);
				String stationName = station.getName();
				
				//Split station name
				String[] nameArray = stationName.split(" ");
				
				String firstName = nameArray[0];
				
				String jsonString = JokeSearcherForTubeStation.getJokeJsonContent(firstName);
				
				String stationDesc = JokeSearcherForTubeStation.processJsonContent(jsonString);
				
				if ((stationDesc == null) || (stationDesc.equals(""))) {
					//Get london tube station static joke
					json = gson.toJson(MongoDbManager.getLondonTubeStationJoke(stopId));
				} else {
					//Give real time joke
					Incentive londonJoke = new Incentive(stopId,
							stationName,
							IncentiveType.JOKE, 
							stationDesc);
					json = gson.toJson(londonJoke);
				}
				
			} else {
				//Paris joke not available, just show thank you message
				Incentive parisJoke = new Incentive(stopId,
						MongoDbManager.getParisMetroStationName(stopId),
						IncentiveType.JOKE, 
						"Thank you for reporting!");
				
				json = gson.toJson(parisJoke);		
			}
		}
		
		resp.addPayload(DataConstants.INCENTIVE_MESSAGE, json);
		
		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "End get incentive message : " + new String(resp.encode()));
        
		return resp;
	}
}

