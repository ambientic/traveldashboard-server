package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;
import traveldashboard.data.comment.TubeStationComment;
import traveldashboard.data.comment.TubeStationCommentsCollection;
import traveldashboard.data.status.TubeStatus;
import traveldashboard.data.status.TubeStatusCollection;


/**
 * Get twitter response
 * @author khoo
 *
 */
public class TwitterResponse {
	
	private static final String TAG = "TwitterResponse";
	
	private static final String CONSUMER_KEY = "PqHLKgT9jYsIpmaUBrZH2w";
	private static final String CONSUMER_SECRET = "50OO60kdd5sqcN1Rl6cZxB1XMtsZp0CcFl7GLFqNIEw";
	private static final String APPLICATION = "TravelDashboard";
	
	private static final String TWITTER_HOST = "api.twitter.com";
	private static final String TWITTER_AUTH_URL = "https://api.twitter.com/oauth2/token";
	//private static final String RADIUS = "3";//km
	
	// Reference : http://www.dreamincode.net/forums/blog/114/entry-4459-demo-of-twitter-application-only-oauth-authentication-using-java/
	// Encodes the consumer key and secret to create the basic authorization key
	private static String encodeKeys(String consumerKey, String consumerSecret) {
	    try {
	        String encodedConsumerKey = URLEncoder.encode(consumerKey, "UTF-8");
	        String encodedConsumerSecret = URLEncoder.encode(consumerSecret, "UTF-8");
	        String fullKey = encodedConsumerKey + ":" + encodedConsumerSecret;
	        byte[] encodedBytes = Base64.encodeBase64(fullKey.getBytes());
	        return new String(encodedBytes); 
	    }
	    catch (UnsupportedEncodingException e) {
	        return new String();
	    }
	}

	// Reference : http://www.dreamincode.net/forums/blog/114/entry-4459-demo-of-twitter-application-only-oauth-authentication-using-java/
	// Constructs the request for requesting a bearer token and returns that token as a string
	private static String requestBearerToken(String endPointUrl, String application) throws IOException, JSONException {
	    HttpsURLConnection connection = null;
	    String encodedCredentials = encodeKeys(CONSUMER_KEY, CONSUMER_SECRET);
	    String bearerToken = "";
	    try {	    	
	        URL url = new URL(endPointUrl);
	        connection = (HttpsURLConnection) url.openConnection();          
	        connection.setDoOutput(true);
	        connection.setDoInput(true);
	        connection.setRequestMethod("POST");
	        connection.setRequestProperty("Host", TWITTER_HOST);
	        connection.setRequestProperty("User-Agent", application);
	        connection.setRequestProperty("Authorization", "Basic " + encodedCredentials);
	        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");
	        connection.setRequestProperty("Content-Length", "29");
	        connection.setUseCaches(false);
	        writeRequest(connection, "grant_type=client_credentials");
	        // Parse the JSON response into a JSON mapped object to fetch fields from.
	        //JSONObject obj = (JSONObject)JSONValue.parse(readResponse(connection));
	        JSONObject obj = new JSONObject(readResponse(connection));
	        
	        if (obj != null) {
	            String tokenType = (String)obj.get("token_type");
	            String token = (String)obj.get("access_token");
	            bearerToken =  ((tokenType.equals("bearer")) && (token != null)) ? token : "";
	        }
	    }
	    catch (MalformedURLException e) {
	        throw new IOException("Invalid endpoint URL specified.", e);
	    }
	    finally {
	        if (connection != null) {
	            connection.disconnect();
	        }
	    }
	    
	    return bearerToken;
	}

	// Reference : http://www.dreamincode.net/forums/blog/114/entry-4459-demo-of-twitter-application-only-oauth-authentication-using-java/
	// Fetches the first tweet from a given user's timeline
	/**
	 * Get tube station twitter comments
	 * @param city
	 * @param stopId
	 * @param maxComments
	 * @return
	 */
	public static TubeStationCommentsCollection getTubeStationTwitterComments(String city, String stopId, int maxComments) {
		
	    HttpsURLConnection connection = null;
	    TubeStationCommentsCollection tubeStationCommentsCollection = null;
	    TubeStationComment[] commentsArray = null;
	    List<TubeStationComment> commentsList = new ArrayList<TubeStationComment>();
	    TubeStation tubeStation = null;
	    String stationName = "";
	    
	    try {	    	
	    	
	    	if (city.equals(DataConstants.PARIS)) {
	    		tubeStation = MongoDbManager.getParisMetroStation(stopId);
	    	} else {
			    tubeStation = MongoDbManager.getLondonMetroStation(stopId);		
	    	}

	    	stationName = tubeStation.getName();
	    	
	    	//Optimal search in London : "stationName" + " station"
	    	String stationNameEncoded = URLEncoder.encode(stationName + " station", "UTF-8");

	    	//String latitude = "" + tubeStation.getLatitude();
	    	//String longitude = "" + tubeStation.getLongitude();
	    	
	    	//URL url = new URL("https://api.twitter.com/1.1/search/tweets.json?q=" + stationNameEncoded + "&count=5&include_entities=false&result_type=mixed&geocode=" + latitude + "," + longitude + "," + RADIUS + "km");
	    	URL url = new URL("https://api.twitter.com/1.1/search/tweets.json?q=" + stationNameEncoded + "&count=5&include_entities=false&result_type=mixed");
	    	
	    	connection = (HttpsURLConnection) url.openConnection();          
	        connection.setDoOutput(true);
	        connection.setDoInput(true);
	        connection.setRequestMethod("GET");
	        connection.setRequestProperty("Host", TWITTER_HOST);
	        connection.setRequestProperty("User-Agent", APPLICATION);
	        connection.setRequestProperty("Authorization", "Bearer " + requestBearerToken(TWITTER_AUTH_URL, APPLICATION));
	        connection.setUseCaches(false);
	        String response = readResponse(connection);
	        JSONObject statuses = new JSONObject(response);
	        JSONArray statusesArray = statuses.getJSONArray("statuses");
	        
			int numberIteration = maxComments;
			if (numberIteration > statusesArray.length()) {
				numberIteration = statusesArray.length();
			}
	        
			int startIndex = statusesArray.length() - numberIteration;
			
	        for (int i = startIndex; i < statusesArray.length(); i++) {
	        	//Get latest information
	        	JSONObject obj = statusesArray.getJSONObject(i);
	        	
	        	String comment = obj.getString("text");
	        	//Example time: Thu Oct 10 10:22:49 +0000 2013
	        	String time  = obj.getString("created_at");
	        	
	        	String format = "E MMM dd HH:mm:ss ZZZZ yyyy";
	        	SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.UK);
	        	Date date = sdf.parse(time);
	        	
	        	long timestamp = date.getTime();
	        	
				String timeZone = "";
				
				if (city.equals(DataConstants.PARIS)) {
					timeZone = "Europe/Paris";
					
				} else if (city.equals(DataConstants.LONDON)) {
					timeZone = "Europe/London";
				}

				//Set to local timestamp
				timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
		    	TimeZone.getTimeZone(timeZone).getRawOffset();
	        	
//	        	String user = obj.getString("id");
				String user = obj.get("id").toString();
	        	
	        	JSONObject userProfil = obj.getJSONObject("user");
	        	
	        	if (userProfil != null) {
	        		
	        		String userName = userProfil.getString("screen_name");
	        		
	        		if (userName != null) {
	        			user = userName;
	        		}
	        	}
	        	
	        	TubeStationComment stationComment = new TubeStationComment(user, comment, timestamp, tubeStation);
	        	commentsList.add(stationComment);
	        } 
	    }
	    catch (Exception e) {
	        e.printStackTrace();
	    }
	    finally {
	        if (connection != null) {
	            connection.disconnect();
	        }
	    }
	    
	    commentsArray = new TubeStationComment[commentsList.size()];
	    
	    for (int i = 0; i < commentsList.size(); i++) {
	    	commentsArray[i] = commentsList.get(i);
	    }
	    
	    tubeStationCommentsCollection = new TubeStationCommentsCollection(commentsArray);
	    
	    return tubeStationCommentsCollection;
	}
	
	/**
	 * Get paris metro status collection
	 * @return Paris metro status collection
	 */
	public static TubeStatusCollection getParisMetroStatusCollection() {
		
		TubeStatusCollection tubeStatusCollection = null;
		TubeStatus[] statusArray = null;
		List<TubeStatus> statusList = new ArrayList<TubeStatus>();
		
		for (String routeName : DataConstants.PARIS_METRO_ROUTE_NAMES) {
			try {
				statusList.add(getParisMetroStatus(routeName));
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
		
		statusArray = new TubeStatus[statusList.size()];
		
		for (int i = 0; i < statusList.size(); i++) {
			statusArray[i] = statusList.get(i);
		}
		
		tubeStatusCollection = new TubeStatusCollection(statusArray);
		
		return tubeStatusCollection;
	}
	
	
	// Reference : http://www.dreamincode.net/forums/blog/114/entry-4459-demo-of-twitter-application-only-oauth-authentication-using-java/
	// Fetches the first tweet from a given user's timeline
	// Get paris tube status
	/**
	 * Get Paris metro status
	 * @param routeId
	 * @return Paris tube status
	 * @throws IOException
	 * @throws JSONException
	 */
	public static TubeStatus getParisMetroStatus(String routeId) throws IOException, JSONException {
		
		String methodTag = "getParisMetroStatus";
		
        //TdServerLogger.print(TAG, methodTag, "Getting paris metro status for route id = " + routeId);		
		
	    HttpsURLConnection connection = null;
	    TubeStatus tubeStatus = null;

		//PARIS
		Tube tube = MongoDbManager.getTube(DataConstants.PARIS, routeId);
		String routeName = tube.getName();

	    try {
			String searchTerm = URLEncoder.encode(DataConstants.PARIS_METRO_TWITTER_SEARCH_TERM.get(routeName), "UTF-8");
			URL url = new URL("https://api.twitter.com/1.1/search/tweets.json?q=" + searchTerm + "&count=5&include_entities=false&result_type=mixed");
			connection = (HttpsURLConnection) url.openConnection();          
	        connection.setDoOutput(true);
	        connection.setDoInput(true);
	        connection.setRequestMethod("GET");
	        connection.setRequestProperty("Host", TWITTER_HOST);
	        connection.setRequestProperty("User-Agent", APPLICATION);
	        connection.setRequestProperty("Authorization", "Bearer " + requestBearerToken(TWITTER_AUTH_URL, APPLICATION));
	        connection.setUseCaches(false);
	        String response = readResponse(connection);
	        
	        JSONObject statuses = new JSONObject(response);
	        JSONArray statusesArray = statuses.getJSONArray("statuses");
	        
	        //We want the latest info	        
	        if (statusesArray.length() > 0) {
	        	
	        	for (int i = 0; i < statusesArray.length(); i++) {
	        		
		        	JSONObject obj = statusesArray.getJSONObject(i);
		        	
		        	if (obj.getJSONObject("user").has("name") && 
		        			DataConstants.PARIS_METRO_TWITTER_SEARCH_USERS.contains(obj.getJSONObject("user").getString("name"))) {
			        	String comment = obj.getString("text");
			        	String time  = obj.getString("created_at").substring(0, 19);
			        	String user = obj.getJSONObject("user").getString("name");
			        	//System.out.println("comment = " + comment);
			        	tubeStatus = new TubeStatus(tube, user + " - " + time, comment);
			        	break;
		        	}
	        	}
	        	
	        }
	        	if (tubeStatus == null) {
	        		tubeStatus = new TubeStatus(tube, "No status", "No status");
	        	}
	        
	    }
	    catch (MalformedURLException e) {
	        throw new IOException("Invalid endpoint URL specified.", e);
	    }
	    finally {
	        if (connection != null) {
	            connection.disconnect();
	        }
	    }

	    return tubeStatus;
	}
			
	// Reference : http://www.dreamincode.net/forums/blog/114/entry-4459-demo-of-twitter-application-only-oauth-authentication-using-java/
	// Writes a request to a connection
	private static boolean writeRequest(HttpsURLConnection connection, String textBody) {
	    try {
	        BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
	        wr.write(textBody);
	        wr.flush();
	        wr.close();
	        return true;
	    }
	    catch (IOException e) { return false; }
	}
	
	// Reference : http://www.dreamincode.net/forums/blog/114/entry-4459-demo-of-twitter-application-only-oauth-authentication-using-java/
	// Reads a response for a given connection and returns it as a string.
	private static String readResponse(HttpsURLConnection connection) {

	    try {
	        StringBuilder str = new StringBuilder();
	        BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	        String line = "";
	        while((line = br.readLine()) != null) {
	            str.append(line + System.getProperty("line.separator"));
	        }
	        return str.toString();
	    }
	    catch (IOException e) { return new String(); }
	}
}
