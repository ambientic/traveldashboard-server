package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.Tram;
import traveldashboard.data.TramStation;
import traveldashboard.data.Tube;
import traveldashboard.data.TubePlatform;
import traveldashboard.data.TubeStation;
import traveldashboard.data.info.BusRealTimeInfo;
import traveldashboard.data.info.BusRealTimeInfosCollection;
import traveldashboard.data.info.TramRealTimeInfo;
import traveldashboard.data.info.TramRealTimeInfosCollection;
import traveldashboard.data.info.TubeRealTimeInfo;
import traveldashboard.data.info.TubeRealTimeInfosCollection;



public class RatpTimeTableProcess {
	
	public static final boolean DEBUG = DataConstants.DEBUG;
	private static final String TAG = "RatpTimeTableProcess";
	
	private final static String USER_AGENT = "Mozilla/5.0";
	
	/**
	 * Sort timetable
	 */
	public static Comparator<String> listComparator = new Comparator<String>() {
	    public int compare(String m1, String m2) {
	    	String[] m1array = m1.split("#");
	    	String[] m2array = m2.split("#");
	    	
	    	if ((m1array.length  < 2) || (m2array.length < 2)) return m1.compareTo(m2);
	    	try {
	    		//Catch in case we can't parse int
		    	Integer time1 = Integer.parseInt(m1array[1].replace(" ", "").replace("min", "").replace("Due", "0"));
		    	Integer time2 = Integer.parseInt(m2array[1].replace(" ", "").replace("min", "").replace("Due", "0"));
		    	//System.out.println("time1 = " + time1 + ", time2 = " + time2);
		        return time1.compareTo(time2);
	    	} catch (Exception e) {
	    		e.printStackTrace();
	    	}
	    	
	    	return m1.compareTo(m2);
	    }
	};
	

	/**
	 * Get Ratp bus timetable
	 * @param routeName
	 * @param stopName
	 * @return
	 */
	public static List<String> getBusTimetable(String routeName, String stopName) {
		
		List<String> passageInfo = new ArrayList<String>();
		
		try {

	        //String url = "http://wap.ratp.fr/siv/schedule?service=next&reseau=bus&referer=station&lineid=B258&stationname=la+source";
			String url = "http://wap.ratp.fr/siv/schedule?service=next&reseau=bus&referer=station&lineid=B" + routeName + "&stationname=" + URLEncoder.encode(stopName, "UTF-8");
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	 
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			if (DEBUG) {
				System.out.println("\nSending 'POST' request to URL : " + url);
				System.out.println("Response Code : " + responseCode);
				}
	 
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			String stringHTML = response.toString();
			
			Document doc = Jsoup.parse(stringHTML);
			
			Elements elementsDirection1 = doc.getElementsByClass("bg1");
			Elements elementsTime1 = doc.getElementsByClass("schmsg1");
			Elements elementsDirection2 = doc.getElementsByClass("bg3");
			Elements elementsTime2 = doc.getElementsByClass("schmsg3");
			
			if ((elementsDirection1.size() != elementsTime1.size()) || (elementsDirection2.size() != elementsTime2.size())) 
				return passageInfo;
			
			for (int i = 0; i < elementsDirection1.size(); i++) {
				String direction = elementsDirection1.get(i).toString();
				direction = direction.replace(direction.substring(0, 30),"");
				direction = direction.replace("\n", "");
				direction = direction.replace("</div>", "");
				String time = elementsTime1.get(i).toString();
				time = time.replace(time.substring(0, 26),"");
				time = time.replace("</b>","");
				time = time.replace("</div>","");
				time = time.replace("\n","");		
				time = time.replace("mn", "min");
				time = time.replace("A l'arret", "Due");
				
				String passageString = direction + "#" + time;
				passageInfo.add(passageString);
				
				direction = elementsDirection2.get(i).toString();
				direction = direction.replace(direction.substring(0, 30),"");
				direction = direction.replace("\n", "");
				direction = direction.replace("</div>", "");
				time = elementsTime2.get(i).toString();
				time = time.replace(time.substring(0, 26),"");
				time = time.replace("</b>","");
				time = time.replace("</div>","");
				time = time.replace("\n","");
				time = time.replace("mn", "min");
				passageString = direction + "#" + time;
				passageInfo.add(passageString);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (DEBUG) {
			for (String string: passageInfo) {
				System.out.println(string);
			}				
		}
		
		//Sort the collection
		Collections.sort(passageInfo, listComparator);
		
		return passageInfo;
	}
		

	/**
	 * Get metro or tram timetable
	 * @param metroOrTram
	 * @param lineId
	 * @param sens
	 * @param stationName
	 * @param nbValues
	 * @return
	 */
	public static List<String> getMetroOrTramTimetable(String metroOrTram, String lineId, String sens, String stationName, int nbValues) {
		
		//String passage info styple = "terminusName#pasageTime";
		List<String> passageInfo = new ArrayList<String>();
		
		try {
			
			//Metro example : http://www.ratp.fr/horaires/fr/ratp/metro/prochains_passages/PP/La+D�fense/1/A
			//Tram example : http://www.ratp.fr/horaires/fr/ratp/tramway/prochains_passages/PP/T1/auguste+delaune/A
			
			String uri = "";
			
			if (metroOrTram.equals(DataConstants.RESEAU_METRO)) {
				//If it is metro
				uri = DataConstants.RATP_TIMETABLE_URL1_RESEAU + metroOrTram
				+ DataConstants.METRO_TIMETABLE_URL2_STATION_NAME + URLEncoder.encode(stationName, "UTF-8")
				+ DataConstants.METRO_TIMETABLE_URL3_LINEID  +  lineId
				+ DataConstants.RER_TIMETABLE_URL4_SENS + sens;
			} else {
				//If it is tramway
				uri = DataConstants.RATP_TIMETABLE_URL1_RESEAU + metroOrTram
				+ DataConstants.METRO_TIMETABLE_URL2_STATION_NAME + lineId
				+ DataConstants.METRO_TIMETABLE_URL3_LINEID  +  URLEncoder.encode(stationName, "UTF-8")
				+ DataConstants.RER_TIMETABLE_URL4_SENS + sens;
			}

			
			if (DEBUG) System.out.println("Url = " + uri);
			
			
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			// Get HTML
			String htmlString = httpClient.execute(httpGet, responseHandler);
			
			//if (debug) System.out.println(htmlString);
			
			
			Document doc = Jsoup.parse(htmlString);
			Element content = doc.getElementById("prochains_passages");
			Element table = content.select("table").first();
			Elements infos = table.select("td");
			
			String passageString = "";
			
			int nbIteration = nbValues*2;//One value needs two iterations
			
			if (nbIteration > infos.size()) nbIteration = infos.size();
			
			for (int i = 0; i < nbIteration; i++) {
				if (DEBUG) System.out.println("####infos###");
				Element info = infos.get(i);
				String infoString = info.toString();
				infoString = infoString.replace("<td>", "");
				infoString = infoString.replace("</td>", "");
				infoString = StringEscapeUtils.unescapeHtml4(infoString);
				if (DEBUG) System.out.println(infoString);
				
				if ((i == 0) || ((i % 2) == 0)) {
					//This is the destination string
					passageString = infoString;
				} else {
					//This is the time string
					if (infoString.equalsIgnoreCase("0 mn") || infoString.contains("Train")  || infoString.contains("approche")  || infoString.contains("quai")) {
						infoString = "Due";
					}
					
					infoString = infoString.replace("mn", "min");
					
					passageString = passageString + "#" + infoString;
					passageInfo.add(passageString);
				}
			}
			
			if (DEBUG) {
				for (int j = 0; j < passageInfo.size(); j++) {
					 System.out.println("Passage info = " + passageInfo.get(j));
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return passageInfo;
	}
	
	/**
	 * Get RER time table
	 * @param lineId RouteId without "R"
	 * @param sens
	 * @param stationName
	 * @param nbValues
	 * @return
	 */
	public static List<String> getRerTimetable(String lineId, String sens, String stationName, int nbValues) {
		
		//String passage info styple = "terminusName#pasageTime";
		List<String> passageInfo = new ArrayList<String>();
		
		try {
			//Example : http://www.ratp.fr/horaires/fr/ratp/rer/prochains_passages/RB/Chatelet/A
			String URL = DataConstants.RATP_TIMETABLE_URL1_RESEAU + DataConstants.RESEAU_RER
					+ DataConstants.RER_TIMETABLE_URL2_LINEID + "R" + lineId
					+ DataConstants.RER_TIMETABLE_URL3_STATION_NAME  + URLEncoder.encode(stationName, "UTF-8") 
					+ DataConstants.RER_TIMETABLE_URL4_SENS + sens;
			
			if (DEBUG) System.out.println("Url = " + URL);
			
			
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(URL);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			// Get HTML
			String htmlString = httpClient.execute(httpGet, responseHandler);

			Document doc = Jsoup.parse(htmlString);
			
			Element content = doc.getElementById("prochains_passages");
			Elements elementsDirection = content.getElementsByClass("terminus");
			Elements elementsTime = content.getElementsByClass("passing_time");
			
			int nbIteration = nbValues + 1; //We don't want the first element
			
			if (nbIteration > elementsDirection.size()) nbIteration = elementsDirection.size();
			
			for (int i = 1; i < nbIteration; i++) {
				Element direction = elementsDirection.get(i);
				String directionCellString = direction.toString();
				String terminusIncomplete = directionCellString.replace("<td class=\"terminus\">", "");
				String terminus = terminusIncomplete.replace("</td>", "");
				terminus = StringEscapeUtils.unescapeHtml4(terminus);
				
				Element time = elementsTime.get(i);
				String timeCellString = time.toString();
				String timeIncomplete = timeCellString.replace("<td class=\"passing_time\">", "");
				String passageTime = timeIncomplete.replace("</td>", "");
			
				String[] testTime = passageTime.split(":");
				if (testTime.length < 2) {
					
					if (passageTime.contains("terminus")) {
						//For "Train terminus"
						passageTime = "Terminus";
					} else if (passageTime.contains("sans")) {
						//For "Trains sans arr�t"
						passageTime = "Non-stop";
					} else {
						passageTime = "Due";
					}
				}
				else {
					//Process into minute
					//Delete detail after time such "D�part voie 1..."
					if (DEBUG) System.out.println("Only time = " + passageTime.substring(0, 5));
					passageTime =  passageTime.substring(0, 5);
					String formatDate = "yyyy-MM-dd'T'";
					SimpleDateFormat sdfDate = new SimpleDateFormat(formatDate, Locale.FRANCE);
					String date = sdfDate.format(new Date());
					String passageDate = date + passageTime;
					String passageDateFormatDate = "yyyy-MM-dd'T'HH:mm";
					SimpleDateFormat passageSdfDate = new SimpleDateFormat(passageDateFormatDate, Locale.FRANCE);
					long passageMs = passageSdfDate.parse(passageDate).getTime();					
					long currentMs = (new Date()).getTime();
					long diffMs = passageMs - currentMs;
					int min = (int) ((diffMs / (1000)) /60);
					if (DEBUG) System.out.println(min + " min");
					passageTime = min + " min";
				}
				
				String composition = terminus + "#" + passageTime;
				passageInfo.add(composition);
			}
			
			if (DEBUG) {
				for (int j = 0; j < passageInfo.size(); j++) {
					 System.out.println("Passage info = " + passageInfo.get(j));
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return passageInfo;
	}
	
	
	/**
	 * Get Paris bus real time infos
	 * @param stopId
	 * @param routeId
	 * @param maxValue
	 * @return Paris real time infos
	 */
	public static BusRealTimeInfosCollection getParisBusRealTimeInfos(String stopId, String routeId, int maxValue) {
		
		String methodTag = "getParisBusRealTimeInfos";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris bus real time info...");
		
		BusRealTimeInfosCollection collection = null;
		
		String routeName = MongoDbManager.getParisBusRouteName(routeId);
		String stationName = MongoDbManager.getParisBusStationName(stopId);
		
		List<String> busTimeTable = getBusTimetable(routeName, stationName);
		
		BusRealTimeInfo[] infos = new BusRealTimeInfo[busTimeTable.size()];
		BusStation station = MongoDbManager.getParisBusStation(stopId);
		
		int iteration = maxValue;
		
		if (iteration > infos.length) {
			iteration = infos.length;
		}
		
		for (int i = 0; i < iteration; i++) {
			String info[] = busTimeTable.get(i).split("#");
			String destination = info[0];
			String time = info[1];
			Bus bus = new Bus(routeId, routeName, destination);
			BusRealTimeInfo rtInfo = new BusRealTimeInfo(bus, station, time);
			infos[i] = rtInfo;
		}
		
		collection = new BusRealTimeInfosCollection(infos);
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, collection.toString());
		
		return collection;
	}
	
	/**
	 * Get Paris metro real time info
	 * @param stopId
	 * @param routeId
	 * @return Paris metro real time info
	 */
	public static TubeRealTimeInfosCollection getParisMetroRealTimeInfos(String stopId, String routeId, int nbValues) {
		
		String methodTag = "getParisMetroRealTimeInfos";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris metro real time info for " +
        		"stopId = " + stopId + ", " + 
        		"routeId = " + routeId + ", " +
        		"nbValues = " + nbValues
        		);
		
		TubeRealTimeInfosCollection parisMetroRealTimeInfos = null;
		
		List<String> metroInfos = new ArrayList<String>();
		
		String stationName = MongoDbManager.getParisMetroStationName(stopId);
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "station name = " + stationName);
		
		String routeName = MongoDbManager.getParisMetroRouteName(routeId);
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "station name = " + stationName + ", route name = " + routeName);
		
		//direction id: 0 for aller, 1 for retour
		String directionId = MongoDbManager.getParisMetroRouteDirectionId(routeId);
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "direction id = " + directionId);
		
		if (directionId.equals("0")) {
			//Aller
			metroInfos.addAll(getMetroOrTramTimetable(DataConstants.RESEAU_METRO, routeName, DataConstants.ALLER_CODE, stationName, nbValues));
		} else if (directionId.equals("1")) {
			//Retour
			metroInfos.addAll(getMetroOrTramTimetable(DataConstants.RESEAU_METRO, routeName, DataConstants.RETOUR_CODE, stationName, nbValues));
		}
		
		if (DEBUG) TdServerLogger.print(TAG,  methodTag, metroInfos.toString());
		
		List<TubeRealTimeInfo> rtInfoList = new ArrayList<TubeRealTimeInfo>();
		
		for (String info : metroInfos) {
			String infoArray[] = info.split("#");
			if (infoArray.length < 2) continue;
			String destination = infoArray[0];
			String time = infoArray[1];
			//RATP doesn't give information on tube platform
			TubePlatform platform = new TubePlatform(stationName + " platform", stationName + " platform");
			Tube tube = new Tube(routeId, routeName, destination);
			TubeStation tubeStation = MongoDbManager.getParisMetroStation(stopId);
			TubeRealTimeInfo rtInfo = new TubeRealTimeInfo(tube, tubeStation, platform, time);
			
			if (DEBUG) TdServerLogger.print(TAG,  methodTag, rtInfo.toString());
			
			rtInfoList.add(rtInfo);
		}
		
		TubeRealTimeInfo[] rtInfosArray = new TubeRealTimeInfo[rtInfoList.size()];
		
		for (int i = 0; i < rtInfoList.size(); i++) {
			rtInfosArray[i] = rtInfoList.get(i);
		}
		
		parisMetroRealTimeInfos = new TubeRealTimeInfosCollection(rtInfosArray);
		
		return parisMetroRealTimeInfos;
	}
	
	
	/**
	 * Get Paris rail real time info
	 * @param stopId
	 * @param routeId
	 * @return Paris rail real time info
	 */
	public static TubeRealTimeInfosCollection getParisRailRealTimeInfos(String stopId, String routeId, int nbValues) {
		
		String methodTag = "getParisRailRealTimeInfos";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris rail real time info...");
		
		TubeRealTimeInfosCollection parisRailRealTimeInfos = null;
		
		List<String> railInfos = new ArrayList<String>();
		
		String stationName = MongoDbManager.getParisRailStationName(stopId);
		
		String routeName = MongoDbManager.getParisRailRouteName(routeId);
		
		//direction id: 0 for aller, 1 for retour
		String directionId = MongoDbManager.getParisRailRouteDirectionId(routeId);
		
		if (directionId.equals("0")) {
			//Aller
			railInfos.addAll(getRerTimetable(routeName, DataConstants.ALLER_CODE, stationName, nbValues));
		} else if (directionId.equals("1")) {
			//Retour
			railInfos.addAll(getRerTimetable(routeName, DataConstants.RETOUR_CODE, stationName, nbValues));
		}
		
		if (DEBUG) TdServerLogger.print(TAG,  methodTag, railInfos.toString());
		
		List<TubeRealTimeInfo> rtInfoList = new ArrayList<TubeRealTimeInfo>();
		
		for (String info : railInfos) {
			String infoArray[] = info.split("#");
			if (infoArray.length < 2) continue;
			String destination = infoArray[0];
			String time = infoArray[1];
			//RATP doesn't give information on tube platform
			TubePlatform platform = new TubePlatform(stationName + " platform", stationName + " platform");
			Tube tube = new Tube(routeId, routeName, destination);
			TubeStation tubeStation = MongoDbManager.getParisRailStation(stopId);
			TubeRealTimeInfo rtInfo = new TubeRealTimeInfo(tube, tubeStation, platform, time);
			
			if (DEBUG) TdServerLogger.print(TAG,  methodTag, rtInfo.toString());
			
			rtInfoList.add(rtInfo);
		}
		
		TubeRealTimeInfo[] rtInfosArray = new TubeRealTimeInfo[rtInfoList.size()];
		
		for (int i = 0; i < rtInfoList.size(); i++) {
			rtInfosArray[i] = rtInfoList.get(i);
		}
		
		parisRailRealTimeInfos = new TubeRealTimeInfosCollection(rtInfosArray);
		
		return parisRailRealTimeInfos;
	}
	
	
	/**
	 * Get Paris tram real time info
	 * @param stopId
	 * @param routeId
	 * @return Paris tram real time info
	 */
	public static TramRealTimeInfosCollection getParisTramRealTimeInfos(String stopId, String routeId, int nbValues) {
		
		String methodTag = "getParisTramRealTimeInfos";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris tram real time info...");
		
		TramRealTimeInfosCollection parisTramRealTimeInfos = null;
		
		List<String> tramInfos = new ArrayList<String>();
		
		String stationName = MongoDbManager.getParisTramStationName(stopId);
		
		String routeName = MongoDbManager.getParisTramRouteName(routeId);
		
		String newRouteName = routeName;
		
		if (routeName.equals("T3")) newRouteName = routeName + "a";
		
		//direction id: 0 for aller, 1 for retour
		String directionId = MongoDbManager.getParisTramRouteDirectionId(routeId);
		
		if (directionId.equals("0")) {
			//Aller
			tramInfos.addAll(getMetroOrTramTimetable(DataConstants.RESEAU_TRAM, newRouteName, DataConstants.ALLER_CODE, stationName, nbValues));
		} else if (directionId.equals("1")) {
			//Retour
			tramInfos.addAll(getMetroOrTramTimetable(DataConstants.RESEAU_TRAM, newRouteName, DataConstants.RETOUR_CODE, stationName, nbValues));
		}
		
		if (DEBUG) TdServerLogger.print(TAG,  methodTag, tramInfos.toString());
		
		List<TramRealTimeInfo> rtInfoList = new ArrayList<TramRealTimeInfo>();
		
		for (String info : tramInfos) {
			String infoArray[] = info.split("#");
			if (infoArray.length < 2) continue;
			String destination = infoArray[0];
			String time = infoArray[1];
			Tram tram = new Tram(routeId, routeName, destination);
			TramStation tramStation = MongoDbManager.getParisTramStation(stopId);
			TramRealTimeInfo rtInfo = new TramRealTimeInfo(tram, tramStation, time);
			
			if (DEBUG) TdServerLogger.print(TAG,  methodTag, rtInfo.toString());
			
			rtInfoList.add(rtInfo);
		}
		
		TramRealTimeInfo[] rtInfosArray = new TramRealTimeInfo[rtInfoList.size()];
		
		for (int i = 0; i < rtInfoList.size(); i++) {
			rtInfosArray[i] = rtInfoList.get(i);
		}
		
		parisTramRealTimeInfos = new TramRealTimeInfosCollection(rtInfosArray);
		
		return parisTramRealTimeInfos;
	}	

}
