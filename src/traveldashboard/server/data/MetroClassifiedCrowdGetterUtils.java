package traveldashboard.server.data;

import java.util.Locale;
import java.util.TimeZone;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class MetroClassifiedCrowdGetterUtils extends ResponseUtils {

	private static final String TAG = "MetroClassifiedCrowdGetterUtils";
	
	public static NetworkMessage getMetroClassifiedCrowdMessage(NetworkMessage req) {
		String methodTag = "getMetroClassifiedCrowdMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Getting metro classified crowd message...");
        
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		long timestamp = Long.parseLong(req.getPayload(DataConstants.PARAM_KEY_TIMESTAMP));
		
		String timeZone = "";
		
		if (city.equals(DataConstants.PARIS)) {
			timeZone = "Europe/Paris";
			
		} else if (city.equals(DataConstants.LONDON)) {
			timeZone = "Europe/London";
		}
		
		//Set to local timestamp
		timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
    	TimeZone.getTimeZone(timeZone).getRawOffset();
		
		Gson gson = new Gson();
		String json = gson.toJson(MongoDbManager.getMetroClassifiedCrowd(city, stopId, routeId, timestamp));
		
		resp.addPayload(DataConstants.METRO_CLASSIFIED_CROWD_MESSAGE, json);
		
		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag,  new String(resp.encode()));
		
		return resp;
	}
}