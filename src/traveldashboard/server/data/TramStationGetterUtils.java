package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import traveldashboard.data.TransportArea;


import com.google.gson.Gson;

public class TramStationGetterUtils extends ResponseUtils {
	
	private static final String TAG = "TramStationGetterUtils";
	
	public static NetworkMessage getTramStationMessage(NetworkMessage req) {
		
		String methodTag = "getTramStationMessage";
		
		TdServerLogger.print(TAG, methodTag, "Start get tram stations message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		String latitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LAT);
		String longitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LON);
		String radius = req.getPayload(DataConstants.PARAM_KEY_RADIUS);
		
		
		Gson gson = new Gson();
		String json = "null";
		
		if (TransportArea.getArea(Double.parseDouble(latitude), Double.parseDouble(longitude)).equals(TransportArea.PARIS)) {
			//Paris
			json = gson.toJson(MongoDbManager.getParisTramStationsCollection(latitude, longitude, radius));
		} 
		
		resp.addPayload(DataConstants.TRAM_STATION_MESSAGE, json);
		
		TdServerLogger.print(TAG, methodTag, "End get tram stations message : " + new String(resp.encode()));
		
		return resp;
	}
}
