package traveldashboard.server.data;

import java.util.List;

import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

/**
 * Generate all stops message from database
 * @author khoo
 *
 */
public class TDMessageGeneratorFromDatabase {
	
	private static final String TAG = "TDMessageGeneratorFromDatabase";
	
    private static final String PARAM_VALUE_STOP_ID = "ID";
    private static final String PARAM_KEY_STOP_NAME = "_Name";
    private static final String PARAM_KEY_STOP_LON = "_Lon";
    private static final String PARAM_KEY_STOP_LAT = "_Lat";
    private static final String PARAM_KEY_ROUTE_ID = "_RouteId";
    private static final String PARAM_KEY_ROUTE_NAME = "_RouteName";
    private static final String PARAM_KEY_DIRECTION_ID = "_Direction";
	
    /**
     * Generate station message from database
     * @param roadType
     * @return a network message or null if request/parsing failed
     * @throws InterruptedException
     */
    public static NetworkMessage generateStationMessageFromDatabase(String city, int roadType) throws InterruptedException {
           
    	String methodTag = "generateStationMessageFromDatabase";
    	
    	try {
            // get the stops
            List<StopForNetworkMessage> stoplist = MongoDbManager.getAllStopsFor(city, roadType);

            TdServerLogger.print(TAG, methodTag, "XXXXXXXXXXXXXXXXXXXXXXXX");
            TdServerLogger.print(TAG, methodTag, " stop list size = " + stoplist.size());
            
            // generate the xml network message
            NetworkMessage res = new NetworkMessageXml();
            
            // for all stops
            int index = 0;
            for (StopForNetworkMessage tmpStop : stoplist) {
            	addSingleStopFromDatabase(res, tmpStop);
            	index++;
            	if ((index%500) == 0)   TdServerLogger.print(TAG, methodTag, "index = " + index);
            }
            
            TdServerLogger.print(TAG, methodTag, "XXXXXXXXXXXXXXXXXXXXXXXX");
            TdServerLogger.print(TAG, methodTag, " after all stops");
            
            return res;
            
         } catch (Exception e) {
            e.printStackTrace();
         }
            
    	return null;
    }

    /**
     * Add single stop from database
     * @param res
     * @param tmpStop
     */
    private static void addSingleStopFromDatabase(NetworkMessage res, StopForNetworkMessage tmpStop) {
    	
            String id = tmpStop.getStopId();

            // if invalid id, then return
            if (id == null || id.equals(""))
                    return;

            // adding ID
            res.addPayload(id, PARAM_VALUE_STOP_ID);

            // adding name
            res.addPayload(id + PARAM_KEY_STOP_NAME, tmpStop.getStopName());

            // adding lon
            res.addPayload(id + PARAM_KEY_STOP_LON, String.valueOf(tmpStop.getStopLon()));

            // adding lat
            res.addPayload(id + PARAM_KEY_STOP_LAT, String.valueOf(tmpStop.getStopLat()));
            
            // adding RouteId
            res.addPayload(id + PARAM_KEY_ROUTE_ID, tmpStop.getRouteId());
            
            // adding RouteName
            res.addPayload(id + PARAM_KEY_ROUTE_NAME, tmpStop.getRouteName());
            
            // adding RouteName
            res.addPayload(id + PARAM_KEY_DIRECTION_ID, tmpStop.getDirection());
    }    
}
