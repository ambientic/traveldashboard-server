package traveldashboard.server.data;


import org.ibicoop.sdp.config.NetworkMessage;

import traveldashboard.data.BusStationsCollection;
import traveldashboard.data.TransportArea;

import com.google.gson.Gson;

public class BusStopGetterUtils extends ResponseUtils {

	private static final String TAG = "BusStopGetterUtils";
	
	/**
	 * Get bus stop json string
	 * @param req Request network message
	 * @return jsons string
	 */
	public static String getBusStopJsonString(NetworkMessage req) {
		String methodTag = "getBusStopJsonString";
		
		TdServerLogger.print(TAG, methodTag, "Start get bus stop message");
		
		String latitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LAT);
		String longitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LON);
		String radius = req.getPayload(DataConstants.PARAM_KEY_RADIUS);
		
		Gson gson = new Gson();
		String json = "null";
		
		if (TransportArea.getArea(Double.parseDouble(latitude), Double.parseDouble(longitude)).equals(TransportArea.LONDON)) {
			//In London
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "#####Bus stop in London#####");
			
			BusStationsCollection collection = MongoDbManager.getLondonBusStationsCollection(latitude, longitude, radius);
			
			int collectionSize = collection.getBusStations().length;
			
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Collection size = " + collectionSize);
			
			json = gson.toJson(collection);
			
		} else {
			//In Paris
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "#####Bus stop in Paris#####");
			
			BusStationsCollection collection = MongoDbManager.getParisBusStationsCollection(latitude, longitude, radius);			
			
			int collectionSize = collection.getBusStations().length;
			
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Collection size = " + collectionSize);
			
			json = gson.toJson(collection);
		}
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "json string = " + json);
		
		return json;
	}		
	
	public static NetworkMessage getBusStopMessage(NetworkMessage req) {
		
		String methodTag = "getBusStopMessage";
		
		TdServerLogger.print(TAG, methodTag, "Start get bus stop message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String latitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LAT);
		String longitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LON);
		String radius = req.getPayload(DataConstants.PARAM_KEY_RADIUS);
		
		Gson gson = new Gson();
		String json = "null";
		
		if (TransportArea.getArea(Double.parseDouble(latitude), Double.parseDouble(longitude)).equals(TransportArea.LONDON)) {
			//In London
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag,"#####Bus stop in London#####");
			BusStationsCollection collection = MongoDbManager.getLondonBusStationsCollection(latitude, longitude, radius);
			int collectionSize = collection.getBusStations().length;
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag,"Collection size = " + collectionSize);
			json = gson.toJson(collection);
		} else {
			//In Paris
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag,"#####Bus stop in Paris#####");
			BusStationsCollection collection = MongoDbManager.getParisBusStationsCollection(latitude, longitude, radius);			
			int collectionSize = collection.getBusStations().length;
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag,"Collection size = " + collectionSize);
			json = gson.toJson(collection);
		}
		
		resp.addPayload(DataConstants.BUS_STOP_MESSAGE, json);
		
		TdServerLogger.print(TAG, methodTag,"End get bus stop message : " + new String(resp.encode()));
		TdServerLogger.print(TAG, methodTag,"size of bus stop message : " + resp.encode().length);
		return resp;
	}
	
}
