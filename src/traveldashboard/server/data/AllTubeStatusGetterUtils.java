package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class AllTubeStatusGetterUtils extends ResponseUtils {

	private static final String TAG = "TubeStatusGetterUtils";
	
	public static NetworkMessage getAllTubeStatusMessage(NetworkMessage req) {
		String methodTag = "getAllTubeStatusMessage";
		
		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "Start get all tube status message");	
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		
		Gson gson = new Gson();
		
		String json = "null";

		json = gson.toJson(MongoDbManager.getMetroStatusCollection(city));

		resp.addPayload(DataConstants.ALL_TUBE_STATUS_MESSAGE, json);
	
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get all tube status message" + new String(resp.encode()));
		
		return resp;
	}
	
}
