package traveldashboard.server.data;


import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import traveldashboard.data.TubeStation;
import traveldashboard.data.comment.TubeStationComment;
import traveldashboard.data.comment.TubeStationCommentsCollection;


import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.FacebookClient.AccessToken;
import com.restfb.Parameter;
import com.restfb.types.CategorizedFacebookType;
import com.restfb.types.Post;

/**
 * Reference: http://restfb.com/
 * @author khoo
 *
 */
public class FacebookResponse {

	private static final String TAG = "FacebookResponse";
	private static final boolean DEBUG = DataConstants.DEBUG;
	
	//Traveldashboard Inria FB account
	private static final String APP_ID = "552368981464785";
	private static final String APP_SECRET = "63c5ec832b851a701a0a62244b650c20";
	//private static final String ACCESS_TOKEN = "552368981464785|hD_nKeEczONTxhJzzgSuR75zFO8";
	private static final int MAX_CHARACTER = 140;
	
	public static TubeStationCommentsCollection getTubeStationFacebookComments(String city, String stopId, int maxComments) {
	    
		String methodTag = "getTubeStationFacebookCommentsCollection";
		
		TubeStationCommentsCollection tubeStationCommentsCollection = null;
	    TubeStationComment[] commentsArray = null;
	    List<TubeStationComment> commentsList = new ArrayList<TubeStationComment>();
	    
	    TubeStation tubeStation = null;
	    String stationName = "";
	    String researchStationName = "";
	    
		try {
			
		    if (city.equals(DataConstants.PARIS)) {
		    	tubeStation = MongoDbManager.getParisMetroStation(stopId);
		    	//stationName = StringHelper.replaceFrenchAccent(tubeStation.getName());
		    	
		    } else {
		    	tubeStation = MongoDbManager.getLondonMetroStation(stopId);	
		    }
			
		    stationName = tubeStation.getName();
		    researchStationName = stationName + " station";
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, "research station name = " + researchStationName);
		    
			AccessToken accessToken = new DefaultFacebookClient().obtainAppAccessToken(APP_ID, APP_SECRET);
			String token = accessToken.getAccessToken();
			FacebookClient facebookClient = new DefaultFacebookClient(token);
						
			Connection<Post> publicSearch =
					  facebookClient.fetchConnection("search", Post.class,
					    Parameter.with("q", researchStationName), 
					    Parameter.with("type", "post"));
			
			List<Post> postList = publicSearch.getData();
			
			int nbComment = (maxComments + 1);
			
			for (int i = 0; i < postList.size(); i++) {
				CategorizedFacebookType from = postList.get(i).getFrom();
				String user = postList.get(i).getId();
				
				if (from != null) user = from.getName();
				
				long timestamp = postList.get(i).getCreatedTime().getTime();
				
				String timeZone = "";
				
				if (city.equals(DataConstants.PARIS)) {
					timeZone = "Europe/Paris";
					
				} else if (city.equals(DataConstants.LONDON)) {
					timeZone = "Europe/London";
				}

				//Set to local timestamp
				timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
		    	TimeZone.getTimeZone(timeZone).getRawOffset();
				
				String comment = postList.get(i).getMessage();
			
				if (comment == null) continue;
				
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, "comment = " + comment);
				
				//If comment contains station name and length is less than 141 characters
				//if ((comment.contains(stationName) || comment.contains(stationName.toLowerCase())) && (comment.length() <= MAX_CHARACTER))  {
				if ((comment.length() <= MAX_CHARACTER)) {
			    	nbComment--;
					if (nbComment <= 0) break;
		        	TubeStationComment stationComment = new TubeStationComment(user, comment, timestamp, tubeStation);
		        	commentsList.add(stationComment);
				}
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
	    commentsArray = new TubeStationComment[commentsList.size()];
	    
	    for (int i = 0; i < commentsList.size(); i++) {
	    	commentsArray[i] = commentsList.get(i);
	    }
	    
	    tubeStationCommentsCollection = new TubeStationCommentsCollection(commentsArray);
		
		return tubeStationCommentsCollection;
		
	}
}
