package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class TwitterCommentGetterUtils extends ResponseUtils {

	private static final String TAG = "TwitterCommentGetterUtils";
	
	public static NetworkMessage getTwitterCommentMessage(NetworkMessage req) {
		String methodTag = "getTwitterCommentMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get twitter comment message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		int maxComments = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
		
		Gson gson = new Gson();
		String json = gson.toJson(TwitterResponse.getTubeStationTwitterComments(city, stopId, maxComments));
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag,"End get twitter comment message" + new String(resp.encode()));
		
		resp.addPayload(DataConstants.TWITTER_COMMENT_MESSAGE, json);
		
		return resp;
	}
}
