package traveldashboard.server.data;

import java.util.TimeZone;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class MetroGeneralCrowdGetterUtils extends ResponseUtils {
	
	private static final String TAG = "GeneralCrowdGetterUtils";
	
	public static NetworkMessage getMetroGeneralCrowdMessage(NetworkMessage req) {
		String methodTag = "getMetroGeneralCrowdMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Getting metro general crowd message...");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		long timestamp = Long.parseLong(req.getPayload(DataConstants.PARAM_KEY_TIMESTAMP));
		long interval = Long.parseLong(req.getPayload(DataConstants.PARAM_KEY_INTERVAL));
		
		String timeZone = "";
		
		if (city.equals(DataConstants.PARIS)) {
			timeZone = "Europe/Paris";
			
		} else if (city.equals(DataConstants.LONDON)) {
			timeZone = "Europe/London";
		}

		//Set to local timestamp
		timestamp = timestamp - TimeZone.getDefault().getRawOffset() + 
    	TimeZone.getTimeZone(timeZone).getRawOffset();
		
		
		Gson gson = new Gson();
		
		//Get crowd data until interval ago
		//String json = gson.toJson(MongoDbManager.getMetroGeneralCrowd(city, stopId, routeId, timestamp, interval));
		//Get crowd data until 6 times interval ago
		String json = gson.toJson(MongoDbManager.getMetroGeneralCrowdCollection(city, stopId, routeId, timestamp, interval));
		
		resp.addPayload(DataConstants.METRO_GENERAL_CROWD_MESSAGE, json);
		
		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "End get crowdedness message : " + new String(resp.encode()));
        
		return resp;
	}
}
