package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class BusCountdownGetterUtils extends ResponseUtils {

	private static final String TAG = "BusCountdownGetterUtils";
	
	public static NetworkMessage getBusCountdownMessage(NetworkMessage req) {
		String methodTag = "getBusCountdownMessage";
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get bus countdown message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		Integer maxValue = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
		
		if (maxValue == null) maxValue = 5; //default value
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag,"stop id = " + stopId);
		
		Gson gson = new Gson();
		
		String json = "null";
		
		if (city.equals(DataConstants.PARIS)) {
			//Paris
			json = gson.toJson(RatpTimeTableProcess.getParisBusRealTimeInfos(stopId, routeId, maxValue));
		} else if (city.equals(DataConstants.LONDON)) {
			//In London
			json = gson.toJson(BusCountdownResponse.getBusRealTimeInfosCollection(stopId, maxValue));
		}
		
		resp.addPayload(DataConstants.BUS_COUNTDOWN_MESSAGE, json);
		
		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "End get bus countdown message : " + new String(resp.encode()));
		
		return resp;
	}	
}
