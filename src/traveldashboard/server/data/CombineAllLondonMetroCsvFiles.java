package traveldashboard.server.data;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class CombineAllLondonMetroCsvFiles {
	
    private static CSVWriter writer;
	public static final String PICCADILLY_LINE = "Piccadilly";
	public static final String DISTRICT_LINE = "District";
	public static final String VICTORIA_LINE = "Victoria";
	public static final String CIRCLE_LINE = "Circle";
	public static final String HAMMERSMITH_CITY_LINE = "Hammersmith and City";
	public static final String BAKERLOO_LINE = "Bakerloo";
	public static final String WATERLOO_CITY_LINE = "Waterloo and City";
	public static final String CENTRAL_LINE = "Central";
	public static final String JUBILEE_LINE = "Jubilee";
	public static final String METROPOLITAN_LINE = "Metropolitan";
	public static final String NORTHERN_LINE = "Northern";
	public static final String HAMMERSMITH_CIRCLE = "Hammersmith or Circle";
	public static final String DLR_LINE = "DLR";
	public static final String OVERGROUND_LINE = "Overground";
	
	public static final Map<String, String> LONDON_ROUTEID_ROUTENAME_MAP;
	public static final HashMap<String, String> LONDON_TUBE_DESTINATIONS;
	
	static{
		//London code line map
		LONDON_ROUTEID_ROUTENAME_MAP = new ConcurrentHashMap<String, String>();
		LONDON_ROUTEID_ROUTENAME_MAP.put("B", BAKERLOO_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("C", CENTRAL_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("D", DISTRICT_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("H", HAMMERSMITH_CIRCLE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("J", JUBILEE_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("M", METROPOLITAN_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("N", NORTHERN_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("P", PICCADILLY_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("V", VICTORIA_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("W", WATERLOO_CITY_LINE);
		LONDON_ROUTEID_ROUTENAME_MAP.put("N/A", "N/A");
		LONDON_ROUTEID_ROUTENAME_MAP.put("*", "*");
		
		LONDON_TUBE_DESTINATIONS = new HashMap<String, String>();
		LONDON_TUBE_DESTINATIONS.put(BAKERLOO_LINE, "Harrow and Wealdstone/Elephant and Castle");
		LONDON_TUBE_DESTINATIONS.put(CENTRAL_LINE, "Ealing Broadway/West Ruislip/Epping");
		LONDON_TUBE_DESTINATIONS.put(CIRCLE_LINE, "Hammersmith/Edgware Road");
		LONDON_TUBE_DESTINATIONS.put(DISTRICT_LINE, "Ealing Broadway/Richmond/Wimbledon/Edgware Road/Upminster");
		LONDON_TUBE_DESTINATIONS.put(HAMMERSMITH_CIRCLE, "Hammersmith/Barking/Edgware Road");
		LONDON_TUBE_DESTINATIONS.put(HAMMERSMITH_CITY_LINE, "Hammersmith/Barking");
		LONDON_TUBE_DESTINATIONS.put(JUBILEE_LINE, "Stanmore/Stratford");
		LONDON_TUBE_DESTINATIONS.put(METROPOLITAN_LINE, "Amersham/Chesham/Watford/Uxbridge/Aldgate");
		LONDON_TUBE_DESTINATIONS.put(NORTHERN_LINE, "Morden/Edgware/Mill Hill East/High Barnet");
		LONDON_TUBE_DESTINATIONS.put(PICCADILLY_LINE, "Heathrow Terminal 5/Uxbridge/Cockfosters");
		LONDON_TUBE_DESTINATIONS.put(VICTORIA_LINE, "Brixton/Walthamstow Central");
		LONDON_TUBE_DESTINATIONS.put(WATERLOO_CITY_LINE, "Waterloo/Bank");
		LONDON_TUBE_DESTINATIONS.put(OVERGROUND_LINE, "Richmond/Watford Junction/Crystal Palace/West Croydon/New Cross/Barking/Stratford");
		LONDON_TUBE_DESTINATIONS.put(DLR_LINE, "Bank/Monument/Tower Gateway/Lewisham/Woolwich Arsenal/Beckton/Stratford International");
		LONDON_TUBE_DESTINATIONS.put("*", "*");
		LONDON_TUBE_DESTINATIONS.put("N/A", "N/A");	
	}
    
	//With initial parameters
	public static void readAndWriteCsvFile(String newFileName, String[] oldFileNames) {
		
		try {
			
			if (writer == null) {
				writer = new CSVWriter(new FileWriter(newFileName), ';', CSVWriter.NO_QUOTE_CHARACTER);
			}
			
			for (int i = 0; i < oldFileNames.length; i++) {
				System.out.println("Writing file number: " + (i + 1));
				InputStream is = new FileInputStream(oldFileNames[i]);
				CSVReader reader = new CSVReader(new InputStreamReader(is), ',');
				List<String[]> stations = reader.readAll();	
				if (i > 0) stations.remove(0);
				writer.writeAll(stations);
				reader.close();
			}
			
			writer.close();
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
    public static final String PARAM_KEY_STOP_NAME = "id";//0
    public static final String PARAM_KEY_STOP_ID = "stc";//1
    public static final String PARAM_KEY_STOP_NLC = "nlc";//2    
    public static final String PARAM_KEY_STOP_LON = "lng";//3
    public static final String PARAM_KEY_STOP_LAT = "lat";//4
    
    public static final String PARAM_KEY_ROUTE_ID1 = "line1";//5
    public static final String PARAM_KEY_ROUTE_NAME1 = "lineName1";//6
    public static final String PARAM_KEY_ROUTE_DIRECTION1 = "lineDirection1";//7
    
    public static final String PARAM_KEY_ROUTE_ID2 = "line2";//8
    public static final String PARAM_KEY_ROUTE_NAME2 = "lineName2";//9
    public static final String PARAM_KEY_ROUTE_DIRECTION2 = "lineDirection2";//10
    
    public static final String PARAM_KEY_ROUTE_ID3 = "line3";//11
    public static final String PARAM_KEY_ROUTE_NAME3 = "lineName3";//12
    public static final String PARAM_KEY_ROUTE_DIRECTION3 = "lineDirection3";//13
    
    public static final String PARAM_KEY_ROUTE_ID4 = "line4";//14
    public static final String PARAM_KEY_ROUTE_NAME4 = "lineName4";//15
    public static final String PARAM_KEY_ROUTE_DIRECTION4 = "lineDirection4";//16
    
    public static final String PARAM_KEY_ROUTE_ID5 = "line5";//17
    public static final String PARAM_KEY_ROUTE_NAME5 = "lineName5";//18
    public static final String PARAM_KEY_ROUTE_DIRECTION5 = "lineDirection5";//19
    
    //Alarm set: 0 if not set, 1 if set
    public static final String PARAM_KEY_ALARM_SET = "AlarmSet";//20
    //Alarm time: HH:mm
    public static final String PARAM_KEY_ALARM_TIME = "AlarmTime";//21
    
    public static final String COLUMN_NAME_NULLABLE = "Nullable";//22
	
	//With extra parameters such as alarm set
	public static void readAndWriteCsvFileWithExtraFeatures(String newFileName, String[] oldFileNames) {
		
		try {
			
			if (writer == null) {
				writer = new CSVWriter(new FileWriter(newFileName), ';', CSVWriter.NO_QUOTE_CHARACTER);
			}
			
			boolean indexNameIsInserted = false;
			
			for (int i = 0; i < oldFileNames.length; i++) {
				System.out.println("Writing file number: " + (i + 1));
				InputStream is = new FileInputStream(oldFileNames[i]);
				CSVReader reader = new CSVReader(new InputStreamReader(is), ',');
				List<String[]> stations = reader.readAll();	
				if (i > 0) stations.remove(0);
				//_id,"nlc","stc","line1","line2","line3","line4","line5","lat","lng"
				
				for (String[] station : stations) {
					
					String[] newStation = new String[23];
					int newStationIndex = 0;
					
					String id = station[0];
					newStation[newStationIndex] = id;
					newStationIndex++;
					
					String nlc = station[1].trim();
					newStation[newStationIndex] = nlc;
					newStationIndex++;
					
					String stc = station[2].trim();
					newStation[newStationIndex] = stc;
					newStationIndex++;
					
					String lat = station[8].trim();
					newStation[newStationIndex] = lat;
					newStationIndex++;
					
					String lng = station[9].trim();
					newStation[newStationIndex] = lng;
					newStationIndex++;
					
					/*
					String line1 = station[3];
					String line2 = station[4];
					String line3 = station[5];
					String line4 = station[6];
					String line5 = station[7];*/
					
					if (!indexNameIsInserted) {
						id = PARAM_KEY_STOP_NAME;
						
						for (int k = 3; k <= 7; k++) {
							String line = station[k].trim();
							newStation[newStationIndex] = line;
							newStationIndex++;
							
							String lineName = "lineName" + (k - 2) ;
							newStation[newStationIndex] = lineName;
							newStationIndex++;						
							
							String lineDestination = "lineDirection" + (k - 2);
							newStation[newStationIndex] = lineDestination;
							newStationIndex++;
						}
						
						newStation[newStationIndex] =  PARAM_KEY_ALARM_SET;
						newStationIndex++;
			
						newStation[newStationIndex] = PARAM_KEY_ALARM_TIME;
						newStationIndex++;
						
						newStation[newStationIndex] = COLUMN_NAME_NULLABLE;
						newStationIndex++;
						
						indexNameIsInserted = true;
						
					} else {
						for (int k = 3; k <= 7; k++) {
							String line = station[k].trim();
							newStation[newStationIndex] = line;
							newStationIndex++;
							
							String lineName = LONDON_ROUTEID_ROUTENAME_MAP.get(line);
							newStation[newStationIndex] = lineName;
							newStationIndex++;						
							
							String lineDestination = LONDON_TUBE_DESTINATIONS.get(lineName);
							newStation[newStationIndex] = lineDestination;
							newStationIndex++;
						}
						
						newStation[newStationIndex] =  "0";
						newStationIndex++;
			
						newStation[newStationIndex] = "1200";
						newStationIndex++;
						
						newStation[newStationIndex] = "";
						newStationIndex++;						
					}
					
					writer.writeNext(newStation);
				}
				
				//writer.writeAll(stations);
				reader.close();
			}
			
			System.out.println("Finish");
			
			writer.close();
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		String[] oldFileNames = new String[8];
		
		for (int i = 0; i < 8; i++) {
			oldFileNames[i] = "stations" + (i + 1) + ".csv";
		}
		
		readAndWriteCsvFileWithExtraFeatures("londonMetroStopExtraCsv.csv", oldFileNames);
	}

}
