package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletContext;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import traveldashboard.data.BikeStation;
import traveldashboard.data.BikeStationsCollection;

public class VelibInfoProcess {
	//private static final String URL = "http://www.velib.paris.fr/service/carto";
	private static final String STATION_URL_HEAD = "http://www.velib.paris.fr/service/stationdetails/";

	public static BikeStationsCollection getBikeStationsCollection(ServletContext context) {
		
		if (DataConstants.DEBUG) System.out.println("Get Velib info");
		
		BikeStationsCollection bikeStationsCollection = null;		
		try {
			
			InputStream is = context.getResourceAsStream("/WEB-INF/cartoVelib.xml");
			
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			BasicResponseHandler responseHandler = new BasicResponseHandler();
							
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();

			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
			
			br.close();
			
			String cartoXml = sb.toString();
			if (DataConstants.DEBUG) System.out.println(cartoXml);
		    // Convert XML to JSON Object
            JSONObject xmlJSONObj = XML.toJSONObject(cartoXml);
            JSONObject cartoObj = xmlJSONObj.getJSONObject("carto");
            //System.out.println(cartoObj.toString());
            JSONObject markersObj = cartoObj.getJSONObject("markers");
            //System.out.println(markersObj.toString());
            JSONArray markerArray = markersObj.getJSONArray("marker");
            //System.out.println(markerArray.toString());
            
			BikeStation[] bikeStations = new BikeStation[markerArray.length()];
            
            for (int i = 0; i < markerArray.length(); i++) {
            	JSONObject infoObj = markerArray.getJSONObject(i);
            	String name = infoObj.getString("name");
            	double lat = infoObj.getDouble("lat");
            	double lng = infoObj.getDouble("lng");
            	int id = infoObj.getInt("number");
            	//System.out.println(name + ", " + id + ", " + lat + ", " + lng + ", " + id);
            	HttpGet httpGet = new HttpGet(STATION_URL_HEAD + id);
    			String stationXml = httpClient.execute(httpGet, responseHandler);

                JSONObject stationJSONObj = XML.toJSONObject(stationXml);

                JSONObject stationDetailJSONObj = stationJSONObj.getJSONObject("station");

                int nbBikes = stationDetailJSONObj.getInt("available");
                int nbEmptyDocks = stationDetailJSONObj.getInt("free");
                int nbTotalDocks = stationDetailJSONObj.getInt("total");
                boolean isLocked = false;
                int open = stationDetailJSONObj.getInt("open");
                if (open == 0) isLocked = true;
				bikeStations[i] = new BikeStation(String.valueOf(id), name, lat, lng, 
						nbBikes, nbEmptyDocks, nbTotalDocks, isLocked);
            }
            
			bikeStationsCollection = new BikeStationsCollection(bikeStations);
			
			if (DataConstants.DEBUG) System.out.println("End get Velib info");
            
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return bikeStationsCollection;
	}
	
}
