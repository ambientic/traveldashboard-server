package traveldashboard.server.data;

public class TdDbProfil {
	private String city;
	private String dbName;
	private String tabBike;
	private String tabBus;
	private String tabMetro;
	private String tabRail;
	private String tabTram;
	private String tabNoise;
	private String tabClassifiedNoise;
	private String tabClassifiedCrowd;
	private String tabGeneralCrowd;
	private String tabUserRatings;
	private String tabGeneralRatings;
	private String tabComments;
	private String tabStatus;
	private String tabFact;
	private String tabJoke;
	
	public TdDbProfil(
			String city,
			String dbName,
			String tabBike,
			String tabBus,
			String tabMetro,
			String tabRail,
			String tabTram,
			String tabNoise,
			String tabClassifiedNoise,
			String tabClassifiedCrowd,
			String tabGeneralCrowd,
			String tabUserRatings,
			String tabGeneralRatings,
			String tabComments,
			String tabStatus,
			String tabFact,
			String tabJoke
			) {
		this.city = city;
		this.dbName = dbName;
		this.tabBike = tabBike;
		this.tabBus = tabBus;
		this.tabMetro = tabMetro;
		this.tabRail = tabRail;
		this.tabTram = tabTram;
		this.tabNoise = tabNoise;
		this.tabClassifiedNoise = tabClassifiedNoise;
		this.tabClassifiedCrowd = tabClassifiedCrowd;
		this.tabGeneralCrowd = tabGeneralCrowd;
		this.tabUserRatings = tabUserRatings;
		this.tabGeneralRatings = tabGeneralRatings;
		this.tabComments = tabComments;
		this.tabStatus = tabStatus;
		this.tabFact = tabFact;
		this.tabJoke = tabJoke;
	}

	public String getCity() {
		return city;
	}

	public String getDbName() {
		return dbName;
	}

	public String getTabBike() {
		return tabBike;
	}

	public String getTabBus() {
		return tabBus;
	}

	public String getTabMetro() {
		return tabMetro;
	}

	public String getTabRail() {
		return tabRail;
	}
	
	public String getTabTram() {
		return tabTram;
	}

	public String getTabNoise() {
		return tabNoise;
	}

	public String getTabClassifiedNoise() {
		return tabClassifiedNoise;
	}
	
	public String getTabClassifiedCrowd() {
		return tabClassifiedCrowd;
	}

	public String getTabGeneralCrowd() {
		return tabGeneralCrowd;
	}

	public String getTabUserRatings() {
		return tabUserRatings;
	}

	public String getTabGeneralRatings() {
		return tabGeneralRatings;
	}

	public String getTabComments() {
		return tabComments;
	}
	
	public String getTabStatus() {
		return tabStatus;
	}
	
	public String getTabFact() {
		return tabFact;
	}
	
	public String getTabJoke() {
		return tabJoke;
	}
	
	@Override
	public String toString() {
		return "city = " + city 
				+ ", dbName = " + dbName
				+ ", tabBike = " + tabBike  
				+ ", tabBus = " + tabBus
				+ ", tabMetro = " + tabMetro
				+ ", tabRail = " + tabRail
				+ ", tabTram = " + tabTram
				+ ", tabNoise = " + tabNoise
				+ ", tabClassifiedNoise = " + tabClassifiedNoise
				+ ", tabClassifiedCrowd = " + tabClassifiedCrowd
				+ ", tabGeneralCrowd = " + tabGeneralCrowd
				+ ", tabUserRatings = " + tabUserRatings
				+ ", tabGeneralRatings = " + tabGeneralRatings
				+ ", tabComments = " + tabComments
				+ ", tabStatus = " + tabStatus
				+ ", tabFact = " + tabFact
				+ ", tabJoke = " + tabJoke
				;
	}
}
