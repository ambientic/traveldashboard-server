package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class TubeRatingGetterUtils extends ResponseUtils {
	
	private static final String TAG = "TubeRatingGetterUtils";
	
	public static NetworkMessage getTubeRatingMessage(NetworkMessage req) {
		
		String methodTag = "getTubeRatingMessage";
		
		TdServerLogger.print(TAG, methodTag, "Start getting tube rating message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		
		Gson gson = new Gson();
		String json = "null";

		json = gson.toJson(MongoDbManager.getMetroGeneralRating(city, routeId));

		resp.addPayload(DataConstants.TUBE_RATING_MESSAGE, json);
		
		TdServerLogger.print(TAG, methodTag, "End get tube rating message : " + new String(resp.encode()));
		return resp;
	}

}
