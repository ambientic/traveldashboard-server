package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class WikiSearcherForLondonTubeStation {

	private static final boolean DEBUG = true;
	
	private static final String URL_QUERY = "http://en.wikipedia.org/w/api.php?format=json&action=query&titles=";
	private static final String URL_QUERY_END = "&prop=revisions&rvprop=content";
	//http://en.wikipedia.org/w/api.php?format=json&action=query&titles=Web%20service&prop=revisions&rvprop=content
	
	
	private final static String USER_AGENT = "Mozilla/5.0";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		generateLondonTubeStationWikiCsv("londonTubeStationWikiCsv.csv");
	}
	
	public static void generateLondonTubeStationWikiCsv(String fileName) {

		try {
			
			CSVWriter writer = new CSVWriter(new FileWriter(fileName), ';', CSVWriter.NO_QUOTE_CHARACTER);
			
			InputStream is = new FileInputStream("londonMetroStopCsv.csv");
			CSVReader reader = new CSVReader(new InputStreamReader(is), ';');
			List<String[]> stations = reader.readAll();
			stations.remove(0);
			
			String[] newCsvTitles = {"_id", "stc","fact"};
			writer.writeNext(newCsvTitles);
			
			for (String[] station : stations) {
				
				try {
					String stationName = station[0];
					//System.out.println("#####Description of " + stationName + "#####");
					String stationNlc = station[1];
					
					if (stationNlc.trim().equals("*")) continue;
					
					String stationId = station[2];

					String stationNameCorrected = stationName.replace("(Bak)", "").replace("(Cir)", "").replace("(Cen)", "").replace("(H&C)", "").replace("(Dis)", "");

					String stationString = "tube station";
										
					if (stationName.equals("Monument")) {
						stationNameCorrected = "Bank and Monument";
						stationString = "stations";
					}
					
					if (stationName.equals("Hammersmith (Dis)")) {
						stationNameCorrected = "Hammersmith tube station (Piccadilly and District lines)";
						stationString = "";
					}					

					if (stationName.equals("Hammersmith (H&C)")) {
						stationNameCorrected = "Hammersmith tube station (Hammersmith & City and Circle lines)";
						stationString = "";
					}	
					
					if (stationName.equals("Heathrow Terminals 123")) {
						stationNameCorrected = "Heathrow Terminals 1, 2, 3";
					}
					
					if (stationName.equals("Wimbledon")) {
						stationNameCorrected = "Wimbledon station";
						stationString = "";
					}
					
					String jsonString = getWikiJsonContent(stationNameCorrected, stationString);
					
					String stationDesc = processJsonContent(stationName, jsonString);
					if (!stationDesc.equals("")) {					
						if (DEBUG) {
							System.out.println("####   " + stationName + "   ###");
							System.out.println(stationDesc);							
						}
					}
					else  {
						if (DEBUG) {
							System.err.println(stationName + " don't have description");
							System.err.println("url  = " + URL_QUERY + URLEncoder.encode(stationName + " " + "tube station", "UTF-8") + URL_QUERY_END);
						}
					}
					
					String[] newCsvContent = {stationName, stationId, stationDesc};
					
					writer.writeNext(newCsvContent);
					
				} catch (Exception exception) {
					exception.printStackTrace();
				}

				
			}
			
			reader.close();
			
			writer.close();
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
	}
	
	public static String getWikiJsonContent(String stationName, String tubeStationOrStation) {
		
		String content = "";
		
		try {
			String url = URL_QUERY + URLEncoder.encode(stationName + " " + tubeStationOrStation, "UTF-8") + URL_QUERY_END;
			
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	 
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			//System.out.println("\nSending 'POST' request to URL : " + url);
			//System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			content = response.toString();
			
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
		}

		return content;
	}

	public static String processJsonContent(String stationName, String jsonContent) {
		
		String desc = "";
		
		try {
			JSONObject contentJsonObj = new JSONObject(jsonContent);
			
			//System.out.println(contentJsonObj.toString(4));
			
			//System.out.println("\n\n");
			
			JSONObject queryJsonObj = contentJsonObj.getJSONObject("query");
			
			//System.out.println(queryJsonObj.toString(4));
			
			//System.out.println("\n\n");
			
			JSONObject pagesJsonObj = queryJsonObj.getJSONObject("pages");
			
			//System.out.println(pagesJsonObj.toString(4));
			
			//System.out.println("\n\n");
			
			Iterator<String> pagesContentIterator = pagesJsonObj.keys();
			
			String key = "";
			
			if (pagesContentIterator.hasNext()) {
				 key = pagesContentIterator.next();
				//System.out.println("key = " + key);
			}
			
			JSONObject pageContentObj = pagesJsonObj.getJSONObject(key);
			
			//System.out.println(pageContentObj.toString(4));
			
			//System.out.println("\n\n");
			
			JSONArray contentArray = pageContentObj.getJSONArray("revisions");
			
			//System.out.println(contentArray.toString(4));
			
			//System.out.println("\n\n");				
			
			//System.out.println("Array size = " + contentArray.length());
			
			JSONObject contentArrayFirstObject = contentArray.getJSONObject(0);
			
			//System.out.println(contentArrayFirstObject.toString(4));
			
			//System.out.println("\n\n");			
			
			String contentString = contentArrayFirstObject.getString("*");
					
			//System.out.println(contentString);
			
			//System.out.println("\n\n");
			
			if (contentString.contains("#REDIRECT") || contentString.contains("#redirect")
					|| contentString.contains("#Redirect")) {
				String subJsonContent = getWikiJsonContent(
						contentString.replace("#REDIRECT [[", "")
						.replace("#REDIRECT[[", "")
						.replace("#Redirect [[", "")
						.replace("#Redirect[[", "")
						.replace("#redirect [[", "")
						.replace("#redirect[[", "")
						.replace("]]", "")
						.replace("\n{{R from modification}}", "")
						.replace("\n{{R from other name}}", "")
						.replace("\n\n[[Category:Redirects with possibilities]]","")
						.replace("\\u", "-"), "");
				
				contentJsonObj = new JSONObject(subJsonContent);
				
				//System.out.println(contentJsonObj.toString(4));
				
				//System.out.println("\n\n");
				
				queryJsonObj = contentJsonObj.getJSONObject("query");
				
				//System.out.println(queryJsonObj.toString(4));
				
				//System.out.println("\n\n");
				
				pagesJsonObj = queryJsonObj.getJSONObject("pages");
				
				//System.out.println(pagesJsonObj.toString(4));
				
				//System.out.println("\n\n");
				
				pagesContentIterator = pagesJsonObj.keys();
				
				key = "";
				
				if (pagesContentIterator.hasNext()) {
					 key = pagesContentIterator.next();
					//System.out.println("key = " + key);
				}
				
				pageContentObj = pagesJsonObj.getJSONObject(key);
				
				//System.out.println(pageContentObj.toString(4));
				
				//System.out.println("\n\n");
				
				contentArray = pageContentObj.getJSONArray("revisions");
				
				//System.out.println(contentArray.toString(4));
				
				//System.out.println("\n\n");				
				
				//System.out.println("Array size = " + contentArray.length());
				
				contentArrayFirstObject = contentArray.getJSONObject(0);
				
				//System.out.println(contentArrayFirstObject.toString(4));
				
				//System.out.println("\n\n");			
				
				contentString = contentArrayFirstObject.getString("*");
						
				//System.out.println(contentString);
				
				//System.out.println("\n\n");
					
			}
			
			String stringToBeSplitted = "";
			
			if (contentString.contains("==History==")) {
				stringToBeSplitted = "==History==";
			} else if (contentString.contains("== History ==")) {
				stringToBeSplitted = "== History ==";				
			} else if (contentString.contains("==Access==")) {
				stringToBeSplitted = "==Access==";		
			} else if (contentString.contains("==Station Amenities==")) {
				stringToBeSplitted = "==Station Amenities==";
			}  else if (contentString.contains("==In popular culture==")) {
				stringToBeSplitted = "==In popular culture==";		
			} else if (contentString.contains("==Transport links==")) {
				stringToBeSplitted = "==Transport links==";
			} else if (contentString.contains("==Transports Links==")) {
				stringToBeSplitted = "==Transports Links==";
			} else if (contentString.contains("== Overview ==")) {
				stringToBeSplitted = "== Overview ==";
			} else if (contentString.contains("==Transport connections==")) {
				stringToBeSplitted = "==Transport connections==";
			} else if (contentString.contains("==Station Information==")) {
				stringToBeSplitted = "==Station Information==";
			}  else if (contentString.contains("==Service Pattern==")) {
				stringToBeSplitted = "==Service Pattern==";
			} else if (contentString.contains("==Local Amenities==")) {
				stringToBeSplitted = "==Local Amenities==";
			}  else if (contentString.contains("==Gallery==")) {
				stringToBeSplitted = "==Gallery==";
			}  else if (contentString.contains("== Closure and relocation ==")) {
				stringToBeSplitted = "== Closure and relocation ==";
			}  else if (contentString.contains("==Location==")) {
				stringToBeSplitted = "==Location==";
			}   else if (contentString.contains("== Location ==")) {
				stringToBeSplitted = "==Location==";
			}	else if (contentString.contains("==Events==")) {
				stringToBeSplitted = "==Events==";
			} else if (contentString.contains("==References==")) {
				stringToBeSplitted = "==References==";
			} else if (contentString.contains("== Development ==")) {
				stringToBeSplitted = "== Development ==";
			} else if (contentString.contains("==Station layout==")) {
				stringToBeSplitted = "==Station layout==";
			} 
			 
			
			
			if (!stringToBeSplitted.equals("")) {
				
				String[] contentSplitArray = contentString.split(stringToBeSplitted);
				
				//System.out.println("content split array size = " + contentSplitArray.length);
				
				String extractContent = contentSplitArray[0];
				
				//System.out.println(extractContent);
				
				//System.out.println(" ########");
				
				String[] splitExtractContent = extractContent.split("'''");
				
				String stationContent = splitExtractContent[( splitExtractContent.length - 1)];
				
				//If stationContent contains ==, remove content after ==
				if (stationContent.contains("==")) {
					String[] splitAgainExtractContent = stationContent.split("==");
					stationContent = splitAgainExtractContent[0];
				}
				
				//stationContent = html2text(stationContent).replace("[[", "").replace("]]", "");
				stationContent = html2text(stationContent);
				
				
				desc = stationName + " " + stationContent;		
			}

			
			if (desc.equals("")) {
				System.out.println(contentString);
			}
			
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
		}

		return desc;
	}
	
	public static String removeUnwantedCharacters(String stationContent) {
		
		return  stationContent.replace("[[", "")
					.replace("]]", "")
					.replace("{{", "")
					.replace("}}", "")
					.replace("<ref>", "")
					.replace("</ref>", "")
					.replace(";", ".")
					.replace("<sub>", " ")
					.replace("</sub>", " ")
					.replace("<ref name=Butt>"," ")
					;
		
	}
	
	public static String html2text(String html) {
		
		String info = Jsoup.parse(html).text();
		
		
		String pattern = "(\\{\\{)(cite.*?)(\\}\\})";

		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 0");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(harvnb.*?)(\\}\\})";

		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 00a");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(convert.*?)(\\|)(.*?)(\\|)(.*?)(\\|.*?)(\\}\\})";

		info = info.replaceAll(pattern, "$4 $6");
		
		//System.out.println("result 0a");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(convert.*?)(\\|)(.*?)(\\|)(.*?)(\\}\\})";

		info = info.replaceAll(pattern, "$4 $6");
		
		//System.out.println("result 0b");
		
		//System.out.println(info);		
		
		pattern = "(\\{\\{)(Dead.*?)(\\}\\})";

		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 0b");
		
		//System.out.println(info);		
		
		pattern = "(\\{\\{)(.*?)(\\|)(.*?)(\\}\\})";

		info = info.replaceAll(pattern, "$1$4$5");		
		
		//System.out.println("result 1");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(title=.*?)(\\}\\})";
			 
		info = info.replaceAll(pattern, "");
		
		pattern = "(\\{\\{)(.*?)(\\=)(.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "$1$4$5");
		
		//System.out.println("result 2");
		
		//System.out.println(info);

		pattern = "(\\{\\{)([^\\}]*)(\\|)([^\\}]*)(\\=)(.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "$6");
		
		//System.out.println("result 3");
		
		//System.out.println(info);			
		
		
		pattern = "(\\[\\[)([^\\]]*)(\\|)(.*?)(\\]\\])";
		 
		info = info.replaceAll(pattern, "$4");
		
		//System.out.println("result 4");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(Day.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 4a");
		
		//System.out.println(info);

		pattern = "(\\{\\{)(http.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 5");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(\\shttp.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 5a");
		
		//System.out.println(info);	
		
		
		pattern = "(\\{\\{)([^\\}]*)(\\|)(url\\=http.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");

		pattern = "(\\{\\{)([^\\}]*)(\\|)(url\\=\\shttp.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 6");
		
		//System.out.println(info);
		
		pattern = "(\\[\\[)(.*?)(\\]\\])";
		 
		info = info.replaceAll(pattern, "$2");
		
		//System.out.println("result 6");
		
		//System.out.println(info);
		
		
		pattern = "(\\{\\{)(.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "$2");
		
		//System.out.println("result 7");
		
		//System.out.println(info);
		
		//pattern = "(Street\\?\\|\\?\\|u\\?\\|d\\?)";
		//info = info.replaceAll(pattern, "");
		//System.out.println("result 9");
		//System.out.println(info);	
		
		//Specified process
		info = info.replace("File:Burnt Oak railway station 1951303 a449cb96.jpg|View SE, towards Golders Green and London in 1961 File:Burnt Oak stn northbound.JPG|Looking north File:Burnt Oak stn southbound.JPG|Looking south File:Burnt Oak roundel.JPG|", "");
		String toBeDeleted = "(?|v|?|k|s|??|l, VOK|sawl)";
		String toBeReplaced = "";
		info = info.replace(toBeDeleted, toBeReplaced);
		toBeDeleted = "?|?|u?|d?";
		info = info.replace(toBeDeleted, toBeReplaced);
		info = info.replace("|- |}", "");
		info = info.replace(";", ".");
		
		//System.out.println("result 10");
		
		//System.out.println(info);
		
		return info;
	}
	
	
}
