package traveldashboard.server.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.utils.StringUtils;


public class GtfsNetworkMessageParser {
	
	private static final String TAG = "GtfsNetworkMessageParser";
	
	public static synchronized ArrayList<StopInfo> parseMessage(byte[] data) {
		
		String methodTag = "parseMessage";
		
		// Parsing the response
		NetworkMessage responseMessage = new NetworkMessageXml();
		boolean result = responseMessage.decode(data);
	
		
		TdServerLogger.print(TAG, methodTag, "decode result = " + result);
		
		if (!result) {
			return null;
		}
		
		Vector<byte[]> stopIdVector = responseMessage.getKeyVector();
		HashMap<String, String> stopInfoMap = new HashMap<String, String>();
		List<String> stopIdList = new ArrayList<String>();
		int stopIdVectorSize = stopIdVector.size();
		
		TdServerLogger.print(TAG, methodTag, "stop id vector size = " + stopIdVectorSize);
		 
		for (int i = 0; i < stopIdVectorSize; i++) {
		        try {
		        	if ((i % 500) == 0) TdServerLogger.print(TAG, methodTag, "stop id vector size = " + i);
		        	
	                String stopIdKey = new String(stopIdVector.elementAt(i),
	                                StringUtils.UTF8);
	                String stopIdData = responseMessage.getPayload(i);
	                stopInfoMap.put(stopIdKey, stopIdData);
	
	                if (stopIdData.equals("ID")) {
	                        stopIdList.add(stopIdKey);
	                }
		        } catch (Exception e) {
		                e.printStackTrace();
		        }
		}
		
		ArrayList<StopInfo> stops = new ArrayList<StopInfo>();
		
		int index = 0;
		// Create StopInfo object
		for (String stopId : stopIdList) {
			
			index++;
			if ((index % 500) == 0) TdServerLogger.print(TAG, methodTag, "Paris bus metro info index = " + index);
			
		        String keyWordName = stopId + "_Name";
		        String keyWordLat = stopId + "_Lat";
		        String keyWordLon = stopId + "_Lon";
		        String keyWordRouteId = stopId + "_RouteId";
		        String keyWordRouteName = stopId + "_RouteName";
		        String keyWordDirection = stopId + "_Direction";
		
		        String name = stopInfoMap.get(keyWordName);
		        String routeId = stopInfoMap.get(keyWordRouteId);
		        String routeName = stopInfoMap.get(keyWordRouteName);
		        double latitude = Double.parseDouble(stopInfoMap.get(keyWordLat));
		        double longitude = Double.parseDouble(stopInfoMap.get(keyWordLon));
		        int direction = Integer.parseInt(stopInfoMap.get(keyWordDirection));
		        stops.add(new StopInfo(stopId, routeId, routeName, name, latitude,
		                        longitude, direction));
		}
		
		// Now sorting the stops in alphabetical order
		Collections.sort(stops, new Comparator<StopInfo>() {
		        @Override
		        public int compare(StopInfo o1, StopInfo o2) {
		                return o1.getStopName().compareTo(o2.getStopName());
		        }
		});
		
		return stops;
	}
}
