package traveldashboard.server.data.common;

import org.ibicoop.adaptation.AdaptationAction;
import org.ibicoop.adaptation.communication.RestartReceiverAction;
import org.ibicoop.adaptation.communication.StartReceiverAction;
import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiReceiver;
import org.ibicoop.communication.common.ReceiverListener;
import org.ibicoop.exceptions.MalformedIbiurlException;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.DataBuffer;

import com.google.gson.Gson;

import traveldashboard.data.core.CrowdSourcedData;
import traveldashboard.data.core.CrowdSourcedRequest;
import traveldashboard.data.core.CrowdSourcedValue;
import traveldashboard.data.core.HotSpot;
import traveldashboard.data.core.NameUtils;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.ResponseUtils;
import traveldashboard.server.data.TdServerLogger;

/**
 * This class is used to handle requests asked for crowdsourced data by mobile phones at a hotspot such as station.
 *
 * @author Cong-Kinh NGUYEN
 *
 * @param <C>
 * @param <H>
 * @param <V>
 * @param <R>
 */
public abstract class PullHandler<C extends CrowdSourcedData<H, V>, H extends HotSpot, V extends CrowdSourcedValue, R extends CrowdSourcedRequest> {

	private static final String TAG = PullHandler.class.getName();
	
	//Ibicoop
	private IbiReceiver ibiReceiver = null;
	private AdaptationAction action = null;

	private Class<C> clazz;
	private Class<R> csRequestClazz;

	/**
	 * Return crowdsourced data for a given request.
	 * @param request
	 * @return
	 */
	public abstract C response(R request);

	public PullHandler(Class<C> clazz, Class<R> csRequestClass) {
		this.clazz = clazz;
		this.csRequestClazz = csRequestClass;
	}
	
	private ReceiverListener userReceiverListener = new ReceiverListener() {

		@Override
		public byte[] receivedMessageRequest(IbiReceiver arg0, String arg1,
				int arg2, int arg3, byte[] data) {
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, "receivedMessageRequest", "Received an iBICOOP request: " + new String(data));
			NetworkMessage message = NetworkMessageXml.readMessage(data);
			
			Gson gson = new Gson();
			R csRequest = gson.fromJson(message.getPayload("0"), csRequestClazz);

			NetworkMessage resMsg = ResponseUtils.prepareMsgReturn(message);
			C obj = response(csRequest);
			if (obj == null) {
				return null;
			}

			resMsg.addPayload("0", gson.toJson(obj));

			return resMsg.encode();
		}

		@Override
		public void receivedMessageData(IbiReceiver arg0, String arg1,
				int arg2, DataBuffer arg3) {		
			if (DataConstants.DEBUG) TdServerLogger.print(TAG, "receivedMessageData","Received an iBICOOP message: " + new String(arg3.internalData, 0, arg3.dataLength));
		}

        @Override
        public void connectionStatus(IbiReceiver receiver, int statusCode) {
        	String methodTag = "connectionStatus";
        	
        	String message = "Receive connection status: " +
        			"receiver = " + receiver.getSource().toString() + ", " +
        			"statusCode = " + statusCode;

        	TdServerLogger.print(TAG, methodTag, message);
        	
            if (statusCode != CommunicationConstants.OK) {
                // We restart the receiver
            	TdServerLogger.print(TAG, methodTag, "Restart " + clazz.getName() + " handler");
            	
                try {
                    RestartReceiverAction action = new RestartReceiverAction(receiver);
                    IbicoopInit.getInstance().getAdaptationManager().addAction(action);
                } catch (Exception e) {
                	TdServerLogger.printError(TAG, methodTag, e.getMessage());
                }
            }  else if (statusCode == CommunicationConstants.OK) {
            	//Take the receiver if is ok
            	if (ibiReceiver == null) {
            		TdServerLogger.print(TAG, methodTag,"Take the callback bike handler receiver");
                    ibiReceiver = receiver;               	
            	}
            }
        }

		@Override
		public boolean acceptSenderConnection(IbiReceiver arg0, String arg1) {
			return true;
		}
	};
	

	public void startReceiver(){
		String methodTag = "startReceiver";
		
		try {
			String clazzName = NameUtils.getClazzName(clazz);
			TdServerLogger.print(TAG, methodTag,"Creating " + clazz.getName() + "handler...");
			
			System.out.println("Creating " + clazzName + "handler...");
			
			IBIURL uri = new IBIURL("ibiurl", DataConstants.USER,
					DataConstants.TERMINAL, DataConstants.APPLICATION, NameUtils.getService(clazzName), NameUtils.getPath(clazzName));
			
			CommunicationOptions options = new CommunicationOptions();
			options.setCommunicationMode(new CommunicationMode(
					CommunicationConstants.MODE_PROXY));

			String proxyAddress = IbicoopLoader.load().getProxyURL();
			action = new StartReceiverAction(uri, options, proxyAddress, userReceiverListener, false);
			
			IbicoopLoader.load().getAdaptationManager().addAction(action);			
		} catch (MalformedIbiurlException e) {
        	TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
	}
	
	public void stopReceiver() {
		if (ibiReceiver != null) ibiReceiver.stop();
	}
}
