package traveldashboard.server.data.common;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains all the pull handlers.
 * @author Cong-Kinh NGUYEN
 *
 */
public class PullHandlersApplication {

	private List<PullHandler> handlers;

	public PullHandlersApplication(Class<PullHandler> ... classes) {
			this.handlers = new ArrayList();
			for (Class<PullHandler> clazz : classes) {
				PullHandler tmp;
				try {
					tmp = clazz.newInstance();
					this.handlers.add(tmp);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
	}

	public void startReceivers() {
		for (PullHandler handler : handlers) {
			if (handler != null) {
			    handler.startReceiver();
			}
		}
	}

	public void stopReceivers() {
		for (PullHandler handler : handlers) {
			if (handler != null) {
			    handler.stopReceiver();
			}
		}
	}
}
