package traveldashboard.server.data.common;

import org.ibicoop.broker.common.BrokerManager;
import org.ibicoop.broker.common.EventNotificationListener;
import org.ibicoop.broker.common.IbiResource;
import org.ibicoop.broker.common.IbiResourceEvent;
import org.ibicoop.broker.common.IbiTopic;
import org.ibicoop.broker.common.IbiTopicEvent;
import org.ibicoop.broker.common.IbiTopicFilter;
import org.ibicoop.init.IbicoopLoader;
import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

import com.google.gson.Gson;

import traveldashboard.data.core.CrowdSourcedData;
import traveldashboard.data.core.CrowdSourcedValue;
import traveldashboard.data.core.HotSpot;
import traveldashboard.data.core.NameUtils;
import traveldashboard.server.data.DataConstants;
import traveldashboard.server.data.TdServerLogger;

/**
 * This class is used to handle data received at server when mobile phones update crowdsourced data.
 *
 * @author Cong-Kinh NGUYEN
 *
 * @param <C>
 * @param <H>
 * @param <V>
 */
public abstract class PushHandler <C extends CrowdSourcedData<H, V>, H extends HotSpot, V extends CrowdSourcedValue> {

	private static final String TAG = PushHandler.class.getName();

	private Class<C> csdClazz;

	/**
	 * This method will be called when mobile phones update crowsourced data to server.
	 * So, in the implementation of this method, you have to store your data or do anything necessary
	 * for processing them.
	 *
	 * @param data
	 */
	public abstract void receiveData(C data);

	public PushHandler(Class<C> clazz) {
		this.csdClazz = clazz;
	}

	/**
	 * Start listening notification.
	 */
	public void startNotification() {
		enableTopic(eventNotif);
	}

	/**
	 * Stop listening notification.
	 */
	public void stopNotification() {
		IbicoopLoader.load().getBrokerManager(getBrokerName()).stopNotification();
	}
	
	public void pauseNotification() {
		IbicoopLoader.load().getBrokerManager(getBrokerName()).pauseNotification();
	}
	
	private EventNotificationListener eventNotif = new EventNotificationListener() {

		@Override
		public boolean acceptNotificationFrom(String arg0) {
			return true;
		}

		@Override
		public void brokerStatus(String arg0, int arg1) {
		}

		@Override
		public void receiveEvent(IbiTopic ibiTopic, IbiTopicEvent event) {
			String eventName = event.getName();
			if (eventName == null || !eventName.equals(getEventName())) return;
			
			String methodTag = "receiveEvent";

			if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "This is really an event for " + csdClazz.getName());
			
			//Get crowd event network message
			NetworkMessage message = NetworkMessageXml.readMessage(event.getData());
			
			//Process message if not null
			if (message != null) {
				
				Gson gson = new Gson();
				
				C obj = gson.fromJson(message.getPayload("0"), csdClazz);

				if (obj == null) return;
				
				receiveData(obj);
			} else {
				if (DataConstants.DEBUG) TdServerLogger.printError(TAG, methodTag, "Network message is null!");
			}
		}

		@Override
		public void receiveEvent(IbiTopic arg0, IbiResourceEvent arg1) {
		}

		@Override
		public void receiveEvent(IbiResource arg0, IbiResourceEvent arg1) {
		}
	};

	/**
	 * Enable ibitopic if not created, else get the topic, and subscribe to the topic 
	 * @return
	 */
	private synchronized IbiTopic enableTopic(EventNotificationListener eventListener) {
		
        TdServerLogger.print(TAG, "enableTopic", "Enable " + getTopicName());
		
		IbiTopic ibiTopic = getTopic();
		
		if (ibiTopic == null) {
			ibiTopic = createNoiseTopic();
		}
		
		//Subscribe to the ibitopic
		ibiTopic.subscribe();

		//Start event notification
		IbicoopLoader.load().getBrokerManager(getBrokerName())
				.startNotification(eventListener);
		
		return ibiTopic;
	}
	
	/**
	 * Get noise topic
	 * @return Noise topic
	 */
	private IbiTopic getTopic() {
		
		String methodTag = "getTopic";
		
        TdServerLogger.print(TAG, methodTag, "Getting " + getTopicName());
        
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(getBrokerName());
		IbiTopicFilter topicFilter = brokerManager.createIbiTopicFilter();
		String topicName = getTopicName();
		String topicType = getTopicType();
	    TdServerLogger.print(TAG, methodTag, "Get topic name : " + topicName + ", topic type : " + topicType);
		topicFilter.setName(topicName);
		topicFilter.setType(topicType);
		IbiTopic[] topics = brokerManager.getTopics(topicFilter);
		
		if (topics == null || topics.length == 0) {
	        TdServerLogger.printError(TAG, methodTag, "Topic is null!");
			return null;
		} else {
	        TdServerLogger.print(TAG, methodTag, "Found topic");
			return topics[0];
		}
	}
	
	/**
	 * Create topic
	 * @return ibi topic
	 */
	private IbiTopic createNoiseTopic() {
		
		String methodTag = "createTopic";
		
        TdServerLogger.print(TAG, methodTag, "Creating " + getTopicName());		
		
		BrokerManager brokerManager = IbicoopLoader.load().getBrokerManager(getBrokerName());
		String topicName = getTopicName();
		String topicType = getTopicType();
		
        TdServerLogger.print(TAG, methodTag, "Topic name : " + topicName + ", topic type : " + topicType);
		
        IbiTopic ibitopic = brokerManager.createIbiTopic(topicName, topicName);
		ibitopic.setType(topicType);
		ibitopic.registerTopic();
		return ibitopic;
	}

	/**
	 * Gets topic name.
	 * @return
	 */
	private String getTopicName() {
		return NameUtils.getTopicName(NameUtils.getClazzName(csdClazz));
	}

	/**
	 * Gets topic type.
	 * @return
	 */
	private String getTopicType() {
		return NameUtils.getTopicType(NameUtils.getClazzName(csdClazz));
	}

	/**
	 * Gets event name.
	 * @return
	 */
	private String getEventName() {
		return NameUtils.getEventName(NameUtils.getClazzName(csdClazz));
	}

	/**
	 * Gets broker manager name.
	 * @return
	 */
	private String getBrokerName() {
		return NameUtils.getBrokerName(NameUtils.getClazzName(csdClazz));
	}
}
