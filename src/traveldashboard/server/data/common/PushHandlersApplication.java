package traveldashboard.server.data.common;

import java.util.ArrayList;
import java.util.List;

import org.ibicoop.init.IbicoopLoader;

import traveldashboard.server.data.DataConstants;

/**
 * This class contains all the push handlers.
 *
 * @author Cong-Kinh NGUYEN
 *
 */
public class PushHandlersApplication {

	private List<PushHandler> handlers;

	public PushHandlersApplication(Class<PushHandler> ... classes) {
		this.handlers = new ArrayList();
		for (Class<PushHandler> clazz : classes) {
			PushHandler tmp;
			try {
				tmp = clazz.newInstance();
				this.handlers.add(tmp);
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}
	}

	public void startNotification() {
		for (PushHandler handler: handlers) {
			handler.startNotification();
		}
	}

	public void stopNotification() {
		for (PushHandler handler: handlers) {
			handler.stopNotification();
		}
	}

	public boolean pauseNotification() {
		for (PushHandler handler: handlers) {
			handler.pauseNotification();
		}
		return true;
	}
}
