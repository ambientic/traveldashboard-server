package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.onebusaway.gtfs.model.Stop;

import traveldashboard.data.BikeStation;
import traveldashboard.data.BikeStationsCollection;
import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.BusStationsCollection;
import traveldashboard.data.Tram;
import traveldashboard.data.TramStation;
import traveldashboard.data.TramStationsCollection;
import traveldashboard.data.TransportArea;
import traveldashboard.data.Tube;
import traveldashboard.data.TubeStation;
import traveldashboard.data.TubeStationsCollection;
import traveldashboard.data.comment.TubeStationComment;
import traveldashboard.data.comment.TubeStationCommentsCollection;
import traveldashboard.data.crowd.Crowd;
import traveldashboard.data.crowd.TubeStationCrowd;
import traveldashboard.data.crowd.TubeStationCrowdsCollection;
import traveldashboard.data.incentive.Incentive;
import traveldashboard.data.incentive.IncentiveType;
import traveldashboard.data.noise.NoiseData;
import traveldashboard.data.noise.NoiseDatasCollection;
import traveldashboard.data.rating.Rating;
import traveldashboard.data.rating.TubeRating;
import traveldashboard.data.rating.TubeRatingsCollection;
import traveldashboard.data.status.TubeStatus;
import traveldashboard.data.status.TubeStatusCollection;

import au.com.bytecode.opencsv.CSVReader;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;


public class MongoDbManager {

	private static final boolean DEBUG = DataConstants.DEBUG;
	private static final String TAG = "MongoDbManager";
	
	//
	//
	//Constant time interval
	private static final String timeList[] = {
		"0500-0515", "0515-0530", "0530-0545", "0545-0600",
		"0600-0615", "0615-0630", "0630-0645", "0645-0700",
		"0700-0715", "0715-0730", "0730-0745", "0745-0800",
		"0800-0815", "0815-0830", "0830-0845", "0845-0900",
		"0900-0915", "0915-0930", "0930-0945", "0945-1000",
		"1000-1015", "1015-1030", "1030-1045", "1045-1100",
		"1100-1115", "1115-1130", "1130-1145", "1145-1200",
		"1200-1215", "1215-1230", "1230-1245", "1245-1300",
		"1300-1315", "1315-1330", "1330-1345", "1345-1400",
		"1400-1415", "1415-1430", "1430-1445", "1445-1500",
		"1500-1515", "1515-1530", "1530-1545", "1545-1600",
		"1600-1615", "1615-1630", "1630-1645", "1645-1700",
		"1700-1715", "1715-1730", "1730-1745", "1745-1800",
		"1800-1815", "1815-1830", "1830-1845", "1845-1900",
		"1900-1915", "1915-1930", "1930-1945", "1945-2000",	
		"2000-2015", "2015-2030", "2030-2045", "2045-2100",
		"2100-2115", "2115-2130", "2130-2145", "2145-2200",
		"2200-2215", "2215-2230", "2230-2245", "2245-2300",
		"2300-2315", "2315-2330", "2330-2345", "2345-2400",
		"0000-0015", "0015-0030", "0030-0045", "0045-0100",
		"0100-0115", "0115-0130", "0130-0145", "0145-0200"
	};

	
	
	
	
	
	//
	//
	//Get database and table names from xml configuration file
	// key = city, value = db profil
	private static HashMap<String, TdDbProfil> tdDbProfils; 
	// key = city, value = database
	private static HashMap<String, DB> dbMap;
	
	/**
	 * Initialize db from xml configuration file
	 * @param context
	 * @return True if init ok, false else
	 */
	public static boolean initDbConfiguration(InputStream is) {
		String methodTag = "initDbConfiguration";
		
		//Access database configuration xml file: td_db_config.xml
		boolean init = false;
		
		try {
			//Initialize mongo db
			Mongo mongo = new Mongo("localhost", 27017);
			
			//Parse XML information into String
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();

			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
			
			br.close();

            if (DEBUG) TdServerLogger.print(TAG, methodTag, sb.toString());
			
			
			//Create new map
			tdDbProfils = new HashMap<String, TdDbProfil>();
			dbMap = new HashMap<String, DB>();
			
			//
			//
			//Extract information from XML String
			//Database profils map, key = city name, value = database profil	
			String td_config_Xml = sb.toString();
		    // Convert XML to JSON Object
            JSONObject td_config_JSONObj = XML.toJSONObject(td_config_Xml);
            JSONObject db_config_JSONObj = td_config_JSONObj.getJSONObject("dbConfig");
            JSONArray cityArray = db_config_JSONObj.getJSONArray("city");
            for (int i = 0; i < cityArray.length(); i++) {
                JSONObject city_JSONObj = cityArray.getJSONObject(i);
                //cityName
                String cityName = city_JSONObj.getString("name");
                
                //dbName
                String dbName = city_JSONObj.getJSONObject("dbName").getString("name");
                
                //tabNames
                JSONObject tabNames_JSONObj = city_JSONObj.getJSONObject("tabNames");
                String bike = tabNames_JSONObj.getJSONObject("bike").getString("name");
                String bus = tabNames_JSONObj.getJSONObject("bus").getString("name");
                String metro = tabNames_JSONObj.getJSONObject("metro").getString("name");
                String rail = tabNames_JSONObj.getJSONObject("rail").getString("name");
                String tram = tabNames_JSONObj.getJSONObject("tram").getString("name");
                String noise = tabNames_JSONObj.getJSONObject("noise").getString("name");
                String classifiedNoise = tabNames_JSONObj.getJSONObject("classifiedNoise").getString("name");
                String classifiedCrowd = tabNames_JSONObj.getJSONObject("classifiedCrowd").getString("name");
                String generalCrowd = tabNames_JSONObj.getJSONObject("generalCrowd").getString("name");
                String userRatings = tabNames_JSONObj.getJSONObject("userRatings").getString("name");
                String generalRatings = tabNames_JSONObj.getJSONObject("generalRatings").getString("name");
                String comments = tabNames_JSONObj.getJSONObject("comments").getString("name");
                String status = tabNames_JSONObj.getJSONObject("status").getString("name");
                String fact = tabNames_JSONObj.getJSONObject("fact").getString("name");
                String joke = tabNames_JSONObj.getJSONObject("joke").getString("name");
                
                TdDbProfil profil = new TdDbProfil(
                		cityName, 
                		dbName, 
                		bike, 
                		bus, 
                		metro, 
                		rail,
                		tram,
                		noise,
                		classifiedNoise,
                		classifiedCrowd, 
                		generalCrowd, 
                		userRatings, 
                		generalRatings, 
                		comments,
                		status,
                		fact,
                		joke
                		);
 
                if (DEBUG) TdServerLogger.print(TAG, methodTag, profil.toString());
                
                tdDbProfils.put(cityName, profil);
                
                DB database = mongo.getDB(dbName);
                dbMap.put(cityName, database);
         }
			
			init = true;
			
			
			TdServerLogger.outLog(TAG, methodTag, "Init ok");
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
			TdServerLogger.outLogError(TAG, methodTag, "Init failed");
		}

		if (DEBUG) TdServerLogger.print(TAG, methodTag, "tdDbProfils size =" + tdDbProfils.size());
		if (DEBUG) TdServerLogger.print(TAG, methodTag, "dbMap size =" + dbMap.size());
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "success = " + init);
        
		return init;
	}
	
	
	/**
	 * Drop all databases
	 * @return True if drop ok, false if drop not ok
	 */
	public static boolean dropAllDb() {
		String methodTag = "dropAllDb";
		
		boolean drop = false;
		
		if (dbMap != null) {
			Set<String> dbKeys = dbMap.keySet();
			
			for (String dbKey: dbKeys) {
				DB database = dbMap.get(dbKey);
				database.dropDatabase();
			}
			dbMap.clear();
		}
		
		if (tdDbProfils != null) {
			tdDbProfils.clear();
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "success = " + drop);
		
		return drop;
	}
	
 
	
    /**
     * Insert GTFS stops data into database
     * @param city paris or london
     * @param roadType 0 for tram, 1 for metro, 2 for rail or 3 for bus stops
     * @param stoplist Stop list
     */
	public static void insertGtfsStopsData(String city, int roadType, Set<Stop> stoplist) {
		String methodTag = "insertGtfsStopsData";
		
		DBCollection table;
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, dbProfil.toString());
        if (DEBUG) TdServerLogger.print(TAG, methodTag, city);	
		
		switch(roadType) {
			case 0: 
				//Tram
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, dbProfil.getTabTram());
				table = database.getCollection(dbProfil.getTabTram());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "tram stops db count = " + table.count());
				break;
			case 1:
				//Metro
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, dbProfil.getTabMetro());
				table = database.getCollection(dbProfil.getTabMetro());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "metro stops db count = " + table.count());
		        break;
			case 2:
				//Rail
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, dbProfil.getTabRail());
				table = database.getCollection(dbProfil.getTabRail());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "rail stops db count = " + table.count());
				break;
			case 3:
				//Bus
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, dbProfil.getTabBus());
				table = database.getCollection(dbProfil.getTabBus());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "bus stops db count = " + table.count());
				break;
			default:
				//Not a available transport type for us!
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "transport type not available!");
				return;
		}
		
        for (Stop tmpStop : stoplist) {
			
        	String stopId = tmpStop.getId().getId();
        	
            // if invalid id, then continue
            if (stopId == null || stopId.equals("")) continue;
            
            String routeId = tmpStop.getRouteId();
            String routeName = tmpStop.getRouteName();
            String stopName = tmpStop.getName();
            double latitude = tmpStop.getLat();
            double longitude = tmpStop.getLon();
            String direction = tmpStop.getDirection();

            String stopString =  "stopId = " + stopId + ", " + 
			            		"routeId = " + routeId + ", " +
			            		"routeName = " + routeName + ", " +
			            		"stopName = " + stopName + ", " +
			            		"latitude= " + latitude + ", " +
			            		"longitude = " + longitude + ", " +
			            		"direction = " + direction;

	        if (DEBUG) TdServerLogger.print(TAG, methodTag, stopString);
            
			BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
            
        	BasicDBObject stopInfo = new BasicDBObject
        			(DataConstants.PARAM_KEY_ROUTE_ID, routeId).
					append(DataConstants.PARAM_KEY_ROUTE_NAME, routeName).
					append(DataConstants.PARAM_KEY_STOP_NAME, stopName).
					append(DataConstants.PARAM_KEY_STOP_LON, longitude).
					append(DataConstants.PARAM_KEY_STOP_LAT, latitude).
					append(DataConstants.PARAM_KEY_DIRECTION_ID, direction)
					;
			
			BasicDBObject setCommand = new BasicDBObject();
			setCommand.put("$set", stopInfo);
			table.update(stopQuery, setCommand, true, true);
        }
        
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Insert GTFS data into db ok: table count = " + table.count());
	}
	
	/**
	 * Get GTFS data stops from database
	 * @param city Paris or London
	 * @param roadType
	 * @return Stops
	 */
	public static List<StopForNetworkMessage> getAllStopsFor(String city, int roadType) {
		String methodTag = "getAllStopsFor";
		
		DBCollection table;
		List<StopForNetworkMessage> stopsFromDb = new ArrayList<StopForNetworkMessage>();
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, city);	
		
		switch(roadType) {
			case 0: 
				//Tram
				table = database.getCollection(dbProfil.getTabTram());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "tram stops db count = " + table.count());			
				break;
			case 1:
				//Metro
				table = database.getCollection(dbProfil.getTabMetro());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "metro stops db count = " + table.count());
				break;
			case 2:
				//Rail
				table = database.getCollection(dbProfil.getTabRail());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "rail stops db count = " + table.count());

				break;
			case 3:
				//Bus
				table = database.getCollection(dbProfil.getTabBus());
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "bus stops db count = " + table.count());
				break;
			default:
				//Not a available transport type for us!
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "transport type not available!");				
				return stopsFromDb;
		}
		
		DBCursor cursor = table.find();

		try {
		   while(cursor.hasNext()) {
		       DBObject stopInfo = cursor.next();
		       StopForNetworkMessage stop = new StopForNetworkMessage(
		    		   (String) stopInfo.get(DataConstants.PARAM_VALUE_STOP_ID), 
		    		   (String) stopInfo.get(DataConstants.PARAM_KEY_ROUTE_ID), 
		    		   (String) stopInfo.get(DataConstants.PARAM_KEY_ROUTE_NAME), 
		    		   (String) stopInfo.get(DataConstants.PARAM_KEY_STOP_NAME),  
		    		   (Double) stopInfo.get(DataConstants.PARAM_KEY_STOP_LON),
		    		   (Double) stopInfo.get(DataConstants.PARAM_KEY_STOP_LAT),
		    		   (String) stopInfo.get(DataConstants.PARAM_KEY_DIRECTION_ID)
		    		   );
		       stopsFromDb.add(stop);
		   }
		} finally {
		   cursor.close();
		}
		
		return stopsFromDb;
	}
	
	
	/**
	 * Get tram stops database size
	 * @param city Paris or London
	 * @return Tram stops databse size
	 */
	public static long getTramStopsSize(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabTram());
		return table.count();
	}
	
	/**
	 * Reset table for tram stops
	 * @param city Paris or London
	 * @return True of reset ok, false else
	 */
	public static boolean resetTramsDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabTram());
		table.drop();
		return (table.count() == 0);
	}
	
	/**
	 * Get metro stops database size
	 * @param city Paris or London
	 * @return Metro stops databse size
	 */
	public static long getMetroStopsSize(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabMetro());
		return table.count();
	}
	
	/**
	 * Reset metros db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetMetrosDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabMetro());
		table.drop();
		return (table.count() == 0);
	}
	
	/**
	 * Get rail stops database size
	 * @param city Paris or London 
	 * @return Rail stops databse size
	 */
	public static long getRailStopsSize(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabRail());		
		return table.count();
	}	
	
	/**
	 * Reset rails db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetRailsDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabRail());
		table.drop();
		return (table.count() == 0);
	}
	
	/**
	 * Get bus stops database size
	 * @param city Paris or London 
	 * @return Bus stops databse size
	 */
	public static long getBusStopsSize(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);	
		DBCollection table = database.getCollection(dbProfil.getTabBus());
		return table.count();
	}
	
	/**
	 * Reset bus db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetBusDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabBus());
		table.drop();
		return (table.count() == 0);	
	}
	
	/**
	 * Reset all tables
	 * @param city Paris or London 
	 * @return True if reset ok, false else
	 */
	public static boolean resetAllTables(String city) {
        if (DEBUG) TdServerLogger.print(TAG, "resetAllTables", city);	
		return (resetTramsDb(city) && resetMetrosDb(city) && resetRailsDb(city) && resetBusDb(city));
	}
	
	/**
	 * Return true if need to parse gtfs data, else false
	 * @param city Paris or London 
	 * @return True if need to parse gtfs data, else false
	 */
	public static boolean needToParseGtfsData(String city) {
		return ((getTramStopsSize(city) + getMetroStopsSize(city) + getRailStopsSize(city) + getBusStopsSize(city)) == 0);
	}
	
	
	
	
	//Reset db
	/**
	 * Reset bike db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetBikeDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabBike());
		table.drop();
		return (table.count() == 0);	
	}
	
	
	/**
	 * Reset joke db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetJokeDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabJoke());
		table.drop();
		return (table.count() == 0);
	}
		
	
	
	/**
	 * Reset fact db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetFactDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabFact());
		table.drop();
		return (table.count() == 0);	
	}
	
	/**
	 * Reset noise db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetNoiseDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabNoise());
		table.drop();
		return (table.count() == 0);	
	}
	
	
	/**
	 * Reset classified noise db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetClassifiedNoiseDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabClassifiedNoise());
		table.drop();
		return (table.count() == 0);
	}
	
	
	/**
	 * Reset classified crowd db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetClassifiedCrowdDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabClassifiedCrowd());
		table.drop();
		return (table.count() == 0);	
	}
	
	/**
	 * Reset general crowd db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetGeneralCrowdDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabGeneralCrowd());
		table.drop();
		return (table.count() == 0);	
	}	
	
	
	/**
	 * Reset user ratings db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetUserRatingsDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabUserRatings());
		table.drop();
		return (table.count() == 0);	
	}
	
	/**
	 * Reset general ratings db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetGeneralRatingsDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabGeneralRatings());
		table.drop();
		return (table.count() == 0);	
	}
	
	
	/**
	 * Reset comments db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetCommentsDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabComments());
		table.drop();
		return (table.count() == 0);	
	}
	
	/**
	 * Reset status db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetStatusDb(String city) {
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabStatus());
		table.drop();
		return (table.count() == 0);	
	}
	
	/**
	 * Reset all db
	 * @param city Paris or London 
	 * @return True of reset ok, false else
	 */
	public static boolean resetOthersDb(String city) {
		return (resetBikeDb(city) && resetNoiseDb(city) && resetClassifiedCrowdDb(city) && resetGeneralCrowdDb(city) 
				&& resetUserRatingsDb(city) && resetGeneralRatingsDb(city) && resetCommentsDb(city) && resetStatusDb(city)
				&& resetFactDb(city) && resetJokeDb(city)
				);	
	}
	
	
	//
	//
	//Noise
	private static final String NOISE_STOP_RADIUS = "200";
	
	/**
	 * Insert noise data into database
	 * @param city
	 * @param latitude
	 * @param longitude
	 * @param noiseLevel
	 */
	public static void insertNoiseData(long timestamp, double latitude, double longitude, int noiseLevel) {
		String methodTag = "insertNoiseData";
				
		String city = TransportArea.getArea(latitude, longitude);
		
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabNoise());
		
		String info = "Noise table count = " + table.count()
				+ ", insert noise data: latitude = " + latitude 
				+ ", longitude = " + longitude 
				+ ", noiseLevel = " + noiseLevel
				+ ", city = " + city;
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, info);	
		
		BasicDBObject noiseData = new BasicDBObject(
				"timestamp", timestamp).
                append("latitude", latitude).
                append("longitude", longitude).
                append("noiseLevel", noiseLevel);
		table.insert(noiseData);
		info = "Noise table new count = " + table.count();
		
		TubeStationsCollection tubeStationsCollection = null;
		//Update classified noise
		if (city.equals(DataConstants.LONDON)) {
			tubeStationsCollection = 
					getLondonMetroStationsCollection(String.valueOf(latitude), String.valueOf(longitude), NOISE_STOP_RADIUS);
		} else if (city.equals(DataConstants.PARIS)) {
			tubeStationsCollection =  getParisMetroStationsCollection(String.valueOf(latitude), String.valueOf(longitude), NOISE_STOP_RADIUS);
		}
		
		if (tubeStationsCollection != null) {
			TubeStation[] tubeStationsArray = tubeStationsCollection.getTubeStations();
			
			for (TubeStation station: tubeStationsArray) {
				
				Tube[] tubes = station.getTubes();
				
				for (Tube tube: tubes) {
					updateClassifiedNoiseDb(city, station.getId(), tube.getId(), convertTimestampToTimeQueryString(timestamp), noiseLevel);
				}

			}
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, info);
	}
	

	/**
	 * Get noise data
	 * @param latitude
	 * @param longitude
	 * @return Noise data
	 */
	public static NoiseData getNoiseData(double latitude, double longitude) {
		
		String methodTag = "getNoiseData";
		
		String city = TransportArea.getArea(latitude, longitude);
		
	    if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get noise data for city = " + city);
		
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabNoise());
		
		ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
		noiseConditionQuery.add(new BasicDBObject("latitude", latitude));
		noiseConditionQuery.add(new BasicDBObject("longitude", longitude));
		
		BasicDBObject tubesQuery = new BasicDBObject("$and", noiseConditionQuery);
		
		DBCursor cursor = table.find(tubesQuery);
		
		JSONArray jsonArray = new JSONArray();
		
		NoiseData noiseData = null;
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Cursor count = " + cursor.count());
			
			try {
				   while(cursor.hasNext()) {
				   jsonArray.put(cursor.next());
				   }
				} finally {
				   cursor.close();
				}
		try {
			JSONObject obj = new JSONObject(jsonArray.getString(jsonArray.length() - 1));
			
			long timestamp = obj.getLong("timestamp");
			int noiseLevel = obj.getInt("noiseLevel");
			
			noiseData = new NoiseData(timestamp, latitude, longitude, noiseLevel, 1);
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}

		
	    if (DEBUG) TdServerLogger.print(TAG, methodTag, noiseData.toString());
		
	    return noiseData;
	}
	

	/**
	 * Get noise datas collection with interval
	 * @param timestamp
	 * @param interval
	 * @return
	 */
	public static NoiseDatasCollection getNoiseDatasWithInterval(String city, long timestamp, long interval) {
		String methodTag = "getNoiseDatasWithInterval";
		
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get noise data with interval for city = " + city);
		
		NoiseDatasCollection noiseDatasCollection = null;
		JSONArray jsonArray = new JSONArray();
        
        try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
    		DBCollection table = database.getCollection(dbProfil.getTabNoise());
    		
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Noise table count = " + table.count());
  			
  			//Timestamp bound
  			BasicDBObject timestampGte = new BasicDBObject("$gte", (timestamp - interval));
  			BasicDBObject noiseTimestamp = new BasicDBObject("timestamp", timestampGte);

  			DBCursor cursor = table.find(noiseTimestamp);
  			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Cursor count = " + cursor.count());
  			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
			
			NoiseData[] noiseDatas = new NoiseData[jsonArray.length()];
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				
				long time = obj.getLong("timestamp");
				double lat = obj.getDouble("latitude");
				double lon = obj.getDouble("longitude");
				int noiseLevel = obj.getInt("noiseLevel");
			
				NoiseData noiseData = new NoiseData(time, lat, lon, noiseLevel, 1);
				
				noiseDatas[i] = noiseData;
			}
			
			noiseDatasCollection = new NoiseDatasCollection(noiseDatas);
	
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		noiseDatasCollection.toString());			
			
        } catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
        }

		return noiseDatasCollection;
	}
	
	
	/**
	 * Get noise datas collection in region
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return Noise data collection
	 */
	public static NoiseDatasCollection getNoiseDatasInRegion(double latitude, double longitude, double radius) {
		
		String methodTag = "getNoiseDatasInRegion";
		
		String city = TransportArea.getArea(latitude, longitude);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get noise datas in region for city = " + city);
		
		NoiseDatasCollection noiseDatasCollection = null;
		JSONArray jsonArray = new JSONArray();
		
        double maxLat = latitude + (radius/111000);
        double minLat = latitude - (radius/111000);
        double maxLon = longitude + (radius/72000);
        double minLon = longitude - (radius/72000);
        
        try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
    		DBCollection table = database.getCollection(dbProfil.getTabNoise());
    		
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Noise table count = " + table.count());

  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject("latitude", latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject("latitude", latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject("longitude", lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject("longitude", lonLt);
  			
  			ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
  			noiseConditionQuery.add(latSup);
  			noiseConditionQuery.add(latInf);
  			noiseConditionQuery.add(lonSup);
  			noiseConditionQuery.add(lonInf);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", noiseConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);
  			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Cursor count = " + cursor.count());
  			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
			
			NoiseData[] noiseDatas = new NoiseData[jsonArray.length()];
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				
				long timestamp = obj.getLong("timestamp");
				double lat = obj.getDouble("latitude");
				double lon = obj.getDouble("longitude");
				int noiseLevel = obj.getInt("noiseLevel");
			
				NoiseData noiseData = new NoiseData(timestamp, lat, lon, noiseLevel, 1);
				
				noiseDatas[i] = noiseData;
			}
			
			noiseDatasCollection = new NoiseDatasCollection(noiseDatas);
	
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		noiseDatasCollection.toString());			
			
        } catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
        }

		return noiseDatasCollection;
	}
	
	
	/**
	 * Get noise datas collection with interval
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @param timestamp
	 * @param interval
	 * @return
	 */
	public static NoiseDatasCollection getNoiseDatasInRegionWithInterval(double latitude, double longitude, double radius, long timestamp, long interval) {
		
		String methodTag = "getNoiseDatasInRegionWithInterval";
		
		String city = TransportArea.getArea(latitude, longitude);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get noise datas in region with interval for city = " + city);
		
		NoiseDatasCollection noiseDatasCollection = null;
		JSONArray jsonArray = new JSONArray();
		
        double maxLat = latitude + (radius/111000);
        double minLat = latitude - (radius/111000);
        double maxLon = longitude + (radius/72000);
        double minLon = longitude - (radius/72000);
        
        try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
    		DBCollection table = database.getCollection(dbProfil.getTabNoise());
    		
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Noise table count = " + table.count());

  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject("latitude", latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject("latitude", latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject("longitude", lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject("longitude", lonLt);
  			
  			//Timestamp bound
  			BasicDBObject timestampGte = new BasicDBObject("$gte", (timestamp - interval));
  			BasicDBObject noiseTimestamp = new BasicDBObject("timestamp", timestampGte);
  			
  			ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
  			noiseConditionQuery.add(latSup);
  			noiseConditionQuery.add(latInf);
  			noiseConditionQuery.add(lonSup);
  			noiseConditionQuery.add(lonInf);
  			noiseConditionQuery.add(noiseTimestamp);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", noiseConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);
  			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Cursor count = " + cursor.count());
  			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
			
			NoiseData[] noiseDatas = new NoiseData[jsonArray.length()];
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				
				long time = obj.getLong("timestamp");
				double lat = obj.getDouble("latitude");
				double lon = obj.getDouble("longitude");
				int noiseLevel = obj.getInt("noiseLevel");
			
				NoiseData noiseData = new NoiseData(time, lat, lon, noiseLevel, 1);
				
				noiseDatas[i] = noiseData;
			}
			
			noiseDatasCollection = new NoiseDatasCollection(noiseDatas);
	
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		noiseDatasCollection.toString());			
			
        } catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
        }

		return noiseDatasCollection;
		
		
	}
	
	/**
	 * Update classified noise db
	 * @param stopId
	 * @param queryTime
	 * @param newLevelString
	 */
	public static boolean updateClassifiedNoiseDb(String city, String stopId, String routeId, String queryTime, int level) {
		
		String methodTag = "updateClassifiedNoiseDb";
		boolean success = false;
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Updating classified noise data for " + city);
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabClassifiedNoise());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise data table count = " + table.count());
			
			ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
			noiseConditionQuery.add(new BasicDBObject("stopId", stopId));
			noiseConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject noiseQuery = new BasicDBObject("$and", noiseConditionQuery);
	        
			DBObject cursor = table.findOne(noiseQuery);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************Before update cursor = ****************");
				TdServerLogger.print(TAG, methodTag, "" + cursor);
			}
			
			double oldLevel = 0.0;
			int nbVotes = 0;
			double newLevel = 0.0;
			
			if (cursor != null) {
				oldLevel = (Double) cursor.get(extractTime(queryTime));
				nbVotes = ((Integer) cursor.get(extractTime(queryTime) + "-VOTES"));	
				newLevel = (double) level;
			}
			
			double updatedLevel = (double) (oldLevel*nbVotes + newLevel)/(nbVotes + 1);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "Ancien number of votes = " + nbVotes);
				TdServerLogger.print(TAG, methodTag, "New level = " + newLevel);
				TdServerLogger.print(TAG, methodTag, "Updated level = " + updatedLevel);			
			}
			
			BasicDBObject updateInfo = new BasicDBObject(extractTime(queryTime), updatedLevel).append(extractTime(queryTime) + "-VOTES", (nbVotes + 1));
			BasicDBObject updateObj = new BasicDBObject();
			updateObj.put("$set", updateInfo);
			table.update(noiseQuery, updateObj, false, true);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************After update cursor = ****************");
				cursor = table.findOne(noiseQuery);
				TdServerLogger.print(TAG, methodTag, "" + cursor);
			}
			
			success = true;
			
		} catch (Exception e) {
			TdServerLogger.print(TAG, methodTag, e.getMessage());
		}
		
		return success;
	}	
	
	
	/**
	 * Get classified noise
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @return NoiseData
	 */
	public static NoiseData getMetroClassifiedNoise(String city, String stopId, String routeId,  long timestamp) {
		
		String methodTag = "getMetroClassifiedNoise";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get classified noise for " + city + ", stopId = " + stopId + ", routeId = " + routeId);
        
		NoiseData noiseData = null;	
		
		try {
			
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabClassifiedNoise());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise table count = " + table.count());			
			
			ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
			noiseConditionQuery.add(new BasicDBObject("stopId", stopId));
			noiseConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject noiseQuery = new BasicDBObject("$and", noiseConditionQuery);
			
			DBObject cursor = table.findOne(noiseQuery);
			
			String queryTime = convertTimestampToTimeQueryString(timestamp);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************Before get classified noise cursor = ****************");
				TdServerLogger.print(TAG, methodTag, "" + cursor);
		        TdServerLogger.print(TAG, methodTag, "query time = " + queryTime);
			}
	        
			int level = (int) Math.round((Double) cursor.get(extractTime(queryTime)));
			int votes =  ((Integer) cursor.get(extractTime(queryTime) + "-VOTES"));
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise level = " + level + ", votes = " + votes);	
	        
	        TubeStation tubeStation = null;
	        
	        if (city.equals(DataConstants.PARIS)) {
	        	//Paris
	        	tubeStation = getParisMetroStation(stopId);
	        } else if (city.equals(DataConstants.LONDON)) {
	        	//London
	        	tubeStation = getLondonMetroStation(stopId);
	        }

	        noiseData = new NoiseData(timestamp, tubeStation.getLatitude(), tubeStation.getLongitude(), level, votes);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "noise data = " + noiseData.toString());
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return noiseData;
	}	
	
	/**
	 * Get classified noise by query time
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param queryTime: HHmm
	 * @return NoiseData
	 */
	public static NoiseData getMetroClassifiedNoiseByQueryTime(String city, String stopId, String routeId, String queryTime) {
		
		String methodTag = "getMetroClassifiedNoiseByQueryTime";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get classified noise by query time for " + city + ", stopId = " + stopId + ", routeId = " + routeId);
        
		NoiseData noiseData = null;	
		
		try {
			
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabClassifiedNoise());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise table count = " + table.count());			
			
			ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
			noiseConditionQuery.add(new BasicDBObject("stopId", stopId));
			noiseConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject noiseQuery = new BasicDBObject("$and", noiseConditionQuery);
			
			DBObject cursor = table.findOne(noiseQuery);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************Before get classified noise cursor = ****************");
				TdServerLogger.print(TAG, methodTag, "" + cursor);
		        TdServerLogger.print(TAG, methodTag, "query time = " + queryTime);
			}
	        
			int level = (int) Math.round((Double) cursor.get(extractTime(queryTime)));
			int votes =  ((Integer) cursor.get(extractTime(queryTime) + "-VOTES"));
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise level = " + level + ", votes = " + votes);	
	        
	        TubeStation tubeStation = null;
	        
	        if (city.equals(DataConstants.PARIS)) {
	        	//Paris
	        	tubeStation = getParisMetroStation(stopId);
	        } else if (city.equals(DataConstants.LONDON)) {
	        	//London
	        	tubeStation = getLondonMetroStation(stopId);
	        }

	        noiseData = new NoiseData(convertDateStringToTimestamp(extractQueryTimeToCurrentTime(queryTime)), tubeStation.getLatitude(), tubeStation.getLongitude(), level, votes);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "noise data = " + noiseData.toString());
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return noiseData;
	}		
	
	
	/**
	 * Get tube station general noise
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @param interval
	 * @return
	 */
	public static NoiseData getMetroNoiseData(String city, double latitude, double longitude, long timestamp, long interval) {
	
		String methodTag = "getMetroNoiseData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get noise data for " + city);
		
		NoiseData noiseData = null;
		
		int level = 0;
		int nbVotes = 0;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabNoise());
			
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get noise data for city = " + city);
			
		    double rad = Double.parseDouble(NOISE_STOP_RADIUS);
		    		
	        double maxLat = latitude + (rad/111000);
	        double minLat = latitude - (rad/111000);
	        double maxLon = longitude + (rad/72000);
	        double minLon = longitude - (rad/72000);
		    
	        if (DEBUG) {
	 		   TdServerLogger.print(TAG, methodTag, 
			    		"latitude = " + latitude + ", " +
			    		"longitude = " + longitude + ", " +
			    		"maxLat = " + maxLat + ", " +
			    		"minLat = " + minLat + ", " + 
			    		"maxLon = " + maxLon + ", " + 
			    		"minLon = " + minLon
			    		);
	        }

	        
  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject("latitude", latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject("latitude", latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject("longitude", lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject("longitude", lonLt);			
			
  			BasicDBObject timestampGte = new BasicDBObject("$gte", (timestamp - interval));
  			BasicDBObject crowdTimestampGte = new BasicDBObject("timestamp", timestampGte);
  			
  			BasicDBObject timestampLte = new BasicDBObject("$lte", timestamp);
  			BasicDBObject crowdTimestampLte = new BasicDBObject("timestamp", timestampLte);
  				
  			ArrayList<BasicDBObject> noiseConditionQuery = new ArrayList<BasicDBObject>();
  			
  			noiseConditionQuery.add(latSup);
  			noiseConditionQuery.add(latInf);
  			noiseConditionQuery.add(lonSup);
  			noiseConditionQuery.add(lonInf); 			
  			noiseConditionQuery.add(crowdTimestampGte);
  			noiseConditionQuery.add(crowdTimestampLte);
  			
  			BasicDBObject noiseQuery = new BasicDBObject("$and", noiseConditionQuery);
			
			DBCursor cursor = table.find(noiseQuery);

	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());	
			
			Map<String, Integer> maps = new HashMap<String, Integer>();
			
			int total = 0;
  			
  			try {
  			
  				   while(cursor.hasNext()) {
  					   String json = cursor.next().toString();
  					   
  				       if (DEBUG) TdServerLogger.print(TAG, methodTag, json);
  				        
  					   JSONObject obj = new JSONObject(json);

  					   int level_db = obj.getInt("noiseLevel");
  					   
  						if (maps.containsKey(level_db + "")) {
  							int count = maps.get(level_db + "");
  							maps.put(level_db + "", count + 1);
  						} else {
  							maps.put(level_db + "", 1);
  						}
  						
  						total++;
  				   }
  				} finally {
  				   cursor.close();
  				}
			
			if (maps.size() > 0) {
				int maxOccurence = -1;
				int levelReturn = -1;
				Iterator<String> it = maps.keySet().iterator();
				for (; it.hasNext(); ) {
					String key = it.next();
					if (maxOccurence < maps.get(key)) {
						maxOccurence = maps.get(key);
						levelReturn = Integer.parseInt(key);
					}
				}
				
				level = levelReturn;
				nbVotes = total;
			}
			
			noiseData = new NoiseData(timestamp, latitude, longitude, level, nbVotes);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "station noise = " + noiseData.toString());
  			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return noiseData;
	}	
	
	
	/**
	 * Get tube station general noise collection
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @param interval
	 * @return
	 */
	public static NoiseDatasCollection getMetroNoiseDatasCollection(String city, String stopId, String routeId, long timestamp, long interval) {
		
		String methodTag = "getMetroNoiseDatasCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get general noise collection for " + city);
		
        TubeStation station = null;
        
        if (city.equals(DataConstants.LONDON)) {
        	station = getLondonMetroStation(stopId);
        } else if (city.equals(DataConstants.PARIS)) {
        	station = getParisMetroStation(stopId);
        }
        
        NoiseDatasCollection noiseDatasCollection = null;
		
		/*
		// example: [now - 2 hours ago]
		long timestamp1 = timestamp;
		
		// example: [2 hours ago - 4 hours ago]
		long timestamp2 = timestamp1 - interval;
		
		// example: [4 hours ago - 6 hours ago]
		long timestamp3 = timestamp2 - interval;
		
		// example: [6 hours ago - 8 hours ago]
		long timestamp4 = timestamp3 - interval;
		
		// example: [8 hours ago - 10 hours ago]
		long timestamp5 = timestamp4 - interval;
		
		// example: [10 hours ago - 12 hours ago]
		long timestamp6 = timestamp5 - interval;
		*/
		
		int nbTimesIntervalAgo = 6;
		NoiseData[] noiseDatas = new NoiseData[nbTimesIntervalAgo];
		
		if (station != null) {
			
			for (int j = 0; j < noiseDatas.length; j++) {
				noiseDatas[j] = getMetroNoiseData(city, station.getLatitude(), station.getLongitude(), (timestamp - interval*j), interval);
			}			
		}

		noiseDatasCollection = new NoiseDatasCollection(noiseDatas);
		
		try {
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return noiseDatasCollection;
	}		

	
	
	
	
	
	
	//
	//
	//Crowd
	/**
	 * Insert crowd data
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @param level
	 * @return True if success, false otherwise
	 */
	public static boolean insertCrowdData(String city, String stopId, String routeId, long timestamp, int level) {
		
		String methodTag = "insertCrowdData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Inserting crowd data for " + city + ", stopId = " + stopId + ", routeId = " + routeId + ", timestamp = " + timestamp + ", level = " + level);
		
		boolean success = false;
		
		try {
			
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection tableGeneralCrowd = database.getCollection(dbProfil.getTabGeneralCrowd());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "general crowd table count = " + tableGeneralCrowd.count());
	
			BasicDBObject updateInfo = new BasicDBObject(
					"stopId", stopId)
					.append("routeId", routeId)
					.append("timestamp", timestamp)
					.append("level", level);
			
			tableGeneralCrowd.insert(updateInfo);
			
			//Update crowd classfied db
			updateClassifiedCrowdDb(city, stopId, routeId, convertTimestampToTimeQueryString(timestamp), level);

			success = true;
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
			}
		
		return success;
	}	
	
	/**
	 * Update classified crowd db
	 * @param stopId
	 * @param queryTime
	 * @param newLevelString
	 */
	public static boolean updateClassifiedCrowdDb(String city, String stopId, String routeId, String queryTime, int level) {
		
		String methodTag = "updateClassifiedCrowdDb";
		boolean success = false;
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Updating classified crowd data for " + city);
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabClassifiedCrowd());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd data table count = " + table.count());
			
			ArrayList<BasicDBObject> crowdConditionQuery = new ArrayList<BasicDBObject>();
			crowdConditionQuery.add(new BasicDBObject("stopId", stopId));
			crowdConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject crowdQuery = new BasicDBObject("$and", crowdConditionQuery);
	        
			DBObject cursor = table.findOne(crowdQuery);
			
			
			if (DEBUG) TdServerLogger.print(TAG, methodTag, "*******************Before update cursor = ****************");
			if (DEBUG) TdServerLogger.print(TAG, methodTag, "" + cursor);
			
			double oldLevel = 0.0;
			int nbVotes = 0;
			double newLevel = 0.0;
			
			if (cursor != null) {
				oldLevel = (Double) cursor.get(extractTime(queryTime));
				nbVotes = ((Integer) cursor.get(extractTime(queryTime) + "-VOTES"));	
				newLevel = (double) level;
			}
			
			double updatedLevel = (double) (oldLevel*nbVotes + newLevel)/(nbVotes + 1);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "Ancien number of votes = " + nbVotes);
				TdServerLogger.print(TAG, methodTag, "New level = " + newLevel);
				TdServerLogger.print(TAG, methodTag, "Updated level = " + updatedLevel);			
			}
			
			BasicDBObject updateInfo = new BasicDBObject(extractTime(queryTime), updatedLevel).append(extractTime(queryTime) + "-VOTES", (nbVotes + 1));
			BasicDBObject updateObj = new BasicDBObject();
			updateObj.put("$set", updateInfo);
			table.update(crowdQuery, updateObj, false, true);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************After update cursor = ****************");
				cursor = table.findOne(crowdQuery);
				TdServerLogger.print(TAG, methodTag, "" + cursor);
			}

			success = true;
			
		} catch (Exception e) {
			TdServerLogger.print(TAG, methodTag, e.getMessage());
		}
		
		return success;
	}
	
	

	/**
	 * Get classified crowd
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @return Tube station crowd
	 */
	public static TubeStationCrowd getMetroClassifiedCrowd(String city, String stopId, String routeId,  long timestamp) {
		
		String methodTag = "getMetroClassifiedCrowd";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get classified crowd for " + city + ", stopId = " + stopId + ", routeId = " + routeId);
        
		TubeStationCrowd stationCrowd = null;	
		
		try {
			
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabClassifiedCrowd());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd table count = " + table.count());			
			
			ArrayList<BasicDBObject> crowdConditionQuery = new ArrayList<BasicDBObject>();
			crowdConditionQuery.add(new BasicDBObject("stopId", stopId));
			crowdConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject crowdQuery = new BasicDBObject("$and", crowdConditionQuery);
			
			DBObject cursor = table.findOne(crowdQuery);
			
			String queryTime = convertTimestampToTimeQueryString(timestamp);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************Before get classified crowd cursor = ****************");
				TdServerLogger.print(TAG, methodTag, "" + cursor);
		        TdServerLogger.print(TAG, methodTag, "query time = " + queryTime);
			}

			int level = (int) Math.round((Double) cursor.get(extractTime(queryTime)));
			int votes =  ((Integer) cursor.get(extractTime(queryTime) + "-VOTES"));
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd level = " + level + ", votes = " + votes);	
	        
	        TubeStation tubeStation = null;
	        
	        if (city.equals(DataConstants.PARIS)) {
	        	//Paris
	        	tubeStation = getParisMetroStation(stopId);
	        } else if (city.equals(DataConstants.LONDON)) {
	        	//London
	        	tubeStation = getLondonMetroStation(stopId);
	        }
	        
			//Maximum values for number of votes to indicate historical datas
			Crowd crowd = new Crowd(DataConstants.CROWD_LEVEL_INT_OBJECT_MAP.get(level), (int) votes);
			stationCrowd = new TubeStationCrowd(tubeStation, crowd);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "station crowd = " + stationCrowd.toString());
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return stationCrowd;
	}	
	
	/**
	 * Get classified crowd by query time HHmm
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param queryTime HHmm
	 * @return Tube station crowd
	 */
	public static TubeStationCrowd getMetroClassifiedCrowdByQueryTime(String city, String stopId, String routeId, String queryTime) {
		
		String methodTag = "getMetroClassifiedCrowdByQueryTime";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get classified crowd for " + city + ", stopId = " + stopId + ", routeId = " + routeId + ", queryTime = " + queryTime);
        
		TubeStationCrowd stationCrowd = null;	
		
		try {
			
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabClassifiedCrowd());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd table count = " + table.count());			
			
			ArrayList<BasicDBObject> crowdConditionQuery = new ArrayList<BasicDBObject>();
			crowdConditionQuery.add(new BasicDBObject("stopId", stopId));
			crowdConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject crowdQuery = new BasicDBObject("$and", crowdConditionQuery);
			
			DBObject cursor = table.findOne(crowdQuery);
			
			if (DEBUG) {
				TdServerLogger.print(TAG, methodTag, "*******************Before get classified crowd cursor = ****************");
				TdServerLogger.print(TAG, methodTag, "" + cursor);
		        TdServerLogger.print(TAG, methodTag, "query time = " + queryTime);	
			}

			int level = (int) Math.round((Double) cursor.get(extractTime(queryTime)));
			int votes =  ((Integer) cursor.get(extractTime(queryTime) + "-VOTES"));
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd level = " + level + ", votes = " + votes);	
	        
	        TubeStation tubeStation = null;
	        
	        if (city.equals(DataConstants.PARIS)) {
	        	//Paris
	        	tubeStation = getParisMetroStation(stopId);
	        } else if (city.equals(DataConstants.LONDON)) {
	        	//London
	        	tubeStation = getLondonMetroStation(stopId);
	        }
	        
			//Maximum values for number of votes to indicate historical datas
			Crowd crowd = new Crowd(DataConstants.CROWD_LEVEL_INT_OBJECT_MAP.get(level), (int) votes);
			stationCrowd = new TubeStationCrowd(tubeStation, crowd);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "station crowd = " + stationCrowd.toString());
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return stationCrowd;
	}	

	
	/**
	 * Get tube station general crowd
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @param interval
	 * @return
	 */
	public static TubeStationCrowd getMetroGeneralCrowd(String city, String stopId, String routeId, long timestamp, long interval) {
	
		String methodTag = "getMetroGeneralCrowd";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get general crowd for " + city);
		
		TubeStationCrowd stationCrowd = null;
		
		int level = 0;
		int nbVotes = 0;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabGeneralCrowd());
	
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "General crowd table count " + table.count());		
			
			BasicDBObject crowdStopId = new BasicDBObject("stopId", stopId);
			BasicDBObject crowdRouteId = new BasicDBObject("routeId", routeId);
			
  			BasicDBObject timestampGte = new BasicDBObject("$gte", (timestamp - interval));
  			BasicDBObject crowdTimestampGte = new BasicDBObject("timestamp", timestampGte);
  			
  			BasicDBObject timestampLte = new BasicDBObject("$lte", timestamp);
  			BasicDBObject crowdTimestampLte = new BasicDBObject("timestamp", timestampLte);
  				
  			ArrayList<BasicDBObject> crowdConditionQuery = new ArrayList<BasicDBObject>();
  			crowdConditionQuery.add(crowdStopId);
  			crowdConditionQuery.add(crowdRouteId);
  			crowdConditionQuery.add(crowdTimestampGte);
  			crowdConditionQuery.add(crowdTimestampLte);
  			
  			BasicDBObject crowdQuery = new BasicDBObject("$and", crowdConditionQuery);

			DBCursor cursor = table.find(crowdQuery);

	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());	
			
			Map<String, Integer> maps = new HashMap<String, Integer>();
			
			int total = 0;
  			
  			try {
  			
  				   while(cursor.hasNext()) {
  					   String json = cursor.next().toString();
  					   
  				       if (DEBUG) TdServerLogger.print(TAG, methodTag, json);
  				        
  					   JSONObject obj = new JSONObject(json);

  					   int level_db = obj.getInt("level");
  					   
  						if (maps.containsKey(level_db + "")) {
  							int count = maps.get(level_db + "");
  							maps.put(level_db + "", count + 1);
  						} else {
  							maps.put(level_db + "", 1);
  						}
  						
  						total++;
  				   }
  				} finally {
  				   cursor.close();
  				}
			
			if (maps.size() > 0) {
				int maxOccurence = -1;
				int levelReturn = -1;
				Iterator<String> it = maps.keySet().iterator();
				for (; it.hasNext(); ) {
					String key = it.next();
					if (maxOccurence < maps.get(key)) {
						maxOccurence = maps.get(key);
						levelReturn = Integer.parseInt(key);
					}
				}
				
				level = levelReturn;
				nbVotes = total;
			}
			
	        TubeStation tubeStation = null;
	        
	        if (city.equals(DataConstants.PARIS)) {
	        	//Paris
	        	tubeStation = getParisMetroStation(stopId);
	        } else if (city.equals(DataConstants.LONDON)) {
	        	//London
	        	tubeStation = getLondonMetroStation(stopId);
	        }
	        
			//Maximum values for number of votes to indicate historical datas
			Crowd crowd = new Crowd(DataConstants.CROWD_LEVEL_INT_OBJECT_MAP.get(level), nbVotes);
			stationCrowd = new TubeStationCrowd(tubeStation, crowd);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "station crowd = " + stationCrowd.toString());
  			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return stationCrowd;
	}	
		
	
	/**
	 * Get tube station general crowd collection
	 * @param city
	 * @param stopId
	 * @param routeId
	 * @param timestamp
	 * @param interval
	 * @return
	 */
	public static TubeStationCrowdsCollection getMetroGeneralCrowdCollection(String city, String stopId, String routeId, long timestamp, long interval) {
		
		String methodTag = "getMetroGeneralCrowdCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Get general crowd collection for " + city);
		
		TubeStationCrowdsCollection stationCrowdsCollection = null;
		
		/*
		// example: [now - 2 hours ago]
		long timestamp1 = timestamp;
		
		// example: [2 hours ago - 4 hours ago]
		long timestamp2 = timestamp1 - interval;
		
		// example: [4 hours ago - 6 hours ago]
		long timestamp3 = timestamp2 - interval;
		
		// example: [6 hours ago - 8 hours ago]
		long timestamp4 = timestamp3 - interval;
		
		// example: [8 hours ago - 10 hours ago]
		long timestamp5 = timestamp4 - interval;
		
		// example: [10 hours ago - 12 hours ago]
		long timestamp6 = timestamp5 - interval;
		*/
		
		int nbTimesIntervalAgo = 6;
		TubeStationCrowd[] stationCrowds = new TubeStationCrowd[nbTimesIntervalAgo];
		
		for (int j = 0; j < stationCrowds.length; j++) {
			stationCrowds[j] = getMetroGeneralCrowd(city, stopId, routeId, (timestamp - interval*j), interval);
		}
		
		stationCrowdsCollection = new TubeStationCrowdsCollection(stationCrowds);
		
		try {
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return stationCrowdsCollection;
	}	
	
	
	
	
	//
	//
	//Ratings
	/**
	 * Check if we need to initialize general ratings db
	 * @param city
	 * @return
	 */
	public static boolean needToInitGeneralRatings(String city) {
		
		String methodTag = "needToInitGeneralRatings";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Checking if need to initialize general ratings for " + city);
        
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabGeneralRatings());
		
		return (table.count() == 0);
	}
	
	
	/**
	 * Initialize general ratings
	 * @param city
	 * @return True if initialize ok, false if failed
	 */
	public static boolean initGeneralRatings(String city) {
		
		String methodTag = "initGeneralRatings";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Initialize ratings for " + city);
		
		boolean success = false;
		JSONArray jsonArray = new JSONArray();
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabGeneralRatings());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "General ratings count = " + table.count());

	        //Need to insert route id instead of route name
	        ArrayList<String> ROUTE_ID = getMetroRouteIdList(city);
	        
			for (int i = 0; i < ROUTE_ID.size(); i++) {
				
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put(DataConstants.PARAM_KEY_ROUTE_ID, ROUTE_ID.get(i));
				
				BasicDBObject updateInfo = new BasicDBObject("0", 0).
	                    append("1", 0).
	                    append("2", 0).
						append("3", 0).
	                    append("4", 0).
						append("5", 0);
	                    
				BasicDBObject updateObj = new BasicDBObject();
				updateObj.put("$set", updateInfo);
				table.update(searchQuery, updateObj, true, true);
			}
			
			if (DEBUG) {
				DBCursor cursor = table.find();
				
				try {
					   while(cursor.hasNext()) {
						   jsonArray.put(cursor.next());
					   }
					} finally {
					   cursor.close();
					}
				
				if (DEBUG) {
					TdServerLogger.print(TAG, methodTag, jsonArray.toString(4));
					TdServerLogger.print(TAG, methodTag, "Initialize ratings ok");	
				}
			}

			success = true;
						
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "success =  " + success);
		
		return success;
	}
	
	/**
	 * Update ratings
	 * @param routeId
	 * @param rating
	 * @return True if update success else False
	 */
	public static boolean updateGeneralRatings(String city, String routeId, int rating) {
		
		String methodTag = "updateGeneralRatings";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Update general ratings for " + city);
		
		boolean success = false;
		
		try {	
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabGeneralRatings());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "General ratings table count = " + table.count());
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
			
			BasicDBObject updateInfo = new BasicDBObject(String.valueOf(rating), 1);		
			BasicDBObject updateObj = new BasicDBObject();
			updateObj.put("$inc", updateInfo);
			table.update(searchQuery, updateObj, false, true);
			
			success = true;
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		} 

        if (DEBUG) TdServerLogger.print(TAG, methodTag, "success = " + success);
		
		return success;
	}
	

	/**
	 * Update user ratings
	 * @param city
	 * @param userName
	 * @param routeId
	 * @param rating
	 * @return True if update ok, false else
	 */
	public static boolean updateUserRatings(String city, String userName, String routeId, int rating) {
		
		String methodTag = "updateUserRatings";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Update user ratings for " + city);
		
		boolean success = false;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabUserRatings());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "User ratings table count = " + table.count());
			
			BasicDBObject query = new BasicDBObject("userName", userName);
			
			DBObject cursor = table.findOne(query);
			
			if (cursor == null) {
				//Create new ratings table for the user
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Create new ratings for user");
		        
		        ArrayList<String> ROUTE_ID = getMetroRouteIdList(city);
				
				for (int i = 0; i < ROUTE_ID.size(); i++) {
					BasicDBObject searchQuery = new BasicDBObject();
					searchQuery.put("userName", userName);
										
					BasicDBObject updateInfo = new BasicDBObject(ROUTE_ID.get(i), 0);
         
					BasicDBObject updateObj = new BasicDBObject();
					updateObj.put("$set", updateInfo);
					table.update(searchQuery, updateObj, true, true);
				}
			} 
			
			
			BasicDBObject searchQuery = new BasicDBObject("userName", userName);

			BasicDBObject updateInfo = new BasicDBObject(routeId, rating);			
			BasicDBObject updateObj = new BasicDBObject();
			updateObj.put("$set", updateInfo);
			
			table.update(searchQuery, updateObj, false, true);
			
			updateGeneralRatings(city, routeId, rating);
			
			success = true;
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Update ratings for user = " + success);
		
		return success;
	}
	
	
	/**
	 * Get metro general ratings collection
	 * @param city
	 * @return Metro ratings collection
	 */
	public static TubeRatingsCollection getMetroGeneralRatingsCollection(String city) {
		
		String methodTag = "getMetroGeneralRatingsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting metro general ratings collection for " + city);
		
	    List<JSONObject> ratingList = new ArrayList<JSONObject>();
		TubeRating tubeRating = null;
		List<TubeRating> tubeRatingList = new ArrayList<TubeRating>();
		TubeRatingsCollection tubeRatingsCollection = null;
	    
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabGeneralRatings());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "General ratings table count = " + table.count());

			DBCursor cursor = table.find();
			
			try {
				   while(cursor.hasNext()) {
					   ratingList.add(new JSONObject(cursor.next().toString()));
				   }
				} finally {
				   cursor.close();
				}
			
			try {
				for(JSONObject obj : ratingList) {
			        if (DEBUG) TdServerLogger.print(TAG, methodTag, obj.toString(4));
					
					int numberOfZero = obj.getInt("0");
					int numberOfOne = obj.getInt("1");
					int numberOfTwo = obj.getInt("2");
					int numberOfThree = obj.getInt("3");
					int numberOfFour = obj.getInt("4");
					int numberOfFive = obj.getInt("5");
					
					int count = numberOfZero + numberOfOne + numberOfTwo + numberOfThree + numberOfFour + numberOfFive;
					float average = (count != 0) ? (numberOfOne + numberOfTwo*2 + numberOfThree*3 + numberOfFour*4 + numberOfFive*5)/count : 0;
					
					String routeId = obj.getString(DataConstants.PARAM_KEY_ROUTE_ID);
					
					Tube tube = getTube(city, routeId);
					
			        if (DEBUG) TdServerLogger.print(TAG, methodTag, tube.toString());
					
					Rating rating = new Rating(DataConstants.RATING_MAP.get((int) average), count);
					
					tubeRating = new TubeRating(tube, rating);
					tubeRatingList.add(tubeRating);
				}
				
				TubeRating[] tubeRatings = new TubeRating[tubeRatingList.size()];
				
				for (int i = 0; i < tubeRatings.length; i++) {
					tubeRatings[i] = tubeRatingList.get(i);
				}
				
				tubeRatingsCollection = new TubeRatingsCollection(tubeRatings);
			} catch (Exception exception) {
		        TdServerLogger.printError(TAG, methodTag, exception.getMessage());
			}
		    
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
			}
		
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeRatingsCollection.toString());
		
		return tubeRatingsCollection;
	}
	
	
	/**
	 * Get metro general rating for route name
	 * @param city
	 * @param routeId
	 * @return Metro general rating
	 */
	public static TubeRating getMetroGeneralRating(String city, String routeId) {
		
		String methodTag = "getMetroGeneralRating";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting metro general ratings for routeId = " + routeId + "in " + city);
		
		TubeRating tubeRating = null;
	    
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabGeneralRatings());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "General ratings table count = " + table.count());

	        BasicDBObject searchQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
	        
			DBObject obj = table.findOne(searchQuery);
				
			int numberOfZero = (Integer) obj.get("0");
			int numberOfOne = (Integer) obj.get("1");
			int numberOfTwo = (Integer) obj.get("2");
			int numberOfThree = (Integer) obj.get("3");
			int numberOfFour = (Integer) obj.get("4");
			int numberOfFive = (Integer) obj.get("5");
					
			int count = numberOfZero + numberOfOne + numberOfTwo + numberOfThree + numberOfFour + numberOfFive;
			float average = (count != 0) ? (numberOfOne + numberOfTwo*2 + numberOfThree*3 + numberOfFour*4 + numberOfFive*5)/count : 0;
						
			Tube tube = getTube(city, routeId);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, tube.toString());
			
			Rating rating = new Rating(DataConstants.RATING_MAP.get((int) average), count);
			
			tubeRating = new TubeRating(tube, rating);
		
		    
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
			}
		
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeRating.toString());
		
		return tubeRating;
	}
	
	

	/**
	 * 	Get metro user ratings collection
	 * @param city
	 * @param userName
	 * @return Tube ratings collection
	 */
	public static TubeRatingsCollection getMetroUserRatingsCollection(String city, String userName) {
		
		String methodTag = "getMetroUserRatingsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting metro user ratings collection for user " + userName + " in " + city);
		
		TubeRatingsCollection tubeRatingsCollection = null;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabUserRatings());
	
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        		"user ratings table count = " + table.count());
			
	        ArrayList<String> ROUTE_ID = getMetroRouteIdList(city);
	        		
			BasicDBObject query = new BasicDBObject("userName", userName);
			
			DBObject cursor = table.findOne(query);
			
			if (cursor == null) {
				//Create new ratings table for the user
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Create new ratings for user");
				
				for (int i = 0; i < ROUTE_ID.size(); i++) {
					BasicDBObject searchQuery = new BasicDBObject();
					searchQuery.put("userName", userName);
										
					BasicDBObject updateInfo = new BasicDBObject(ROUTE_ID.get(i), 0);
         
					BasicDBObject updateObj = new BasicDBObject();
					updateObj.put("$set", updateInfo);
					table.update(searchQuery, updateObj, true, true);
				}
			} 
			
			cursor = table.findOne(query);
			
			JSONObject ratingObj = new JSONObject(cursor.toString());

	        if (DEBUG) TdServerLogger.print(TAG, methodTag, ratingObj.toString(4));
						
			//Create tube rating object
			TubeRating[] tubeRatings = new TubeRating[ROUTE_ID.size()];
			
			for (int i = 0; i < ROUTE_ID.size(); i++) {
				String routeId = ROUTE_ID.get(i);

				Tube tube = getTube(city, routeId);
				
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, tube.toString());
				
				//Number of votes equals to 1 because the voter is the user himself
				Rating rating = new Rating(DataConstants.RATING_MAP.get(ratingObj.getInt(ROUTE_ID.get(i))), 1);
				TubeRating tubeRating = new TubeRating(tube, rating);
				tubeRatings[i] = tubeRating;
			}
			
			tubeRatingsCollection = new TubeRatingsCollection(tubeRatings);
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
			}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeRatingsCollection.toString());
		
		return tubeRatingsCollection;
	}
	
	
	/**
	 * 	Get metro user rating
	 * @param city
	 * @param userName
	 * @param routeId
	 * @return Tube rating
	 */
	public static TubeRating getMetroUserRating(String city, String userName, String routeId) {
		
		String methodTag = "getMetroUserRating";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting metro user rating for user = " + userName + " and route id = " + routeId + " in " + city);
		
		TubeRating tubeRating = null;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabUserRatings());
	
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        		"user ratings table count = " + table.count());
			
			ArrayList<BasicDBObject> ratingConditionQuery = new ArrayList<BasicDBObject>();
			ratingConditionQuery.add(new BasicDBObject("userName", userName));
			ratingConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId));
			
			BasicDBObject routeQuery = new BasicDBObject("$and", ratingConditionQuery);
			
			DBObject cursor = table.findOne(routeQuery);
			
			JSONObject ratingObj = new JSONObject(cursor.toString());

	        if (DEBUG) TdServerLogger.print(TAG, methodTag, ratingObj.toString(4));
									
			Tube tube = getTube(city, routeId);
					
			//Number of votes equals to 1 because the voter is the user himself
			Rating rating = new Rating(DataConstants.RATING_MAP.get(ratingObj.getInt(routeId)), 1);
			tubeRating = new TubeRating(tube, rating);
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeRating.toString());
		
		return tubeRating;
	}
	
	
	
	
	
	//
	//
	//Comments
	/**
	 * Insert comment into database
	 * @param city
	 * @param timestamp
	 * @param userName
	 * @param stopId
	 * @param comment
	 * @return True if success, false else
	 */
	public static boolean insertComment(String city, long timestamp, String userName, String stopId, String routeId, String comment) {
		
		String methodTag = "insertComment";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Insert comment for userName = " + userName + ", stopId = " + stopId + ", routeId = " + routeId + ", comment = " + comment);
		
		boolean success = false;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabComments());
					
			if (DEBUG) TdServerLogger.print(TAG, methodTag, 
					"Comments table count = " + table.count());
			
			BasicDBObject updateInfo = new BasicDBObject("userName", userName)
					.append("timestamp", timestamp)
                    .append("stopId", stopId)
                    .append("routeId", routeId)
                    .append("comment", comment);
			
			table.insert(updateInfo);
			
			success = true;
		} catch(Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return success;
	}
	
	
	/**
	 * Get comments collection
	 * @param city
	 * @param stopId
	 * @param maxComments
	 * @return Comments collection
	 */
	public static TubeStationCommentsCollection getMetroCommentsCollection(String city, String stopId, String routeId, int maxComments) {
		
		String methodTag = "getMetroCommentsCollection";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get comments collection for city = " + city + ", stopId = " + stopId + ", routeId = " + routeId + ", maxComments = " + maxComments);
		
		TubeStationCommentsCollection commentsCollection = null;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabComments());
			
			if (DEBUG) TdServerLogger.print(TAG, methodTag, 
					"Comments tab count = " + table.count());
			
			ArrayList<BasicDBObject> commentConditionQuery = new ArrayList<BasicDBObject>();
			commentConditionQuery.add(new BasicDBObject("stopId", stopId));
			commentConditionQuery.add(new BasicDBObject("routeId", routeId));
			
			BasicDBObject commentQuery = new BasicDBObject("$and", commentConditionQuery);
			
			DBCursor cursor = table.find(commentQuery);
			TubeStation station = null;
			
			if (city.equals(DataConstants.PARIS)) {
				//DataConstants.PARIS
				station = getParisMetroStation(stopId);
			} else if (city.equals(DataConstants.LONDON)) {
				//DataConstants.LONDON
				station = getLondonMetroStation(stopId);
			}
			
			if (DEBUG) TdServerLogger.print(TAG, methodTag, "Station = " + station.toString());
			
			List<TubeStationComment> commentsList = new ArrayList<TubeStationComment>();
			
			try {					
				   while(cursor.hasNext()) {  
				       String commentInfo = cursor.next().toString();
				       JSONObject commentObj = new JSONObject(commentInfo);
				       String userName = commentObj.getString("userName");
				       String comment = commentObj.getString("comment");
				       long timestamp = commentObj.getLong("timestamp");
				       TubeStationComment stationComment = new TubeStationComment(userName, comment, timestamp, station);
				       commentsList.add(stationComment);
				       if (DEBUG) TdServerLogger.print(TAG, methodTag, "comment = " + comment);
				   }
				} finally {
				   cursor.close();
				}
			
			int numberIteration = maxComments;
			if (numberIteration > commentsList.size()) {
				numberIteration = commentsList.size();
			}
			
			TubeStationComment[] commentsArray = new TubeStationComment[numberIteration];
			
			int startIndex = commentsList.size() - numberIteration;
			int j = 0;
			
			for (int i = startIndex; i < commentsList.size(); i++) {
				//Get latest comments
				commentsArray[j] = commentsList.get(i);
				j++;
			}
			
			commentsCollection = new TubeStationCommentsCollection(commentsArray);
			
			if (DEBUG) TdServerLogger.print(TAG, methodTag, commentsCollection.toString());
			
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return commentsCollection;
	}
	
	/**
	 * Get comments collection for user
	 * @param city
	 * @param user
	 * @param stopId
	 * @param maxComments
	 * @return Metro station comments collection
	 */
	public static TubeStationCommentsCollection getMetroCommentsCollectionForUser(String city, String userName, String stopId, String routeId, int maxComments) {
		
		String methodTag = "getMetroCommentsCollectionForUser";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get comments collection for city = " + city + ", user = " + userName + ", stopId = " + stopId + ", routeId = " + routeId + ", maxComments = " + maxComments);
		
		TubeStationCommentsCollection commentsCollection = null;
		
		try {		
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabComments());
			
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
					"Comments tab count = " + table.count());

			ArrayList<BasicDBObject> commentConditionQuery = new ArrayList<BasicDBObject>();
			commentConditionQuery.add(new BasicDBObject("stopId", stopId));
			commentConditionQuery.add(new BasicDBObject("routeId", routeId));
			commentConditionQuery.add(new BasicDBObject("userName", userName));
			
			BasicDBObject commentQuery = new BasicDBObject("$and", commentConditionQuery);
			
			DBCursor cursor = table.find(commentQuery);
			
			TubeStation station = null;
			
			if (city.equals(DataConstants.PARIS)) {
				//DataConstants.PARIS
				station = getParisMetroStation(stopId);
			} else if (city.equals(DataConstants.LONDON)) {
				//DataConstants.LONDON
				station = getLondonMetroStation(stopId);
			}
			
			List<TubeStationComment> commentsList = new ArrayList<TubeStationComment>();
			
			try {	
				   while(cursor.hasNext()) {  
				       String commentInfo = cursor.next().toString();
				       JSONObject commentObj = new JSONObject(commentInfo);
				       String comment = commentObj.getString("comment");
				       long timestamp = commentObj.getLong("timestamp");
				       TubeStationComment stationComment = new TubeStationComment(userName, comment, timestamp, station);
				       commentsList.add(stationComment);
				   }
				} finally {
				   cursor.close();
				}
			
			int numberIteration = maxComments;
			if (numberIteration > commentsList.size()) {
				numberIteration = commentsList.size();
			}
			
			TubeStationComment[] commentsArray = new TubeStationComment[numberIteration];
			
			int startIndex = commentsList.size() - numberIteration;
			int j = 0;
			
			for (int i = startIndex; i < commentsList.size(); i++) {
				commentsArray[j] = commentsList.get(i);
				j++;
			}
			
			commentsCollection = new TubeStationCommentsCollection(commentsArray);
			
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, commentsCollection.toString());
					
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return commentsCollection;
	}	
		
	
	
	
	
	
	
	//
	//
	//Metro status
	public static boolean needToInitMetroStatus(String city) {
		
		String methodTag = "needToInitMetroStatus";
		
		if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
				"Checking if need to initialize metro status for city = " + city + "...");
		
		DB database = dbMap.get(city);
		TdDbProfil dbProfil = tdDbProfils.get(city);
		DBCollection table = database.getCollection(dbProfil.getTabStatus());
				
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"status table count = " + table.count());
		
		return (table.count() == 0);
	}
	
	
	
	
	/**
	 * Init metro status
	 * @param city
	 * @return True if success, False else
	 */
	public static boolean initMetroStatus(String city) {
		
		String methodTag = "initMetroStatus";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Initialize metro status for city = " + city + "...");
		
		boolean success = false;
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabStatus());
					
			if (DEBUG) TdServerLogger.print(TAG, methodTag, 
					"status table count = " + table.count());
			
			ArrayList<String> ROUTE_ID = getMetroRouteIdList(city);
	        
			for (int i = 0; i < ROUTE_ID.size(); i++) {		
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put(DataConstants.PARAM_KEY_ROUTE_ID, ROUTE_ID.get(i));
				
				BasicDBObject updateInfo = new BasicDBObject("status", "No status").
	                    append("description", "No description");
	                    
				BasicDBObject updateObj = new BasicDBObject();
				updateObj.put("$set", updateInfo);
				table.update(searchQuery, updateObj, true, true);
			}
			
			if (DEBUG) {
				DBCursor cursor = table.find();
				
				TdServerLogger.print(TAG, methodTag, 
						"cursor = " + cursor.toString());		
				
				TdServerLogger.print(TAG, methodTag, 
						"status table count = " + table.count());
			}

			success = true;
		} catch(Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return success;
	}
	
	/**
	 * Update metro status
	 * @param city
	 * @param routeId
	 * @param status
	 * @param description
	 * @return True if success, False else
	 */
	public static boolean updateMetroStatus(String city, String routeId, String status, String description) {
		
		String methodTag = "initMetroStatus";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Initialize metro status for city = " + city 
				+ ", routeId = " + routeId
				+ ", status = " + status
				+ ", description = " + description);
		
		boolean success = false;
		
		if (routeId.equals("*") || routeId.equals("N/A")) {
			return success;
		}
		
		try {
    		DB database = dbMap.get(city);
    		TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabStatus());
					
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
					"status table count = " + table.count());
		
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
				
			BasicDBObject updateInfo = new BasicDBObject("status", status).
                    append("description", description);
                    
			BasicDBObject updateObj = new BasicDBObject();
			updateObj.put("$set", updateInfo);
			table.update(searchQuery, updateObj, false, true);
				
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
					"status table count = " + table.count());
			
			success = true;
			
		} catch(Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
		return success;
	}
	
	
	/**
	 * Get metro status collection
	 * @param city
	 * @return Status collection
	 */
	public static TubeStatusCollection getMetroStatusCollection(String city) {
		
		String methodTag = "getMetroStatusCollection";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get metro status collection for " + city);
		
		TubeStatusCollection collection = null;
	    List<JSONObject> statusList = new ArrayList<JSONObject>();
	    List<TubeStatus> tubeStatusList = new ArrayList<TubeStatus>();
 		
		try {
			DB database = dbMap.get(city);
			TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabStatus());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Metro status table count = " + table.count());

			DBCursor cursor = table.find();
			
			try {
				   while(cursor.hasNext()) {
					   statusList.add(new JSONObject(cursor.next().toString()));
				   }
				} finally {
				   cursor.close();
				}
			
			for(JSONObject obj : statusList) {
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, obj.toString(4));
				
		        String routeId = obj.getString(DataConstants.PARAM_KEY_ROUTE_ID);
		        String status = obj.getString("status");
		        String description = obj.getString("description");

				Tube tube = getTube(city, routeId);
				
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, tube.toString());
		        
		        TubeStatus tubeStatus = new TubeStatus(tube, status, description);
		        
		        tubeStatusList.add(tubeStatus);
			}

			TubeStatus[] statusArray = new TubeStatus[tubeStatusList.size()];
			
			for (int i = 0; i < tubeStatusList.size(); i++) {
				statusArray[i] = tubeStatusList.get(i);
			}
			
			collection = new TubeStatusCollection(statusArray);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, collection.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
	
		return collection;
	}
	
	
	/**
	 * Get tube status
	 * @param city
	 * @param routeId
	 * @return tube status
	 */
	public static TubeStatus getMetroStatus(String city, String routeId) {
		
		String methodTag = "getMetroStatusCollection";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get metro status collection for " + city);
		
		TubeStatus tubeStatus = null;
 		
		try {
			
			DB database = dbMap.get(city);
			TdDbProfil dbProfil = tdDbProfils.get(city);
			DBCollection table = database.getCollection(dbProfil.getTabStatus());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Metro status table count = " + table.count());

			BasicDBObject statusQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
			
			DBObject cursor = table.findOne(statusQuery);
			
			String status = (String) cursor.get("status");
			String description = (String) cursor.get("description");
			
			Tube tube = getTube(city, routeId);
			
			tubeStatus = new TubeStatus(tube, status, description);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeStatus.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
	
		return tubeStatus;
	}
	
	
	
	
	//
	//
	//Get tube
	/**
	 * Get tube
	 * @param city
	 * @param routeId
	 * @return Tube
	 */
	public static Tube getTube(String city, String routeId) {
		
		String methodTag = "getTube";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get tube for routeId = " + routeId + " in " + city);
		
		String routeName = "";
		String routeDestination = "";
		
		if (city.equals(DataConstants.PARIS)) {
			//DataConstants.PARIS
			//Use 0 as direction by default
			routeName = getParisMetroRouteName(routeId);
			String routeDirectionId = getParisMetroRouteDirectionId(routeId);
			routeDestination = DataConstants.PARIS_METRO_DESTINATIONS.get(routeName).getDirection(routeDirectionId);
			
		} else {
			//DataConstants.LONDON
			routeName = getLondMetroRouteName(routeId);
			routeDestination = DataConstants.LONDON_TUBE_DESTINATIONS.get(routeName);
		}
		
		Tube tube = new Tube(routeId, routeName, routeDestination);
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				tube.toString());
		
		return tube;
	}
	
	
	
	
	//
	//
	//Get route id list
	/**
	 * Get route id list
	 * @param city
	 * @return Route id list
	 */
	public static ArrayList<String> getMetroRouteIdList(String city) {
		String methodTag = "getRouteIdList";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get route id list for " + city);
		
        ArrayList<String> ROUTE_ID = new ArrayList<String>();
        
        if (city.equals(DataConstants.PARIS)) {
        	
        	//Paris metro
        	ArrayList<String> PARIS_ROUTE_NAMES = new ArrayList<String>();
        	PARIS_ROUTE_NAMES.addAll(DataConstants.PARIS_METRO_ROUTE_NAMES);
        	
        	for (String routeName : PARIS_ROUTE_NAMES) {
        		String routeId0 = getParisMetroRouteId(routeName, "0");
        		String routeId1 = getParisMetroRouteId(routeName, "1");
        		ROUTE_ID.add(routeId0);
        		ROUTE_ID.add(routeId1);	
        	}
        	   	
        } else if (city.equals(DataConstants.LONDON)) {
        	for (String routeId : DataConstants.LONDON_METRO_ROUTE_ID) {
            	if (!routeId.equals("*") && (!routeId.equals("N/A"))) {
            		ROUTE_ID.add(routeId);
            	}
        	}
        }
        
        return ROUTE_ID;
	}
	
	
	
	
	
	//
	//
	//Bike
	
	/**
	 * Update bike database
	 * @param city
	 * @param bikeStationsCollection
	 */
	public static void updateBikeDb(String city, BikeStationsCollection bikeStationsCollection) {
		
		String methodTag = "updateBikeDb";
		
	    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        		"Update bike database for " + city);
		
		try {			
        	DB database = dbMap.get(city);
        	TdDbProfil dbProfil = tdDbProfils.get(city);
        	DBCollection table = database.getCollection(dbProfil.getTabBike());
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Bike table count = " + table.count());
			
			BikeStation[] stations = bikeStationsCollection.getBikeStations();
			
			for (BikeStation station : stations) {
				BasicDBObject searchQuery = new BasicDBObject();
				searchQuery.put("id", String.valueOf(station.getId()));
				
				BasicDBObject updateInfo = new BasicDBObject("name", station.getName()).
	                    append("nbDocks", String.valueOf(station.getNbTotalDocks())).
	                    append("lat", station.getLatitude()).
	                    append("long", station.getLongitude()).
	                    append("locked", String.valueOf(station.isLocked())).
	                    append("nbBikes", String.valueOf(station.getNbBikes())).
	                    append("nbEmptyDocks", String.valueOf(station.getNbEmptyDocks()));
	                    
				BasicDBObject updateObj = new BasicDBObject();
				updateObj.put("$set", updateInfo);
				table.update(searchQuery, updateObj, true, true);
			}
			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"End of update, bike table count = " + table.count());
			
		} catch(Exception e) {
			TdServerLogger.printError(TAG, methodTag, 
            		e.getMessage());
		}
	}
	

	
	/**
	 * Get bike station
	 * @param city
	 * @param stopId
	 * @return bike station
	 */
	public static BikeStation getBikeStation(String city, String stopId) {
		
		String methodTag = "getBikeStation";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting bike station for stopId = " + stopId + " in " + city);
		
		BikeStation bikeStation = null;
		
		try {
			DB database = dbMap.get(city);
			TdDbProfil dbProfil = tdDbProfils.get(city);			
			DBCollection table = database.getCollection(dbProfil.getTabBike());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("id", stopId);
			  			
		    DBObject nxtObj = table.findOne(searchQuery);
		    
			String name = (String) nxtObj.get("name");
			double latBike = (Double) nxtObj.get("lat");
			double lonBike = (Double) nxtObj.get("long");
			int nbBikes = Integer.parseInt((String) nxtObj.get("nbBikes"));
			int nbEmptyDocks = Integer.parseInt((String) nxtObj.get("nbEmptyDocks"));
			int nbTotalDocks = Integer.parseInt((String) nxtObj.get("nbDocks"));
			boolean locked = Boolean.parseBoolean((String) nxtObj.get("locked"));
			bikeStation = new BikeStation(stopId, name, latBike, lonBike, 
					nbBikes, nbEmptyDocks, nbTotalDocks, locked);
		    
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, bikeStation.toString());
		    
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		
		return bikeStation;
	}
	
	/**
	 * Get bike stations collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return
	 */
	public static BikeStationsCollection getBikeStationsCollection(String latitude, String longitude, String radius) {
		
		String methodTag = "getBikeStationsCollection";
		
		String city = TransportArea.getArea(Double.parseDouble(latitude), Double.parseDouble(longitude));
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting bike stations collection for " + city);
		
		BikeStationsCollection bikeStationsCollection = null;
		String json = "";
		JSONArray jsonArray = new JSONArray();
		
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
		
        try {
        	DB database = dbMap.get(city);
        	TdDbProfil dbProfil = tdDbProfils.get(city);
        	DBCollection table = database.getCollection(dbProfil.getTabBike());
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Bike table count = " + table.count());

			//Latitude inferior bound
			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
			BasicDBObject latInf = new BasicDBObject("lat", latGte);
		
			//Latitude superior bound
			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
			BasicDBObject latSup = new BasicDBObject("lat", latLt);
			
			//Longitude inferior bound
			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
			BasicDBObject lonInf = new BasicDBObject("long", lonGte);
		
			//Longitude superior bound
			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
			BasicDBObject lonSup = new BasicDBObject("long", lonLt);
			
			ArrayList<BasicDBObject> bikeConditionQuery = new ArrayList<BasicDBObject>();
			bikeConditionQuery.add(latSup);
			bikeConditionQuery.add(latInf);
			bikeConditionQuery.add(lonSup);
			bikeConditionQuery.add(lonInf);
			
			BasicDBObject bikeQuery = new BasicDBObject("$and", bikeConditionQuery);
			
			DBCursor cursor = table.find(bikeQuery);
			
			try {
				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
				   }
				} finally {
				   cursor.close();
				}
			
			BikeStation[] bikeStations = new BikeStation[jsonArray.length()];
			
			for (int j = 0; j < jsonArray.length(); j++) {
				json = jsonArray.getString(j);
				JSONObject jsonCurrentObject = new JSONObject(json);
				String id = jsonCurrentObject.getString("id");
				String name = jsonCurrentObject.getString("name");
				double latBike = jsonCurrentObject.getDouble("lat");
				double lonBike = jsonCurrentObject.getDouble("long");
				int nbBikes = jsonCurrentObject.getInt("nbBikes");
				int nbEmptyDocks = jsonCurrentObject.getInt("nbEmptyDocks");
				int nbTotalDocks = jsonCurrentObject.getInt("nbDocks");
				boolean locked = jsonCurrentObject.getBoolean("locked");
				bikeStations[j] = new BikeStation(id, name, latBike, lonBike, 
						nbBikes, nbEmptyDocks, nbTotalDocks, locked);
			}	
			
			bikeStationsCollection = new BikeStationsCollection(bikeStations);
			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		bikeStationsCollection.toString());
			
        } catch (Exception e) {
        	 TdServerLogger.printError(TAG, methodTag, 
             		e.getMessage());
        } 
        
		return bikeStationsCollection;
	}
		

	
	

	
	
	
	
	
	
	
	
	
	//
	//
	//Paris
	

	
	
	/**
	 * Check if need to insert paris all metros string
	 * @return True if needed, false if not need
	 */
	public static boolean needToInsertParisAllMetroStopsString() {
		String methodTag = "needToInsertParisAllMetroStopsString";
		
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllMetroStopsString");
        	return (collection.count() == 0);
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
		return true;
	}
	
	
	/**
	 * Reset paris all metro stops string db
	 * @return True if reset ok, false if not ok
	 */
	public static boolean resetParisAllMetroStopsString() {
		
		String methodTag = "resetParisAllMetroStopsString";
		
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllMetroStopsString");
        	collection.drop();
        	return (collection.count() == 0);
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
		return false;
	}
	
	
	/**
	 * Insert paris all metro stops string
	 * @param networkmessageString
	 */
	public static void insertParisAllMetroStopsString(String networkmessageString) {
		String methodTag = "insertParisAllMetroStopsString";
		
		if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
        		networkmessageString);
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllMetroStopsString");
        	DBObject obj = new BasicDBObject("string", networkmessageString);
        	collection.insert(obj);
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
	}
	
	/**
	 * Get paris all metro stops string
	 * @return All paris all metro stops string
	 */
	public static String getParisAllMetroStopsString() {
		
		String methodTag = "getParisAllMetroStopsString";
		
		if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
        		"Get paris all metro stops string");
		
		String metrosString = "";
		
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllMetroStopsString");
        	
    		DBCursor cursor = collection.find();
    		JSONObject obj = new JSONObject(cursor.next().toString());
    		metrosString = obj.getString("string");
    		
    		if (DEBUG)  TdServerLogger.print(TAG, methodTag, "Metro string : " + metrosString);
        	
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
		return metrosString;
	}
	
	
	
	/**
	 * Reset paris all bus stops string db
	 * @return True if reset ok, false if not ok
	 */
	public static boolean resetParisAllBusStopsString() {
		
		String methodTag = "resetParisAllBusStopsString";
		
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllBusStopsString");
        	collection.drop();
        	return (collection.count() == 0);
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
		return false;
	}
	
	
	/**
	 * Check if need to insert paris all bus stops string
	 * @return True if needed, false if not need
	 */
	public static boolean needToInsertParisAllBusStopsString() {
		String methodTag = "needToInsertParisAllBusStopsString";
		
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllBusStopsString");
        	return (collection.count() == 0);
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
		return true;
	}
	//Insert all stops network string in db
	public static void insertParisAllBusStopsString(String networkmessageString) {
		String methodTag = "insertParisAllMetroStopsString";
		
		if (DEBUG)  TdServerLogger.print(TAG, methodTag, 
        		networkmessageString);
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllBusStopsString");
        	DBObject obj = new BasicDBObject("string", networkmessageString);
        	collection.insert(obj);
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
	}
	
	/**
	 * Get paris all bus stops string
	 * @return All paris all bus stops string
	 */
	public static String getParisAllBusStopsString() {
		
		String methodTag = "getParisAllBusStopsString";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Get paris all bus stops string");
		
		String busString = "";
		
		try {
        	DB database = dbMap.get("paris");
        	DBCollection collection = database.getCollection("parisAllBusStopsString");
        	
    		DBCursor cursor = collection.find();
    		JSONObject obj = new JSONObject(cursor.next().toString());
    		busString = obj.getString("string");
    		
    		if (DEBUG)  TdServerLogger.print(TAG, methodTag, "Bus string : " + busString);
        	
		} catch (Exception e) {
       	 TdServerLogger.printError(TAG, methodTag, 
          		e.getMessage());
		}
		
		return busString;
	}
	
	
	//
	//
	//Bus
	public static void insertParisBusData(InputStream is)	 {
		
		String methodTag = "insertParisBusData";
		
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabBus());
			
			//Parse XML information into String
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();

			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
			
			br.close();

			//
			//
			//Extract information from XML String
			//Database profils map, key = city name, value = database profil	
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, "Going to parse bus message");
			
			ArrayList<StopInfo> stopInfoList = GtfsNetworkMessageParser.parseMessage(sb.toString().getBytes());
			
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, "Bus stop info list size = " + stopInfoList.size());
			
			//int index = 0;

			for (StopInfo tmpStop : stopInfoList) {
				
				
				//index++;
				//if ((index % 1000) == 0) TdServerLogger.print(TAG, methodTag, "Paris bus info index = " + index);
				
	        	String stopId = tmpStop.getStopId();
	        	
	            // if invalid id, then continue
	            if (stopId == null || stopId.equals("")) continue;
	            
	            String routeId = tmpStop.getRouteId();
	            String routeName = tmpStop.getRouteName();
	            String stopName = tmpStop.getStopName();
	            double latitude = tmpStop.getLatitude();
	            double longitude = tmpStop.getLongitude();
	            String direction = String.valueOf(tmpStop.getDirection());

	            String stopString =  "stopId = " + stopId + ", " + 
				            		"routeId = " + routeId + ", " +
				            		"routeName = " + routeName + ", " +
				            		"stopName = " + stopName + ", " +
				            		"latitude= " + latitude + ", " +
				            		"longitude = " + longitude + ", " +
				            		"direction = " + direction;

		        if (DEBUG) TdServerLogger.print(TAG, methodTag, stopString);
	            
				BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
	            
	        	BasicDBObject stopInfo = new BasicDBObject
	        			(DataConstants.PARAM_KEY_ROUTE_ID, routeId).
						append(DataConstants.PARAM_KEY_ROUTE_NAME, routeName).
						append(DataConstants.PARAM_KEY_STOP_NAME, stopName).
						append(DataConstants.PARAM_KEY_STOP_LON, longitude).
						append(DataConstants.PARAM_KEY_STOP_LAT, latitude).
						append(DataConstants.PARAM_KEY_DIRECTION_ID, direction)
						;
				
				BasicDBObject setCommand = new BasicDBObject();
				setCommand.put("$set", stopInfo);
				table.update(stopQuery, setCommand, true, true);
	        }
	        
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, "Insert Paris bus data into db ok: table count = " + table.count());
			
		} catch (Exception exception){
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
	}
	
	
	
	/**
	 * Get paris bus route name
	 * @param routeId
	 * @return Route name
	 */
	public static String getParisBusRouteName(String routeId) {
		String methodTag = "getParisBusRouteName";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris bus route name for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabBus());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeName = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeName = " + routeName);
        
        return routeName;
	}
	
	
	/**
	 * Get paris bus route direction id
	 * @param routeId
	 * @return Route direction id: 0 or 1
	 */
	public static String getParisBusRouteDirectionId(String routeId) {
		String methodTag = "getParisBusRouteDirection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris bus route direction for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabBus());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeDirectionId = (String) cursor.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeDirectionId = " + routeDirectionId);
        
        return routeDirectionId;
	}
	

	/**
	 * Get paris bus route id
	 * @param routeName
	 * @return
	 */
	public static String getParisBusRouteId(String routeName, String direction) {
		
		String methodTag = "getParisBusRouteId";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris bus route id for routeName = " + routeName + ", direction = " + direction);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabBus());  	
		
		
		ArrayList<BasicDBObject> routeConditionQuery = new ArrayList<BasicDBObject>();
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_NAME, routeName));
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_DIRECTION_ID, direction));
		
		BasicDBObject routeQuery = new BasicDBObject("$and", routeConditionQuery);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeId = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeId = " + routeId);
        
        return routeId;
	}
	
	
	/**
	 * Get paris bus station name by stop id
	 * @param stopId
	 * @return Paris bus station name
	 */
	public static String getParisBusStationName(String stopId) {
		
		String methodTag = "getParisMetroStationName";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris bus station name...");
		
        String stopName = "";
        
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabBus());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);

		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		       
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", stopName = " + stopName);
	        
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return stopName;
	}
	
	
	/**
	 * Get bus station
	 * @param stopId
	 * @return tramstation
	 */	
	public static BusStation getParisBusStation(String stopId) {
		
		String methodTag = "getParisBusStation";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting paris bus station...");
		
		BusStation station = null;
		
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabBus());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);
			  			
		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    String stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		    String routeName = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		    String routeId = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_ID);
		    double lat = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LON);
		    double lng = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LAT);
		    String direction = (String) nxtObj.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		    
		    Bus bus = new Bus(routeId, routeName, direction);
		    Bus[] buses = {bus};
		    station = new BusStation(stopId, stopName, lat, lng, buses);
		    
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, station.toString());
		    
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return station;
	}
	
	
	/**
	 * Get bus stations collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return Bus stations collection
	 */
	public static BusStationsCollection getParisBusStationsCollection(String latitude, String longitude, String radius) {
		
		String methodTag = "getParisBusStationsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris bus stations collection...");
        
		BusStationsCollection busStationsCollection = null;
		JSONArray jsonArray = new JSONArray();
        
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
        
        try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabBus());
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Buses table count = " + table.count());

  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonLt);
  			
  			ArrayList<BasicDBObject> tubesConditionQuery = new ArrayList<BasicDBObject>();
  			tubesConditionQuery.add(latSup);
  			tubesConditionQuery.add(latInf);
  			tubesConditionQuery.add(lonSup);
  			tubesConditionQuery.add(lonInf);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", tubesConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);
  			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		"Cursor count = " + cursor.count());
  			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
			
			BusStation[] busStations = new BusStation[jsonArray.length()];
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				String stationName = obj.getString(DataConstants.PARAM_KEY_STOP_NAME);
				double stationLat = obj.getDouble(DataConstants.PARAM_KEY_STOP_LAT);
				double stationLng = obj.getDouble(DataConstants.PARAM_KEY_STOP_LON);
				String stationId = obj.getString(DataConstants.PARAM_VALUE_STOP_ID);
				String routeId = obj.getString(DataConstants.PARAM_KEY_ROUTE_ID);
				String routeName = obj.getString(DataConstants.PARAM_KEY_ROUTE_NAME);
				String direction = obj.getString(DataConstants.PARAM_KEY_DIRECTION_ID);				
				Bus bus = new Bus(routeId, routeName, direction);
				Bus[] buses = {bus};
				BusStation station = new BusStation(stationId, stationName, stationLat, stationLng, buses);
				busStations[i] = station;
			}
			
			busStationsCollection = new BusStationsCollection(busStations);
	
            if (DEBUG) TdServerLogger.print(TAG, methodTag, 
            		busStationsCollection.toString());			
			
        } catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
        }
			
		return busStationsCollection;
	}
	
	
	

	
	
	
	
	//
	//
	//Metro
	public static void insertParisMetroData(InputStream is)	 {
		
		String methodTag = "insertParisMetroData";
		
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabMetro());
			
			//Parse XML information into String
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line;
			StringBuilder sb = new StringBuilder();

			while((line=br.readLine())!= null){
			    sb.append(line.trim());
			}
			
			br.close();

			//
			//
			//Extract information from XML String
			//Database profils map, key = city name, value = database profil	
			
			ArrayList<StopInfo> stopInfoList = GtfsNetworkMessageParser.parseMessage(sb.toString().getBytes());

			if (DEBUG) TdServerLogger.print(TAG, methodTag, "Paris metro info list size = " + stopInfoList.size());
			
			//int index = 0;
			
			for (StopInfo tmpStop : stopInfoList) {
				
				//index++;
				//if ((index % 500) == 0) TdServerLogger.print(TAG, methodTag, "Paris bus metro info index = " + index);
				
	        	String stopId = tmpStop.getStopId();
	        	
	            // if invalid id, then continue
	            if (stopId == null || stopId.equals("")) continue;
	            
	            String routeId = tmpStop.getRouteId();
	            String routeName = tmpStop.getRouteName();
	            String stopName = tmpStop.getStopName();
	            double latitude = tmpStop.getLatitude();
	            double longitude = tmpStop.getLongitude();
	            String direction = String.valueOf(tmpStop.getDirection());

	            String stopString =  "stopId = " + stopId + ", " + 
				            		"routeId = " + routeId + ", " +
				            		"routeName = " + routeName + ", " +
				            		"stopName = " + stopName + ", " +
				            		"latitude= " + latitude + ", " +
				            		"longitude = " + longitude + ", " +
				            		"direction = " + direction;

		        if (DEBUG) TdServerLogger.print(TAG, methodTag, stopString);
	            
				BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
	            
	        	BasicDBObject stopInfo = new BasicDBObject
	        			(DataConstants.PARAM_KEY_ROUTE_ID, routeId).
						append(DataConstants.PARAM_KEY_ROUTE_NAME, routeName).
						append(DataConstants.PARAM_KEY_STOP_NAME, stopName).
						append(DataConstants.PARAM_KEY_STOP_LON, longitude).
						append(DataConstants.PARAM_KEY_STOP_LAT, latitude).
						append(DataConstants.PARAM_KEY_DIRECTION_ID, direction)
						;
				
				BasicDBObject setCommand = new BasicDBObject();
				setCommand.put("$set", stopInfo);
				table.update(stopQuery, setCommand, true, true);
	        }
	        
			if (DEBUG)  TdServerLogger.print(TAG, methodTag, "Insert Paris metro data into db ok: table count = " + table.count());
	        
			if (DEBUG) TdServerLogger.outLog(TAG, methodTag, "Insert Paris metro data into db ok");
			
		} catch (Exception exception){
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
			TdServerLogger.outLogError(TAG, methodTag, "Insert Paris metro data into db failed");
		}
	}

	

	/**
	 * Get all paris metro stop ids
	 * @return Paris metro stop ids
	 */
	public static List<String> getAllParisMetroStopIds() {
		String methodTag = "getAllParisMetroStopIds";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting all paris metro stops id...");
        
		List<String> stopIds = new ArrayList<String>();
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabMetro());
		
		DBCursor cursor = table.find();
        
	    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	    		"Cursor count = " + cursor.count());
			
		try {
			   while(cursor.hasNext()) {
				   DBObject obj = cursor.next();
				   String stopId = (String) (obj.get(DataConstants.PARAM_VALUE_STOP_ID));
				   stopIds.add(stopId);
			   }
			} finally {
			   cursor.close();
			}

		return stopIds;
	}
	
	
	/**
	 * Get paris metro route name
	 * @param routeId
	 * @return Route name
	 */
	public static synchronized String getParisMetroRouteName(String routeId) {
		String methodTag = "getParisMetroRouteName";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris metro route name for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabMetro());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeName = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeName = " + routeName);
        
        return routeName;
	}
	
	
	/**
	 * Get paris metro route direction id
	 * @param routeId
	 * @return Route direction id: 0 or 1
	 */
	public static String getParisMetroRouteDirectionId(String routeId) {
		String methodTag = "getParisMetroRouteDirection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris metro route direction for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabMetro());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeDirectionId = (String) cursor.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeDirectionId = " + routeDirectionId);
        
        return routeDirectionId;
	}
	

	/**
	 * Get paris metro route id
	 * @param routeName
	 * @return
	 */
	public static String getParisMetroRouteId(String routeName, String direction) {
		
		String methodTag = "getParisMetroRouteId";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris metro route id for routeName = " + routeName + ", direction = " + direction);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabMetro());  	
		
		
		ArrayList<BasicDBObject> routeConditionQuery = new ArrayList<BasicDBObject>();
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_NAME, routeName));
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_DIRECTION_ID, direction));
		
		BasicDBObject routeQuery = new BasicDBObject("$and", routeConditionQuery);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeId = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeId = " + routeId);
        
        return routeId;
	}
	
	
	/**
	 * Get paris metro station name by stop id
	 * @param stopId
	 * @return Paris metro station name
	 */
	public static String getParisMetroStationName(String stopId) {
		
		String methodTag = "getParisMetroStationName";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris metro station name...");
		
        String stopName = "";
        
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabMetro());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);

		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		       
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", stopName = " + stopName);
	        
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return stopName;
	}
	
	
	/**
	 * Get paris metro station
	 * @param stopId
	 * @return tube station
	 */
	public static TubeStation getParisMetroStation(String stopId) {
		
		String methodTag = "getParisMetroStation";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris metro station...");
		
		TubeStation tubeStation = null;
		
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabMetro());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);
			  			
		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    String stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		    String routeId = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_ID);
		    double lat = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LON);
		    double lng = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LAT);
		       
		    Tube tube = getTube("paris", routeId);
		    
		    Tube[] tubes = {tube};
		    tubeStation = new TubeStation(stopId, stopName, stopId, lat, lng, tubes);
		    
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeStation.toString());
		    
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return tubeStation;
	}
	
	
	/**
	 * Get Paris tube stations collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return
	 */
	public static TubeStationsCollection getParisMetroStationsCollection(String latitude, String longitude, String radius) {
	
		String methodTag = "getParisMetroStationsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting paris metro stations collection...");
		
		TubeStationsCollection tubeStationsCollection = null;
		JSONArray jsonArray = new JSONArray();
		
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
        
        try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabMetro());
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
		    		"metros table count = " + table.count());

  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonLt);
  			
  			ArrayList<BasicDBObject> tubesConditionQuery = new ArrayList<BasicDBObject>();
  			tubesConditionQuery.add(latSup);
  			tubesConditionQuery.add(latInf);
  			tubesConditionQuery.add(lonSup);
  			tubesConditionQuery.add(lonInf);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", tubesConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);
  			
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
		    		"Cursor count = " + cursor.count());
  			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
		
			List<TubeStation> tubeStations = new ArrayList<TubeStation>();
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				String stopId = obj.getString(DataConstants.PARAM_VALUE_STOP_ID);
				String stationName = obj.getString(DataConstants.PARAM_KEY_STOP_NAME);
				double stationLat = obj.getDouble(DataConstants.PARAM_KEY_STOP_LAT);
				double stationLng = obj.getDouble(DataConstants.PARAM_KEY_STOP_LON);
				String routeId = obj.getString(DataConstants.PARAM_KEY_ROUTE_ID);
				String routeName = obj.getString(DataConstants.PARAM_KEY_ROUTE_NAME);				
				String direction = obj.getString(DataConstants.PARAM_KEY_DIRECTION_ID);
				
				direction = DataConstants.PARIS_METRO_DESTINATIONS.get(routeName).getDirection(direction);
				
				Tube tube = new Tube(routeId, routeName, direction);
				Tube[] tubes = {tube};
				
				TubeStation tubeStation = new TubeStation(
						stopId, stationName, stopId, stationLat, stationLng, tubes);
				
				tubeStations.add(tubeStation);
				
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
			    		tubeStation.toString());
			}
			
			TubeStation[] tubeStationsArray = new TubeStation[tubeStations.size()];
			
			for (int h = 0; h < tubeStations.size(); h++) {
				tubeStationsArray[h] = tubeStations.get(h);
			}
			
			tubeStationsCollection = new TubeStationsCollection(tubeStationsArray);
			
          } catch (Exception e) {
        	  TdServerLogger.printError(TAG, methodTag, e.getMessage());
          }  
        
			return tubeStationsCollection;
	}
	
	
	
	
	
	//
	//
	//Rail stations
	/**
	 * Get rail
	 * @param city
	 * @param routeId
	 * @return Tube
	 */
	public static Tube getParisRail(String routeId) {
		
		String methodTag = "getParisRail";
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				"Get rail for routeId = " + routeId + " in Paris ");
		
		String routeName = "";
		String routeDestination = "";

		//DataConstants.PARIS
		//Use 0 as direction by default
		routeName = getParisRailRouteName(routeId);
		String routeDirectionId = getParisRailRouteDirectionId(routeId);
		routeDestination = DataConstants.PARIS_RAIL_DESTINATIONS.get(routeName).getDirection(routeDirectionId);

		Tube tube = new Tube(routeId, routeName, routeDestination);
		
		if (DEBUG) TdServerLogger.print(TAG, methodTag, 
				tube.toString());
		
		return tube;
	}
	
	
	/**
	 * Get paris rail route name
	 * @param routeId
	 * @return Route name
	 */
	public static String getParisRailRouteName(String routeId) {
		String methodTag = "getParisRailRouteName";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris rail route name for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabRail());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeName = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeName = " + routeName);
        
        return routeName;
	}
	
	
	
	/**
	 * Get paris metro route id
	 * @param routeName
	 * @return
	 */
	public static String getParisRailRouteId(String routeName, String direction) {
		
		String methodTag = "getParisRailRouteId";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris metro route id for routeName");
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabRail());  	
		
		
		ArrayList<BasicDBObject> routeConditionQuery = new ArrayList<BasicDBObject>();
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_NAME, routeName));
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_DIRECTION_ID, direction));
		
		BasicDBObject routeQuery = new BasicDBObject("$and", routeConditionQuery);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeId = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeId = " + routeId);
        
        return routeId;
	}
	
	
	/**
	 * Get paris rail route direction id
	 * @param routeId
	 * @return Route direction id: 0 or 1
	 */
	public static String getParisRailRouteDirectionId(String routeId) {
		String methodTag = "getParisRailRouteDirectionId";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris rail route direction for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabRail());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeDirectionId = (String) cursor.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeDirectionId = " + routeDirectionId);
        
        return routeDirectionId;
	}
	
	
	/**
	 * Get paris rail station name by stop id
	 * @param stopId
	 * @return Paris rail station name
	 */
	public static String getParisRailStationName(String stopId) {
		
		String methodTag = "getParisRailStationName";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris rail station name...");
		
        String stopName = "";
        
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabRail());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);

		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		       
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", stopName = " + stopName);
	        
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return stopName;
	}
	
	/**
	 * Get paris rail station
	 * @param stopId
	 * @return tube station
	 */
	public static TubeStation getParisRailStation(String stopId) {
		
		String methodTag = "getParisRailStation";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting paris rail station...");
		
		TubeStation tubeStation = null;
		
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabRail());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId.trim());
			  			
		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    String stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		    String routeName = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		    String routeId = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_ID);
		    double lat = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LON);
		    double lng = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LAT);
		    String direction = (String) nxtObj.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		    
		    direction = DataConstants.PARIS_RAIL_DESTINATIONS.get(routeName).getDirection(direction);
		    
		    Tube tube = new Tube(routeId, routeName, direction);
		    Tube[] tubes = {tube};
		    tubeStation = new TubeStation(stopId, stopName, stopId, lat, lng, tubes);
		    
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeStation.toString());
		    
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return tubeStation;
	}	
	
	
	
	
	//
	//
	//Rails
	/**
	 * Get Paris rail stations (RER) collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return
	 */
	public static TubeStationsCollection getParisRailStationsCollection(String latitude, String longitude, String radius) {
	
		String methodTag = "getParisRailStationsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting paris rail stations collection...");
		
		TubeStationsCollection tubeStationsCollection = null;
		JSONArray jsonArray = new JSONArray();
		
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
        
        try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabRail());
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
		    		"rail table count = " + table.count());

  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonLt);
  			
  			ArrayList<BasicDBObject> tubesConditionQuery = new ArrayList<BasicDBObject>();
  			tubesConditionQuery.add(latSup);
  			tubesConditionQuery.add(latInf);
  			tubesConditionQuery.add(lonSup);
  			tubesConditionQuery.add(lonInf);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", tubesConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);
  			
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
		    		"Cursor count = " + cursor.count());
  			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
		
			List<TubeStation> tubeStations = new ArrayList<TubeStation>();
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				String stopId = obj.getString(DataConstants.PARAM_VALUE_STOP_ID);
				String stationName = obj.getString(DataConstants.PARAM_KEY_STOP_NAME);
				double stationLat = obj.getDouble(DataConstants.PARAM_KEY_STOP_LAT);
				double stationLng = obj.getDouble(DataConstants.PARAM_KEY_STOP_LON);
				String routeId = obj.getString(DataConstants.PARAM_KEY_ROUTE_ID);
				String routeName = obj.getString(DataConstants.PARAM_KEY_ROUTE_NAME);				
				String direction = obj.getString(DataConstants.PARAM_KEY_DIRECTION_ID);
				
			    direction = DataConstants.PARIS_RAIL_DESTINATIONS.get(routeName).getDirection(direction);
				
				Tube tube = new Tube(routeId, routeName, direction);
				Tube[] tubes = {tube};
				
				TubeStation tubeStation = new TubeStation(
						stopId, stationName, stopId, stationLat, stationLng, tubes);
				
				tubeStations.add(tubeStation);
				
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, 
			    		tubeStation.toString());
			}
			
			TubeStation[] tubeStationsArray = new TubeStation[tubeStations.size()];
			
			for (int h = 0; h < tubeStations.size(); h++) {
				tubeStationsArray[h] = tubeStations.get(h);
			}
			
			tubeStationsCollection = new TubeStationsCollection(tubeStationsArray);
			
          } catch (Exception e) {
          	TdServerLogger.printError(TAG, methodTag, 
		    		e.getMessage());
          }  
        
			return tubeStationsCollection;
	}
		

	
	
	//
	//
	//Trams
	/**
	 * Get patis tram route name
	 * @param routeId
	 * @return Route name
	 */
	public static String getParisTramRouteName(String routeId) {
		String methodTag = "getParisTramRouteName";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris tram route name for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabTram());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeName = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeName = " + routeName);
        
        return routeName;
	}
	
	
	/**
	 * Get paris tram route id
	 * @param routeName
	 * @param direction
	 * @return Tram route id
	 */
	public static String getParisTramRouteId(String routeName, String direction) {
		
		String methodTag = "getParisTramRouteId";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris tram route id for routeName = " + routeName);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabTram());  	
		
		
		ArrayList<BasicDBObject> routeConditionQuery = new ArrayList<BasicDBObject>();
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_NAME, routeName));
		routeConditionQuery.add(new BasicDBObject(DataConstants.PARAM_KEY_DIRECTION_ID, direction));
		
		BasicDBObject routeQuery = new BasicDBObject("$and", routeConditionQuery);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeId = (String) cursor.get(DataConstants.PARAM_KEY_ROUTE_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeId = " + routeId);
        
        return routeId;
	}
	
	
	/**
	 * Get paris tram route direction id
	 * @param routeId
	 * @return Route direction id: 0 or 1
	 */
	public static String getParisTramRouteDirectionId(String routeId) {
		String methodTag = "getParisTramRouteDirectionId";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"Getting paris tram route direction for routeId = " + routeId);
        
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
		DBCollection table = database.getCollection(dbProfil.getTabTram());  	
		
		BasicDBObject routeQuery = new BasicDBObject(DataConstants.PARAM_KEY_ROUTE_ID, routeId);
        
		DBObject cursor = table.findOne(routeQuery);
		
		String routeDirectionId = (String) cursor.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, 
        		"routeDirectionId = " + routeDirectionId);
        
        return routeDirectionId;
	}
	
	/**
	 * Get paris tram station name by stop id
	 * @param stopId
	 * @return Paris tram station name
	 */
	public static String getParisTramStationName(String stopId) {
		
		String methodTag = "getParisTramStationName";
		
        if (DEBUG) TdServerLogger.print(TAG,  methodTag, "Getting paris tram station name...");
		
        String stopName = "";
        
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabTram());  			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);

		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		       
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", stopName = " + stopName);
	        
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return stopName;
	}
	
	/**
	 * Get tram station
	 * @param stopId
	 * @return tramstation
	 */
	public static TramStation getParisTramStation(String stopId) {
		
		String methodTag = "getParisTramStation";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting paris tram station...");
		
		TramStation station = null;
		
		try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabTram());
			
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId.trim());
			  			
		    DBObject nxtObj = table.findOne(searchQuery);
		    
		    String stopName = (String) nxtObj.get(DataConstants.PARAM_KEY_STOP_NAME);
		    String routeName = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_NAME);
		    String routeId = (String) nxtObj.get(DataConstants.PARAM_KEY_ROUTE_ID);
		    double lat = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LON);
		    double lng = (Double) nxtObj.get(DataConstants.PARAM_KEY_STOP_LAT);
		    String direction = (String) nxtObj.get(DataConstants.PARAM_KEY_DIRECTION_ID);
		    
		    direction = DataConstants.PARIS_TRAM_DESTINATIONS.get(routeName).getDirection(direction);
		    
		    Tram tram = new Tram(routeId, routeName, direction);
		    Tram[] trams = {tram};
		    station = new TramStation(stopId, stopName, lat, lng, trams);
		    
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, station.toString());
		    
		}  catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag,exception.getMessage());
		}
		
		return station;	
	}
	
	
	/**
	 * Get tram stations collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return
	 */
	public static TramStationsCollection getParisTramStationsCollection(String latitude, String longitude, String radius) {
		
		String methodTag = "getParisTramStationsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting paris tram stations collection...");
		
		TramStationsCollection tramStationsCollection = null;
				
		JSONArray jsonArray = new JSONArray();
		
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
        
        try {
			DB database = dbMap.get(DataConstants.PARIS);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);			
			DBCollection table = database.getCollection(dbProfil.getTabTram());
            if (DEBUG) TdServerLogger.print(TAG, "getParisTramStationsCollection", "Paris trams table count = " + table.count());

  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonLt);
  			
  			ArrayList<BasicDBObject> tubesConditionQuery = new ArrayList<BasicDBObject>();
  			tubesConditionQuery.add(latSup);
  			tubesConditionQuery.add(latInf);
  			tubesConditionQuery.add(lonSup);
  			tubesConditionQuery.add(lonInf);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", tubesConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);

            if (DEBUG) TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());
			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
  			
			TramStation[] tramStations = new TramStation[jsonArray.length()];
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				String stationName = obj.getString(DataConstants.PARAM_KEY_STOP_NAME);
				double stationLat = obj.getDouble(DataConstants.PARAM_KEY_STOP_LAT);
				double stationLng = obj.getDouble(DataConstants.PARAM_KEY_STOP_LON);
				String stationId = obj.getString(DataConstants.PARAM_VALUE_STOP_ID);
				String routeId = obj.getString(DataConstants.PARAM_KEY_ROUTE_ID);
				String routeName = obj.getString(DataConstants.PARAM_KEY_ROUTE_NAME);
				String direction = obj.getString(DataConstants.PARAM_KEY_DIRECTION_ID);
				
			    direction = DataConstants.PARIS_TRAM_DESTINATIONS.get(routeName).getDirection(direction);
				
				Tram tram = new Tram(routeId, routeName, direction);
				Tram[] trams = {tram};
				TramStation station = new TramStation(stationId, stationName, stationLat, stationLng, trams);
				tramStations[i] = station;
			}
			
			tramStationsCollection = new TramStationsCollection(tramStations);
			
            if (DEBUG) TdServerLogger.print(TAG, methodTag, tramStationsCollection.toString());
			
        } catch (Exception exception) {
        	TdServerLogger.printError(TAG, methodTag, exception.getMessage());
        }
			
		return tramStationsCollection;
	}
	
	//
	//
	//Classified noise
	/**
	 * Check if we need to insert paris classified noise data
	 * @return True if needed, False not needed
	 */
	public static boolean needInitParisNoiseData() {
        if (DEBUG) TdServerLogger.print(TAG, "needInitParisNoiseData", 
        		"Checking if need to insert paris noise data...");
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);
		DBCollection tableClassifiedNoise = database.getCollection(dbProfil.getTabClassifiedNoise());
		return (tableClassifiedNoise.count() == 0);
	}
	
	/**
	 * Initialize paris classified noise data
	 */
	public static void initParisClassifiedNoiseData() {
		
		String methodTag = "initParisClassifiedNoiseData";
		
		try {		
    		DB database = dbMap.get(DataConstants.PARIS);
    		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);
			DBCollection tableClassifiedNoise = database.getCollection(dbProfil.getTabClassifiedNoise());
						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise count = " + tableClassifiedNoise.count());
			
	        List<String> allParisMetroStopIds = getAllParisMetroStopIds();
			
	        for (String stopId: allParisMetroStopIds) {
	        	
	        	TubeStation station = getParisMetroStation(stopId);
	        	
	        	BasicDBObject searchQuery = new BasicDBObject("stopId", stopId);

				BasicDBObject updateClassifiedInfo = new BasicDBObject();
	
				String routeId = station.getTubes()[0].getId();
				updateClassifiedInfo.put("routeId", routeId);
				
				for (int j = 0; j < timeList.length; j++) {
					int min = 10;
					int max = 3000;
					
					if ((j < 7) || (j > (timeList.length - 10))) max = 600;
					
					double randomNoise = (double) (min + (Math.random() * (max - min)));
					
					updateClassifiedInfo.put(timeList[j], randomNoise);
					
					min = 1;
					max = INITIAL_VOTER;
					
					int randomVoter = (int) (min + (Math.random() * (max - min)));
					
					updateClassifiedInfo.put(timeList[j] + "-VOTES", randomVoter);
				}
				
				BasicDBObject updateObj = new BasicDBObject();
				updateObj.put("$set", updateClassifiedInfo);
				
				if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", routeId = " + routeId);
				
				tableClassifiedNoise.update(searchQuery, updateObj, true, true);
				
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise count = " + tableClassifiedNoise.count());
	        }
	        
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise count = " + tableClassifiedNoise.count());
	        
	        if (DEBUG) {
	        	
	        	DBCursor cursor = tableClassifiedNoise.find();
	        	
	        	TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());
		 			
	  			try {
	  				   while(cursor.hasNext()) {
	  					TdServerLogger.print(TAG, methodTag, cursor.next().toString());
	  				   }
	  				} finally {
	  				   cursor.close();
	  				}
	        }
		    
	        
	        if (DEBUG)  TdServerLogger.outLog(TAG, methodTag, "Init paris classified noise data ok");
	        
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
			
			TdServerLogger.outLogError(TAG, methodTag, "Init paris classified noise data failed");
		}
		
	}	
	
	
	
	
	//
	//
	//Crowdedness
	/**
	 * Check if we need to insert paris classified crowd data for paris
	 * @return True if needed, False not needed
	 */
	public static boolean needInitParisCrowdData() {
        if (DEBUG) TdServerLogger.print(TAG, "needInitParisCrowdData", 
        		"Checking if need to insert paris crowd data...");
		DB database = dbMap.get(DataConstants.PARIS);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);
		DBCollection tableClassifiedCrowd = database.getCollection(dbProfil.getTabClassifiedCrowd());
		return (tableClassifiedCrowd.count() == 0);
	}
	
	private static final int INITIAL_VOTER = 50;
	
	/**
	 * Initialize paris classified crowd data
	 */
	public static void initParisClassifiedCrowdData() {
		
		String methodTag = "initParisClassifiedCrowdData";
		
		try {		
    		DB database = dbMap.get(DataConstants.PARIS);
    		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.PARIS);
			DBCollection tableClassifiedCrowd = database.getCollection(dbProfil.getTabClassifiedCrowd());
						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd count = " + tableClassifiedCrowd.count());
			
	        List<String> allParisMetroStopIds = getAllParisMetroStopIds();
			
	        for (String stopId: allParisMetroStopIds) {
	        	
	        	TubeStation station = getParisMetroStation(stopId);
	        	
	        	BasicDBObject searchQuery = new BasicDBObject("stopId", stopId);

				BasicDBObject updateClassifiedInfo = new BasicDBObject();
	
				String routeId = station.getTubes()[0].getId();
				updateClassifiedInfo.put("routeId", routeId);
				
				for (int j = 0; j < timeList.length; j++) {
					int min = 1;
					int max = 5;
					
					if ((j < 7) || (j > (timeList.length - 10))) max = 2;
					
					double randomCrowd = (double) (min + (Math.random() * (max - min)));
					
					updateClassifiedInfo.put(timeList[j], randomCrowd);
					
					min = 1;
					max = INITIAL_VOTER;
					
					int randomVoter = (int) (min + (Math.random() * (max - min)));
					
					updateClassifiedInfo.put(timeList[j] + "-VOTES", randomVoter);
				}
				
				BasicDBObject updateObj = new BasicDBObject();
				updateObj.put("$set", updateClassifiedInfo);
				
				if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", routeId = " + routeId);
				
				tableClassifiedCrowd.update(searchQuery, updateObj, true, true);
				
		        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd count = " + tableClassifiedCrowd.count());
	        }
	        
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd count = " + tableClassifiedCrowd.count());
	        
	        if (DEBUG) {
	        	
	        	DBCursor cursor = tableClassifiedCrowd.find();
	        	
	  			TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());
		 			
	  			try {
	  				   while(cursor.hasNext()) {
	  					 TdServerLogger.print(TAG, methodTag, cursor.next().toString());
	  				   }
	  				} finally {
	  				   cursor.close();
	  				}
	        }
		    
	        
			TdServerLogger.outLog(TAG, methodTag, "Init paris classified crowd data ok");
	        
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
			
			TdServerLogger.outLogError(TAG, methodTag, "Init paris classified crowd data failed");
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	//
	//
	//London
	
	
	//Fact
	private static final String FACT = "FACT";
	
	/**
	 * Checking if need to insert london tube station data
	 * @return True if need to insert, false if don't need
	 */
	public static boolean needToInsertLondonTubeStationFactsData() {
        if (DEBUG) TdServerLogger.print(TAG, "needToInsertLondonTubeStationFactsData", 
        		"Checking if need to insert london tube station data...");
		DB database = dbMap.get(DataConstants.LONDON);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
		DBCollection table = database.getCollection(dbProfil.getTabFact());
		return (table.count() == 0);
	}
	
	/**
	 * Insert london tube station facts data
	 * @param is InputStream of facts csv file
	 */
	public static void insertLondonTubeStationFactsData(InputStream is) {
		String methodTag = "insertLondonTubeStationFactsData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating london tube station facts db...");
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabFact());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london tube station fact count = " + table.count());
	        
			CSVReader reader = new CSVReader(new InputStreamReader(is), ';');
			
			List<String[]> stations = reader.readAll();
			
			for (int i = 1; i < stations.size(); i++) {
				
				String[] station = stations.get(i);
				
				String stopName = station[0];
		        String stopId = station[1].trim();
		        String fact = station[2];
		        
	        	if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        			"stopId = " + stopId + ", stopName = " + stopName + ", fact = " + fact);
	        	
				BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
				
				BasicDBObject updateInfo = new BasicDBObject(
						DataConstants.PARAM_KEY_STOP_NAME, stopName).
	                    append(FACT, fact);		        

				BasicDBObject setCommand = new BasicDBObject();
				setCommand.put("$set", updateInfo);
				
				table.update(stopQuery, setCommand, true, true);
			}
		
			reader.close();
	        
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london tube station facts count = " + table.count());						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating tube stations fact db ok");
	        
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}		
	}
	
	/**
	 * Get london tube station fact
	 * @param stopId
	 * @return fact
	 */
	public static Incentive getLondonTubeStationFact(String stopId) {
		
		String methodTag = "getLondonTubeStationFact";
	
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london tube station fact...");
		
        Incentive incentive = null;
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabFact());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london tube station fact count = " + table.count());

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);
			
  			DBObject obj = table.findOne(searchQuery);
  			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "obj = " + obj.toString());
	        
			String stopName = (String) obj.get(DataConstants.PARAM_KEY_STOP_NAME);
			String fact = (String) obj.get(FACT);
			
			incentive = new Incentive(stopId, stopName, IncentiveType.FACT, fact);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, incentive.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
		
		return incentive;
	}
	
	//Joke
	private static final String JOKE = "JOKE";
	
	/**
	 * Checking if need to insert london tube station joke data
	 * @return True if need to insert, false if don't need
	 */
	public static boolean needToInsertLondonTubeStationJokesData() {
        if (DEBUG) TdServerLogger.print(TAG, "needToInsertLondonTubeStationJokesData", 
        		"Checking if need to insert london tube station jokes data...");
		DB database = dbMap.get(DataConstants.LONDON);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
		DBCollection table = database.getCollection(dbProfil.getTabJoke());
		return (table.count() == 0);
	}
	
	/**
	 * Insert london tube station jokes data
	 * @param is InputStream of jokes csv file
	 */
	public static void insertLondonTubeStationJokesData(InputStream is) {
		String methodTag = "insertLondonTubeStationJokesData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating london tube station jokes db...");
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabJoke());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london tube station joke count = " + table.count());
	        
			CSVReader reader = new CSVReader(new InputStreamReader(is), ';');
			
			List<String[]> stations = reader.readAll();
			
			for (int i = 1; i < stations.size(); i++) {
				
				String[] station = stations.get(i);
				
				String stopName = station[0];
		        String stopId = station[1].trim();
		        String joke = station[2];
		        
	        	if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        			"stopId = " + stopId + ", stopName = " + stopName + ", joke = " + joke);
	        	
				BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
				
				BasicDBObject updateInfo = new BasicDBObject(
						DataConstants.PARAM_KEY_STOP_NAME, stopName).
	                    append(JOKE, joke);        

				BasicDBObject setCommand = new BasicDBObject();
				setCommand.put("$set", updateInfo);
				
				table.update(stopQuery, setCommand, true, true);
			}
		
			reader.close();
	        
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london tube station jokes count = " + table.count());						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating tube stations joke db ok");
	        
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}		
	}
	
	/**
	 * Get london tube station joke
	 * @param stopId
	 * @return joke
	 */
	public static Incentive getLondonTubeStationJoke(String stopId) {
		
		String methodTag = "getLondonTubeStationJoke";
	
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london tube station joke...");
		
        Incentive incentive = null;
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabJoke());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london tube station joke count = " + table.count());

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);
			
  			DBObject obj = table.findOne(searchQuery);
  			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "obj = " + obj.toString());
	        
			String stopName = (String) obj.get(DataConstants.PARAM_KEY_STOP_NAME);
			String joke = (String) obj.get(JOKE);
			
			incentive = new Incentive(stopId, stopName, IncentiveType.JOKE, joke);
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, incentive.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
		
		return incentive;
	}
	
		
	//
	//
	//Bus
	//Insert bus metro stops data into database
	private static final String BUS = "BUS";
	private static final String _ID = "_ID";
	private static final String _DEST = "_DEST";
	private static final String _NAME = "_NAME";
	private static final int MAX_BUS = 20;
	
	
	/**
	 * Check if need to insert london bus stops data
	 * @return True if need to insert, false if don't need
	 */
	public static boolean needToInsertLondonBusStopsData() {
        if (DEBUG) TdServerLogger.print(TAG, "needToInsertLondonBusStopsData", 
        		"Checking if need to insert london bus stops data...");
		DB database = dbMap.get(DataConstants.LONDON);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
		DBCollection table = database.getCollection(dbProfil.getTabBus());
		return (table.count() == 0);
	}
	
	
	/**
	 * Insert london bus stops data
	 */
	public static void insertLondonBusStopsData(InputStream is) {
		
		String methodTag = "insertLondonBusStopsData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating london bus stations db...");
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabBus());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london bus table count = " + table.count());
	        
	        /*
	        double londonCenterLat = (TransportArea.LONDON_MAX_LAT + TransportArea.LONDON_MIN_LAT)*0.5;
	        double londonCenterLon = (TransportArea.LONDON_MAX_LON + TransportArea.LONDON_MIN_LON)*0.5;
	        double radius = 1000 * 40; //40 km
	        
	        BusStationsCollection busStationsCollection = 
	        		BusStopResponse.getBusStationsCollection(String.valueOf(londonCenterLat), String.valueOf(londonCenterLon), String.valueOf(radius));
	        */
	        
	        BusStationsCollection busStationsCollection = LondonBusStopsCsvHelper.getBusStationsCollectionByCsvFile(is);
	        
	        BusStation[] busStations = busStationsCollection.getBusStations();
	        
	        
	        for (int i = 0; i < busStations.length; i++) {
	        	BusStation station = busStations[i];
	        	String stopId = station.getId();
	        	String stopName = station.getName();
	        	double stopLat = station.getLatitude();
	        	double stopLon = station.getLongitude();
	        	
	        	if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        			"stopId = " + stopId + ", stopName = " + stopName + ", stopLat = " + stopLat + ", stopLon = " + stopLon);
	        	
				BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
				BasicDBObject updateInfo = new BasicDBObject(
						DataConstants.PARAM_KEY_STOP_NAME, stopName).
	                    append(DataConstants.PARAM_KEY_STOP_LAT, stopLat).
	                    append(DataConstants.PARAM_KEY_STOP_LON, stopLon);
					        	
	        	Bus[] buses = station.getBuses();
	 	        	
	        	//Format
	        	// (BUS1_ID, "260")
	        	// (BUS1_NAME, "260")
	        	// (BUS1_DEST, "White City")
	        	for (int j = 0; j < buses.length; j++) {
	        		updateInfo.append(BUS + (j + 1) + _ID, buses[j].getId());
	        		updateInfo.append(BUS + (j + 1) + _NAME, buses[j].getName());
	        		updateInfo.append(BUS + (j + 1) + _DEST, buses[j].getDestination());
	        	}
	        	
	        	//Fill in empty case
	        	// (BUS1_ID, "*")
	        	// (BUS1_NAME, "*")
	        	// (BUS1_DEST, "*")
	        	for (int j = buses.length; j < MAX_BUS; j++) {
	        		//System.out.println("Update empty case: j = " + j);
	        		updateInfo.append(BUS + (j + 1) + _ID, "*");	        		
	        		updateInfo.append(BUS + (j + 1) + _NAME, "*");
	        		updateInfo.append(BUS + (j + 1) + _DEST, "*");
	        	}
	        	//System.out.println("After update empty case!");
	        	
				BasicDBObject setCommand = new BasicDBObject();
				setCommand.put("$set", updateInfo);
				
				table.update(stopQuery, setCommand, true, true);
	        }
	        
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london bus table count = " + table.count());						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating bus stations db ok");		
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
	}
	
	public static void insertLondonBusStopsDataByDirectMethod(InputStream is) {
		
		String methodTag = "insertLondonBusStopsDataByDirectMethod";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating london bus stations db...");
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabBus());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london bus table count = " + table.count());
	        
			CSVReader reader = new CSVReader(new InputStreamReader(is), ';');
			
			List<String[]> stations = reader.readAll();	
			
			for (int i = 1; i < stations.size(); i++) {
				
				String[] station = stations.get(i);
				
		        String stopId = station[0].trim();
		        String stopName = station[1].trim();
		        double stopLat = Double.parseDouble(station[2]);
		        double stopLon = Double.parseDouble(station[3]);
		        
	        	if (DEBUG) TdServerLogger.print(TAG, methodTag, 
	        			"stopId = " + stopId + ", stopName = " + stopName + ", stopLat = " + stopLat + ", stopLon = " + stopLon);
	        	
				BasicDBObject stopQuery = new BasicDBObject(DataConstants.PARAM_VALUE_STOP_ID, stopId);
				BasicDBObject updateInfo = new BasicDBObject(
						DataConstants.PARAM_KEY_STOP_NAME, stopName).
	                    append(DataConstants.PARAM_KEY_STOP_LAT, stopLat).
	                    append(DataConstants.PARAM_KEY_STOP_LON, stopLon);		        

		        int bus_index = 0;
		        
		        for (int k = 4; k <= (station.length - 3); k = k + 3) {		       
	        		updateInfo.append(BUS + (bus_index + 1) + _ID, station[k]);
	        		updateInfo.append(BUS + (bus_index + 1) + _NAME, station[k + 1]);
	        		updateInfo.append(BUS + (bus_index + 1) + _DEST, station[k + 2]);
	        		bus_index++;
		        }
		        
				BasicDBObject setCommand = new BasicDBObject();
				setCommand.put("$set", updateInfo);
				
				table.update(stopQuery, setCommand, true, true);
			}
		
			reader.close();
	        
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london bus table count = " + table.count());						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating bus stations db ok");		
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
	}	
	
	
	/**
	 * Get london bus route name
	 * @param routeId
	 * @return Bus route name
	 */
	public static String getLondonBusRouteName(String routeId) {
		
		String methodTag = "getLondonBusRouteName";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london bus route name...");
		
		String routeName = "";
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabBus());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london bus table count = " + table.count());
	        
			BasicDBObject searchQuery = new BasicDBObject();
			
			for (int j = 1; j <= MAX_BUS; j++) {
				String busIdKey = BUS + j + _ID;
				searchQuery.put(busIdKey, routeId);
				DBObject obj = table.findOne(searchQuery);
				
				if (obj != null) {
					//Get the route name
					String busNameKey = BUS + j + _NAME;
					String busName = (String) obj.get(busNameKey);
					if (!(busName == null) && !busName.equals("*")) {
						routeName = busName;
						break;
					}
				}
			}
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
		
		return routeName;
	}
	
	/**
	 * Get london bus station
	 * @param stopId
	 * @return bus station
	 */
	public static BusStation getLondonBusStation(String stopId) {
		
		String methodTag = "getLondonBusStation";
	
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london bus station...");
		
		BusStation busStation = null;
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabBus());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london bus table count = " + table.count());

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(DataConstants.PARAM_VALUE_STOP_ID, stopId);
			
  			DBObject obj = table.findOne(searchQuery);
  			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "obj = " + obj.toString());
	        
			String stationName = (String) obj.get(DataConstants.PARAM_KEY_STOP_NAME);
			double stationLat = (Double) obj.get(DataConstants.PARAM_KEY_STOP_LAT);
			double stationLng = (Double) obj.get(DataConstants.PARAM_KEY_STOP_LON);

			List<Bus> buses = new ArrayList<Bus>();

			for (int j = 1; j <= MAX_BUS; j++) {
				//Read BUS1 to BUSMAX_BUS
				String busIdKey = BUS + j + _ID;
				String busNameKey = BUS + j + _NAME;
				String busDestKey = BUS + j + _DEST;
				
				String busId =  (String) obj.get(busIdKey);
				String busName = (String) obj.get(busNameKey);
				String busDest = (String) obj.get(busDestKey);
				
				if (busId.equals("*")) { 
					//Break
					j = MAX_BUS + 1;
				} else {
					Bus bus = new Bus(busId, busName, busDest);
					buses.add(bus);
				}
			}
			
			Bus[] busesArray = new Bus[buses.size()];
			
			for (int k = 0; k < buses.size(); k++) {
				busesArray[k] = buses.get(k);
			}
			
			busStation = new BusStation(
					stopId, stationName, stationLat, stationLng, busesArray);
  			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, busStation.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
		
		return busStation;
	}	

	/**
	 * Get bus stations collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return Bus stations collection
	 */
	public static BusStationsCollection getLondonBusStationsCollection(String latitude, String longitude, String radius) {
		
		String methodTag = "getLondonBusStationsCollection";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london bus stations collection...");
		
		BusStationsCollection busStationsCollection = null;
		JSONArray jsonArray = new JSONArray();
		
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
        
        try {
        	
    		DB database = dbMap.get(DataConstants.LONDON);
    		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabBus());

      	    if (DEBUG) TdServerLogger.print(TAG, methodTag, "London bus table count = " + table.count());
      		
  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LAT, latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject(DataConstants.PARAM_KEY_STOP_LON, lonLt);
  			
  			ArrayList<BasicDBObject> busesConditionQuery = new ArrayList<BasicDBObject>();
  			busesConditionQuery.add(latSup);
  			busesConditionQuery.add(latInf);
  			busesConditionQuery.add(lonSup);
  			busesConditionQuery.add(lonInf);
  			
  			BasicDBObject busesQuery = new BasicDBObject("$and", busesConditionQuery);
  			
  			DBCursor cursor = table.find(busesQuery);
   	    
  			if (DEBUG) TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());
      		 			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
  			
			List<BusStation> busStations = new ArrayList<BusStation>();
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				String stopId = (String) obj.get(DataConstants.PARAM_VALUE_STOP_ID);
				String stationName = (String) obj.get(DataConstants.PARAM_KEY_STOP_NAME);
				double stationLat = (Double) obj.get(DataConstants.PARAM_KEY_STOP_LAT);
				double stationLng = (Double) obj.get(DataConstants.PARAM_KEY_STOP_LON);

				List<Bus> buses = new ArrayList<Bus>();

				for (int j = 1; j <= MAX_BUS; j++) {
					//Read BUS1 to BUSMAX_BUS
					String busIdKey = BUS + j + _ID;
					String busNameKey = BUS + j + _NAME;
					String busDestKey = BUS + j + _DEST;
					
					String busId =  (String) obj.get(busIdKey);
					String busName = (String) obj.get(busNameKey);
					String busDest = (String) obj.get(busDestKey);
					
					if (busId.equals("*")) { 
						//Break
						j = MAX_BUS + 1;
					} else {
						Bus bus = new Bus(busId, busName, busDest);
						buses.add(bus);
					}
				}
				
				Bus[] busesArray = new Bus[buses.size()];
				
				for (int k = 0; k < buses.size(); k++) {
					busesArray[k] = buses.get(k);
				}
				
				BusStation busStation = new BusStation(
						stopId, stationName, stationLat, stationLng, busesArray);
				busStations.add(busStation);
			}
			
			BusStation[] busStationsArray = new BusStation[busStations.size()];
			
			for (int h = 0; h < busStations.size(); h++) {
				busStationsArray[h] = busStations.get(h);
			}
			
			busStationsCollection = new BusStationsCollection(busStationsArray);

  			if (DEBUG) TdServerLogger.print(TAG, methodTag, busStationsCollection.toString());
  			 
          } catch (Exception e) {
  				TdServerLogger.printError(TAG, methodTag, e.getMessage());
          }  
        
		return busStationsCollection;
	}
	
	
	
	

	
	//
	//
	//Metro
	// Insert london metro stops data into database
	private static final String ID = "id";
	private static final String NLC = "nlc";
	private static final String STC = "stc";
	
	private static final String LINE1 = "line1";
	private static final String LINE2 = "line2";
	private static final String LINE3 = "line3";
	private static final String LINE4 = "line4";
	private static final String LINE5 = "line5";

	private static final String LINE_NAME1 = "lineName1";
	private static final String LINE_NAME2 = "lineName2";
	private static final String LINE_NAME3 = "lineName3";
	private static final String LINE_NAME4 = "lineName4";
	private static final String LINE_NAME5 = "lineName5";
	
	private static final String LINE_DIRECTION1 = "lineDirection1";
	private static final String LINE_DIRECTION2 = "lineDirection2";
	private static final String LINE_DIRECTION3 = "lineDirection3";
	private static final String LINE_DIRECTION4 = "lineDirection4";
	private static final String LINE_DIRECTION5 = "lineDirection5";
	
	private static final String LAT = "lat";
	private static final String LNG = "lng";
	
	
	/**
	 * Check if need to insert london metro stops data
	 * @return True if need to insert, false if don't need
	 */
	public static boolean needToInsertLondonMetroStopsData() {
        if (DEBUG) TdServerLogger.print(TAG, "needToInsertLondonMetroStopsData", 
        		"Checking if need to insert london metro stops data...");
		DB database = dbMap.get(DataConstants.LONDON);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
		DBCollection table = database.getCollection(dbProfil.getTabMetro());
		return (table.count() == 0);
	}
	
	
	/**
	 * Insert london metro stops data into database
	 * @param stationsIs
	 */
	public static void insertLondonMetroStopsData(InputStream[] stationsIs) {
		
		String methodTag = "insertLondonMetroStopsData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating london metro stations db...");
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabMetro());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london metros table count = " + table.count());			
		
			for (int index = 0; index < 8; index ++)  {
				CSVReader reader = new CSVReader(new InputStreamReader(stationsIs[index]));
				List<String[]> stations = reader.readAll();
				
				for (int i = 1; i < stations.size(); i++) {
					//Ignore first row
					String[] station = stations.get(i);
					
					if (!station[1].trim().equalsIgnoreCase("*") && !station[2].trim().equalsIgnoreCase("*")) {
						BasicDBObject stopQuery = new BasicDBObject(ID, station[0]);
						
						BasicDBObject updateInfo = new BasicDBObject(
								NLC, station[1].trim()).
			                    append(STC, station[2].trim());
						
						for (int k = 1; k <= 5; k++) {
							String line = station[k + 2].trim();
							String lineName = DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(line);
							String lineDirection = DataConstants.LONDON_TUBE_DESTINATIONS.get(lineName);
							updateInfo.append("line" + k, line);
							updateInfo.append("lineName" + k, lineName);
							updateInfo.append("lineDirection" + k, lineDirection);
						}

						updateInfo.append(LAT, Double.parseDouble(station[8].trim()));
						updateInfo.append(LNG, Double.parseDouble(station[9].trim()));
						
						BasicDBObject setCommand = new BasicDBObject();
						setCommand.put("$set", updateInfo);
						
						table.update(stopQuery, setCommand, true, true);
					}
				}
				
				reader.close();
			}
				
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london metros table count = " + table.count());						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Creating tube stations db ok");		
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
	}
	
	
	/**
	 * Get metro route name
	 * @param routeId
	 * @return Route name (line name)
	 */
	public static String getLondMetroRouteName(String routeId) {
		return DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(routeId);
	}
	
	/**
	 * Get london metro station
	 * @param stopId
	 * @return tube station
	 */
	public static TubeStation getLondonMetroStation(String stopId) {
		
		String methodTag = "getLondonMetroStation";
				
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london metro station...");
		
		TubeStation tubeStation = null;
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabMetro());
			
	        if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStation", "london metros table count = " + table.count());

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(STC, stopId.trim());
			
  			DBObject obj = table.findOne(searchQuery);
  			
			String stationName = (String) obj.get(ID);
			double stationLat = (Double) obj.get(LAT);
			double stationLng = (Double) obj.get(LNG);
			String stationNLC = ((String) obj.get(NLC)).trim();
			List<Tube> tubes = new ArrayList<Tube>();

			for (int j = 1; j <= 5; j++) {
				//Read line1 to line5
				String line = "line" + j;
				String tubeCode = ((String) obj.get(line)).trim();
				
				if (tubeCode.equals("*")) { 
					//Break
					j = 6;
				} else {
					String tubeName = DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(tubeCode);
					String tubeDestinations = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
					Tube tube = new Tube(tubeCode, tubeName, tubeDestinations);
					tubes.add(tube);
				}
			}
			
			Tube[] tubesArray = new Tube[tubes.size()];
			
			for (int k = 0; k < tubes.size(); k++) {
				tubesArray[k] = tubes.get(k);
			}
			
			tubeStation = new TubeStation(
					stopId, stationName, stationNLC, stationLat, stationLng, tubesArray);
  			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeStation.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
		
		return tubeStation;
	}
	
	/**
	 * Get london metro station by stop name
	 * @param stopId
	 * @return tube station
	 */
	public static TubeStation getLondonMetroStationByStopName(String stopName) {
		
		String methodTag = "getLondonMetroStationByStopName";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Getting london metro station by stop name...");
		
		TubeStation tubeStation = null;
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabMetro());
			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "london metros table count = " + table.count());

			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put(ID, stopName);
			
  			DBObject obj = table.findOne(searchQuery);
  			
			String stopId = (String) obj.get(STC);
			double stationLat = (Double) obj.get(LAT);
			double stationLng = (Double) obj.get(LNG);
			String stationNLC = ((String) obj.get(NLC)).trim();
			List<Tube> tubes = new ArrayList<Tube>();

			for (int j = 1; j <= 5; j++) {
				//Read line1 to line5
				String line = "line" + j;
				String tubeCode = ((String) obj.get(line)).trim();
				
				if (tubeCode.equals("*")) { 
					//Break
					j = 6;
				} else {
					String tubeName = DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(tubeCode);
					String tubeDestinations = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
					Tube tube = new Tube(tubeCode, tubeName, tubeDestinations);
					tubes.add(tube);
				}
			}
			
			Tube[] tubesArray = new Tube[tubes.size()];
			
			for (int k = 0; k < tubes.size(); k++) {
				tubesArray[k] = tubes.get(k);
			}
			
			tubeStation = new TubeStation(
					stopId, stopName, stationNLC, stationLat, stationLng, tubesArray);
  			
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, tubeStation.toString());
			
		} catch (Exception exception) {
			TdServerLogger.printError(TAG, methodTag, exception.getMessage());
		}
		
		return tubeStation;
	}	
	
	
	
	/**
	 * Get london metro station route name
	 * @param routeId
	 * @return route name
	 */
	public static String getLondonMetroRouteName(String routeId) {
		
		if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationRouteName", 
        		"get route name for " + routeId);
		
		return DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(routeId);
	}
	
	/**
	 * Get london metro station route id
	 * @param routeName
	 * @return route id
	 */
	public static String getLondonMetroRouteId(String routeName) {
		
		if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationRouteId", 
        		"get route id for route name " + routeName);
		
		return DataConstants.LONDON_ROUTENAME_ROUTEID_MAP.get(routeName);
	}
	
	
	/**
	 * Get london metro station by location code
	 * @param locationCode
	 * @return
	 */
	public static TubeStation getLondonMetroStationByLocationCode(String locationCode) {
        
		if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationByLocationCode", 
        		"Getting london metro station from location code " + locationCode);
        
		return getLondonMetroStation(getLondonMetroStationCodeFromLocationCode(locationCode));
	}
	
	/**
	 * Get london metro station code from location code
	 * @param locationCode
	 * @return London metro station code
	 */
	public static String getLondonMetroStationCodeFromLocationCode(String locationCode) {
		
        if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationCodeFromLocationCode", 
        		"Getting london metro station code from location code " + locationCode);
		
		String stationCode = "*";
		
		String json = "";
		
		try {
			DB database = dbMap.get(DataConstants.LONDON);
			TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabMetro());
			
	        if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationCodeFromLocationCode", 
	        		"london metros table count = " + table.count());
      		
			BasicDBObject searchQuery = new BasicDBObject();
			searchQuery.put("nlc", locationCode.trim());
			
  			DBCursor cursor = table.find(searchQuery);
  			
  			if (cursor.count() == 0) return stationCode;
  			
  			try {
  				   while(cursor.hasNext()) {
  					   json = cursor.next().toString();
  				   }
  				} finally {
  				   cursor.close();
  				}
  			
  			JSONObject obj = new JSONObject(json);

			stationCode = obj.getString("stc").trim();
  			
	        if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationCodeFromLocationCode", 
	        		"station code = " + stationCode);			
			
		} catch (Exception exception) {
			TdServerLogger.print(TAG, "getLondonMetroStationCodeFromLocationCode", 
					exception.getMessage());
		}
		
		return stationCode;
	}

	public static List<TubeStation> getAllTubeStations(String city) {
		List<TubeStation> tubeStations = new ArrayList<TubeStation>();
		TdDbProfil dbProfil = null;
		DB database = null;
		if (DataConstants.LONDON.equalsIgnoreCase(city)) {
			database = dbMap.get(DataConstants.LONDON);
    		dbProfil = tdDbProfils.get(DataConstants.LONDON);
		} else {
			database = dbMap.get(DataConstants.PARIS);
    		dbProfil = tdDbProfils.get(DataConstants.PARIS);
		}
		DBCollection table = database.getCollection(dbProfil.getTabMetro());
		DBCursor cursor = table.find();
		while (cursor.hasNext()) {
			DBObject dbObj = cursor.next();
			tubeStations.add(createTubeStation(dbObj));
		}
		cursor.close();
		return tubeStations;
	}

	private static TubeStation createTubeStation(DBObject dbObj) {
		String stationName = (String) dbObj.get(ID);
		double stationLat = (Double) dbObj.get(LAT);
		double stationLng = (Double) dbObj.get(LNG);
		String stationNLC = ((String) dbObj.get(NLC)).trim();
		String stationSTC = ((String) dbObj.get(STC)).trim();
		List<Tube> tubes = new ArrayList<Tube>();

		for (int j = 1; j <= 5; j++) {
			//Read line1 to line5
			String line = "line" + j;
			String tubeCode = ((String) dbObj.get(line)).trim();
			
			if (tubeCode.equals("*")) { 
				//Break
				j = 6;
			} else {
				String tubeName = DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(tubeCode);
				String tubeDestinations = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
				Tube tube = new Tube(tubeCode, tubeName, tubeDestinations);
				tubes.add(tube);
			}
		}
		
		Tube[] tubesArray = new Tube[tubes.size()];
		
		for (int k = 0; k < tubes.size(); k++) {
			tubesArray[k] = tubes.get(k);
		}
		
		TubeStation tubeStation = new TubeStation(
				stationSTC, stationName, stationNLC, stationLat, stationLng, tubesArray);
		return tubeStation;
	}

	/**
	 * Get tube stations collection
	 * @param latitude
	 * @param longitude
	 * @param radius
	 * @return
	 */
	public static TubeStationsCollection getLondonMetroStationsCollection(String latitude, String longitude, String radius) {
	
        if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationsCollection", "Getting london metro stations collection...");
		
		TubeStationsCollection tubeStationsCollection = null;
		JSONArray jsonArray = new JSONArray();
		
		double lat = Double.parseDouble(latitude);
		double lon = Double.parseDouble(longitude);
		double rad = Double.parseDouble(radius);
		
        double maxLat = lat + (rad/111000);
        double minLat = lat - (rad/111000);
        double maxLon = lon + (rad/72000);
        double minLon = lon - (rad/72000);
        
        try {
        	
    		DB database = dbMap.get(DataConstants.LONDON);
    		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection table = database.getCollection(dbProfil.getTabMetro());

      	    if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationsCollection", "London metros table count = " + table.count());
      		
  			//Latitude inferior bound
  			BasicDBObject latGte = new BasicDBObject("$gte", minLat);
  			BasicDBObject latInf = new BasicDBObject("lat", latGte);
  		
  			//Latitude superior bound
  			BasicDBObject latLt = new BasicDBObject("$lt", maxLat);
  			BasicDBObject latSup = new BasicDBObject("lat", latLt);
  			
  			//Longitude inferior bound
  			BasicDBObject lonGte = new BasicDBObject("$gte", minLon);
  			BasicDBObject lonInf = new BasicDBObject("lng", lonGte);
  		
  			//Longitude superior bound
  			BasicDBObject lonLt = new BasicDBObject("$lt", maxLon);
  			BasicDBObject lonSup = new BasicDBObject("lng", lonLt);
  			
  			ArrayList<BasicDBObject> tubesConditionQuery = new ArrayList<BasicDBObject>();
  			tubesConditionQuery.add(latSup);
  			tubesConditionQuery.add(latInf);
  			tubesConditionQuery.add(lonSup);
  			tubesConditionQuery.add(lonInf);
  			
  			BasicDBObject tubesQuery = new BasicDBObject("$and", tubesConditionQuery);
  			
  			DBCursor cursor = table.find(tubesQuery);
   	    
  			if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationsCollection", "Cursor count = " + cursor.count());
      		 			
  			try {
  				   while(cursor.hasNext()) {
					   jsonArray.put(cursor.next());
  				   }
  				} finally {
  				   cursor.close();
  				}
  			
			List<TubeStation> tubeStations = new ArrayList<TubeStation>();
			
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject obj = new JSONObject(jsonArray.getString(i));
				String stationName = obj.getString(ID);
				double stationLat = obj.getDouble(LAT);
				double stationLng = obj.getDouble(LNG);
				String stationNLC = obj.getString(NLC).trim();
				String stationSTC = obj.getString(STC).trim();
				List<Tube> tubes = new ArrayList<Tube>();
	
				for (int j = 1; j <= 5; j++) {
					//Read line1 to line5
					String line = "line" + j;
					String tubeCode = obj.getString(line).trim();
					
					if (tubeCode.equals("*")) { 
						//Break
						j = 6;
					} else {
						String tubeName = DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(tubeCode);
						String tubeDestinations = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
						Tube tube = new Tube(tubeCode, tubeName, tubeDestinations);
						tubes.add(tube);
					}
				}
				
				Tube[] tubesArray = new Tube[tubes.size()];
				
				for (int k = 0; k < tubes.size(); k++) {
					tubesArray[k] = tubes.get(k);
				}
				
				TubeStation tubeStation = new TubeStation(
						stationSTC, stationName, stationNLC, stationLat, stationLng, tubesArray);
				tubeStations.add(tubeStation);
			}
			
			TubeStation[] tubeStationsArray = new TubeStation[tubeStations.size()];
			
			for (int h = 0; h < tubeStations.size(); h++) {
				tubeStationsArray[h] = tubeStations.get(h);
			}
			
			tubeStationsCollection = new TubeStationsCollection(tubeStationsArray);

  			if (DEBUG) TdServerLogger.print(TAG, "getLondonMetroStationsCollection", tubeStationsCollection.toString());
  			 
          } catch (Exception e) {
  				TdServerLogger.printError(TAG, "getLondonMetroStationsCollection",  e.getMessage());
          }  
        
		return tubeStationsCollection;
	}	


	
	
	//
	//
	//Classified noise data
	public static boolean needInitLondonClassifiedNoiseData() {
        if (DEBUG) TdServerLogger.print(TAG, "needInitLondonClassifiedNoiseData", 
        		"Checking if need to insert london classified noise data...");
		DB database = dbMap.get(DataConstants.LONDON);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
		DBCollection tableClassifiedNoise = database.getCollection(dbProfil.getTabClassifiedNoise());
		return (tableClassifiedNoise.count() == 0);
	}
	
	
	private static final int MAX_NOISE = 3000;
	
	/**
	 * Insert London classified noise data
	 */
	public static void insertLondonClassifiedNoiseData(InputStream is) {
		
		String methodTag = "insertLondonClassifiedNoiseData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Inserting london classified noise data...");
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					TdServerLogger.printError(TAG, methodTag, e.getMessage());
				}
			}
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, sb.toString());
		
		String json = sb.toString();
		
		try {
			JSONArray jsonArray = new JSONArray(json);

    		DB database = dbMap.get(DataConstants.LONDON);
    		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection tableClassifiedNoise = database.getCollection(dbProfil.getTabClassifiedNoise());
						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise count = " + tableClassifiedNoise.count());
			
			for (int i = 0; i < jsonArray.length(); i++) {
				
				JSONObject jsonObj = jsonArray.getJSONObject(i);
				String stopId = getLondonMetroStationCodeFromLocationCode(jsonObj.getString("nlc"));
				
				if (stopId.equals("*")) continue;
				
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId);
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, jsonObj.toString());
			    
			    TubeStation station = getLondonMetroStation(stopId);
			    Tube[] tubes = station.getTubes();
			    
			    for (int k = 0; k < tubes.length; k++) {
					BasicDBObject updateClassifiedInfo = new BasicDBObject();
					
					updateClassifiedInfo.put("stopId", stopId);
					
					//Reinitialize number of votes as historical voters
					updateClassifiedInfo.put("routeId", tubes[k].getId());
					
					for (int j = 0; j < timeList.length; j++) {
						//Put double value in the database to make data more accurate
						double crowd = DataConstants.CROWD_LEVEL_MAP.get(jsonObj.getString(timeList[j]));
						int min = 10;
						int max = (int) (crowd * 600); //crowd * 3000 / 5
						double randomNoise = (double) (min + (Math.random() * (max - min)));
						
						updateClassifiedInfo.put(timeList[j], randomNoise);
						
						min = 50;
						max = HISTORICAL_VOTERS;
						int randomVoter = (int) (min + (Math.random() * (max - min)));
						
						updateClassifiedInfo.put(timeList[j] + "-VOTES", randomVoter);
					    //if (DEBUG) TdServerLogger.print(TAG, methodTag, "interval = " + timeList[j] + ", crowd = " + crowd);
					}
					
					tableClassifiedNoise.insert(updateClassifiedInfo);
			    }
			}
			
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified noise table count = " + tableClassifiedNoise.count());
		    
	        if (DEBUG) {
	        	
	        	DBCursor cursor = tableClassifiedNoise.find();
	        	
	  			TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());
		 			
	  			try {
	  				   while(cursor.hasNext()) {
	  					 TdServerLogger.print(TAG, methodTag, cursor.next().toString());
	  				   }
	  				} finally {
	  				   cursor.close();
	  				}
	        }
		    
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
	}
	
	
	
	
	//
	//
	//Crowdedness
	private static final int HISTORICAL_VOTERS = 100;
	
	public static boolean needInitLondonCrowdData() {
        if (DEBUG) TdServerLogger.print(TAG, "needInitLondonCrowdData", 
        		"Checking if need to insert london crowd data...");
		DB database = dbMap.get(DataConstants.LONDON);
		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
		DBCollection tableClassifiedCrowd = database.getCollection(dbProfil.getTabClassifiedCrowd());
		return (tableClassifiedCrowd.count() == 0);
	}
	
	
	/**
	 * Insert London classified crowd data
	 * @param is
	 */
	public static void insertLondonClassifiedCrowdData(InputStream is) {
		
		String methodTag = "insertLondonClassifiedCrowdData";
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Inserting london classified crowd data...");
		
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
		} catch (IOException e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					TdServerLogger.printError(TAG, methodTag, e.getMessage());
				}
			}
		}
		
        if (DEBUG) TdServerLogger.print(TAG, methodTag, sb.toString());
		
		String json = sb.toString();
		
		try {		
			JSONArray jsonArray = new JSONArray(json);

    		DB database = dbMap.get(DataConstants.LONDON);
    		TdDbProfil dbProfil = tdDbProfils.get(DataConstants.LONDON);
			DBCollection tableClassifiedCrowd = database.getCollection(dbProfil.getTabClassifiedCrowd());
						
	        if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd count = " + tableClassifiedCrowd.count());
			
			for (int i = 0; i < jsonArray.length(); i++) {
				
				JSONObject jsonObj = jsonArray.getJSONObject(i);
				String stopId = getLondonMetroStationCodeFromLocationCode(jsonObj.getString("nlc"));
				
				if (stopId.equals("*")) continue;
				
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId);
			    if (DEBUG) TdServerLogger.print(TAG, methodTag, jsonObj.toString());
			    
			    TubeStation station = getLondonMetroStation(stopId);
			    Tube[] tubes = station.getTubes();
			    
			    for (int k = 0; k < tubes.length; k++) {
					BasicDBObject updateClassifiedInfo = new BasicDBObject();
					
					updateClassifiedInfo.put("stopId", stopId);
					
					//Reinitialize number of votes as historical voters
					updateClassifiedInfo.put("routeId", tubes[k].getId());
					
					for (int j = 0; j < timeList.length; j++) {
						//Put double value in the database to make data more accurate
						double crowd = DataConstants.CROWD_LEVEL_MAP.get(jsonObj.getString(timeList[j]));
						updateClassifiedInfo.put(timeList[j], crowd);
						
						int min = 50;
						int max = HISTORICAL_VOTERS;
						int randomVoter = (int) (min + (Math.random() * (max - min)));
						
						updateClassifiedInfo.put(timeList[j] + "-VOTES", randomVoter);
					    //if (DEBUG) TdServerLogger.print(TAG, methodTag, "interval = " + timeList[j] + ", crowd = " + crowd);
					}
					
					tableClassifiedCrowd.insert(updateClassifiedInfo);
			    }
			}
			
		    if (DEBUG) TdServerLogger.print(TAG, methodTag, "Classified crowd table count = " + tableClassifiedCrowd.count());
		    
	        if (DEBUG) {
	        	
	        	DBCursor cursor = tableClassifiedCrowd.find();
	        	
	  			TdServerLogger.print(TAG, methodTag, "Cursor count = " + cursor.count());
		 			
	  			try {
	  				   while(cursor.hasNext()) {
	  					 TdServerLogger.print(TAG, methodTag, cursor.next().toString());
	  				   }
	  				} finally {
	  				   cursor.close();
	  				}
	        }
		    
		} catch (Exception e) {
			TdServerLogger.printError(TAG, methodTag, e.getMessage());
		}
		
	}
	
	
	
	//
	//
	// Processing query time
	/**
	 * Extract current time to interval
	 * @param initialTime
	 * @return
	 */
	public static String extractTime(String initialTime) {
		String time = "";
		
		int hour = Integer.parseInt(initialTime.substring(0, 2));
		int minute = Integer.parseInt(initialTime.substring(2));
		int interval = (int) (Math.floor(minute/15)*15); 
		
		if (interval != 45) {
			time = intToTime(hour) + intToTime(interval) + "-" + intToTime(hour) + intToTime(interval + 15);
		} else {
			time = intToTime(hour) + intToTime(interval) + "-" + intToTime(hour + 1) + "00";
		}
		
		return time;
	}
	
	/**
	 * Extract query time to current time format
	 * @param initialTime
	 * @return current time format
	 */
	public static String extractQueryTimeToCurrentTime(String initialTime) {
		
		String newTime = "";
			
		String hour = initialTime.substring(0, 2);
		String minute = initialTime.substring(2);

		String formatDate = "yyyy-MM-dd'T'";
		SimpleDateFormat sdfDate = new SimpleDateFormat(formatDate, Locale.FRANCE);
		String date = sdfDate.format(new Date());
		
		String formatSecond = ":ss.SSS'Z'";
		SimpleDateFormat sdfSecond = new SimpleDateFormat(formatSecond, Locale.FRANCE);
		String second = sdfSecond.format(new Date());
		
		newTime = date + hour + ":" + minute + second;
		
		return newTime;
	}
	
	/**
	 * Convert time integer to time string
	 * @param timeInt
	 * @return  time string
	 */
	public static String intToTime(int timeInt) {
		
		String time = "00";
		
		if (timeInt < 10) {
			time = "0" + timeInt;
		} else {
			time = String.valueOf(timeInt);
		}
		
		return time;
	}
	
	/**
	 * Convert current date string to timestamp
	 * @param dateString
	 * @return timestamp
	 */
	public static long convertDateStringToTimestamp(String dateString) {
		
		long timestamp = -1l;
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			Date date = sdf.parse(dateString);
			timestamp = date.getTime();
		} catch (Exception e) {
			e.printStackTrace();
			TdServerLogger.printError(TAG, "convertDateStringToTimestamp", e.getMessage());
		}
		
		return timestamp;
		
	}
	
	/**
	 * Convert timestamp to current time string
	 * @param timestamp
	 * @return current time string
	 */
	public static String convertTimestampToDateString(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		String date = sdf.format(new Date(timestamp));
		
		return date;
	}
	
	/**
	 * Convert current time to time query string
	 * @param timestamp
	 * @return time query string
	 */
	public static String convertTimestampToTimeQueryString(long timestamp) {
		SimpleDateFormat sdf = new SimpleDateFormat("HHmm");
		String date = sdf.format(new Date(timestamp));
		return date;
	}
}
