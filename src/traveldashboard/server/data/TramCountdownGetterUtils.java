package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class TramCountdownGetterUtils extends ResponseUtils {
	
	private static final String TAG = "TramCountdownGetterUtils";
	
	public static NetworkMessage getTramCountdownMessage(NetworkMessage req) {
		String methodTag = "getTramCountdownMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start getting tram countdown message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		int maxValues = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
		
		Gson gson = new Gson();
		String json = "null";
		
		if (city.equals(DataConstants.PARIS)) {
			json = gson.toJson(RatpTimeTableProcess.getParisTramRealTimeInfos(stopId, routeId, maxValues));
		}

		resp.addPayload(DataConstants.TRAM_COUNTDOWN_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End getting tram countdown message : " + new String(resp.encode()));
		
		return resp;
	}
	
}

