package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;


import com.google.gson.Gson;

public class TubeCountdownGetterUtils extends ResponseUtils {
	
	private static final String TAG = "TubeCountdownGetterUtils";
	
	public static NetworkMessage getTubeCountdownMessage(NetworkMessage req) {
		
		String methodTag = "getTubeCountdownMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start tube countdown message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		int maxValues = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
		
		Gson gson = new Gson();
		String json = "null";
				
		if (city.equals(DataConstants.PARIS)) {
			json = gson.toJson(RatpTimeTableProcess.getParisMetroRealTimeInfos(stopId, routeId, maxValues));
		} else if (city.equals(DataConstants.LONDON)) {
			json = gson.toJson(TubeCountdownResponse.getTubeRealTimeInfosCollection(stopId, routeId, maxValues));
		}
		
		resp.addPayload(DataConstants.TUBE_COUNTDOWN_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get tube countdown message : " + new String(resp.encode()));
		
		return resp;
	}
	
}
