package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import traveldashboard.server.routeplanner.TFL_Journey_Planner;

import com.google.gson.Gson;

public class RoutePlannerGetterUtils extends ResponseUtils {
	
	private static final String TAG = "RoutePlannerGetterUtils";
	
	public static NetworkMessage getRoutePlannerMessage(NetworkMessage req) {
		
		String methodTag = "getRoutePlannerMessage";
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get route planner message");
        
		NetworkMessage resp = prepareMsgReturn(req);
		
		String routePlannerCity = req.getPayload(DataConstants.ROUTE_PLANNER_CITY);
		
		//Not available in Paris
		if (routePlannerCity.equals(DataConstants.PARIS)) {
			resp.addPayload(DataConstants.ROUTE_PLANNER_MESSAGE, "null");
			return resp;
		}
		
		String depStopName = req.getPayload(DataConstants.DEPART_STOP_NAME);
		String arrStopName =  req.getPayload(DataConstants.ARRIVAL_STOP_NAME);
		String depDate = req.getPayload(DataConstants.DEPART_DATE);
		String depTime = req.getPayload(DataConstants.DEPART_TIME);
		
		int totalMotType = Integer.parseInt(req.getPayload(DataConstants.TOTAL_MOT_TYPE_INCLUDED));

		int[] motTypeIncluded = new int[totalMotType];
		
		for (int i = 0; i < totalMotType; i++) {
			motTypeIncluded[i] = Integer.parseInt(req.getPayload(DataConstants.MOT_TYPE_INCLUDED_ + i));
		}
		
		Gson gson = new Gson();
		String json = gson.toJson(TFL_Journey_Planner.getItdItinerary(depStopName, arrStopName, depDate, depTime, motTypeIncluded));
		resp.addPayload(DataConstants.ROUTE_PLANNER_MESSAGE, json);

		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Finish get route planner message : "  + new String(resp.encode()));
        
		return resp;
	}
}
