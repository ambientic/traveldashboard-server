package traveldashboard.server.data;


import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import traveldashboard.data.BikeStation;
import traveldashboard.data.BikeStationsCollection;


public class TflBikeInfoProcess {

	private static final String URL = "http://www.tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml";
	private static final boolean debug = DataConstants.DEBUG;
	
	/**
	 * Get london bike stations collection
	 * @return London bike stations collection
	 */
	public static BikeStationsCollection getBikeStationsCollection() {
		BikeStationsCollection bikeStationsCollection = null;
		
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URL);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);
					
		    // Convert XML to JSON Object
            JSONObject xmlJSONObj = XML.toJSONObject(json);
            
			JSONObject stationsObj = xmlJSONObj.getJSONObject("stations");
			
			JSONArray stationArray = stationsObj.getJSONArray("station");
			
			BikeStation[] bikeStations = new BikeStation[stationArray.length()];
			
			for (int i = 0; i  < stationArray.length(); i++) {	
				JSONObject jsonCurrentObject = stationArray.getJSONObject(i);
				String id = jsonCurrentObject.getString("id");
				String name = jsonCurrentObject.getString("name");
				double latBike = jsonCurrentObject.getDouble("lat");
				double lonBike = jsonCurrentObject.getDouble("long");
				int nbBikes = jsonCurrentObject.getInt("nbBikes");
				int nbEmptyDocks = jsonCurrentObject.getInt("nbEmptyDocks");
				int nbTotalDocks = jsonCurrentObject.getInt("nbDocks");
				boolean locked = jsonCurrentObject.getBoolean("locked");
				bikeStations[i] = new BikeStation(id, name, latBike, lonBike, 
						nbBikes, nbEmptyDocks, nbTotalDocks, locked);
			}
			
			if (debug) {
				for (BikeStation station : bikeStations) {
					System.out.println(station.toString());
				}
			}
			
			bikeStationsCollection = new BikeStationsCollection(bikeStations);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return bikeStationsCollection;
	}
}
