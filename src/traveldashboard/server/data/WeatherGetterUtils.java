package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class WeatherGetterUtils extends ResponseUtils {

	private static final String TAG = "WeatherGetterUtils";
	
	public static NetworkMessage getWeatherMessage(NetworkMessage req) {
		
		String methodTag = "getWeatherMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get weather message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String latitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LAT);
		String longitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LON);
		
		Gson gson = new Gson();
		String json = gson.toJson(WeatherResponse.getWeather(latitude, longitude));
		
		resp.addPayload(DataConstants.WEATHER_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get weather message" + new String(resp.encode()));
		
		return resp;
	}
}
