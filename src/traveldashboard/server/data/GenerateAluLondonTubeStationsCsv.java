package traveldashboard.server.data;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class GenerateAluLondonTubeStationsCsv {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		generateAluLondonTubeStationCsv("alu_stations_with_locations.csv");
	}

	public static void generateAluLondonTubeStationCsv(String fileName) {

		try {
			
			//CSVWriter writer = new CSVWriter(new FileWriter(fileName), ';', CSVWriter.NO_QUOTE_CHARACTER);
			CSVWriter writer = new CSVWriter(new FileWriter(fileName), ',', CSVWriter.NO_QUOTE_CHARACTER, CSVWriter.NO_ESCAPE_CHARACTER);
			
			InputStream is = new FileInputStream("stations_with_locations.csv");
			CSVReader reader = new CSVReader(new InputStreamReader(is), ',');
			List<String[]> stations = reader.readAll();
			
			for (String[] station : stations) {
				
				try {
					/*
					String stationString = station[0];
					
					System.out.println("stationString = " + stationString);
	
					String[] splitStation = stationString.split(",");
					*/
					
					String stationId = station[0];
					
					String stationName = station[1];

					System.out.println("stationName = " + stationName);
					
					String stationLon = "";
					String stationLat = "";
					String stationZone = "";
					
					if (station.length > 2) {
						stationLon = station[2];
						
						stationLat = station[3];
						
						if (station.length > 4) {
							stationZone = station[4];
						}
						else{
							continue;
						}
						
						if (stationZone.equals("")) continue;
						stationZone = stationZone.replace(",", "/");
						
					} else {
						continue;
					}
	
					System.out.println("stationId = " + stationId 
							+ ", stationName = "  + stationName
							+ ", stationLat = " + stationLat
							+ ", stationLon = " + stationLon
							+ ", stationZone = " + stationZone
							);
					
					String[] newCsvContent = {stationId, stationName, stationLon, stationLat, stationZone};
					
					writer.writeNext(newCsvContent);
					
				} catch (Exception exception) {
					exception.printStackTrace();
				}

				
			}
			
			reader.close();
			
			writer.close();
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
	}
	
}
