package traveldashboard.server.data;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;

import traveldashboard.data.weather.Weather;


/**
 * Get weather informationn through open weather map api
 * @author khoo
 *
 */
public class WeatherResponse {
	
	/**
	 * Get wetaher object
	 * @param latitude
	 * @param longitude
	 * @return Weather object
	 */
	public static Weather getWeather(String latitude, String longitude) {
		Weather weather = null;
		
		try {
			//http://api.openweathermap.org/data/2.5/find?lat=48.85297&lon=2.3499&cnt=1
			String uri = "http://api.openweathermap.org/data/2.5/find?lat=" + latitude + "&lon=" + longitude + "&cnt=1";
			if (DataConstants.DEBUG) System.out.println(uri);
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			String json = httpClient.execute(httpGet, responseHandler);			
			JSONObject jsonObject1 = new JSONObject(json);
			JSONArray jsonArray1 = jsonObject1.getJSONArray("list");
			JSONObject jsonObject2 =  jsonArray1.getJSONObject(0);
			JSONObject jsonObject3 = jsonObject2.getJSONObject("main");
			int humidity = jsonObject3.getInt("humidity");
			double pressure = jsonObject3.getDouble("pressure");
			double temperature = jsonObject3.getDouble("temp") - 273.15;
			double tempMax = jsonObject3.getDouble("temp_max") - 273.15;
			double tempMin = jsonObject3.getDouble("temp_min") - 273.15;			
			JSONArray jsonArray2 = jsonObject2.getJSONArray("weather");
			JSONObject jsonObject4 = jsonArray2.getJSONObject(0);
			String description = jsonObject4.getString("description");
			String icon = jsonObject4.getString("icon");
			if (DataConstants.DEBUG) System.out.println(
					"humidity = " + humidity + "%, " + 
					"pressure = " + pressure + "hPa, " +
					"temperature = " + temperature + "�C, " +
					"temperature max = " + tempMax + "�C, " +
					"temperature_min = " + tempMin + "�C, " + 
					"description = " + description + ", " + 
					"icon = " + icon
					);
			//Format temperature double value to 2 decimal places
			NumberFormat nf = NumberFormat.getNumberInstance(Locale.UK);
			DecimalFormat df = (DecimalFormat)nf;
			df.applyPattern("#.##");
			String temperatureString = df.format(temperature);
			weather = new Weather(Double.parseDouble(latitude), Double.parseDouble(longitude), temperatureString, description, icon);
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
		}
		
		return weather;
	}
}
