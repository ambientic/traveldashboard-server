package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class FacebookCommentGetterUtils extends ResponseUtils {

	private static final String TAG = "FacebookCommentGetterUtils";
	
	public static NetworkMessage getFacebookCommentMessage(NetworkMessage req) {
		String methodTag = "getFacebookCommentMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get facebook comment message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		int maxComments = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
				
		Gson gson = new Gson();
		
		String json = gson.toJson(FacebookResponse.getTubeStationFacebookComments(city, stopId, maxComments));
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get facebook comment message" + new String(resp.encode()));
		
		resp.addPayload(DataConstants.FACEBOOK_COMMENT_MESSAGE, json);
		return resp;
	}
}
