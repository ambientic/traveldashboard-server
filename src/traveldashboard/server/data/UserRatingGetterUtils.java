package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class UserRatingGetterUtils extends ResponseUtils {

	private static final String TAG = "UserRatingGetterUtils";
	
	public static NetworkMessage getUserRatingMessage(NetworkMessage req) {
		
		String methodTag = "getUserRatingMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start get user rating message");	
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String username = req.getPayload(DataConstants.PARAM_KEY_USER_NAME);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		
		Gson gson = new Gson();
		
		String json = gson.toJson(MongoDbManager.getMetroUserRating(city, username, routeId));
		
		resp.addPayload(DataConstants.USER_RATING_MESSAGE, json);

		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get user rating message" + new String(resp.encode()));
        
		return resp;
	}
}

