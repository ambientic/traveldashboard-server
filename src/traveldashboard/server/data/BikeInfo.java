package traveldashboard.server.data;

public class BikeInfo {

	private String name;
	private int id;
	private double lat;
	private double lon;
	private String nbBikes;
	private String nbEmptyDocks;
	private String nbDocks;
	private String locked;
	
	/**
	 * 
	 * @param name
	 * @param id
	 * @param lat
	 * @param lon
	 * @param nbBikes
	 * @param nbEmptyDocks
	 * @param nbDocks
	 * @param locked
	 */
	public BikeInfo(String name, int id, double lat, double lon,
			String nbBikes, String nbEmptyDocks, String nbDocks, String locked) {
		this.name = name;
		this.id = id;
		this.lat = lat;
		this.lon = lon;
		this.nbBikes = nbBikes;
		this.nbEmptyDocks = nbEmptyDocks;
		this.nbDocks = nbDocks;
		this.locked = locked;
	}
	
	public String getName() {return name;}
	
	public int getId() {return id;}
	
	public double getLat() {return lat;}
	
	public double getLon() {return lon;}
	
	public String getNbBikes() {return nbBikes;}
	
	public String getNbEmptyDocks() {return nbEmptyDocks;}
	
	public String getNbDocks() {return nbDocks;}
	
	public String getLocked() {return locked;}
	
	@Override
	public String toString() {
		return "id = " + String.valueOf(id) + "\n" 
				+ "name = " + name + "\n"
				+ "lat = " + String.valueOf(lat) + "\n"
				+ "lon = " + String.valueOf(lon) + "\n"
				+ "nbBikes = " + nbBikes + "\n"
				+ "nbEmptyDocks = " + nbEmptyDocks + "\n"
				+ "dbDocks = " + nbDocks + "\n"
				+ "locked = " + locked
				;
	}
}
