package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;

public class ResponseUtils {
	
	public static NetworkMessage prepareMsgReturn(NetworkMessage req) {
		NetworkMessageXml resp = new NetworkMessageXml();
		resp.setMsgId(req.getMsgId());
		resp.setMsgType(NetworkMessageXml.MSG_TYPE_RESPONSE);
		resp.setMsgOperation(req.getMsgOperation());
		return resp;
	}
}
