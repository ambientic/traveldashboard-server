package traveldashboard.server.data;

/**
 * Stop for network message
 * @author khoo
 *
 */
public class StopForNetworkMessage {
	private String stopId;
	private String routeId;
	private String routeName;
	private String stopName;
	private double stopLon;
	private double stopLat;
	private String direction;
	
	public StopForNetworkMessage(
			String stopId, String routeId, String routeName, String stopName, 
			double stopLon, double stopLat, String direction) {
		this.stopId = stopId;
		this.routeId = routeId;
		this.routeName = routeName;
		this.stopName = stopName;
		this.stopLon = stopLon;
		this.stopLat = stopLat;
		this.direction = direction;
	}
	
	public String getStopId() {
		return stopId;
	}
	
	public String getRouteId() {
		return routeId;
	}
	
	public String getRouteName() {
		return routeName;
	}
	
	public String getStopName() {
		return stopName;
	}
	
	public double getStopLon() {
		return stopLon;
	}
	
	public double getStopLat() {
		return stopLat;
	}
	
	public String getDirection() {
		return direction;
	}
}
