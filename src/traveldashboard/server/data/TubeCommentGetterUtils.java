package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class TubeCommentGetterUtils extends ResponseUtils {

	private static final String TAG = "TubeCommentGetterUtils";
	
	public static NetworkMessage getTubeCommentMessage(NetworkMessage req) {
		
		String methodTag = "getTubeCommentMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start getting tube comment message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		int maxComments = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
		
		Gson gson = new Gson();
		String json = gson.toJson(MongoDbManager.getMetroCommentsCollection(city, stopId, routeId, maxComments));
		
		resp.addPayload(DataConstants.TUBE_COMMENT_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get tube comment message : " + new String(resp.encode()));
		
		return resp;
	}

}
