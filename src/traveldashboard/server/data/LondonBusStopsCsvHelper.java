package traveldashboard.server.data;

import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import traveldashboard.data.Bus;
import traveldashboard.data.BusStation;
import traveldashboard.data.BusStationsCollection;
import traveldashboard.data.TransportArea;


import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;


public class LondonBusStopsCsvHelper {

	private static final String BUS = "BUS";
	private static final String _ID = "_ID";
	private static final String _DEST = "_DEST";
	private static final String _NAME = "_NAME";
	private static final int MAX_BUS = 20;
	
    private static final String[] titles = {"stopId", "stopName", "stopLat", "stopLon"
    		,"busId1", "busName1", "busDest1", "busId2", "busName2", "busDest2"
    		,"busId3", "busName3", "busDest3", "busId4", "busName4", "busDest4"	  
    		,"busId5", "busName5", "busDest5", "busId6", "busName6", "busDest6"
    		,"busId7", "busName7", "busDest7", "busId8", "busName8", "busDest8"
    		,"busId9", "busName9", "busDest9", "busId10", "busName10", "busDest10"
    		,"busId11", "busName11", "busDest11", "busId12", "busName12", "busDest12"	  
    		,"busId13", "busName13", "busDest13", "busId14", "busName14", "busDest14"
    		,"busId15", "busName15", "busDest15", "busId16", "busName16", "busDest16"
    		,"busId17", "busName17", "busDest17", "busId18", "busName18", "busDest18"
    		,"busId19", "busName19", "busDest19", "busId20", "busName20", "busDest20"		        		
    }; 
	
	/**
	 * Generate london bus stops data
	 */
	public static void generateLondonBusStopsCsvFile(String fileName) {
		try {
	        double londonCenterLat = (TransportArea.LONDON_MAX_LAT + TransportArea.LONDON_MIN_LAT)*0.5;
	        double londonCenterLon = (TransportArea.LONDON_MAX_LON + TransportArea.LONDON_MIN_LON)*0.5;
	        double radius = 1000 * 40; //40 km
	        
	       // System.out.println("Going to get bus station collection");
	        
	        BusStationsCollection busStationsCollection = 
	        		BusStopResponse.getBusStationsCollection(String.valueOf(londonCenterLat), String.valueOf(londonCenterLon), String.valueOf(radius));
	        
	        
	        BusStation[] busStations = busStationsCollection.getBusStations();
	        
	        System.out.println("Going to parse bus station");
	        
	        CSVWriter writer = new CSVWriter(new FileWriter(fileName
					), ';', CSVWriter.NO_QUOTE_CHARACTER);
	        		
	        writer.writeNext(titles);
	        
	        for (int i = 0; i < busStations.length; i++) {
	        	BusStation station = busStations[i];
	        	String[] values = new String[titles.length];
	        	int index = 0;
	        	String stopId = station.getId();
	        	values[index] = stopId; index++;
	        	String stopName = station.getName();
	        	values[index] = stopName; index++;
	        	double stopLat = station.getLatitude();
	        	values[index] = "" + stopLat; index++;
	        	double stopLon = station.getLongitude();
	        	values[index] = "" + stopLon; index++;
	
	        	//System.out.println("stopId = " + stopId + ", stopName = " + stopName + ", stopLat = " + stopLat + ", stopLon = " +  stopLon);
	        	
	        	Bus[] buses = station.getBuses();
	 
	        	//Format
	        	// (BUS1_ID, "260")
	        	// (BUS1_NAME, "260")
	        	// (BUS1_DEST, "White City")	        	
	        	for (int j = 0; j < buses.length; j++) {
	        		System.out.println(BUS + (j + 1) + _ID + "=" +  buses[j].getId());	        		
	        		System.out.println(BUS + (j + 1) + _NAME + "=" + buses[j].getName());
	        		System.out.println(BUS + (j + 1) + _DEST + "=" +  buses[j].getDestination());
		        	values[index] = buses[j].getId(); index++;
		        	values[index] = buses[j].getName(); index++;
		        	values[index] = buses[j].getDestination(); index++;
	        	}
	        	
	        	//Fill in empty case
	        	// (BUS1_ID, "*")
	        	// (BUS1_NAME, "*")
	        	// (BUS1_DEST, "*")
	        	for (int j = buses.length; j < MAX_BUS; j++) {
	        		System.out.println(BUS + (j + 1) + _ID + "=" + "*");	        		
	        		System.out.println(BUS + (j + 1) + _NAME + "=" + "*");
	        		System.out.println(BUS + (j + 1) + _DEST + "=" + "*");
		        	values[index] = "*"; index++;
		        	values[index] = "*"; index++;
		        	values[index] = "*"; index++;
	        	}
	        	
	        	writer.writeNext(values);
	        	
	        }
	        
	        writer.close();
	        	
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}

	public static BusStationsCollection getBusStationsCollectionByCsvFile(InputStream is) {
		BusStation[] busStations = null;
		BusStationsCollection busStationsCollection = null;
		
		try {		
			CSVReader reader = new CSVReader(new InputStreamReader(is), ';');
			
			List<String[]> stations = reader.readAll();	
			
			busStations = new BusStation[stations.size() - 1];
			
			for (int i = 1; i < stations.size(); i++) {
				
				String[] station = stations.get(i);
				
		        String stopId = station[0].trim();
		        String stopName = station[1].trim();
		        double stopLat = Double.parseDouble(station[2]);
		        double stopLon = Double.parseDouble(station[3]);
		        
		        //System.out.println("stopId = " + stopId + ", stopName = " + stopName + ", stopLat = " + stopLat + ", stopLon = " + stopLon);
		        
		        Bus[] buses = new Bus[20];
		        
		        int bus_index = 0;
		        
		        for (int k = 4; k <= (station.length - 3); k = k + 3) {
		        	/*
		        	System.out.println("busid = " + station[k]); //4
		        	System.out.println("busName = " + station[k + 1]); //5
		        	System.out.println("busDestination = " + station[k + 2]); //6
		        	*/		        	
		        	buses[bus_index] = new Bus(station[k], station[k + 1], station[k + 2]);
		        	bus_index++;
		        }
		        
		        BusStation busStop = new BusStation(stopId, stopName, stopLat, stopLon, buses);
		        busStations[i - 1] = busStop;
			}
		
			reader.close();
			
			busStationsCollection = new BusStationsCollection(busStations);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return busStationsCollection;
	}

}
