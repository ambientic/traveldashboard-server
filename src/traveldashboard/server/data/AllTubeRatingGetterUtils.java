package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class AllTubeRatingGetterUtils extends ResponseUtils {
	
	private static final String TAG = "TubeRatingGetterUtils";
	
	public static NetworkMessage getAllTubeRatingMessage(NetworkMessage req) {
		
		String methodTag = "getAllTubeRatingMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start getting all tube ratings");	
        
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		
		Gson gson = new Gson();
		
		String json = gson.toJson(MongoDbManager.getMetroGeneralRatingsCollection(city));
		
		resp.addPayload(DataConstants.ALL_TUBE_RATING_MESSAGE, json);

		if (DataConstants.DEBUG)  TdServerLogger.print(TAG, methodTag, "End get all tube rating message : " + new String(resp.encode()));	
		
        return resp;
	}
}
