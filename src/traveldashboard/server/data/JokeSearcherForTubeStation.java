package traveldashboard.server.data;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;

public class JokeSearcherForTubeStation {
	private static final boolean DEBUG = DataConstants.DEBUG;
	
	private static final String URL_QUERY = "http://api.icndb.com/jokes/random?firstName=";
	private static final String URL_QUERY_END = ";lastName=";
	//http://api.icndb.com/jokes/random?firstName=John&amp;lastName=Doe
	
	
	private final static String USER_AGENT = "Mozilla/5.0";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		generateLondonTubeStationWikiCsv("londonTubeStationJokeCsv.csv");
	}
	
	public static void generateLondonTubeStationWikiCsv(String fileName) {

		try {
			
			CSVWriter writer = new CSVWriter(new FileWriter(fileName), ';', CSVWriter.NO_QUOTE_CHARACTER);
			
			InputStream is = new FileInputStream("londonMetroStopCsv.csv");
			CSVReader reader = new CSVReader(new InputStreamReader(is), ';');
			List<String[]> stations = reader.readAll();
			stations.remove(0);
			
			String[] newCsvTitles = {"_id", "stc","joke"};
			writer.writeNext(newCsvTitles);
			
			for (String[] station : stations) {
				
				try {
					String stationName = station[0];
					if (DEBUG) System.out.println("#####Joke of " + stationName + "#####");
					String stationNlc = station[1];
					
					if (stationNlc.trim().equals("*")) continue;
					
					String stationId = station[2];
					
					//Split station name
					String[] nameArray = stationName.split(" ");
					
					String firstName = nameArray[0];
					
					String lastName = "";
					
					for (int i = 1; i < nameArray.length; i++) {
						lastName = lastName + nameArray[i];
						
						if (i < (nameArray.length - 1)) {
							lastName = lastName + " ";
						}
					}
					
					String jsonString = getJokeJsonContent(firstName);
					
					String stationDesc = processJsonContent(jsonString);
					
					if (DEBUG) {
						if (!stationDesc.equals("")) {					
							System.out.println(stationDesc);
						}
						else  {
							System.err.println(stationName + " don't have description");
						}
					}

					
					String[] newCsvContent = {stationName, stationId, stationDesc};
					
					writer.writeNext(newCsvContent);
					
				} catch (Exception exception) {
					exception.printStackTrace();
				}

				
			}
			
			reader.close();
			
			writer.close();
			
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		
	}
	
	public static String getJokeJsonContent(String firstName) {
		
		String content = "";
		
		try {
			
			//System.out.println("Get joke json content for station first name = " + firstName);
			
			//Result should replace "Norris" by last name
			
			String url = URL_QUERY + URLEncoder.encode(firstName, "UTF-8");
			
			URL obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
	 
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	 
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.flush();
			wr.close();
	 
			int responseCode = con.getResponseCode();
			//System.out.println("\nSending 'POST' request to URL : " + url);
			//System.out.println("Response Code : " + responseCode);
	 
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();
	 
			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	 
			content = response.toString();
			
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
		}

		return content;
	}

	public static String processJsonContent(String jsonContent) {
		
		String desc = "";
		
		try {
			JSONObject contentJsonObj = new JSONObject(jsonContent);
			
			JSONObject valueJsonObj = contentJsonObj.getJSONObject("value");
						
			desc = valueJsonObj.getString("joke");
			
			//Replace Norris by ""
			desc = desc.replace(" Norris", "");
			desc = desc.replace("&quot;", "\"");
			desc = desc.replace("&amp;", "&");
			desc = desc.replace("&lt;", "<");
			desc = desc.replace("&gt;", ">");
		} catch (Exception exception) {
			System.err.println(exception.getMessage());
		}

		return desc;
	}
	
	public static String removeUnwantedCharacters(String stationContent) {
		
		return  stationContent.replace("[[", "")
					.replace("]]", "")
					.replace("{{", "")
					.replace("}}", "")
					.replace("<ref>", "")
					.replace("</ref>", "")
					.replace(";", ".")
					.replace("<sub>", " ")
					.replace("</sub>", " ")
					.replace("<ref name=Butt>"," ")
					;
		
	}
	
	public static String html2text(String html) {
		
		String info = Jsoup.parse(html).text();
		
		
		String pattern = "(\\{\\{)(cite.*?)(\\}\\})";

		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 0");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(harvnb.*?)(\\}\\})";

		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 00a");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(convert.*?)(\\|)(.*?)(\\|)(.*?)(\\|.*?)(\\}\\})";

		info = info.replaceAll(pattern, "$4 $6");
		
		//System.out.println("result 0a");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(convert.*?)(\\|)(.*?)(\\|)(.*?)(\\}\\})";

		info = info.replaceAll(pattern, "$4 $6");
		
		//System.out.println("result 0b");
		
		//System.out.println(info);		
		
		pattern = "(\\{\\{)(Dead.*?)(\\}\\})";

		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 0b");
		
		//System.out.println(info);		
		
		pattern = "(\\{\\{)(.*?)(\\|)(.*?)(\\}\\})";

		info = info.replaceAll(pattern, "$1$4$5");		
		
		//System.out.println("result 1");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(title=.*?)(\\}\\})";
			 
		info = info.replaceAll(pattern, "");
		
		pattern = "(\\{\\{)(.*?)(\\=)(.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "$1$4$5");
		
		//System.out.println("result 2");
		
		//System.out.println(info);

		pattern = "(\\{\\{)([^\\}]*)(\\|)([^\\}]*)(\\=)(.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "$6");
		
		//System.out.println("result 3");
		
		//System.out.println(info);			
		
		
		pattern = "(\\[\\[)([^\\]]*)(\\|)(.*?)(\\]\\])";
		 
		info = info.replaceAll(pattern, "$4");
		
		//System.out.println("result 4");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(Day.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 4a");
		
		//System.out.println(info);

		pattern = "(\\{\\{)(http.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 5");
		
		//System.out.println(info);
		
		pattern = "(\\{\\{)(\\shttp.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 5a");
		
		//System.out.println(info);	
		
		
		pattern = "(\\{\\{)([^\\}]*)(\\|)(url\\=http.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");

		pattern = "(\\{\\{)([^\\}]*)(\\|)(url\\=\\shttp.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "");
		
		//System.out.println("result 6");
		
		//System.out.println(info);
		
		pattern = "(\\[\\[)(.*?)(\\]\\])";
		 
		info = info.replaceAll(pattern, "$2");
		
		//System.out.println("result 6");
		
		//System.out.println(info);
		
		
		pattern = "(\\{\\{)(.*?)(\\}\\})";
		 
		info = info.replaceAll(pattern, "$2");
		
		//System.out.println("result 7");
		
		//System.out.println(info);
		
		//pattern = "(Street\\?\\|\\?\\|u\\?\\|d\\?)";
		//info = info.replaceAll(pattern, "");
		//System.out.println("result 9");
		//System.out.println(info);	
		
		//Specified process
		info = info.replace("File:Burnt Oak railway station 1951303 a449cb96.jpg|View SE, towards Golders Green and London in 1961 File:Burnt Oak stn northbound.JPG|Looking north File:Burnt Oak stn southbound.JPG|Looking south File:Burnt Oak roundel.JPG|", "");
		String toBeDeleted = "(?|v|?|k|s|??|l, VOK|sawl)";
		String toBeReplaced = "";
		info = info.replace(toBeDeleted, toBeReplaced);
		toBeDeleted = "?|?|u?|d?";
		info = info.replace(toBeDeleted, toBeReplaced);
		info = info.replace("|- |}", "");
		info = info.replace(";", ".");
		
		//System.out.println("result 10");
		
		//System.out.println(info);
		
		return info;
	}
}
	
