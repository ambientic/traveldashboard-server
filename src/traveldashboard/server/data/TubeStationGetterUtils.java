package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import traveldashboard.data.TransportArea;


import com.google.gson.Gson;

public class TubeStationGetterUtils extends ResponseUtils {
	
	private static final String TAG = "TubeStationGetterUtils";
	
	public static NetworkMessage getTubeStationMessage(NetworkMessage req) {
		
		String methodTag = "getTubeStationMessage";
		
		TdServerLogger.print(TAG, methodTag, "Start getting tube stations message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String latitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LAT);
		String longitude = req.getPayload(DataConstants.PARAM_KEY_STOP_LON);
		String radius = req.getPayload(DataConstants.PARAM_KEY_RADIUS);
		
		Gson gson = new Gson();
		String json = "null";
		
		if (TransportArea.getArea(Double.parseDouble(latitude), Double.parseDouble(longitude)).equals(TransportArea.PARIS)) {
			//Paris
			json = gson.toJson(MongoDbManager.getParisMetroStationsCollection(latitude, longitude, radius));
		} else {
			//London
			json = gson.toJson(MongoDbManager.getLondonMetroStationsCollection(latitude, longitude, radius));
		}
		
		resp.addPayload(DataConstants.TUBE_STATION_MESSAGE, json);
		
		TdServerLogger.print(TAG, methodTag, "End get tube stations message : " + new String(resp.encode()));
		
		return resp;
	}
}
