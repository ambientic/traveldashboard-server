package traveldashboard.server.data;

import org.ibicoop.filemanager.FileManager;
import org.ibicoop.init.IbicoopInit;

public final class TdServerLogger {

	public static String LOG_FILE_LOCATION = "TravelDashboardServerLog";
	public static String LOG_FILE_COMMON_LOCATION = LOG_FILE_LOCATION + "/TravelDashboardServerLogDirectory";
	public static String LOG_FILE_NAME_HEADER = LOG_FILE_COMMON_LOCATION + "/TravelDashboardServerLog";
	public static String LOG_FILE_NAME_EXTENSION = ".txt";
	public static String LOG_FILE_NAME = "";
	
	public static FileManager fileManager = null;
	
	public static void print(String className, String method, String message) {
		System.out.println(className + "|" + method + "|" + message);
	}
	
	public static void printError(String className, String method, String message) {
		System.err.println(className + "|" + method + "|" + "Error" + "|" + message);
	}
	
	public static void setUpFileManager(String fileId) {
		//Create ibicoop file manager
		try {
			fileManager = IbicoopInit.getInstance().getFileManager();
			//Create file exchange directory and file
			fileManager.createDirectoryUserContent(LOG_FILE_LOCATION);
			fileManager.createDirectoryUserContent(LOG_FILE_COMMON_LOCATION);
			LOG_FILE_NAME = LOG_FILE_NAME_HEADER + "_" + fileId + LOG_FILE_NAME_EXTENSION;
			fileManager.createFileUserContent(LOG_FILE_NAME);
			print("FileManager", "setUpFileManager", "OK");
		} catch (Exception exception) {
			printError("FileManager", "setUpFileManager", exception.getMessage());
		}
	}
	
	public static void outLog(String className, String method, String message) {
		String outLogMessage = className + "|" + method + "|" + message + "\n";
		
		print("FileManager", "outLog", LOG_FILE_NAME);
		
		try{
			
			if (fileManager.existsUserContent(LOG_FILE_NAME)) {
				fileManager.writeUserContent(LOG_FILE_NAME, outLogMessage.getBytes());
			} else {
				fileManager.createFileUserContent(LOG_FILE_NAME);
			}
			
			print("FileManager", "outLog", "OK");
		}
		 catch (Exception exception) {
				printError("FileManager", "outLog", exception.getMessage());
			}
	}
	
	public static void outLogError(String className, String method, String message) {
		String outLogMessage = className + "|" + method + "|" + "Error" + "|" + message + "\n";
		
		print("FileManager", "outLogError", LOG_FILE_NAME);
		
		try{
			
			if (fileManager.existsUserContent(LOG_FILE_NAME)) {
				fileManager.writeUserContent(LOG_FILE_NAME, outLogMessage.getBytes());
			} else {
				fileManager.createFileUserContent(LOG_FILE_NAME);
			}
			
			print("FileManager", "outLogError", "OK");
		}
		 catch (Exception exception) {
				printError("FileManager", "outLog", exception.getMessage());
			}
	}	
	
}
