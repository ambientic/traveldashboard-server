package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

/**
 * Prepare get all stops return network message
 * @author khoo
 *
 */
public class AllStopsGetterUtils {
	
	public static NetworkMessage getAllStops(NetworkMessage req) {	
		String cityType =  req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String routeType = req.getPayload(DataConstants.PARAM_KEY_ROUTE_TYPE);
		if (DataConstants.DEBUG) TdServerLogger.print("AllStopsGetterUtils", "getAllStops", "city type = " + cityType + ", route type = " + routeType);
		try {
			return TDMessageGeneratorFromDatabase.generateStationMessageFromDatabase(cityType, Integer.parseInt(routeType));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
