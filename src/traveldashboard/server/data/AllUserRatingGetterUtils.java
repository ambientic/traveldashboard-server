package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class AllUserRatingGetterUtils extends ResponseUtils {

	private static final String TAG = "AllUserRatingGetterUtils";
	
	public static NetworkMessage getAllUserRatingMessage(NetworkMessage req) {
		
		String methodTag = "getAllUserRatingMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start getting all user ratings message");		
		
		NetworkMessage resp = prepareMsgReturn(req);
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String username = req.getPayload(DataConstants.PARAM_KEY_USER_NAME);
		
		Gson gson = new Gson();
		String json = gson.toJson(MongoDbManager.getMetroUserRatingsCollection(city, username));
		
		resp.addPayload(DataConstants.ALL_USER_RATING_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get all user ratings message" + new String(resp.encode()));
        
		return resp;
	}
}
