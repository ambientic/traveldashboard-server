package traveldashboard.server.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import traveldashboard.data.Tube;
import traveldashboard.data.TubePlatform;
import traveldashboard.data.TubeStation;
import traveldashboard.data.info.TubeRealTimeInfo;
import traveldashboard.data.info.TubeRealTimeInfosCollection;


/**
 * Tube count down response
 * @author khoo
 *
 */
public class TubeCountdownResponse {

	private static final String TAG = "TubeCountdownResponse";
	private static final String URL = "http://cloud.tfl.gov.uk/TrackerNet/PredictionDetailed/";
	private static JSONObject outputPlatformArrayObj;
	private static JSONArray outputTrainsInfo;
	private static JSONArray outputAllPlatformsArray;
	private static boolean debug = DataConstants.DEBUG;
	private static List<TubeRealTimeInfo> tubeRealTimeInfos;

	/**
	 * Get London tube count down
	 * @param stopId
	 * @param routeId
	 * @param nbValues
	 * @return London tube count down
	 */
	public static TubeRealTimeInfosCollection getTubeRealTimeInfosCollection(String stopId, String routeId, int nbValues) {
		
		String methodTag = "getTubeRealTimeInfosCollection";
		
		if (debug) TdServerLogger.print(TAG, methodTag, "stopId = " + stopId + ", routeId = " + routeId + ", nbValues = " + nbValues);
		
		TubeRealTimeInfosCollection tubeRealTimeInfosCollection = null;
		tubeRealTimeInfos = new ArrayList<TubeRealTimeInfo>();
		
		try {
			String uri = URL + routeId + "/" + stopId;
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(uri);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			// Get XML
			String json = httpClient.execute(httpGet, responseHandler);
					
		    // Convert XML to JSON Object
            JSONObject xmlJSONObj = XML.toJSONObject(json);
		
			JSONObject rootObj = xmlJSONObj.getJSONObject("ROOT");

			JSONObject stationObj = rootObj.getJSONObject("S");

			outputAllPlatformsArray = new JSONArray();
			
			JSONArray allPlatformsArray = stationObj.optJSONArray("P");
			
			if (debug) TdServerLogger.print(TAG, methodTag, allPlatformsArray.toString(4));
			
			if (allPlatformsArray != null) {
				if (debug) TdServerLogger.print(TAG, methodTag,"Handle several platforms");
				
				//The case where we have several platforms
				for (int i = 0; i < allPlatformsArray.length(); i++) {
					JSONObject platformObj = allPlatformsArray.getJSONObject(i);
					processPlatform(stopId, routeId, nbValues, platformObj);
				}
			}
			else {
				if (debug) TdServerLogger.print(TAG, methodTag,"Handle one platform");
				JSONObject platformObj  = stationObj.optJSONObject("P");
				if (platformObj == null) return null;
				processPlatform(stopId, routeId, nbValues, platformObj);
			}

          	TubeRealTimeInfo[] infos = new TubeRealTimeInfo[tubeRealTimeInfos.size()];
          			
			for (int i = 0; i < tubeRealTimeInfos.size(); i++) {
				infos[i] = tubeRealTimeInfos.get(i);
			}
			
			tubeRealTimeInfosCollection = new TubeRealTimeInfosCollection(infos);
			
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
				
		return tubeRealTimeInfosCollection;
		
	}
	
	
	private static void processPlatform(String stopId, String routeId, int nbValues, JSONObject platformObj) throws JSONException {
		
		String methodTag = "processPlatform";

		if (debug)  TdServerLogger.print(TAG, methodTag,"Processing platform.....");
		
		outputPlatformArrayObj = new JSONObject(); 
		outputTrainsInfo = new JSONArray(); // Trains info array
		
		String platformName = platformObj.getString("N");
		outputPlatformArrayObj.put("PlatformName", platformName); // Put plaform name in output json object
		
		System.out.println("platform object = " + platformObj);
		
//		String platformNum = platformObj.getString("Num");
		String platformNum = platformObj.get("Num").toString();
		outputPlatformArrayObj.put("PlatformNum", platformNum); // Put plaform name in output json object

		JSONArray platformArray = platformObj.optJSONArray("T");
		
		
		if (debug)  TdServerLogger.print(TAG, methodTag,"Platform name = " + platformName + ", platformNum = " + platformNum);
		
		if (platformArray != null) {
			if (debug)  TdServerLogger.print(TAG, methodTag,"Handle several time infos");
			
			int iterationOfTrains = nbValues;
			if (iterationOfTrains > platformArray.length()) iterationOfTrains = platformArray.length();
			
			for (int j = 0; j < iterationOfTrains; j++) {
				JSONObject platformInfoObj = platformArray.getJSONObject(j);
				processTimeInfo(stopId, routeId, platformInfoObj, platformNum, platformName);
			}
		}
		else {
			if (debug)  TdServerLogger.print(TAG, methodTag,"Handle one time info");
			JSONObject platformInfoObj = platformObj.optJSONObject("T");
			
			if (platformInfoObj == null) return;
			
			processTimeInfo(stopId, routeId, platformInfoObj, platformNum, platformName);
		}
		
		outputPlatformArrayObj.put("Trains", outputTrainsInfo);
		outputAllPlatformsArray.put(outputPlatformArrayObj);
			
	}
	
	private static void processTimeInfo(String stopId, String routeId, 
			JSONObject platformInfoObj, 
			String platformNumber, String platformName) throws JSONException {
		
		String methodTag = "processTimeInfo";

		if (debug)  TdServerLogger.print(TAG, methodTag,"Processing time info.....");
		
		
		String destination = platformInfoObj.getString("Destination");
		
		if (destination.equals("Unknown")) destination = "See front of train";
										
		String timeToComplet = platformInfoObj.getString("TimeTo");
		String timeTo;
		if (timeToComplet.equals("-")) timeTo = "At platform";
		else {
			String[] timeToArray = timeToComplet.split(":");
			
			if (timeToArray[0].equals("0")) timeTo = "Due";
			else timeTo = timeToArray[0] + " min";
		}
		
		JSONObject outputTrains = new JSONObject();
		outputTrains.put("Destination", destination); // Put destination in output train json object
		outputTrains.put("TimeTo", timeTo); // Put time to in output train json object
		outputTrainsInfo.put(outputTrains); // Trains
		//End special process for object or array
		
		if (debug) if (debug)  TdServerLogger.print(TAG, methodTag,"destination = " + destination + ", time to = " + timeTo);
		
		
		//Create tube real time object
		if (tubeRealTimeInfos != null) {
			String routeName = MongoDbManager.getLondMetroRouteName(routeId);
			Tube tube = new Tube(routeId, routeName, destination);
			TubeStation station = MongoDbManager.getLondonMetroStation(stopId);
			TubePlatform tubePlatform = new TubePlatform(platformNumber, platformName);
			TubeRealTimeInfo info = new TubeRealTimeInfo(tube, station, tubePlatform, timeTo);
			tubeRealTimeInfos.add(info);
		}
	}
	
}


