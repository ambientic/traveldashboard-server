package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;

import com.google.gson.Gson;

public class UserCommentGetterUtils extends ResponseUtils {

	private static final String TAG = "UserCommentGetterUtils";
	
	public static NetworkMessage getUserCommentMessage(NetworkMessage req) {
		
		String methodTag = "getUserCommentMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start getting user comment message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String userName = req.getPayload(DataConstants.PARAM_KEY_USER_NAME);
		String stopId = req.getPayload(DataConstants.PARAM_KEY_STOP_ID);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);
		int maxComments = Integer.parseInt(req.getPayload(DataConstants.PARAM_KEY_MAX_VALUES));
		
		Gson gson = new Gson();
		String json = gson.toJson(MongoDbManager.getMetroCommentsCollectionForUser(city, userName, stopId, routeId, maxComments));
		
		resp.addPayload(DataConstants.USER_COMMENT_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get user comment message" + new String(resp.encode()));
		
		return resp;
	}
}
