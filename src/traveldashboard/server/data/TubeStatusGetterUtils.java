package traveldashboard.server.data;

import org.ibicoop.sdp.config.NetworkMessage;


import com.google.gson.Gson;

public class TubeStatusGetterUtils extends ResponseUtils {

	private static final String TAG = "TubeStatusGetterUtils";
	
	public static NetworkMessage getTubeStatusMessage(NetworkMessage req) {
		
		String methodTag = "getTubeStatusMessage";
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "Start getting tube status message");
		
		NetworkMessage resp = prepareMsgReturn(req);
		
		String city = req.getPayload(DataConstants.PARAM_KEY_CITY_TYPE);
		String routeId = req.getPayload(DataConstants.PARAM_KEY_ROUTE_ID);

		Gson gson = new Gson();
		
		String json = gson.toJson(MongoDbManager.getMetroStatus(city, routeId));		
		
		resp.addPayload(DataConstants.TUBE_STATUS_MESSAGE, json);
		
		if (DataConstants.DEBUG) TdServerLogger.print(TAG, methodTag, "End get tube status message" + new String(resp.encode()));
		
		return resp;
	}
}
