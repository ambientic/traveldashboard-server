package traveldashboard.server.data;

import java.util.Set;

import org.ibicoop.sdp.config.NetworkMessage;
import org.ibicoop.sdp.config.NetworkMessageXml;
import org.onebusaway.gtfs.model.Stop;

import traveldashboard.gtfs.process.GtfsProcessor;

public class TDMessageGenerator {
        final String PARAM_VALUE_STOP_ID = "ID";
        final String PARAM_KEY_STOP_NAME = "_Name";
        final String PARAM_KEY_STOP_LON = "_Lon";
        final String PARAM_KEY_STOP_LAT = "_Lat";
        final String PARAM_KEY_ROUTE_ID = "_RouteId";
        final String PARAM_KEY_ROUTE_NAME = "_RouteName";
        final String PARAM_KEY_DIRECTION_ID = "_Direction";

        GtfsProcessor proc;

        Boolean initDone = null;
        Boolean parsingDone = null;

        public TDMessageGenerator(GtfsProcessor proc) {
                this.proc = proc;
        }

        /**
         * 
         * @param roadType
         * @return a network message or null if request/parsing failed
         * @throws InterruptedException
         */
        public NetworkMessage generateStationMessage(int roadType) throws InterruptedException {
                // wait for init to be completed
                //System.out.println("Waiting for init to be done");
                // synchronized (initDone) {
                // while (!parsingDone) {
                // initDone.wait();
                // }
                // }

                // if init failed for some reason, return null
                //if (initDone == false)
                //      return null;

                //System.out.println("Init done");
                try {
                        // get the stops
                        Set<Stop> stoplist = proc.getAllStopsFor(roadType);

                        // generate the xml network message
                        NetworkMessage res = new NetworkMessageXml();
                        // for all stops
                        for (Stop tmpStop : stoplist) {
                                addSingleStop(res, tmpStop);
                        }
                        return res;
                } catch (Exception e) {
                        e.printStackTrace();
                }
                return null;
        }

        private void addSingleStop(NetworkMessage res, Stop tmpStop) {

                String id = tmpStop.getId().getId();

                // if invalid id, then return
                if (id == null || id.equals(""))
                        return;

                // adding ID
                res.addPayload(id, PARAM_VALUE_STOP_ID);

                // adding name
                res.addPayload(id + PARAM_KEY_STOP_NAME, tmpStop.getName());

                // adding lon
                res.addPayload(id + PARAM_KEY_STOP_LON, String.valueOf(tmpStop.getLon()));

                // adding lat
                res.addPayload(id + PARAM_KEY_STOP_LAT, String.valueOf(tmpStop.getLat()));
                
                // adding RouteId
                res.addPayload(id + PARAM_KEY_ROUTE_ID, tmpStop.getRouteId());
                
                // adding RouteName
                res.addPayload(id + PARAM_KEY_ROUTE_NAME, tmpStop.getRouteName());
                
                // adding RouteName
                res.addPayload(id + PARAM_KEY_DIRECTION_ID, tmpStop.getDirection());
        }    
}