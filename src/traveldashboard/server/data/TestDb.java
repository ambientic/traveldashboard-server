package traveldashboard.server.data;


public class TestDb {

	private static final String TAG = "TestDb";
	
	public static void testDb() {
		final String methodTag = "testDb";
		
		TdServerLogger.print(TAG, methodTag, "Testing db");
		
		new Thread(new Runnable() {

			@Override
			public void run() {
				
//				//Test get tube station : id, name, destination
//				//London
//				TdServerLogger.print(TAG, methodTag, "Testing London tube station...");
//				
//				TdServerLogger.print(TAG, methodTag, "Getting route name from route id...");
//				
//				for (String routeID : DataConstants.LONDON_METRO_ROUTE_ID) {
//					TdServerLogger.print(TAG, methodTag, "London routeID = " + routeID 
//							+ ", routeName = " + MongoDbManager.getLondonMetroRouteName(routeID));
//				}
//				
//				TdServerLogger.print(TAG, methodTag, "Getting route id from route name...");
//				
//				for (String routeNAME : DataConstants.LONDON_METRO_ROUTE_NAMES) {
//					TdServerLogger.print(TAG, methodTag, "London routeName = " + routeNAME
//							+ ", routeId = " + MongoDbManager.getLondonMetroRouteId(routeNAME));
//				}
//				
//				
//				
//				
//				//Paris
//				TdServerLogger.print(TAG, methodTag, "Testing paris metro station...");
//				
//				TdServerLogger.print(TAG, methodTag, "Getting route name from route id...");
//				
//				routeId = "918044";
//				TdServerLogger.print(TAG, methodTag, "Paris routeID = " + routeId
//							+ ", routeName = " + MongoDbManager.getParisMetroRouteName(routeId));
//				
//				routeId = "918045";
//				TdServerLogger.print(TAG, methodTag, "Paris routeID = " + routeId
//							+ ", routeName = " + MongoDbManager.getParisMetroRouteName(routeId));
//				
//				
//				routeId = "860877";
//				TdServerLogger.print(TAG, methodTag, "Paris routeID = " + routeId
//							+ ", routeName = " + MongoDbManager.getParisMetroRouteName(routeId));
//				
//				routeId = "860878";
//				TdServerLogger.print(TAG, methodTag, "Paris routeID = " + routeId
//							+ ", routeName = " + MongoDbManager.getParisMetroRouteName(routeId));
//				
//				
//				routeId = "708886";
//				TdServerLogger.print(TAG, methodTag, "Paris routeID = " + routeId
//							+ ", routeName = " + MongoDbManager.getParisMetroRouteName(routeId));
//				
//				routeId = "708887";
//				TdServerLogger.print(TAG, methodTag, "Paris routeID = " + routeId
//							+ ", routeName = " + MongoDbManager.getParisMetroRouteName(routeId));
//				
//				
//				
//				TdServerLogger.print(TAG, methodTag, "Getting route id from route name...");
//				
//				for (String routeNAME : DataConstants.PARIS_METRO_ROUTE_NAMES) {
//					TdServerLogger.print(TAG, methodTag, "Paris routeName = " + routeNAME + ", direction = 0"
//							+ ", routeId = " + MongoDbManager.getParisMetroRouteId(routeNAME, "0"));
//					
//					TdServerLogger.print(TAG, methodTag, "Paris routeName = " + routeNAME + ", direction = 1"
//							+ ", routeId = " + MongoDbManager.getParisMetroRouteId(routeNAME, "1"));
//				}
				
				
				
				//Test get bus station
				//London
//				TdServerLogger.print(TAG, methodTag, "London bus : stopId = 77103 : " + MongoDbManager.getLondonBusStation("77103").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "London bus : stopId = 71217 : " + MongoDbManager.getLondonBusStation("71217").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "London bus : stopId = 49054 : " + MongoDbManager.getLondonBusStation("49054").toString());		
				
				//TdServerLogger.print(TAG, methodTag, "London bus : stops" + MongoDbManager.getLondonBusStationsCollection("51.52352400539179", "-0.13282299041748047", "1500").toString());
				
				//Paris
//				TdServerLogger.print(TAG, methodTag, "Paris bus : stopId = 3663491 : " + MongoDbManager.getParisBusStation("3663491").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris bus : stopId = 3663595 : " + MongoDbManager.getParisBusStation("3663595").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris bus : stopId = 3663840 : " + MongoDbManager.getParisBusStation("3663840").toString());		
//				
				//TdServerLogger.print(TAG, methodTag, "Paris bus : stops" + MongoDbManager.getParisBusStationsCollection("48.81561237752004", "2.4314174629192427", "1500").toString());
				
				
				
				
				//Test get bike station
				//London
				//TdServerLogger.print(TAG, methodTag, "London bike  : " + MongoDbManager.getBikeStationsCollection("51.52352400539179", "-0.13282299041748047", "1500").toString());
				
				//TdServerLogger.print(TAG, methodTag, "London bike  : " + MongoDbManager.getBikeStation("london", "192").toString());
				
				//Paris
				//TdServerLogger.print(TAG, methodTag, "Paris bike  : " + MongoDbManager.getBikeStationsCollection("48.81561237752004", "2.4314174629192427", "1500").toString());
				//TdServerLogger.print(TAG, methodTag, "Paris bike  : " + MongoDbManager.getBikeStation("paris", "44102").toString());
				
				
				//Test get rail station
				//London
				
				
				//Paris
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailStation("161468").toString());
//	
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailStation("1628").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailRouteName("959287").toString());	
//				
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailRouteId("A", "0").toString());
//	
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailRouteId("A", "1").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailRouteId("B", "0").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailRouteId("B", "1").toString());
				
				//TdServerLogger.print(TAG, methodTag, "Paris rail  : " + MongoDbManager.getParisRailStationsCollection("48.87416474488983", "2.29507256293689", "1500").toString());
				
				
				
				//Test get tram station
				//London			
				//Paris
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramStation("3798439").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramStationsCollection("48.89523832538295", "2.4596680533915762", "1500").toString());
//				
//				
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteId("T1", "0").toString());
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteId("T1", "1").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteId("T3", "0").toString());
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteId("T3", "1").toString());
//			
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteId("T3b", "0").toString());
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteId("T3b", "1").toString());
//				
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteName("911929").toString());
//				TdServerLogger.print(TAG, methodTag, "Paris tram  : " + MongoDbManager.getParisTramRouteName("939888").toString());

				
				//Test user and general comment
				//Put
				//Paris
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "inria", "1765", MongoDbManager.getParisMetroRouteId("3", "0"), "the station 3 is very nice");
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "inria", "1765", MongoDbManager.getParisMetroRouteId("3", "0"), "the station 3 is hot");
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "arles", "1765", MongoDbManager.getParisMetroRouteId("3", "0"), "the station 3 is not very nice");
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "arles", "1765", MongoDbManager.getParisMetroRouteId("3", "0"), "the station 3 is not hot");
//
//				
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "arles", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), "the station 10 is very nice");
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "arles", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), "the service 9 is good");
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "inria", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), "the station 10 is not very nice");
//				MongoDbManager.insertComment("paris", System.currentTimeMillis(), "inria", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), "the service 9 is not good");
//				
//				TubeStationCommentsCollection comment_col = MongoDbManager.getMetroCommentsCollection("paris", "1765", MongoDbManager.getParisMetroRouteId("3", "0"), 5);
//				
//				TubeStationComment[] comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for stopId = 1765  and routeName = 3 : " + comments[i].toString());
//				}
//				
//				
//				comment_col = MongoDbManager.getMetroCommentsCollection("paris", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), 5);
//
//				comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for stopId = 1811  and routeName = 10 : " + comments[i].toString());
//				}
//				
//				
//				comment_col = MongoDbManager.getMetroCommentsCollectionForUser("paris", "inria", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), 5);
//
//				comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for user inria, stopId = 1811  and routeName = 10 : " + comments[i].toString());
//				}
//				
//				
//				comment_col = MongoDbManager.getMetroCommentsCollectionForUser("paris", "arles", "1811", MongoDbManager.getParisMetroRouteId("10", "0"), 5);
//
//				comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for user arles, stopId = 1811  and routeName = 10 : " + comments[i].toString());
//				}
//				
//				
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "inria", "ACT", "D", "the station ACT is very nice");
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "inria", "ACT", "D", "the station ACT is hot");
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "arles", "ACT", "D", "the station ACT is not very nice");
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "arles", "ACT", "D", "the station ACT is not hot");
//
//				
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "arles", "SPK", "B", "the station SPK is very nice");
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "arles", "SPK", "B", "the service SPK is good");
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "inria", "SPK", "B", "the station SPK is not very nice");
//				MongoDbManager.insertComment("london", System.currentTimeMillis(), "inria", "SPK", "B", "the service SPK is not good");
//				
//				comment_col = MongoDbManager.getMetroCommentsCollection("london", "ACT", "D", 5);
//				
//				comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for for stopId = ACT  and routeId = D : " + comments[i].toString());
//				}
//				
//				
//				comment_col = MongoDbManager.getMetroCommentsCollection("london", "SPK", "B", 5);
//
//				comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for stopId = SPK  and routeId = B :  " + comments[i].toString());
//				}
//				
//
//				comment_col = MongoDbManager.getMetroCommentsCollectionForUser("london", "inria", "SPK","B", 5);
//
//				comments = comment_col.getTubeStationComments();
//				
//				for (int i = 0; i < comments.length; i++) {
//					TdServerLogger.print(TAG, methodTag, "comment_col for user inria, stopId = SPK  and routeId = B :" + comments[i].toString());
//				}
//				
				
//				TubeStationCommentsCollection comment_col = MongoDbManager.getMetroCommentsCollectionForUser("london", "arles", "SPK", "B", 5);
//
//				TdServerLogger.print(TAG, methodTag, "comment_col for user arles, stopId = SPK  and routeId = B : " + comment_col.toString());
//
//				
			
				//Test status
				//Put
				
				//Get
//				TubeStatusCollection status_col = MongoDbManager.getMetroStatusCollection("paris");
//				TdServerLogger.print(TAG, methodTag, "status_col for paris = " + status_col.toString());
//				
//				TubeStatus status = MongoDbManager.getMetroStatus("paris", MongoDbManager.getParisMetroRouteId("1", "0"));
//				TdServerLogger.print(TAG, methodTag, "status for ligne 1 in paris = " + status_col.toString());		
//				
//				status_col = MongoDbManager.getMetroStatusCollection("london");
//				TdServerLogger.print(TAG, methodTag, "status_col for london = " + status_col.toString());
//				
//				status = MongoDbManager.getMetroStatus("london", "B");
//				TdServerLogger.print(TAG, methodTag, "status for bakerloo in london = " + status.toString());		
				
				////
				
				//Test classified and general crowd
//				//Put
//				MongoDbManager.insertCrowdData("london", "WEM", "B", System.currentTimeMillis(), 4);
//				MongoDbManager.insertCrowdData("london", "WEM", "B", System.currentTimeMillis(), 3);
//				
//				TdServerLogger.print(TAG, methodTag, "london classified crowd = " + MongoDbManager.getMetroClassifiedCrowd("london", "WEM", "B", System.currentTimeMillis()).toString());
//				TdServerLogger.print(TAG, methodTag, "london general crowd = " + MongoDbManager.getMetroGeneralCrowd("london", "WEM", "B", System.currentTimeMillis(), 1000*60*60).toString());
//				
//				
//				
//				
//				MongoDbManager.insertCrowdData("paris", "1166839", MongoDbManager.getParisMetroRouteId("14", "1"), System.currentTimeMillis(), 4);
//				MongoDbManager.insertCrowdData("paris", "1166839", MongoDbManager.getParisMetroRouteId("14", "1"), System.currentTimeMillis(), 3);
//				
//				TdServerLogger.print(TAG, methodTag, "paris classified crowd = " + MongoDbManager.getMetroClassifiedCrowd("paris", "1166839", MongoDbManager.getParisMetroRouteId("14", "1"), System.currentTimeMillis()).toString());
//				TdServerLogger.print(TAG, methodTag, "paris general crowd = " + MongoDbManager.getMetroGeneralCrowd("paris", "1166839", MongoDbManager.getParisMetroRouteId("14", "1"), System.currentTimeMillis(), 1000*60*60).toString());
//				

				
				//Test general and user rating
				//Paris
//				TdServerLogger.print(TAG, methodTag, "Testing general rating for Paris...");
//				
//				//Get
//				TubeRatingsCollection metro_ratings_co = MongoDbManager.getMetroGeneralRatingsCollection("paris");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());
//				
//				//Put
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("1", "0"), 1);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("1", "1"), 2);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("14", "0"), 5);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("14", "1"), 4);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("10", "0"), 3);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("10", "1"), 2);
//				
//				//Get
//				metro_ratings_co = MongoDbManager.getMetroGeneralRatingsCollection("paris");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());
//				
//				//Put
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("1", "0"), 5);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("1", "1"), 5);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("1", "0"), 5);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("1", "1"), 5);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("14", "0"), 1);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("14", "1"), 1);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("10", "0"), 1);
//				MongoDbManager.updateGeneralRatings("paris", MongoDbManager.getParisMetroRouteId("10", "1"), 1);
//				
//				//Get
//				metro_ratings_co = MongoDbManager.getMetroGeneralRatingsCollection("paris");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());
				
//				MongoDbManager.updateUserRatings("paris", "inria", MongoDbManager.getParisMetroRouteId("1", "0"), 2);
//				MongoDbManager.updateUserRatings("paris", "inria", MongoDbManager.getParisMetroRouteId("1", "1"), 2);
//				MongoDbManager.updateUserRatings("paris", "inria", MongoDbManager.getParisMetroRouteId("4", "0"), 4);
//				MongoDbManager.updateUserRatings("paris", "inria", MongoDbManager.getParisMetroRouteId("4", "1"), 4);		
//				MongoDbManager.updateUserRatings("paris", "inria", MongoDbManager.getParisMetroRouteId("5", "0"), 5);
//				MongoDbManager.updateUserRatings("paris", "inria", MongoDbManager.getParisMetroRouteId("5", "1"), 5);
//				
//				TubeRatingsCollection metro_ratings_co = MongoDbManager.getMetroGeneralRatingsCollection("paris");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());
//				
//				metro_ratings_co = MongoDbManager.getMetroUserRatingsCollection("paris", "inria");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());		
				
				
//				MongoDbManager.updateUserRatings("london", "inria", "B", 2);
//				MongoDbManager.updateUserRatings("london", "inria", "C", 2);
//				MongoDbManager.updateUserRatings("london", "inria", "H", 4);
//				MongoDbManager.updateUserRatings("london", "inria", "J", 4);		
//				MongoDbManager.updateUserRatings("london", "inria", "M", 5);
//				MongoDbManager.updateUserRatings("london", "inria", "N", 5);
//				
//				TubeRatingsCollection metro_ratings_co = MongoDbManager.getMetroGeneralRatingsCollection("london");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());
//				
//				metro_ratings_co = MongoDbManager.getMetroUserRatingsCollection("london", "inria");
//				TdServerLogger.print(TAG, methodTag, "metro_ratings_co = " + metro_ratings_co.toString());	
				
				/////
				
				//Test noise
				//London
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.52352400539179, -0.13282299041748047, 250);
//	
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.52352400539179, -0.13282299041748047, 500);
//				
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.52352400539179, -0.13282299041748047, 750);
//				
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 51.52352400539179, -0.13282299041748047, 1000);
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseData(51.52352400539179, -0.13282299041748047).toString());
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseDatasInRegion(51.52352400539179, -0.13282299041748047, 500).toString());
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseDatasInRegionWithInterval(51.52352400539179, -0.13282299041748047, 500, System.currentTimeMillis(), 1000*60*60).toString());
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseDatasWithInterval("london", System.currentTimeMillis(), 1000*60*60).toString());
//
//				TdServerLogger.print(TAG, methodTag, "########################");
//				
//				//Paris
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 48.844652150982945, 2.37310814754528, 250);
//				
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 48.844652150982945, 2.37310814754528, 500);
//				
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 48.844652150982945, 2.37310814754528, 750);
//				
//				MongoDbManager.insertNoiseData(System.currentTimeMillis(), 48.844652150982945, 2.37310814754528, 1000);
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseData(48.844652150982945, 2.37310814754528).toString());
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseDatasInRegion(48.844652150982945, 2.37310814754528, 500).toString());
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseDatasInRegionWithInterval(48.844652150982945, 2.37310814754528, 500, System.currentTimeMillis(), 1000*60*60).toString());
//				
//				TdServerLogger.print(TAG, methodTag, MongoDbManager.getNoiseDatasWithInterval("paris", System.currentTimeMillis(), 1000*60*60).toString());
//
//				
				//// 
				
				//Test facebook comment
//				//London
//				//Edgware Road
//				TdServerLogger.print(TAG, methodTag, FacebookResponse.getTubeStationFacebookCommentsCollection("london","ERD" , 10).toString());
//				
//				//Embankment
//				TdServerLogger.print(TAG, methodTag, FacebookResponse.getTubeStationFacebookCommentsCollection("london","EMB" , 10).toString());
//				
//				
//				//Paris
//				//Ch�telet
//				TdServerLogger.print(TAG, methodTag, FacebookResponse.getTubeStationFacebookCommentsCollection("paris","1965", 10).toString());
//				
//				//Gare de l'Est
//				TdServerLogger.print(TAG, methodTag, FacebookResponse.getTubeStationFacebookCommentsCollection("paris","2076", 10).toString());
//				
				
				//Test twitter comment
//				//London

				//Embankment
				TdServerLogger.print(TAG, methodTag, TwitterResponse.getTubeStationTwitterComments("london", "EMB" , 10).toString());
				
				
				//Paris
				//Ch�telet
				TdServerLogger.print(TAG, methodTag, TwitterResponse.getTubeStationTwitterComments("paris", "1965" , 10).toString());
								
				
				
				
				
				//Test timetable
				
				//Metro
				//London
				
				
				
				
				//Paris
				//Gare de lyon, metro 14, direction 0
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisMetroRealTimeInfos("1166832", "831554", 10).toString());
				
//				//Gare de lyon, metro 14, direction 1
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisMetroRealTimeInfos("1166833", "831555", 10).toString());
//				
//				
//				//Place d'Italie, metro 7, direction 0
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisMetroRealTimeInfos("1648", "656041", 10).toString());
//				
//				//Nation, metro 9, direction 0
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisMetroRealTimeInfos("1651", "711644", 10).toString());
//				
				
				//London
//				//Wembley Park M
//				TdServerLogger.print(TAG, methodTag,TubeCountdownResponse.getTubeRealTimeInfosCollection("WPK", "M", 10).toString());
//				
//				//Sudbury Town P
//				TdServerLogger.print(TAG, methodTag,TubeCountdownResponse.getTubeRealTimeInfosCollection("STN", "P", 10).toString());
//				
//				//Ealing Common
//				TdServerLogger.print(TAG, methodTag, TubeCountdownResponse.getTubeRealTimeInfosCollection("ECM", "D", 10).toString());
//				
				
//				//Rail
//				//Val d'europe, direction 0
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisRailRealTimeInfos("161468", "742332", 10).toString());
//				
//				//Denfert-Rochereau, direction 0
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisRailRealTimeInfos("1998", "959287", 10).toString());
//				
				//Bus
				//Paris
				//Porte de Clichy, 173
				//TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisBusRealTimeInfos("3664178", "959911", 10).toString());
				
				//Hoche, 330
				//TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisBusRealTimeInfos("3664997", "959206", 10).toString());
							
				//maurice audin, 350
				//TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisBusRealTimeInfos("3666632", "959214", 10).toString());
						
				
				//London
				//TdServerLogger.print(TAG, methodTag, MongoDbManager.getLondonBusRouteName("111"));
				
				//TdServerLogger.print(TAG, methodTag, BusCountdownResponse.getBusRealTimeInfosCollection("56004", "111", 10).toString());
				
				//Gospel Oak Station
//				TdServerLogger.print(TAG, methodTag, BusCountdownResponse.getBusRealTimeInfosCollection("72579", 10).toString());
//				
//				
//				//Herne Hill Station
//				TdServerLogger.print(TAG, methodTag, BusCountdownResponse.getBusRealTimeInfosCollection("55607", 10).toString());
//				
//				
//				//Upper Tooting Park
//				TdServerLogger.print(TAG, methodTag, BusCountdownResponse.getBusRealTimeInfosCollection("76373", 10).toString());
				
				
				
				//Tram
				//Le village, T1
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisTramRealTimeInfos("3798500", "905313", 10).toString());
//				
//				//Colette Besson, T3b
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisTramRealTimeInfos("3813338", "911928", 10).toString());
//				
//				//Brancion, T3
//				TdServerLogger.print(TAG, methodTag, RatpTimeTableProcess.getParisTramRealTimeInfos("3813510", "939887", 10).toString());
//				
			}
			
		}).start();
		

		
		
		
	}
}
