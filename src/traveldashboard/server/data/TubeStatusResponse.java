package traveldashboard.server.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

import traveldashboard.data.Tube;
import traveldashboard.data.status.TubeStatus;
import traveldashboard.data.status.TubeStatusCollection;

public class TubeStatusResponse {

	private static final String URL = "http://cloud.tfl.gov.uk/TrackerNet/LineStatus";
		
	public static TubeStatus getTubeStatus(String routeId) {
		
		TubeStatus tubeStatus = null;
		
		try {
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(URL);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			// Get XML
			String json = httpClient.execute(httpGet, responseHandler);

		    // Convert XML to JSON Object
            JSONObject xmlJSONObj = XML.toJSONObject(json);

			JSONObject rootObj = xmlJSONObj.getJSONObject("ArrayOfLineStatus");

            JSONArray lineStatus = rootObj.getJSONArray("LineStatus");
			
            for (int i = 0; i < lineStatus.length(); i++) {
            	JSONObject globalLineStatusObj = lineStatus.getJSONObject(i);
            	JSONObject lineObj = globalLineStatusObj.getJSONObject("Line");
            	JSONObject statusObj = globalLineStatusObj.getJSONObject("Status");

            	//String tubeId = "" + lineObj.get("ID");
            	String tubeName = (String) lineObj.get("Name");
            	String tubeId = DataConstants.LONDON_ROUTENAME_ROUTEID_MAP.get(tubeName);
            	
            	if (DataConstants.DEBUG) System.out.println("Tube name = " +  tubeName + ", tube id = " + tubeId + ", lineCode = " + routeId);
            	
            	if (tubeId == null) continue;
            	
            	if (tubeId.equals(routeId)) {
            		//Found the matching line id
                	String tubeDestination = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
                	Tube tube = new Tube(tubeId, tubeName, tubeDestination);
                	String status = (String) statusObj.get("Description");
                	String description = (String) globalLineStatusObj.get("StatusDetails");
                	//If the line has good service, we add more description on good service
    		    	if(status.equalsIgnoreCase("Good service")) { 
    		    		description = "There is currently good service on this line.";
    		    	}
    		    	
    		    	tubeStatus = new TubeStatus(tube, status, description);	
            		break;
            	}
            }
            
            if (tubeStatus == null) {
            	if (DataConstants.DEBUG) System.err.println("tube status = null");
        		String tubeName = DataConstants.LONDON_ROUTEID_ROUTENAME_MAP.get(routeId);
        		String tubeDestination = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
        		Tube tube = new Tube(routeId, tubeName, tubeDestination);
    	    	String status = "Status unavailable";
    	    	String description = "Status unavailable. Station closed or TFL cannot provide status";
            	tubeStatus = new TubeStatus(tube, status, description);
            }
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return tubeStatus;
	}
	
	public static TubeStatusCollection getTubeStatusCollection() {
		
		TubeStatusCollection tubeStatusCollection = null;
		List<TubeStatus> tubeStatusList = new ArrayList<TubeStatus>();
		List<String> tubeNameList = new ArrayList<String>();
		
		try {
			HttpParams httpParameters = new BasicHttpParams();
			// Set the timeout in milliseconds until a connection is established.
			// The default value is zero, that means the timeout is not used. 
			HttpConnectionParams.setConnectionTimeout(httpParameters, DataConstants.TIMEOUT_CONNECTION);
			// Set the default socket timeout (SO_TIMEOUT) 
			// in milliseconds which is the timeout for waiting for data.
			HttpConnectionParams.setSoTimeout(httpParameters, DataConstants.TIMEOUT_SOCKET);
			HttpClient httpClient = new DefaultHttpClient(httpParameters);
			HttpGet httpGet = new HttpGet(URL);
			
			BasicResponseHandler responseHandler = new BasicResponseHandler();
		
			// Get XML
			String json = httpClient.execute(httpGet, responseHandler);

		    // Convert XML to JSON Object
            JSONObject xmlJSONObj = XML.toJSONObject(json);

			JSONObject rootObj = xmlJSONObj.getJSONObject("ArrayOfLineStatus");

            JSONArray lineStatus = rootObj.getJSONArray("LineStatus");
			
            for (int i = 0; i < lineStatus.length(); i++) {
            	JSONObject globalLineStatusObj = lineStatus.getJSONObject(i);
            	JSONObject lineObj = globalLineStatusObj.getJSONObject("Line");
            	JSONObject statusObj = globalLineStatusObj.getJSONObject("Status");

            	//String tubeId = "" + lineObj.get("ID");
            	String tubeName = (String) lineObj.get("Name");
            	tubeNameList.add(tubeName);
            	String tubeId = DataConstants.LONDON_ROUTENAME_ROUTEID_MAP.get(tubeName);
            	String tubeDestination = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);
            	Tube tube = new Tube(tubeId, tubeName, tubeDestination);
            	String status = (String) statusObj.get("Description");
            	String description = (String) globalLineStatusObj.get("StatusDetails");
            	//If the line has good service, we add more description on good service
		    	if(status.equalsIgnoreCase("Good service")) { 
		    		description = "There is currently good service on this line.";
		    	}
            	TubeStatus tubeStatus = new TubeStatus(tube, status, description);
            	tubeStatusList.add(tubeStatus);
            }
            
            //If the line doesn't have status, we put status unavailable
		    for (String line : DataConstants.LONDON_METRO_ROUTE_NAMES) {
		    	System.out.println("line name = " + line);
		    	if(!tubeNameList.contains(line)) {
		    		String tubeName = line;
		    		String tubeId = DataConstants.LONDON_ROUTENAME_ROUTEID_MAP.get(tubeName);
		    		System.out.println("tubeId = " + tubeId);
		    		String tubeDestination = DataConstants.LONDON_TUBE_DESTINATIONS.get(tubeName);

		    		if (tubeId != null && !tubeId.equals("*") &&  !tubeId.equals("N/A")) {
			    		Tube tube = new Tube(tubeId, tubeName, tubeDestination);
				    	String status = "Status unavailable";
				    	String description = "Status unavailable. Station closed or TFL cannot provide status";
		            	TubeStatus tubeStatus = new TubeStatus(tube, status, description);
		            	tubeStatusList.add(tubeStatus);		    			
		    		}
		    	}
		    }
           
            TubeStatus[] tubeStatusArray = new TubeStatus[tubeStatusList.size()];
            
            for (int i = 0; i < tubeStatusList.size(); i++) {
            	tubeStatusArray[i] = tubeStatusList.get(i);
            }
            
            tubeStatusCollection = new TubeStatusCollection(tubeStatusArray);
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
		
		return tubeStatusCollection;
	}	
}
