package traveldashboard.server;

import org.ibicoop.communication.common.CommunicationConstants;
import org.ibicoop.communication.common.CommunicationMode;
import org.ibicoop.communication.common.CommunicationOptions;
import org.ibicoop.communication.common.IbiReceiver;
import org.ibicoop.communication.common.ReceiverListener;
import org.ibicoop.exceptions.ConnectionFailedException;
import org.ibicoop.exceptions.MalformedIbiurlException;
import org.ibicoop.init.IbicoopInit;
import org.ibicoop.sdp.naming.IBIURL;
import org.ibicoop.utils.DataBuffer;

/**
 * Receives and displays any Hello message received
 * 
 * @author andriesc
 *
 */
public class HelloTdServer {
        ReceiverListener dummyReceiverListener = new ReceiverListener() {

                @Override
                public byte[] receivedMessageRequest(IbiReceiver arg0, String arg1,
                                int arg2, int arg3, byte[] arg4) {
                        // TODO Auto-generated method stub
                        return null;
                }

                @Override
                public void receivedMessageData(IbiReceiver arg0, String arg1,
                                int arg2, DataBuffer arg3) {
                        
                        System.out.println("Received an iBICOOP message: " + new String(arg3.internalData, 0, arg3.dataLength));
                }

                @Override
                public void connectionStatus(IbiReceiver arg0, int arg1) {
                        
                }

                @Override
                public boolean acceptSenderConnection(IbiReceiver arg0, String arg1) {
                        return true;
                }
        };
        
        public void startReceiver(){
                try {
                        IBIURL uri = new IBIURL("ibiurl", "TravelDashboard@ibicoop.org",
                                        "Server", "TravelDashboard", "Test", "1");
                        CommunicationOptions options = new CommunicationOptions();
                        options.setCommunicationMode(new CommunicationMode(
                                        CommunicationConstants.MODE_PROXY));

                        IbicoopInit
                                        .getInstance()
                                        .getCommunicationManager()
                                        .createReceiver(uri, options,
                                                        IbicoopInit.getInstance().getProxyURL(),
                                                        dummyReceiverListener);
                } catch (ConnectionFailedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                } catch (MalformedIbiurlException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                }
        }
}