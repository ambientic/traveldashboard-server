package traveldashboard.gtfs;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import org.onebusaway.gtfs.model.AgencyAndId;
import org.onebusaway.gtfs.model.Route;
import org.onebusaway.gtfs.model.ServiceCalendar;
import org.onebusaway.gtfs.model.ServiceCalendarDate;
import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;
import org.onebusaway.gtfs.model.Transfer;
import org.onebusaway.gtfs.model.Trip;

public class GtfsParser {
	public void parse(TransitSystem tsys, File directory) throws IOException {
		File routesFile = new File(directory, "routes.txt");
		if(routesFile.exists())
			parseRoutes(new FileInputStream(routesFile), tsys);
		else System.err.println("routes.txt doesn't exist");
		
		File tripsFile = new File(directory, "trips.txt");
		if(tripsFile.exists())
			parseTrips(new FileInputStream(tripsFile), tsys);
		else System.err.println("trips.txt doesn't exist");
		
		File calendarFile = new File(directory, "calendar.txt");
		if(calendarFile.exists())
			parseCalendar(new FileInputStream(calendarFile), tsys);
		else System.err.println("calendar.txt doesn't exist");
		
		File calendarDatesFile = new File(directory, "calendar_dates.txt");
		if(calendarDatesFile.exists())
			parseCalendarDates(new FileInputStream(calendarDatesFile), tsys);
		else System.err.println("calendar_dates.txt doesn't exist");
		
		File stopsFile = new File(directory, "stops.txt");
		if(stopsFile.exists())
			parseStops(new FileInputStream(stopsFile), tsys);
		else System.err.println("stops.txt doesn't exist");
	
		File stopTimesFile = new File(directory, "stop_times.txt");
		if(stopTimesFile.exists())
			parseStopTimes(new FileInputStream(stopTimesFile), tsys);
		else System.err.println("stop_times.txt doesn't exist");

		File transfersFile = new File(directory, "transfers.txt");
		if(transfersFile.exists())
			parseTransfers(new FileInputStream(transfersFile), tsys);
		else System.err.println("transfers.txt doesn't exist");
	}

	public void parseRoutes(InputStream routesFile, TransitSystem tsys) throws IOException{
		System.out.println("Start parse routes");
		CsvParser csv = new CsvParser(routesFile);
		
		while(csv.nextLine()){
			Route route = new Route(); 
			if(csv.get(0) != null)
				route.setId(new AgencyAndId("", csv.get(0).trim()));
			if(csv.get(1) != null)
				route.getId().setAgencyId(csv.get(1).trim());
			if(csv.get(2) != null)
				route.setShortName(csv.get(2).trim());
			if(csv.get(3) != null)
				route.setLongName(csv.get(3).trim());
			if(csv.get(4) != null)
				route.setDesc(csv.get(4));
			if(csv.get(5) != null)
				route.setType(Integer.parseInt(csv.get(5)));
			
			tsys.routes.put(route.getId().getId(), route);
		}
		
		//Close input stream
		routesFile.close();
		
		System.out.println("End parse routes");
	}
	
	public void parseTrips(InputStream tripsFile, TransitSystem tsys) throws IOException {
		System.out.println("Start parse trips");
		CsvParser csv = new CsvParser(tripsFile);
		
		while(csv.nextLine()){
			Trip trip = new Trip();
			if(csv.get(0) != null){
				trip.setRoute(tsys.routes.get(csv.get(0).trim()));
			}
			if(csv.get(1) != null){
				trip.setServiceId(new AgencyAndId("", csv.get(1).trim()));
			}
			if(csv.get(2) != null){
				trip.setId(new AgencyAndId("", csv.get(2).trim()));
			}
			if(csv.get(5) != null){
				trip.setDirectionId(csv.get(5).trim());
			}
			
			tsys.trips.put(trip.getId().getId(), trip);
		}
		
		//Close input stream tripsfile
		tripsFile.close();
		
		System.out.println("End parse trips");
	}
	
	public void parseCalendar(InputStream calendarFile, TransitSystem tsys) throws IOException {
		System.out.println("Start parse calendar");
		CsvParser csv = new CsvParser(calendarFile);
		
		DateFormat dateFmt = new SimpleDateFormat("yyyyMMdd", Locale.FRANCE);
		
		while(csv.nextLine()){
			try {
				ServiceCalendar cal = new ServiceCalendar();
				if(csv.get(0) != null)
					cal.setServiceId(new AgencyAndId("", csv.get(0).trim()));
				
				if(csv.get(1) != null)
					cal.setMonday(Integer.parseInt(csv.get(1).trim()));
				
				if(csv.get(2) != null)
					cal.setTuesday(Integer.parseInt(csv.get(2).trim()));
				
				if(csv.get(3) != null)
					cal.setWednesday(Integer.parseInt(csv.get(3).trim()));
				
				if(csv.get(4) != null)
					cal.setThursday(Integer.parseInt(csv.get(4).trim()));
				
				if(csv.get(5) != null)
					cal.setFriday(Integer.parseInt(csv.get(5).trim()));
				
				if(csv.get(6) != null)
					cal.setSaturday(Integer.parseInt(csv.get(6).trim()));
				
				if(csv.get(7) != null)
					cal.setSunday(Integer.parseInt(csv.get(7).trim()));
				
				if(csv.get(8) != null)
					cal.setStartDate(dateFmt.parse(csv.get(8).trim()));
				
				if(csv.get(9) != null)
					cal.setEndDate(dateFmt.parse(csv.get(9).trim()));

				tsys.calendar.put(cal.getServiceId().getId(), cal);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		//Close input stream calendar files
		calendarFile.close();
		
		System.out.println("End parse calendar");
	}
	
	public void parseCalendarDates(InputStream calendarDatesFile, TransitSystem tsys) throws IOException {
		System.out.println("Start parse calendar dates");
		CsvParser csv = new CsvParser(calendarDatesFile);
		
		DateFormat dateFmt = new SimpleDateFormat("yyyyMMdd", Locale.FRANCE);
		
		while(csv.nextLine()){
			try {
				ServiceCalendarDate calDate = new ServiceCalendarDate();
				if(csv.get(0) != null)
					calDate.setServiceId(new AgencyAndId("", csv.get(0).trim()));
				
				if(csv.get(1) != null)
						calDate.setDate(dateFmt.parse(csv.get(1).trim()));
				
				if(csv.get(2) != null)
					calDate.setExceptionType(Integer.parseInt(csv.get(2).trim()));
				
				tsys.calendarDates.put(calDate.getServiceId().getId(), calDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		//Close input stream for calendar dates file
		calendarDatesFile.close();
		
		System.out.println("End parse calendar dates");
	}
	
	public void parseStops(InputStream stopsFile, TransitSystem tsys) throws IOException {
		System.out.println("Start parse stops");
		CsvParser csv = new CsvParser(stopsFile);
		
		while(csv.nextLine()){
			Stop stop = new Stop();
			if(csv.get(0) != null){
				stop.setId(new AgencyAndId("", csv.get(0).trim()));
			}
			if(csv.get(2) != null){
				stop.setName(csv.get(2).trim());
			}
			if(csv.get(4) != null){
				stop.setLat(Double.parseDouble(csv.get(4).trim()));
			}
			if(csv.get(5) != null){
				stop.setLon(Double.parseDouble(csv.get(5).trim()));
			}
			
			tsys.stops.put(stop.getId().getId(), stop);
		}
		
		//Close stops file input stream
		stopsFile.close();
		
		System.out.println("End parse stops");
	}
	
	public void parseStopTimes(InputStream stopTimesFile, TransitSystem tsys) throws IOException {
		System.out.println("Start parse stop times");
		CsvParser csv = new CsvParser(stopTimesFile);
		
		//DateFormat dateFmt = new SimpleDateFormat("HH:mm:ss", Locale.FRANCE);
		//Calendar c = Calendar.getInstance();
		String[] timeString;
		
		while(csv.nextLine()){
			try {
				StopTime stopTime = new StopTime();
				
				if(csv.get(0) != null)
					stopTime.setTrip(tsys.trips.get(csv.get(0).trim()));
				
				// arrival time, in seconds since midnight
				if(csv.get(1) != null){
					//c.setTime(dateFmt.parse(csv.get(1).trim()));
					//stopTime.setArrivalTime((int) (c.getTimeInMillis() / 1000));
					timeString = csv.get(1).trim().split(":");
					if(timeString.length == 3){
						stopTime.setArrivalTime(
								Integer.parseInt(timeString[0]) * 3600 + 
								Integer.parseInt(timeString[1]) * 60 + 
								Integer.parseInt(timeString[2]));
					}
				}
				
				if(csv.get(2) != null){
					//c.setTime(dateFmt.parse(csv.get(2).trim()));
					//stopTime.setDepartureTime((int) (c.getTimeInMillis() / 1000));
					timeString = csv.get(2).trim().split(":");
					if(timeString.length == 3){
						stopTime.setDepartureTime(
								Integer.parseInt(timeString[0]) * 3600 + 
								Integer.parseInt(timeString[1]) * 60 + 
								Integer.parseInt(timeString[2]));
					}
				}
				if(csv.get(3) != null){
					stopTime.setStop(tsys.stops.get(csv.get(3).trim()));
				}
				if(csv.get(4) != null){
					stopTime.setStopSequence(Integer.parseInt(csv.get(4).trim()));
				}
				tsys.stopTimes.add(stopTime);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		//Close stop time files input stream
		stopTimesFile.close();
		
		System.out.println("End parse stop times");
	}
	
	public void parseTransfers(InputStream transfersFile, TransitSystem tsys) throws IOException {
		System.out.println("Start parse transfers");
		CsvParser csv = new CsvParser(transfersFile);
		
		while(csv.nextLine()){
			Transfer transfer = new Transfer();
			if(csv.get(0) != null){
				transfer.setFromStop(tsys.stops.get(csv.get(0).trim()));
			}
			if(csv.get(1) != null){
				transfer.setToStop(tsys.stops.get(csv.get(1).trim()));
			}
			if(csv.get(2) != null){
				transfer.setTransferType(Integer.parseInt(csv.get(2).trim()));
			}
			if(csv.get(3) != null){
				transfer.setMinTransferTime(Integer.parseInt(csv.get(3).trim()));
			}
			tsys.transfers.add(transfer);
		}
		
		//Close input stream for transfersFile
		transfersFile.close();
		
		System.out.println("End parse transfers");
	}
}
