package traveldashboard.gtfs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.onebusaway.gtfs.model.*;

public class TransitSystem {
	public List<Agency> agencies = new ArrayList<Agency>();
	public HashMap<String, Route> routes = new HashMap<String, Route>();
	public HashMap<String, Stop> stops = new HashMap<String, Stop>();
	public List<StopTime> stopTimes = new ArrayList<StopTime>();
	public List<Transfer> transfers = new ArrayList<Transfer>();
	public HashMap<String, ServiceCalendar> calendar = new HashMap<String, ServiceCalendar>();
	public HashMap<String, ServiceCalendarDate> calendarDates = new HashMap<String, ServiceCalendarDate>();
	public HashMap<String, Trip> trips = new HashMap<String, Trip>();
}
