package traveldashboard.gtfs;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ibicoop.utils.StringUtils;

import au.com.bytecode.opencsv.CSVReader;

public class CsvParser {
	private CSVReader reader;
	private static final Pattern pattern = Pattern.compile("\"([^\"]+)\"\\s*,|([^,]+)\\s*,");
	Matcher matcher;
	private String[] lineTokens = { };

	public CsvParser(InputStream csvInput) throws IOException {
		reader = new CSVReader(new InputStreamReader(csvInput,StringUtils.UTF8));
		// ignore first line
		reader.readNext();
	}

	public boolean nextLine() throws IOException {
		
		lineTokens = reader.readNext();
		if (lineTokens == null)
			return false;
		
		return true;
	}
	
	public String get(int index){
		if(index < 0 || index >= lineTokens.length)
			return null;
		return lineTokens[index];
	}
}
