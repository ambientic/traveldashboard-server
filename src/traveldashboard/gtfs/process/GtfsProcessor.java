package traveldashboard.gtfs.process;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import org.onebusaway.gtfs.model.Stop;
import org.onebusaway.gtfs.model.StopTime;

import traveldashboard.gtfs.GtfsParser;
import traveldashboard.gtfs.TransitSystem;

public class GtfsProcessor {
	private static GtfsProcessor singleton;
	private TransitSystem tsys;
	public static synchronized GtfsProcessor getInstance(){
		if(singleton == null){
			singleton = new GtfsProcessor();
		}
		return singleton;
	}

	/**
	 * This method loads the GTFS of the entire network.
	 * Should call this in a worker thread since this might take some time.
	 * 
	 */
	public void parseAllHeavyData(final File gtfsDir ,final GtfsParseCallback callback){
		final GtfsParser parser = new GtfsParser();
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					long initTime = System.currentTimeMillis();
					parser.parse(tsys, gtfsDir);
					
					double totalMillis = System.currentTimeMillis() - initTime;
					System.out.println("Routes: " + tsys.routes.size());
					System.out.println("Trips: " + tsys.trips.size());
					System.out.println("Calendars: " + tsys.calendar.size());
					System.out.println("Calendar Dates: " + tsys.calendarDates.size());
					System.out.println("Stops: " + tsys.stops.size());
					System.out.println("Stop Times: " + tsys.stopTimes.size());
					System.out.println("Transfers: " + tsys.transfers.size());
					System.out.println("Parsed in " + totalMillis + "ms");
					
					for(StopTime stopTime : tsys.stopTimes){
						if(stopTime.getTrip().getRoute().getLongName().contains("Aller")){
							stopTime.getStop().setRouteId(stopTime.getTrip().getRoute().getId().getId());
							stopTime.getStop().setRouteName(stopTime.getTrip().getRoute().getShortName());
						}
					}
					
					callback.parsingComplete();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					callback.parsingFailled();
				} 
			}
		}).start();
	}
	
	public synchronized Set<Stop> getAllStopsFor(int routeType){
		Set<Stop> resultSet = new TreeSet<Stop>(new Comparator<Stop>() {
			@Override
			public int compare(Stop o1, Stop o2) {
				return o1.getId().getId().compareTo(o2.getId().getId());
			}
		});
		for(StopTime stopTime : tsys.stopTimes){
			// stopTime.getTrip().getRoute().getLongName().contains("Aller")) for single direction
			if(stopTime.getTrip().getRoute().getType() == routeType){
				stopTime.getStop().setRouteId(stopTime.getTrip().getRoute().getId().getId());
				stopTime.getStop().setRouteName(stopTime.getTrip().getRoute().getShortName());
				stopTime.getStop().setDirection(stopTime.getTrip().getDirectionId());
				resultSet.add(stopTime.getStop());
			}
		}
		return resultSet;
	}
	
	public interface GtfsParseCallback{
		public void parsingComplete();
		public void parsingFailled();
	}

	public TransitSystem getSystem() {
		return tsys;
	}
	
	private GtfsProcessor() {
		tsys = new TransitSystem();
	}
}
