package traveldashboard.gtfs.process;

import java.io.File;

import traveldashboard.gtfs.process.GtfsProcessor.GtfsParseCallback;

public class GtfsProcessorExample {
	public static void runExample(){
//		File gtfsDir = new File("/home/emil/ny");
		File gtfsDir = new File("D:\\apache-tomcat-7.0.23\\webapps\\ratp");
		//File gtfsDir = new File("/Users/andriesc/Downloads/RATP_GTFS_FULL");
		final GtfsProcessor proc = GtfsProcessor.getInstance();
		proc.parseAllHeavyData(gtfsDir, new GtfsParseCallback() {
			@Override
			public void parsingComplete() {
				System.out.println("TOTAL stops: "+proc.getSystem().stops.size());
				long ini = System.currentTimeMillis();
				System.out.println("Tram stops: "+proc.getAllStopsFor(0).size() + ", prased in "+(System.currentTimeMillis()-ini) + "ms");
				ini = System.currentTimeMillis();
				System.out.println("Metro stops: "+proc.getAllStopsFor(1).size() + ", prased in "+(System.currentTimeMillis()-ini) + "ms");
				ini = System.currentTimeMillis();
				System.out.println("Train stops: "+proc.getAllStopsFor(2).size() + ", prased in "+(System.currentTimeMillis()-ini) + "ms");
				ini = System.currentTimeMillis();
				System.out.println("Bus stops: "+proc.getAllStopsFor(3).size() + ", prased in "+(System.currentTimeMillis()-ini) + "ms");
			}

			@Override
			public void parsingFailled() {
				// TODO Auto-generated method stub
				
			}
		});
	}
}
