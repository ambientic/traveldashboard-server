package traveldashboard.gtfs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CsvParser2 {
	private BufferedReader reader;
	private static final Pattern pattern = Pattern.compile("\"([^\"]+)\"\\s*,|([^,]+)\\s*,");
	Matcher matcher;
	private List<String> lineTokens = new ArrayList<String>();

	public CsvParser2(InputStream csvInput) throws IOException {
		reader = new BufferedReader(new InputStreamReader(csvInput));

		// ignore first line
		reader.readLine();
	}

	public boolean nextLine() throws IOException {
		
		String line = reader.readLine();
		if (line == null)
			return false;
		
		lineTokens.clear();
		matcher = pattern.matcher(line);
		
		while(matcher.find()){
			if(matcher.group(1) == null){
				lineTokens.add(matcher.group(2));
			} else {
				lineTokens.add(matcher.group(1));
			}
	    }
		return true;
	}
	
	public String get(int index){
		if(index < 0 || index >= lineTokens.size())
			return null;
		return lineTokens.get(index);
	}
}
