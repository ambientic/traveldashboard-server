1. This project should compile and run as a Dynamic Web application without any extra configuration.
The iBICOOP library is already included as a JAR in the "./WebContent/WEB-INF/lib" folder

2. Need to run MongoDB first before running the server

3. Need to have RATP GTFS data (agency.txt, calendar.txt, calendar_dates.txt,
routes.txt, stop_times.txt, stops.txt, transfers.txt, trips.txt) in 
"apache-tomcat-x.x.x/Webapps/ratp" folder. The data can be downloaded from RATP
website: http://data.ratp.fr/?id=101&tx_icsoddatastore_pi1%5Buid%5D=32
or specifically: http://data.ratp.fr/?eID=ics_od_datastoredownload&file=82

